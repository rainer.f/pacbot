# Maintainer: Rainer Finke <rainer@finke.cc>

_pkg_name="pacbot"
_install_dir="opt/$_pkg_name"
_symlink_dir="usr/bin"
_systemd_dir="etc/systemd/system"
_launcher_dir="usr/share/applications"

pkgname="$_pkg_name"
pkgver=29
pkgrel=1
pkgdesc="A utility to automatically perform Arch Linux system maintenance"
arch=('any')
url="https://gitlab.com/rainer.f/pacbot"
license=('GPLv3')
depends=(
    'gawk'
    'git'
    'pacman-contrib'
    'pacutils'
    #'python'
    'python-dateutil'
    'python-xmltodict'
    'sed'
    'sudo'
    'nano'
    'dialog'
    'bc')
optdepends=(
    'reflector'
    'fwupd'
    'smartmontools')
backup=("$_install_dir/settings.sh")

#source=("https://gitlab.com/rainer.f/pacbot/raw/master/pkg/$pkgname-$pkgver.tar.bz2")
#Prepare for zstd and consider release
source=("https://gitlab.com/rainer.f/pacbot/raw/master/pkg/$pkgname-$pkgver.$pkgrel.tar.zstd")

sha256sums=('fd57b24acedd999b0741d3fbe5df18561eb1de284abd82561ceb52ae62d82dcc')

build() {
    umask 022
    mkdir -p "$_install_dir"
    mkdir -p "$_symlink_dir"

    #sed -i "s|{{PKG_PATH}}|/${_install_dir}|" "pacbot.sh"

    install -m 755 "archNews.py" "$_install_dir"
    install -m 755 "archbot.sh" "$_install_dir"
    install -m 755 "archinstallbot.sh" "$_install_dir"
    install -m 755 "backup.sh" "$_install_dir"
    install -m 755 "helpers.sh" "$_install_dir"
    install -m 755 "interface.sh" "$_install_dir"
    install -m 755 "pacbot.png" "$_install_dir"
    install -m 755 "pacbot.sh" "$_install_dir"
    install -m 755 "settings.sh" "$_install_dir"
    install -m 755 "gamebot.sh" "$_install_dir"
    install -m 755 "sweeper.sh" "$_install_dir"
    install -m 755 "sysinfo.sh" "$_install_dir"
    install -m 755 "systemchecks.sh" "$_install_dir"
    install -m 755 "usbstickbot.sh" "$_install_dir"
    install -m 755 "userprofile.sh" "$_install_dir"

    # For systemd
    install -dm 0755                        "${_systemd_dir}/"
    install -m 0644 pacbot-daily.service    "${_systemd_dir}/pacbot-daily.service"
    install -m 0644 pacbot-daily.timer      "${_systemd_dir}/pacbot-daily.timer"
    install -m 0644 pacbot-weekly.service   "${_systemd_dir}/pacbot-weekly.service"
    install -m 0644 pacbot-weekly.timer     "${_systemd_dir}/pacbot-weekly.timer"

    # Desktop launcher
    install -dm 0755                        "${_launcher_dir}/"
    install -m 0644 archbot.desktop         "${_launcher_dir}/archbot.desktop"
    install -m 0644 gamebot.desktop        "${_launcher_dir}/gamebot.desktop"
}

package() {
    install_base=$(echo "$_install_dir" | cut -d '/' -f 1)
    symlink_base=$(echo "$_symlink_dir" | cut -d '/' -f 1)
    systemd_base=$(echo "$_systemd_dir" | cut -d '/' -f 1)
    launcher_base=$(echo "$_launcher_dir" | cut -d '/' -f 1)

    cp -r "$install_base" "$pkgdir"
    cp -r "$symlink_base" "$pkgdir"
    cp -r "$systemd_base" "$pkgdir"
    cp -r "$launcher_base" "$pkgdir"
    ln -s "/$_install_dir/pacbot.sh" "$pkgdir/$_symlink_dir/$pkgname"
    ln -s "/$_install_dir/usbstickbot.sh" "$pkgdir/$_symlink_dir/usbstickbot"
    ln -s "/$_install_dir/archbot.sh" "$pkgdir/$_symlink_dir/archbot"
    ln -s "/$_install_dir/archinstallbot.sh" "$pkgdir/$_symlink_dir/archinstallbot"
    #ln -s "/$_install_dir/gamebot.sh" "$pkgdir/$_symlink_dir/gamebot"
}
