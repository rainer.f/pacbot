# pacbot - Automated Pacman and System Maintenance Utlity

## Disclaimer
This is an early test application only for a personal use case to play around with system maintenance and it will probably destroy your system

## Precondition
Arch Linux based system with systemd. Run the installation commands as a normal user.

## Installation
`git clone https://gitlab.com/rainer.f/pacbot.git`<br />
`cd pacbot`<br />
`makepkg -sirc`

Pacbot offers an integrated updater, it will use the installation method above.

## Usage
`pacbot`

## Automatic Maintenance
Enable the following systemd services for daily/weekly automatic runs:
- systemctl enable pacbot-daily.timer
- systemctl enable pacbot-weekly.timer

Restart Timers and check network config
1. systemctl restart timers.target && systemctl list-timers
2. Check what "systemctl is-enabled systemd-networkd-wait-online.service NetworkManager-wait-online.service" returns, only one service should be enabled <br />
   a. If first enabled, and second disabled, then everything is ok (you run systemd.networkd) <br />
   b. If first disabled, and second disabled as well, enable "sudo systemctl enable systemd-networkd-wait-online.service" <br />
   c. If first and second are enabled, run "sudo systemctl disable NetworkManager-wait-online.service" <br />

## Output
Adjust per user the file ".bashrc" to see automatically on a new terminal the log messages of pacbot plus error log entries. This will be created by archbot automatically. <br />

# archbot
Archbot provides 3 tools for Arch Linux:

## usbstickbot - Download Arch Linux & Write Install Media to USB Stick
Download the latest Arch Linux image and write it to an USB stick. Run usbstickbot directly from the command line:<br />
`usbstickbot`

## archinstallbot - Automated Installer
Archinstallbot is an automated installation script for Arch Linux reduced to UEFI, GPT, systemd, fat32 boot partiton and btrfs root partition compressed with zstd. Use archinstallbot on an Arch Linux live or an existing Arch Linux system from the command line:<br />
`archinstallbot`<br />
<br />
To run archinstallbot on a Arch Linux live system without installing pacbot, follow the "Installation" procedure above, but instead of "makepkg -sirc" do the following:<br />
`sh pacbot/src/pacbot/archinstallbot.sh`<br />

## archbot - Automated system configuration
As soon as a minimal Arch Linux system is installed, archbot can automatically init a default configuration and optimize the system. Scope is limited to systemd, btrfs, zstd, dbus-broker, KDE, Firefox and open source GPU drivers. Run archbot from the command line:<br />
`archbot`
