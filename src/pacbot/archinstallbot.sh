#!/bin/bash
# archinstallbot - Arch Linux Installation tool, author Rainer Finke, license GPLv3
# Precondition: Arch Linux based system, installation restricted to uefi + gpt + vfat + btrfs + zstd + sgdisk + systemd + encrypted luks home + single boot OS
# Btrfs snapshots are not supported as paccheck can verify installed packages, worst case reinstalling the system is always possible.
# Disk Layout:
# |--> /boot 512MB
# |--> /root 24GB
# |--> /home OPTIONAL Rest of Available Disk Space, Encrypted

####################################
######### LOCAL ADJUSTMENTS ########
####################################

# Adjust to your local needs
## Enter user short name
userNameNew=""
## Predefine a location/city and a number as an identifier of the new system. If pcCity is empty, automatically the City from the time zone is selected
pcCity=""
pcNumber="1"
## List options: localectl list-keymaps
loadKeyboard="de-latin1"
## List UTF-8 locals: more /etc/locale.gen | grep UTF-8 | grep CountryCode
export localeConf="de_DE.UTF-8"
## List time zones and city: timedatectl list-timezones
region="Europe"
city="Berlin"
installPacbot=true
useSwap=true
swapSize=4096
initramfs=""

####################################
######### Global Variables #########
####################################

# No edit required!
declare -a allDisks
declare -a availableDisks
allDisks=$(lsblk --nodeps | awk '!/loop/ {print}' | awk '!/archiso/ {print}' | awk 'NR>1{ print $1 }' | sort -u)
installDir="/mnt/install"
#pkgPath="."
#scriptName="pacbot"
#taskName="Arch Linux Installation"
targetText="Target disk for installation available"
taskText="install Arch Linux on the automatically partitioned & formatted disk"
computerType=$(hostnamectl | grep "Chassis:" | sed 's/         Chassis: *//')
efi=false
gpt=true
diskPrepared=false
installFinished=false
encryptRoot=false
encryptHome=false
emptyArray=false
reinstallSystem=false
encryptedNameHome="crypthome"
installBoot=""
installRoot=""
installHome=""
color=3
useSubvolumes=true
ExpectedBootPartitionSizeMB=512
ExpectedRootPartitionSizeGB=24
optimalSectorSize=0
formatDiskToSectorSizeRecommend=false
alternativeSectorSizes=0
nvmeDisk=false
#ATTENTION Disable quiet boot as if the kernel crashes (experienced with amdgpu) it can happen that no error output will be visible at all and the user just sees a black screen wondering if the system is broken especially if combined with a desktop monitor than turns on slowly and where then no UEFI picture or systemd-boot menu would be shown
export quietBoot=false
export bootPar="rw"
soc=""
target=""
enableRoot=false
enableHomed=false
chrootActive=false
#Copied from PKGBUILD file from parameter depends=()
dependsPacbotPKGBUILD="awk git pacman-contrib pacutils python python-dateutil python-xmltodict sed sudo nano dialog bc"

####################################
########## Init Functions ##########
####################################

archinstallbot_init() {
    if [[ -d "/sys/firmware/efi" ]]; then
        efi=true
    fi
}

partition_names_init() {
    #Partition names
    disk_type_init
    if [[ "$diskType" == "nvme" ]]; then
        installBoot="${target}p1"
        installRoot="${target}p2"
        installHome="${target}p3"
    else
        #if [[ "$runParameterLoc" == "--aarch64" ]] && [[ "$soc" == "oc2" ]]; then
        #    installRoot="${target}1"
        #    installHome="${target}2"
        #else
            installBoot="${target}1"
            installRoot="${target}2"
            installHome="${target}3"
        #fi
    fi
}

disk_type_init() {
    diskType=$(echo "$target" | cut -c -4)
    if [[ "$diskType" == "nvme" ]]; then
        diskType="nvme"
    else
        if [[ "$(lsblk -d -o name,rota $target | awk 'NR>1 { print $2 }')" == "1" ]]; then
            diskType="ssd"
        else
            diskType="hdd"
        fi
    fi
}

####################################
########## Install Arch ############
####################################

archlinux_installer() {
    echo ">>>>>>>>>>>> archinstallbot <<<<<<<<<<<<"
    read -r -p "$(tput setaf $color)Do you want to install a new Arch Linux system including pacbot in $(tput setaf 2)${region}/${city}$(tput setaf $color) with language $(tput setaf 2)${localeConf}$(tput setaf $color) and keyboard $(tput setaf 2)$loadKeyboard$(tput setaf $color)?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        archlinux_preparedisks
        archlinux_install
        archlinux_installcleanup
    else
        read -r -p "$(tput setaf $color)Edit proposed user and locale in nano?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            read -r -p "$(tput setaf $color)Get Country, City and Region from https://ipinfo.io (proprietary service) and edit proposed user and locale? Exit with >CRTL+x<?$(tput sgr0)  [y/N]"
            if [[ "$REPLY" =~ [yY] ]]; then
                geo_location_service
            fi
            #Does this work if pacbot is installed? /opt/pacbot?
            archinstallbot_edit_config_runagain
        else
            echo "$(tput setaf $color)Arch Linux installation cancelled$(tput sgr0)"
        fi
    fi
}

archinstallbot_edit_config_runagain() {
    nano ./archinstallbot.sh
    sh ./archinstallbot.sh "$runParameterLoc" "$runParameterLoc2"
}

archlinux_preparedisks() {
    if ( ! $efi ); then
        #echo "Installation only supported if started on UEFI"
        #exit
        echo "LEGACY NON-UEFI MODE - INSTALLING GRUB IN BIOS MODE!!!"
    fi
    check_target_disk
}

prepare_disk_tasks() {
    partition_names_init
    umount_cleanup
    disk_target_layout
}

disk_target_layout() {
    if [[ "$runParameterLoc" == "--aarch64" ]]; then
        echo "[       Info      ] As of aarch64 btrfs cannot be used, fallback mode to ext4"
        useSubvolumes=false
    else
        read -r -p "$(tput setaf $color)Do you want to use $(tput setaf 2)btrfs subvolumes?$(tput sgr0) [y/N]"
        if [[ "$REPLY" =~ [nN] ]]; then
            useSubvolumes=false
        fi
    fi

    reinstall_existing_disk
    disk_sector_size_check "$target"
    if ( $reinstallSystem ); then
        if ( $formatDiskToSectorSizeRecommend ); then
            echo "$(tput setaf 3)[   WARNING  ]$(tput sgr0) It is recommeded to not keep the existing partions on the disk due to suboptimal sector sizes. By not agreeing to reinstall the system the disk can be newly formatted with the optimal sector sizes of $(tput setaf 2)$optimalSectorSize bytes$(tput sgr0)"
        fi
        read -r -p "$(tput setaf 2)Keep existing boot+root partitions on [$target]?$(tput setaf $color) This will keep other partitions like home unmodified! Do you want to reinstall the system without changing the other partitions?$(tput sgr0) [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            reinstallSystem=true
        else
            reinstallSystem=false
        fi
    fi

    #read -r -p "Do you want to encrypt the root partition on [$installRoot]? Recommended for mobile devices. Use encryption? [y/N]"
    #if [[ "$REPLY" =~ [yY] ]]; then
    #    encryptRoot=true
    #else
    #    encryptRoot=false
    #fi
    if ( ! $reinstallSystem ) && ( ! $enableHomed ); then
        read -r -p "$(tput setaf $color)Do you want to encrypt the home partition on [$installHome]? Recommended for all devices except for tablets with only a virtual keyboard! If you run a server, you can uncomment in fstab + crypttab the home partition to boot without password request and mount it on demand only! $(tput setaf 2)Create encrypted home?$(tput sgr0) [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            encryptHome=true
        else
            encryptHome=false
        fi
    fi

    disk_format
}

disk_format() {
    #Erease disk and partition new filesystem
    if ( ! $reinstallSystem ); then
        #if [[ "$runParameterLoc" == "--aarch64" ]]; then
            #Manual Partitioning / Testing
            #https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4
            #sudo fdisk /dev/"$target"
            #Shows kernel couln't be informed - sudo partprobe "/dev/$installRoot"
        #else
            format_disk_target_layout
        #fi

        echo "----------Partitions created-----------"
    else
        echo "Skipping disk partition step 1-3 as system will be reinstalled"
        echo ""
        echo "---Use existing root+boot Partitions---"
    fi

    #Print disk layout
    sudo sgdisk -p /dev/"$target"
    echo "---------------------------------------"

    #Sync Time for later installation (do it now as it might take some time)
    sudo timedatectl set-ntp true

    partitions_format_create

    partition_mount_root

    btrfs_subvolumes_create
    btrfs_subvolumes_mount

    partition_mount_boot_home

    echo "$(tput setaf 2)--------------NEW PARTITIONS------------$(tput sgr0)"
    mount | grep "install"
    echo "$(tput setaf 2)----------------------------------------$(tput sgr0)"
    echo "Disk is prepared, please continue with the Arch Linux installation!"
    diskPrepared=true
}

format_disk_target_layout() {
    local sectorSize=512
    #https://wiki.archlinux.org/title/Advanced_Format#Partition_alignment
    #sgdisk -a, --set-alignment=value
    #sgdisk -I, --align-end recommened for LUKS?
    disk_sector_size_optimze_before_format

    if ( $gpt ); then
        echo "[ partition table ] 1. Creating an empty disk with GPT partition table"
        sudo sgdisk -og /dev/"$target"
    else
        echo "[ partition table ] 1. Creating an empty disk with MBR partition table"
        #sudo sgdisk -og /dev/"$target"
        #sudo sgdisk --gpttombr /dev/"$target"
        #sudo sfdisk --delete /dev/"$target"
        ( echo o; echo w;) | sudo fdisk /dev/"$target" | grep "DOS"
    fi

    echo "[ boot partition  ] 2. Creating /boot partition ${ExpectedBootPartitionSizeMB}MB"
    if ( $gpt ); then
        sudo sgdisk -a $sectorSize --align-end -n 1:2048:+${ExpectedBootPartitionSizeMB}M -c 1:"UEFI" -t 1:ef00 /dev/"$target"
    else
#        if [[ "$soc" == "rp4" ]]; then
            ( echo n; echo p; echo 1; echo ""; echo +200M; echo w;) | sudo fdisk /dev/"$target" | grep "Linux"
            #c W95 FAT32 (LBA)
            sleep 1s
            ( echo t; echo c; echo w;) | sudo fdisk /dev/"$target" | grep "FAT32"
#        elif [[ "$soc" == "oc2" ]]; then
            #1. Zero the beginning of the SD card or eMMC module:
#            dd if=/dev/zero of=/dev/"$target" bs=1M count=8
            #No boot partition required on oc2
#        fi
    fi

    echo "[ root partition  ] 3. Creating / partition for root filesystem ${ExpectedRootPartitionSizeGB}GB"
    #int() converts e.g. 7,42 to 7
    local lowDiskSizeGB=$(sudo fdisk -l /dev/$target | grep /dev/$target | grep -i Bytes | grep -i GiB | awk '{print int($3)}')
    if (( $lowDiskSizeGB < $ExpectedRootPartitionSizeGB )) && [[ ! -z "$lowDiskSizeGB" ]]; then
        echo "$(tput setaf $color)[    EXCEPTION    ] Low disk space $lowDiskSizeGB GB. Disabling separate home partition! $(tput sgr0)"
        if ( $gpt ); then
            sudo sgdisk -a $sectorSize --align-end -n 2:0:0 -c 2:"Linux" -t 2:8300 /dev/"$target"
        else
            #BUG As of Linux ~5.12 fdisk fails as disk is not ready yet
            sleep 2s
            #sudo partprobe

#            if [[ "$soc" == "rp4" ]]; then
                ( echo n; echo p; echo 2; echo ""; echo ""; echo w;) | sudo fdisk /dev/"$target" | grep "Linux"
#            elif [[ "$soc" == "oc2" ]]; then
                #2. Start fdisk to partition the SD card: & 3. At the fdisk prompt, create the new partition:
#                ( echo n; echo p; echo 1; echo ""; echo ""; echo w;) | sudo fdisk /dev/"$target"
#            fi
        fi
        encryptHome=false
    else
        if ( $gpt ); then
            sudo sgdisk -a $sectorSize --align-end -n 2:0:+${ExpectedRootPartitionSizeGB}G -c 2:"Linux" -t 2:8300 /dev/"$target"
        else
            #BUG As of Linux ~5.12 fdisk fails as disk is not ready yet
            sleep 2s
            #sudo partprobe

#            if [[ "$soc" == "rp4" ]]; then
                ( echo n; echo p; echo 2; echo ""; echo +${ExpectedRootPartitionSizeGB}G; echo w;) | sudo fdisk /dev/"$target" | grep "Linux"
#            elif [[ "$soc" == "oc2" ]]; then
                #2. Start fdisk to partition the SD card: & 3. At the fdisk prompt, create the new partition:
#                ( echo n; echo p; echo 1; echo ""; echo +${ExpectedRootPartitionSizeGB}G; echo w;) | sudo fdisk /dev/"$target"
#            fi
        fi
    fi

    if ( $gpt ); then
        if ( ! $efi ); then
            echo "[ bios partition ] 3.0 Creating on non-EFI system a 2MB bios partition"
            sudo sgdisk -a $sectorSize --align-end -n 4:34:2047 -c 4:"BIOS" -t 4:ef02 /dev/"$target"
        fi
    fi

    if ( $encryptHome ); then
        echo "[ encrypted home ] 3.1 Creating encrypted /home partition for home, VM's, glusterfs. Use rest of available disk space!"
        #Using ENDSECTOR doesn't work, size is different per partition table, use 0 instead
        #ENDSECTOR=$(sgdisk -E /dev/"$target")
        if ( $gpt ); then
            sudo sgdisk -a $sectorSize --align-end -n 3:0:0 -c 3:"Home" -t 3:8300 /dev/"$target"
        else
            #BUG As of Linux ~5.12 fdisk fails as disk is not ready yet
            sleep 2s
            #sudo partprobe
            ( echo n; echo p; echo 3; echo ""; echo ""; echo w;) | sudo fdisk /dev/"$target"
        fi
    fi
}

disk_sector_size_optimze_before_format() {
    if (( optimalSectorSize > 0 )); then
        if (( $nvmeDisk )); then
            #nvme provides better/best performance mode indication, so show the output here
            sudo nvme id-ns -H /dev/$diskLoc | grep "Relative Performance"
        fi
        read -r -p "$(tput setaf $color)Do you allow formatting the disk $target (optimal sector size of $optimalSectorSize bytes) and to use it to for the new partition layout?$(tput sgr0) ALL DATA WILL BE DELETED [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sectorSize=$optimalSectorSize
            if (( $nvmeDisk )); then
                echo "$(tput setaf 2)[ FORMATTING NVME ]$(tput sgr0) The NVME $target is now formatted to the optimal sector size of $optimalSectorSize bytes"
                nvme format --lbaf=1 /dev/"$target"
            else
                echo "$(tput setaf 2)[ FORMATTING SSD/HDD ]$(tput sgr0) The SDD/HDD $target is now formatted to the optimal sector size of $optimalSectorSize bytes"
                hdparm --set-sector-size 4096 --please-destroy-my-drive /dev/"$target"
            fi
        else
            echo "[   SKIPPED  ] Sector Sizes are not changed!"
        fi
    else
        echo "[  ERROR  ] Sector size is still 0. Continue with default sector size of $sectorSize"
    fi
}

partitions_format_create() {
    #Create file systems
    #VFat uses by default 512 byte sector size, change manually to 4096
#    if [[ "$soc" != "oc2" ]]; then
        echo "[    format boot    ] 4. Create fat32 on /boot partition"
        sudo mkfs.vfat -S 4096 /dev/"$installBoot"

        check_partition_error "$installBoot"
#    fi

    #Btrfs uses by default 4096 byte sector size
    #https://wiki.archlinux.org/title/Advanced_Format#File_systems
    if ( $encryptRoot ); then
        if [[ "$runParameterLoc" == "--aarch64" ]]; then
            echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Not supported"
            exit
        else
            encryptedName="container"
            echo ">>>>>>>>>>>> ENCRYPTION <<<<<<<<<<<<"
            echo "$(tput setaf $color)5.1 Please confirm with YES and enter a secure password to encrypt the new root partition!$(tput sgr0)"
            sudo cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat /dev/"$installRoot"
            echo "$(tput setaf $color)>>>>>> OPEN ENCRYPTED DEVICE <<<<<<<$(tput sgr0)"
            sudo cryptsetup open --type luks /dev/"$installRoot" "$encryptedName"
            if [[ "$runParameterLoc" == "--aarch64" ]]; then
                echo "[ format encrypted root ] 5.2 Create ext4 on / in a encrypted partition for root"
                echo "[      Info     ] Ext4 fallback mode, cannot use btrfs"
                yes | sudo mkfs.ext4 "/dev/mapper/$encryptedName"
            else
                echo "[ format encrypted root ] 5.2 Create btrfs on / in a encrypted partition for root"
                sudo mkfs.btrfs -f "/dev/mapper/$encryptedName"
            fi

            check_partition_error "$encryptedName"
        fi
    else
        if [[ "$runParameterLoc" == "--aarch64" ]]; then
            echo "[    format root    ] 5. Create ext4 on / partition for root"
            echo "[        Info       ] Ext4 fallback mode, cannot use btrfs"
            #yes | sudo mkfs.ext4 -f /dev/"$installRoot" &> /dev/null
            yes | sudo mkfs.ext4 /dev/"$installRoot"
        else
            echo "[    format root    ] 5. Create btrfs on / partition for root"
            sudo mkfs.btrfs -f /dev/"$installRoot" &> /dev/null
        fi

        check_partition_error "$installRoot"

        if ( $encryptHome ); then
            #Sometimes sda3 is not recognized immediatly, so do a partprobe
            sudo partprobe /dev/"$target"
            echo ">>>>>>>>>>>> ENCRYPTION <<<<<<<<<<<<"
            echo "$(tput setaf $color)5.1 Please confirm with YES and enter a secure password to encrypt the new home partition!$(tput sgr0)"
            sudo cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat /dev/"$installHome"
            echo "$(tput setaf $color)>>>>>> OPEN ENCRYPTED DEVICE <<<<<<<$(tput sgr0)"
            if sudo cryptsetup open --type luks /dev/"$installHome" "$encryptedNameHome"; then
                echo "[ format encrypted home ] 5.2 Create btrfs on /home in a encrypted partition"
                sudo mkfs.btrfs -f "/dev/mapper/$encryptedNameHome"
            else
                #ATTENTION Repeat on wrong password?
                if sudo cryptsetup open --type luks /dev/"$installHome" "$encryptedNameHome"; then
                    echo "[ format encrypted home ] 5.2 Create btrfs on /home in a encrypted partition"
                    sudo mkfs.btrfs -f "/dev/mapper/$encryptedNameHome"
                else
                    exit
                fi
            fi
        else
            if sudo cryptsetup luksDump /dev/$installHome  &> /dev/null; then
                read -r -p "$(tput setaf $color)To keep the encrypted home partition on [$installHome] for /home, you need the existing password. Continue?$(tput sgr0) [y/N]"
                if [[ "$REPLY" =~ [yY] ]]; then
                    encryptHome=true
                    echo ""
                    echo "$(tput setaf $color)>>>>>> OPEN ENCRYPTED DEVICE with YOUR EXISTING PASSWORD <<<<<<<$(tput sgr0)"
                    sudo cryptsetup open --type luks /dev/"$installHome" "$encryptedNameHome"
                else
                    encryptHome=false
                fi
            fi
        fi
        check_partition_error "$installHome encryption=$encryptHome"
    fi
}

check_partition_error() {
    if [[ "$?" == "1" ]]; then
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) $1 not availabble! $(tput setaf 1)ERROR$(tput sgr0)"
        umount_cleanup
        exit
    fi
}

partition_mount_root() {
    #Mount new partitions to installation folder
    sudo mkdir "$installDir" &> /dev/null
    if ( $encryptRoot ); then
        echo "[     mount    ] 6. Mount new encrypted root partition to $installDir"
        if [[ "$runParameterLoc" == "--aarch64" ]]; then
            sudo mount "/dev/mapper/$encryptedName" "$installDir"
        else
            sudo mount -o compress-force=zstd "/dev/mapper/$encryptedName" "$installDir"
        fi
    else
        #Workaround. Some UUID are not always 100% reliable available when doing later ls -al /dev/disk/by-uuid
        sudo partprobe "/dev/$installRoot"

        echo "[     mount    ] 6. Mount new root partition to $installDir"
        if [[ "$runParameterLoc" == "--aarch64" ]]; then
            sudo mount "/dev/$installRoot" "$installDir"
        else
            sudo mount -o compress-force=zstd "/dev/$installRoot" "$installDir"
        fi
    fi
}

partition_mount_boot_home() {
#    if [[ "$soc" != "oc2" ]]; then
        sudo mkdir "${installDir}/boot" &> /dev/null
        echo "[     mount    ] 7. Mount new fat32 boot partition to ${installDir}/boot"
        sudo mount "/dev/$installBoot" "${installDir}/boot"
#    fi

    #Mount Home for installation to include it later into fstab
    if ( $encryptHome ); then
        sudo mkdir "${installDir}/home" &> /dev/null
        echo "[     mount    ] 8. Mount new btrfs encrypted home partition to ${installDir}/home"
        sudo mount -o compress=zstd "/dev/mapper/$encryptedNameHome" "$installDir/home"
        if sudo btrfs filesystem df "$installDir/home"  &> /dev/null; then
            echo "Succesfully mounted encrypted home partition with btrfs + zstd, continue..."
        else
            echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Encrypted home partition is NOT supporting btrfs, close home! If you want to use this encrypted home partition anyway please add it manually!"
            sudo cryptsetup close "$encryptedNameHome"
        fi
    fi
}

btrfs_subvolumes_create() {
    echo "[ btrfs subvolumes ]6.1 Create btrfs subvoles on root partition to $installDir"
    #Based on https://rootco.de/2018-01-19-opensuse-btrfs-subvolumes/

    #1. Partitions are created already above

    #2. Btrfs is created already above

    #3. Btrfs partitions are mounted already

    #4. Create the default subvolume layout (this assumes an intel architecture, the /boot/grub2/* paths are different for different architectures)
    # https://en.opensuse.org/SDB:BTRFS#Default_Subvolumes
    sudo btrfs subvolume create "$installDir"/@
    if ( $useSubvolumes ); then
        sudo btrfs subvolume create "$installDir"/@/.snapshots
        sudo mkdir "$installDir"/@/.snapshots/1
        sudo btrfs subvolume create "$installDir"/@/.snapshots/1/snapshot
        #sudo mkdir -p "$installDir"/@/boot/grub2/
        #sudo btrfs subvolume create "$installDir"/@/boot/grub2/i386-pc
        #sudo btrfs subvolume create "$installDir"/@/boot/grub2/x86_64-efi
        if ( ! $encryptHome ); then
            #If /home does not reside on a separate partition, it is excluded to avoid data loss on rollbacks.
            sudo btrfs subvolume create "$installDir"/@/home
        fi
        #Third-party products usually get installed to /opt. It is excluded to avoid uninstalling these applications on rollbacks.
        sudo btrfs subvolume create "$installDir"/@/opt
        #The root users home directory should also be preserved during a rollback
        sudo btrfs subvolume create "$installDir"/@/root
        #Contains data for Web and FTP servers. It is excluded to avoid data loss on rollbacks.
        sudo btrfs subvolume create "$installDir"/@/srv
        #All directories containing temporary files and caches are excluded from snapshots.
        sudo btrfs subvolume create "$installDir"/@/tmp
        sudo mkdir "$installDir"/@/usr/
        #This directory is used when manually installing software. It is excluded to avoid uninstalling these installations on rollbacks.
        sudo btrfs subvolume create "$installDir"/@/usr/local
        #This directory contains many variable files, including logs, temporary caches, third party products in /var/opt, and is the default location for many virtual machine images and databases. Therefore this subvolume is created to exclude all of this variable data from snapshots and is created with Copy-On-Write disabled.
        sudo btrfs subvolume create "$installDir"/@/var
    fi
    if ( $useSwap ); then
        # Swap - https://wiki.archlinux.org/index.php/Swap - non-compressed, non-snapshotted and no data Copy on Write
        #sudo mkdir "$installDir"/@/swap/
        sudo btrfs subvolume create "$installDir"/@/swap
    fi

    #Copy On Write -> Disable
    #5: Disable copy-on-write for var to improve performance of any databases and VM images within
    #Disable copy-on-write for var to improve performance of any databases and VM images within
    #Question: This seems very generic? Maybe only for /var/lib/mysql ???
    if ( $useSubvolumes ); then
        chattr +C /"$installDir"/@/var
    fi
    if ( $useSwap ); then
        # Swap - https://wiki.archlinux.org/index.php/Swap
        chattr +C /"$installDir"/@/swap
    fi

    if ( $useSubvolumes ); then
        #6: Create /mnt/@/.snapshots/1/info.xml file for snapper’s configuration. Include the following content, replacing $DATE with the current system date/time.
        echo "Create .snapshot/1 info.xml"
        local file="/$installDir/@/.snapshots/1/info.xml"
        local DATE=$(date)
        echo "<?xml version="1.0"?>
<snapshot>
  <type>single</type>
  <num>1</num>
  <date>$DATE</date>
  <description>first root filesystem</description>
</snapshot>" | sudo tee "$file" &> /dev/null

        #7: Set snapshot 1 as the default snapshot for your root file system, unmount it, and remount it.
        sudo btrfs subvolume set-default $(sudo btrfs subvolume list "$installDir" | grep "@/.snapshots/1/snapshot" | grep -oP '(?<=ID )[0-9]+') "$installDir"
        if [[ "$?" == "1" ]]; then
            echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) mount points are not cleaned from previous installation attempt!"
        fi
    fi

    sudo umount "$installDir"
    #Mount root again with the new subvolumes?
    #sudo mount /dev/sda1 "$installDir"
    partition_mount_root

    if ( $useSubvolumes ); then
        #8: You should be able to confirm the above worked by doing ls /mnt which should repond with an empty result.

        #9: You now need to create a skeleton of the filesystem to mount all of our subvolumes
        sudo mkdir "$installDir"/.snapshots
        #sudo mkdir -p "$installDir"/boot/grub2/i386-pc
        #sudo mkdir -p "$installDir"/boot/grub2/x86_64-efi
        if ( ! $encryptHome ); then
            sudo mkdir "$installDir"/home
        fi
        sudo mkdir "$installDir"/opt
        sudo mkdir "$installDir"/root
        sudo mkdir "$installDir"/srv
        sudo mkdir "$installDir"/tmp
        sudo mkdir -p "$installDir"/usr/local
        sudo mkdir "$installDir"/var
    fi
    if ( $useSwap ); then
        # Swap - https://wiki.archlinux.org/index.php/Swap
        sudo mkdir "$installDir"/swap
    fi
}

btrfs_subvolumes_mount() {
    if ( $useSubvolumes ); then
        #10: Mount all of the subvolumes
        if ( $encryptRoot ); then
            local mountPartition="/dev/mapper/$encryptedName"
        else
            local mountPartition="/dev/$installRoot"
        fi
        sudo mount "$mountPartition" "$installDir"/.snapshots -o subvol=@/.snapshots
        #sudo mount "$mountPartition" "$installDir"/boot/grub2/i386-pc -o subvol=@/boot/grub2/i386-pc
        #sudo mount "$mountPartition" "$installDir"/boot/grub2/x86_64-efi -o subvol=@/boot/grub2/x86_64-efi
        if ( ! $encryptHome ); then
            sudo mount "$mountPartition" "$installDir"/home -o subvol=@/home
        fi
        sudo mount "$mountPartition" "$installDir"/opt -o subvol=@/opt
        sudo mount "$mountPartition" "$installDir"/root -o subvol=@/root
        sudo mount "$mountPartition" "$installDir"/srv -o subvol=@/srv
        sudo mount "$mountPartition" "$installDir"/tmp -o subvol=@/tmp
        sudo mount "$mountPartition" "$installDir"/usr/local -o subvol=@/usr/local
        sudo mount "$mountPartition" "$installDir"/var -o subvol=@/var
    fi

    if ( $useSwap ); then
        # Swap - https://wiki.archlinux.org/index.php/Swap
        sudo mount "$mountPartition" "$installDir"/swap -o subvol=@/swap
        # Attention https://jlk.fjfi.cvut.cz/arch/manpages/man/swapon.8#Files_with_holes
        #sudo truncate -s 0 "$installDir"/swap/swapfile
        echo "Creating ${swapSize}MB Swap file"
        sudo dd if=/dev/zero of="$installDir"/swap/swapfile bs=1M count=$swapSize status=progress
        # Already done via directory
        #chattr +C "$installDir"/swap/swapfile
        sudo btrfs property set "$installDir"/swap/swapfile compression none
        sudo chmod 600 "$installDir"/swap/swapfile
        sudo mkswap "$installDir"/swap/swapfile
        sudo swapon "$installDir"/swap/swapfile
    fi

    #11. Once populated, care should be made to ensure the /mnt/etc/fstab also includes the appropriate entries for each of the subvolumes except @/.snapshots/1/snapshot which should not be mounted as it provides your initial installed system.
}

archlinux_install() {
    # This can either run from an existing Arch Linux installation or from the Live USB Stick
    # / and /boot must be mounted to $installDir
    if ( $diskPrepared ); then
        read -r -p "$(tput setaf $color)>>>>>>>>>> Continue with installing Arch Linux? <<<<<<<<<$(tput sgr0) [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            # Check if necessary install tool is available, usally only available on Arch USB stick
            sudo pacman -Sy
            if ! pacman -Q "arch-install-scripts" &> /dev/null; then
                sudo pacman -Sy --noconfirm "arch-install-scripts"
            fi
            sudo pacman -Sy --noconfirm "reflector"
            #Same reflector command as in pacbot
            sudo reflector --verbose --latest 5 --number 5 --age 3 --completion-percent 100 --sort rate --protocol https --save /etc/pacman.d/mirrorlist

            #NOTE If you use an older Archlinux ISO, the keyring might be outdated for some packages
            sudo pacman -Sy --noconfirm "archlinux-keyring"

            #Install all mandatory base packages. And for bootloader: dosfstools gptfdisk amd-ucode intel-ucode. Sudo is installed automatically.
            sudo pacstrap "$installDir" base base-devel linux linux-firmware btrfs-progs snapper openssh dosfstools gptfdisk amd-ucode intel-ucode git nano dialog sudo $initramfs
            if [[ "$?" == "1" ]]; then
                echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Fatal error, check if internet connection was lost or if there are unknown package signatures. $(tput setaf $color)Fatal ERROR$(tput sgr0)"
                exit
            fi

            # Generate fstab
            #sudo genfstab -U "$installDir" >> "$installDir/"etc/fstab
            sudo genfstab -U "$installDir" | sudo tee -a "$installDir/"etc/fstab
            ### How to add encrypted home to fstab? => This is automatically for all mounted partitions!

            archlinux_prepare_chroot
        else
            echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Installation aborted. $(tput setaf $color)ERROR$(tput sgr0)"
            cleanup_task
        fi
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Disk is not prepared, $(tput setaf $color)ERROR$(tput sgr0)"
        cleanup_task
    fi
}

archlinux_prepare_chroot() {
    #Copy script to chroot environement
    sudo cp archinstallbot.sh "${installDir}/"
    # Chroot into the new system, now you are root there (NO SUDO REQUIRED, but used for helpers)
    if ( $enableRoot ); then
        sudo arch-chroot "$installDir" sh ./archinstallbot.sh --chroot --enableRoot
    else
        sudo arch-chroot "$installDir" sh ./archinstallbot.sh --chroot
    fi
    installFinished=true
}

archlinux_install_chroot() {
    if [[ -d "/sys/firmware/efi" ]]; then
        efi=true
    fi

    #Required to setup an optimized crypttab
    target=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -4)
    disk_type_init

    locale_init
    timezone_init
    initramfs_init
    enable_network
    enable_default_services
    install_bootloader

    #User Action Required
    hostname_conf
    user_init "wheel"

    root_account

    #Remove chroot archinstallbot script and add archinstallbot.conf file for archbot. On a new aarch64 this is not available so supress error message about missing file
    rm archinstallbot.sh &> /dev/null
    archinstallbot_conf

    # Exit Chroot. On a new aarch64 system this needs to be skipped!
    if [[ "$(uname -m)" != "aarch64" ]]; then
        exit
    fi
}

archlinux_installcleanup() {
    if ( $installFinished ); then
        if ( $enableHomed ); then
            echo "$(tput setaf 2)>>>>>>>>>> INSTALLATION FINISHED for ROOT <<<<<<<<<<<<$(tput sgr0)"
        else
            echo "$(tput setaf 2)>>>>>>>>>> INSTALLATION FINISHED for $userNameNew <<<<<<<<<<<<$(tput sgr0)"
        fi
        if [[ "$installMedia" != "archiso" ]]; then
            read -r -p "$(tput setaf $color)Please remove USB Stick! Reboot to new system?$(tput sgr0)  [y/N]"
        else
            read -r -p "$(tput setaf $color)Reboot to new system?$(tput sgr0)  [y/N]"
        fi
        if [[ "$REPLY" =~ [yY] ]]; then
            umount_cleanup
            #sudo reboot
            systemctl reboot
        else
            umount_cleanup
            echo "Exit without reboot"
        fi
        echo "Finished auto installation of Arch Linux - DONE"
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) during installation. $(tput setaf $color)ERROR$(tput sgr0)"
    fi
}

reinstall_existing_disk() {
    local diskLayout=$(sudo partprobe -d -s /dev/"$target" | awk '{print $2}')
    #df works only for mounted partitions, use fdisk
    #local bootPartitionSizeMB=$(df -h | grep "$installBoot" | awk '{print $2}' | rev | cut -c 2- | rev)
    #local rootPartitionSizeGB=$(df -h | grep "$installRoot" | awk '{print $2}' | rev | cut -c 2- | rev)
    local bootPartitionSizeMB=$(sudo fdisk -l /dev/$target | grep "$installBoot" | awk '{print $5}' | grep M | rev | cut -c 2- | rev)
    #15.5G requires rounding as float is not supported in bash, so round down with awk
    local rootPartitionSizeGB=$(sudo fdisk -l /dev/$target | grep "$installRoot" | awk '{print $5}' | grep G | rev | cut -c 2- | rev | awk '{print int($1)}')
    echo "----------------------------------"
    echo ""
    echo "Total Disk Space:"
    sudo fdisk -l /dev/"$target" | grep "$target" | awk 'FNR==1' | awk '{ print $2 " " $3 $4}' | rev | cut -c 2- | rev
    echo "Actual Disk Layout:"
    sudo fdisk -l /dev/"$target" | grep "$target" | awk 'FNR>1' | awk '{ print $1 " " $5 }'
    echo ""
    echo "----------------------------------"
    if [[ "$diskLayout" == "gpt" ]]; then
        if [[  ! -z "$bootPartitionSizeMB" ]] && (( $bootPartitionSizeMB > ($ExpectedBootPartitionSizeMB - 256) )) && (( $bootPartitionSizeMB < ($ExpectedBootPartitionSizeMB + 100) )); then
            #ATTENTION Doesn't work if the disk space is low like in a VM install with 10GB, out of scope
            if [[  ! -z "$rootPartitionSizeGB" ]] && (( $rootPartitionSizeGB > ($ExpectedRootPartitionSizeGB - 12) )) && (( $rootPartitionSizeGB < ($ExpectedRootPartitionSizeGB + 8) )); then
                reinstallSystem=true
            else
                reinstallSystem=false
                echo "$(tput setaf $color)[  Skipping  ]$(tput sgr0) Cannot reuse partition Root $rootPartitionSizeGB"
            fi
        else
            reinstallSystem=false
            echo "$(tput setaf $color)[  Skipping  ]$(tput sgr0) Cannot reuse partition Boot $bootPartitionSizeMB"
        fi
    fi
}

disk_sector_size_check() {
    local diskLoc="$1"
    if [[ "$diskLoc" == "" ]]; then
        echo "[  ERROR  ] diskLoc is empty"
        exit
    fi
    optimalSectorSize=0
    inUseSectorSize=0
    #https://wiki.archlinux.org/title/Advanced_Format
    #Some disk offer different sector sizes, check HDD's/SSD's (hdparm) and NVME's (nvme)
    local nvmeDiskLoc=$(echo $diskLoc | cut -c -4)
    if [[ "$nvmeDiskLoc" == "nvme" ]]; then
        nvmeDisk=true
        disk_sector_size_nvme
        disk_sector_size_info
    else
        nvmeDisk=false
        disk_sector_size_sd
        disk_sector_size_info
    fi
}

disk_sector_size_nvme() {
    #echo "Checking sector size of disk $diskLoc:"
    #sudo nvme id-ns -H /dev/$diskLoc | grep "Relative Performance"
    if ! pacman -Q "nvme-cli" &> /dev/null; then
        sudo pacman -Sy "nvme-cli"
    fi
    local SIZES=$(sudo nvme id-ns -H /dev/$diskLoc | grep "Relative Performance" | awk '{ print $12 }' | sort -n)
    for sector in $SIZES; do
        #Sorted from low to high, use highest number?
        optimalSectorSize=$sector
    done
    inUseSectorSize=$(sudo nvme id-ns -H /dev/$diskLoc | grep "Relative Performance" | grep "use" | awk '{ print $12 }')
}

disk_sector_size_sd() {
    #echo "Checking sector size of disk $diskLoc:"
    #sudo sudo hdparm -I /dev/$diskLoc | grep 'Sector size:'
    if [[ "$(echo $diskLoc | cut -c -2)" == "vd" ]]; then
        #echo "[  Skipped  ] Disk sector sizes are not checked as $diskLoc is a virtual disk!"
        #Set a unkown sector size in VM
        optimalSectorSize="-1"
    else
        if ! pacman -Q "hdparm" &> /dev/null; then
            sudo pacman -Sy "hdparm"
        fi
        optimalSectorSize=$(sudo hdparm -I /dev/$diskLoc | grep 'Sector size:' | grep "Phys" | awk '{ print $4 }')
        inUseSectorSize=$(sudo hdparm -I /dev/$diskLoc | grep 'Sector size:' | grep "Logi" | awk '{ print $4 }')
        alternativeSectorSizes=$(sudo hdparm -I /dev/$diskLoc | grep 'Sector size:' | grep "Logi" | awk '{ print $7 " " $8 " " $9 }')
        if [[ "$alternativeSectorSizes" == "  " ]]; then
            alternativeSectorSizes="-1"
        fi
    fi
}

disk_sector_size_info() {
    if [[ "$optimalSectorSize" != "" ]] && [[ "$inUseSectorSize" != "" ]]; then
        if (( $optimalSectorSize == $inUseSectorSize )); then
            echo "$(tput setaf 2)Optimal Sector Size$(tput sgr0) $optimalSectorSize bytes in use on disk $diskLoc"
            formatDiskToSectorSizeRecommend=false
        else
            if [[ "$alternativeSectorSizes" == "-1" ]]; then
                echo "$(tput setaf 2)Optimal Sector Size$(tput sgr0) $inUseSectorSize bytes in use on disk $diskLoc. Not possible to changing to physcial $optimalSectorSize bytes format!"
                formatDiskToSectorSizeRecommend=false
            else
                if [[ "$optimalSectorSize" == "-1" ]]; then
                    #VM
                    echo "Sector sizes are unkown inside of a VM!"
                    formatDiskToSectorSizeRecommend=false
                else
                    echo "$(tput setaf 1)Suboptimal Sector Size $inUseSectorSize bytes slows down IO performance$(tput sgr0). For optimal performance use a Sector Size of $(tput setaf 2)$optimalSectorSize bytes$(tput sgr0). Disk $(tput setaf 3)$diskLoc should be formatted$(tput sgr0)"
                    formatDiskToSectorSizeRecommend=true
                fi
            fi
        fi
    else
        echo "$(tput setaf 1)[  ERROR  ]$(tput sgr0) Cannot check sector sizes of disk $diskLoc!"
        #exit
    fi
}

cleanup_task() {
    #Nothing to do
    diskPrepared=false
    echo " "
    umount_cleanup
}

umount_cleanup() {
    #Cleanup left overs from previous install atempts
    sudo umount /dev/"$installBoot" &> /dev/null
    #subvolumes
    sudo umount "$installDir"/home &> /dev/null
    sudo umount "$installDir"/opt &> /dev/null
    sudo umount "$installDir"/root &> /dev/null
    sudo umount "$installDir"/srv &> /dev/null
    sudo umount "$installDir"/tmp &> /dev/null
    sudo umount "$installDir"/usr/local &> /dev/null
    sudo umount "$installDir"/var &> /dev/null
    sudo swapoff "$installDir"/swap/swapfile &> /dev/null
    sudo rm "$installDir"/swap/swapfile &> /dev/null
    sudo umount "$installDir"/.snapshots &> /dev/null
    sudo umount "$installDir"/swap &> /dev/null
    sudo umount /dev/"$installHome" &> /dev/null

    #Probably wrong command???
    #sudo umount /dev/mapper/"$encryptedNameHome" &> /dev/null
    sudo cryptsetup close "$encryptedNameHome" &> /dev/null

    #Last umount root
    #Brfs subvolumes require multiple umounts
    mntRootList=$(mount | grep /dev/"$installRoot" | awk '{ print $1 }')
    for i in $mntRootList; do
        sudo umount /dev/"$installRoot" &> /dev/null
    done
}

####################################
######## Install Functions #########
####################################

hostname_conf() {
    # Create hostname in file /etc/hostname
    echo "... setup hostname"
    local file="/etc/hostname"
    if [[ "$pcCity" == "" ]]; then
        local cityCharacter=$(echo "$city" | cut -c -1 | awk '{print tolower($0)}')
    else
        local cityCharacter=$(echo "$pcCity" | cut -c -1 | awk '{print tolower($0)}')
    fi
    local machineCharacter=$(echo "$computerType" | cut -c -1)
    local hostnameNew="${cityCharacter}${machineCharacter}${pcNumber}"
    echo "$hostnameNew" > $file
    read -r -p "$(tput setaf $color)Do you want to edit the proposed hostname [$hostnameNew]?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        sudo nano $file
    else
        echo "...continue without edit"
    fi
    #From Arch Wiki, required?
    #Add matching entries to hosts(5):
    #nano /etc/hosts
    #127.0.0.1	localhost
    #::1		localhost
    #127.0.1.1	myhostname.localdomain	myhostname
}

locale_init() {
    # Define local in locale.conf and in locale.gen, then run locale-gen
    echo "... setup locale.conf with local language and vconsole.conf for local keyboard layout"
    locale_setup
    vconsole_conf
}

locale_setup() {
    locale_conf
    locale_gen
}

locale_conf() {
    # Add locale_conf with local language
    echo "LANG=$localeConf
LC_COLLATE=$localeConf" | sudo tee "/etc/locale.conf"
}

locale_gen() {
    local file="/etc/locale.gen"
    ### THERE ARE 2 LINES WITH THIS, do not use config_uncomment ###
    #config_uncomment "en_US.UTF-8 UTF-8" "#" "$file"
    config_replacevalue "#en_US.UTF-8 UTF-8" "en_US.UTF-8 UTF-8" "$file"

    config_uncomment "$localeConf UTF-8" "#" "$file"
    sudo locale-gen
}

vconsole_conf() {
    local file="/etc/vconsole.conf"
    #echo "KEYMAP=$languageCode" > $file
    echo "KEYMAP=$loadKeyboard" | sudo tee -a $file
    #HiDPI, font size 16
    #echo "FONT=lat2-16" | sudo tee -a $file
    #echo "FONT_MAP=8859-2" | sudo tee -a $file

}

timezone_init() {
    # Define Time Zone and Location
    echo "... link time zone and location in /etc/localtime to /usr/share/zoneinfo/$region/$city"
    ln -sf "/usr/share/zoneinfo/$region/$city" "/etc/localtime"
    #Run hwclock(8) to generate /etc/adjtime
    #INFO Not available on ARM
    hwclock --systohc
}

initramfs_init() {
    if [[ "$initramfs" == "dracut" ]]; then
        dracut_init
    else
        mkinitcpio_init
    fi
}

dracut_init() {
    #NOTE Inside of chroot
    if ! pacman -Q dracut &> /dev/null; then
        pacman -Sy dracut
    fi

    dracut_config

    local kerneltype="linux"
    dracut --hostonly --force /boot/initramfs-"${kerneltype}"-dracut.img
    dracut --no-hostonly --zstd --force /boot/initramfs-"${kerneltype}"-fallback-dracut.img
}

dracut_config() {
    local file="/etc/dracut.conf.d/archbot.conf"
    echo "compress=\"zstd\"" | sudo tee "$file"
    #Restrict Firmware Bloat by Removing GPU Firmware from initrd by using SimpleDRM.
    #https://hansdegoede.dreamwidth.org/28291.html
    echo "omit_drivers+=\" amdgpu radeon nouveau i915 \"" | sudo tee -a "$file"
    #Host only boot with always the same file name (-H), overwrite -f

    #BUG Dracut detects automatically if encrsption needs to be used, but it doesn't work always (bootloop)
    echo "add_dracutmodules+=\" crypt \"" | sudo tee -a "$file"

    #NO AUR SUPPORT YET
    #yay dracut-hook
    git clone https://aur.archlinux.org/dracut-hook.git
    cd dracut-hook
    makepkg -sirc --noconfirm
    cd ..
#     #??? Use dracut for all linux kernels???
#     local file="/usr/local/bin/dracut-linux.sh"
#     echo "#!/bin/bash" | sudo tee "$file"
#     echo "#Created by $scriptName" | sudo tee -a "$file"
#     echo "args=('-H' '--no-hostonly-cmdline')" | sudo tee -a "$file"
#     echo "while read -r line; do" | sudo tee -a "$file"
#     echo "  if [[ \$line = usr/lib/modules/+([^/])/pkgbase ]]; then" | sudo tee -a "$file"
#     echo "      mapfile -O \${#pkgbase[@]} -t pkgbase < \"/\$line\"" | sudo tee -a "$file"
#     echo "      kver=\${line#\"usr/lib/modules/\"}" | sudo tee -a "$file"
#     echo "      kver=\${kver%\"/pkgbase\"}" | sudo tee -a "$file"
#     echo "  fi" | sudo tee -a "$file"
#     echo "done" | sudo tee -a "$file"
#     echo "dracut \"\${args[@]}\" -f /boot/initramfs-\"\${pkgbase[@]}\"-dracut.img --kver \"\${kver[@]}\"" | sudo tee -a "$file"
#     echo "dracut -f /boot/initramfs-\"\${pkgbase[@]}\"-fallback-dracut.img --kver \"\${kver[@]}\"" | sudo tee -a "$file"
#     #BUG Arch Linux doesn't want to do this automatically anymore!!!
#     echo "cp /usr/lib/modules/\"\$kver\"/vmlinuz /boot/vmlinuz-linux" | sudo tee -a "$file"
#     sudo chmod a+x "$file"
#
#     #Run dracut after pacman installs linux
#     fileDracutBin="$file"
#     sudo mkdir -p /etc/pacman.d/hooks/
#     local file="/etc/pacman.d/hooks/90-dracut-linux.hook"
#     echo "#Created by $scriptName" | sudo tee "$file"
#     echo "[Trigger]" | sudo tee -a "$file"
#     echo "Type = File" | sudo tee -a "$file"
#     echo "Operation = Install" | sudo tee -a "$file"
#     echo "Operation = Upgrade" | sudo tee -a "$file"
#     echo "Target = usr/lib/modules/*/pkgbase" | sudo tee -a "$file"
#     echo "" | sudo tee -a "$file"
#     echo "[Action]" | sudo tee -a "$file"
#     echo "Description = Updating linux initcpio's (with dracut!)..." | sudo tee -a "$file"
#     echo "When = PostTransaction" | sudo tee -a "$file"
#     echo "Exec = $fileDracutBin" | sudo tee -a "$file"
#     echo "NeedsTargets" | sudo tee -a "$file"
}

mkinitcpio_init() {
    # Edit mkinitcpio and create it
    echo "... create Linux mkinitcpio boot files"
    local installRoot=""
    local checkEncryption=""
    installRoot=$(mount | grep " / " | awk '{print $1}' | cut -c 6-)
    checkEncryption=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -6)
    checkEncryptionHome=$(mount | grep " /home " | awk '{print $1}' | cut -c 6- | cut -c -6)
    #Microcode is selected automatically, if after autodetect then only the one for the current CPU is selected otherwise all the available ones are added to initramfs => https://wiki.archlinux.org/title/Microcode#mkinitcpio
    #To verify if microcode is inside the initramfs, use: lsinitcpio --early /boot/initramfs-linux.img
    config_replacevalue "autodetect" "autodetect microcode" "/etc/mkinitcpio.conf"
    #fsck not required for btrfs, encrypt not required for encrypted home only for encrypted root!
    if [[ "$checkEncryption" == "mapper" ]]; then
        config_replacevalue "fsck" "keymap encrypt" "/etc/mkinitcpio.conf"
    else
        config_replacevalue "fsck" "" "/etc/mkinitcpio.conf"
    fi
    #INFO this fails if linux-rasperrypi4 is installed already
    if [[ "$(pacman -Q linux-raspberrypi4 &> /dev/null | awk '{ print $1 }')" == "linux-raspberrypi4" ]] && [[ "$(uname -m)" == "aarch64" ]]; then
        mkinitcpio -p linux-raspberrypi4
    else
        mkinitcpio -p linux
    fi
}

user_homed_init() {
    local adminUser="$1"
    sudo systemctl enable systemd-homed.service
    sudo systemctl start systemd-homed.service
    #homectl create USERNAME --storage=luks --shell=/usr/bin/zsh --uid=60100 --member-of=wheel,adm,uucp
    if [[ "$adminUser" == "wheel" ]]; then
        sudo homectl create "$userNameNew" --storage=luks --member-of=wheel,adm,uucp
    else
        sudo homectl create "$userNameNew" --storage=luks
    fi
    if [[ "$?" == "0" ]]; then
        do=false
    else
        do=true
    fi
}

user_init() {
    # Create new user and password, add user to sudo group (wheel)???
    #echo "Create new user [ $userNameNew ]"
    local adminUser="$1"
    if ( $enableHomed ) && ( $chrootActive ); then
        echo "[  chroot  ] setting up a systend-homed user is only possible after 1. boot!"
        systemctl enable systemd-homed.service
    else
        local do=true
        while $do; do
            read -r -p "$(tput setaf $color)Define your new $(tput setaf 2)user name:$(tput sgr0)"
            if [[ "$REPLY" != "" ]] && [[ "$REPLY" != " " ]]; then
                userNameNew="$REPLY"
                if ( $enableHomed ); then
                    user_homed_init "$adminUser"
                else
                    user_add "$adminUser"
                fi
            else
                echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Please type a user name"
            fi
        done
    fi

    group_sudo
}

user_add() {
    #Provide "wheel" as a group for sudo admin users
    local adminUser="$1"
    if [[ "$adminUser" == "wheel" ]]; then
        #ROOT ONLY, skipped if run as normal user & no output
        groupadd "$adminUser"
        #Space is not allowed inside of a script between -Ggroupname
        adminUser="-G$adminUser"
    fi
    if [[ "$EUID" -ne 0 ]]; then
        sudo useradd -m "$adminUser" "$userNameNew"
    else
        useradd -m "$adminUser" "$userNameNew"
    fi
    if [[ "$?" == "0" ]]; then
        do=false
        user_passwd_set "$userNameNew"
    else
        do=true
    fi
}

user_passwd_set() {
    local userNamePWLoc="$1"
    echo "$(tput setaf $color)[$loadKeyboard]Define your new secure password for user $(tput setaf 2)$userNamePWLoc $(tput sgr0)"
    local do=true
    while $do; do
        if [[ "$EUID" -ne 0 ]]; then
            sudo passwd "$userNamePWLoc"
        else
            passwd "$userNamePWLoc"
        fi
        if [[ "$?" == "1" ]]; then
            do=true
        else
            do=false
        fi
    done
}

group_sudo() {
    #ROOT ONLY, skipped if run as normal user & no output
    # Add for group wheel sudo priviliges
    local file="/etc/sudoers.d/wheel"
    echo "%wheel      ALL=(ALL) ALL" | sudo tee "$file" &> /dev/null
}

enable_network() {
    #Enable network
    #https://wiki.archlinux.org/index.php/Systemd-networkd
    echo "Enable network based on systemd-networkd and systemd-resolved..."
    sudo systemctl enable systemd-networkd.service
    sudo systemctl enable systemd-resolved.service
    local file="/etc/systemd/network/wired.network"
    #Should ipv4 be replaced with yes so that it works for ipv6 as well??? => not required as ipv6 does SLAAC local config! https://en.wikipedia.org/wiki/DHCPv6
    #ipv6_privacy prefer-public/kernel or no/yes
    #mDNS
    echo "[Match]
Name=e*

[Network]
DHCP=ipv4
IPv6PrivacyExtensions=yes
MulticastDNS=yes" | sudo tee "$file" &> /dev/null
    sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
    #local networks=$(ip link show | awk 'NR%2==1 {print $2}' | awk 'NR>=2' | rev | cut -c 2- | rev)
    #for i in $network
    #    ip link set $i up
    #done

    ipv6_privacy_enable
}

ipv6_privacy_enable() {
    #https://wiki.archlinux.org/index.php/IPv6#Privacy_extensions
    local file="/etc/sysctl.d/40-ipv6.conf"
    echo "# Enable IPv6 Privacy Extensions
net.ipv6.conf.all.use_tempaddr = 2
net.ipv6.conf.default.use_tempaddr = 2" | sudo tee "$file" &> /dev/null
    #How to check for multiple nic's?
    local nic=$(ls /sys/class/net)
    for i in ${nic[*]}; do
        if [[ "$i" != "lo" ]]; then
            echo "net.ipv6.conf.${i}.use_tempaddr = 2" | sudo tee -a "$file" &> /dev/null
        fi
    done
}

enable_default_services() {
    #Enable Services
    printf "\nEnable default services - $(tput setaf 2)ssh time-sync trim$(tput sgr0)...\n"
    # Enable Time Sync and Services (SSH, Trim)
    sudo timedatectl set-ntp true
    sudo systemctl enable sshd.service
    sudo systemctl enable fstrim.timer

    #On some times systemd-boot will waits 90 seconds for TPM2 devices if encryption is used, but this is NOT used yet by archinstallbot
    sudo systemctl mask tpm2.target
}

install_bootloader() {
    # Install Bootloader
    echo "Installing bootloader"
    #The packages dosfstools gptfdisk amd-ucode intel-ucode are directly installed with all the others above with pacstrap
    if ( $efi ); then
        pacman -Sy --noconfirm efibootmgr
        bootctl install
        install_linux ""
        systemdboot_loader
    else
        if [[ "$(uname -m)" != "aarch64" ]]; then
            #https://wiki.archlinux.org/index.php/Grub
            pacman -Sy --noconfirm grub
            if [[ "$diskType" == "nvme" ]]; then
                local target=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | rev | cut -c 3- | rev)
            else
                local target=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | rev | cut -c 2- | rev)
            fi
            grub-install --target=i386-pc /dev/"$target"
            install_linux ""
            #Create main config /boot/grub/grub.cfg, set defaults in /etc/default/grub
        else
            echo "[ Info ] No bootloader installed"
        fi
    fi
}

install_linux() {
    kerneltype="linux$1"
    if [[ "$kerneltype" == "linux" ]] || [[ "$kerneltype" == "linux-lts" ]] || [[ "$kerneltype" == "linux-hardened" ]] || [[ "$kerneltype" == "linux-libre" ]] || [[ "$kerneltype" == "linux-libre-lts" ]] || [[ "$kerneltype" == "linux-libre-hardened" ]] || [[ "$kerneltype" == "linux-zen" ]]; then
        if ! pacman -Q "$kerneltype" &> /dev/null; then
            sudo pacman -Sy --noconfirm "$kerneltype"
        fi

        #BUG Workaround for huge initramfs for the fallback image (>240MB bug, resolved, now 120 !!!) due to nouveau/nvidia
        #https://gitlab.archlinux.org/archlinux/mkinitcpio/mkinitcpio/-/issues/238
        local file="/etc/mkinitcpio.d/$kerneltype.preset"
        config_setvalue "fallback_options=" "\"-S autodetect -S kms\"" "$file"


        # Install too root disk, IT IS ALWAYS EMPTY as during install we are now in chroot
        #BUG Can I use blkid to make the code easier?
        local installRoot=""
        local checkEncryption=""
        installRoot=$(mount | grep " / " | awk '{print $1}' | cut -c 6-)
        checkEncryption=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -6)
        checkEncryptionHome=$(mount | grep " /home " | awk '{print $1}' | cut -c 6- | cut -c -6)
        if [[ "$checkEncryption" == "mapper" ]]; then
            installRoot=""
            encryptRoot=true
            encryptedName=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c 8-)
        fi
        if [[ "$checkEncryptionHome" == "mapper" ]]; then
            installHome=""
            encryptHome=true
            encryptedNameHome=$(mount | grep " /home " | awk '{print $1}' | cut -c 6- | cut -c 8-)
        fi
        if ( $encryptRoot ); then
            installRootEncrypted=$(ls -al /dev/mapper/ | grep "$encryptedName" | awk '{print $11}' | cut -c 4-)
            local CRYPTUUID=""
            CRYPTUUID=$(ls -al /dev/disk/by-uuid | grep "$installRootEncrypted" | awk '{print $9}')
            #Now get with the dm-x info the root partition
            local lsblkLineNo=""
            #lsblkLineNo=$(lsblk --fs -s | awk '{if ($4 ~ /$CRYPTUUID/) print NR}')
            lsblkLineNo=$(lsblk -fs -s | awk -v var1="$CRYPTUUID" '{if ($4 ~ var1) print NR}')
            #if  there is no label, there is one parameter less
            if [[ -z $lsblkLineNo ]]; then
                #lsblkLineNo=$(lsblk --fs -s | awk '{if ($3 ~ /$CRYPTUUID/)mount | grep " /home " | awk '{print $1}' | cut -c 6- | cut -c 8- print NR}')
                lsblkLineNo=$(lsblk --fs -s | awk -v var1="$CRYPTUUID" '{if ($3 ~ var1) print NR}')
            fi
            ((lsblkLineNo++))
            installRoot=$(lsblk --fs -s | awk -v var1="$lsblkLineNo" 'NR==var1 {print $1}' | cut -c 3- | cut -c 5-)
            #echo "DEBUG...Encrypted Root --- [$encryptedName] --- [$installRootEncrypted] --- [$CRYPTUUID] --- [$lsblkLineNo] --- [$installRoot] ---"
            local UUID=""
            UUID=$(ls -al /dev/disk/by-uuid | grep "$installRoot" | awk '{print $9}')
        elif ( $encryptHome ); then
            ###encryptNameHome expects something like crypthome
            #installHomeEncrypted=$(ls -al /dev/mapper/ | grep "$encryptedNameHome" | awk '{print $11}' | cut -c 4-)
            #local CRYPTUUID=""
            ###installHomeEncrypted expects something like nvme0n1p3 but now BUG and it gets dm-0
            #CRYPTUUID=$(ls -al /dev/disk/by-uuid | grep "$installHomeEncrypted" | awk '{print $9}')
            #Now get with the dm-x info the home partition
            #local lsblkLineNo=""
            #lsblkLineNo=$(lsblk --fs -s | awk '{if ($4 ~ /$CRYPTUUID/) print NR}')
            #lsblkLineNo=$(lsblk -fs -s | awk -v var1="$CRYPTUUID" '{if ($4 ~ var1) print NR}')
            #if  there is no label, there is one parameter less, do it again
            #if [[ -z $lsblkLineNo ]]; then
                #lsblkLineNo=$(lsblk --fs -s | awk '{if ($3 ~ /$CRYPTUUID/) print NR}')
            #    lsblkLineNo=$(lsblk --fs -s | awk -v vaid-CRYPT-LUKS2-41958635bf4a41e3r1="$CRYPTUUID" '{if ($3 ~ var1) print NR}')
            #fi
            #((lsblkLineNo++))
            #installHome=$(lsblk --fs -s | awk -v var1="$lsblkLineNo" 'NR==var1 {print $1}' | cut -c 3- | cut -c 5-)
            #echo "DEBUG:...Encrypted Home --- [$encryptedNameHome] --- [$installHomeEncrypted] --- [$CRYPTUUID] --- [$lsblkLineNo] --- [$installHome] ---"
            local UUIDhome=""
            #UUIDhome=$(ls -al /dev/disk/by-uuid | grep "$installHome" | awk '{print $9}')
            UUIDhome=$(blkid -t PARTLABEL="Home" | awk '{ print $2 }' | cut -c 7- | rev | cut -c 2- | rev)

            #Warning: What if there is already another home drive available? Get the one that is mounted...
            #NOTE From blkid we get the possible home partitions, but we don't know which one is mounted. Then we check if the uuid is known by ls /dev/disk-id. To search for the uuid in the disk-id we need to remove - character with sed. If there is a disk-id with the uuid, we know it is mounted and we use it as home.
            for i in $UUIDhome; do
                if [[ "$(ls -alh /dev/disk/by-id/ | grep $(echo $i | sed 's/-//g'))" != "" ]]; then
                    UUIDhome="$i"
                    read -r -p "$(tput setaf $color)Is the uuid $i mounted as your home drive? Check with ls -alh /dev/disk/by-id/<  [y/N]"
                    if [[ "$REPLY" =~ [yY] ]]; then
                        echo "[  CONFIRMED  ] $i will be used in cryptab for opening your encrypted home."
                    else
                        echo "[  WARNING POSSIBLE ERROR  ] Multiple UUID's $i will be used in cryptab for opening your encrypted home. Please manually remove the non valid UUID now in crypttab before rebooting!"
                    fi
                #else
                    #echo "Skipped $i as it is not mounted"
                fi
            done

            if ( $chrootActive ); then
                create_crypttab
            fi

            #Get root UUID
            UUID=$(ls -al /dev/disk/by-uuid | grep "$installRoot" | awk '{print $9}')
        else
            UUID=$(ls -al /dev/disk/by-uuid | grep "$installRoot" | awk '{print $9}')
        fi
        #Workaround. In some cases in chroot the uuid is still empty with ls -al /dev/disk/by-uuid. BUG??? Added partprobe above before mounting
        #if [[ "$UUID" == "" ]]; then
        #    UUID=$(blkid -o value "$installRoot" | awk 'NR<2')
        #fi
        #When installing new kernel, make sure that it is known by the bootloader with systemdboot_config or grub-mkconfig
        local bootParLoc=""
        if ( $quietBoot ); then
            bootParLoc="$bootPar quiet"
        else
            bootParLoc="$bootPar"
        fi
        if ( $efi ); then
            #Parameter: Filename LinuxType Fallback DiskID
            #Remove quiet as it is better to see the linux boot output in case of errors and it feels faster even if it is probably slower
            #Not required anymore as "$bootPar" is set to rw by default and will add quiet if necessary
            systemdboot_config "arch${kerneltype}.conf" "$kerneltype" "" "$UUID" "$bootParLoc" "$CRYPTUUID"
            #else
            #    systemdboot_config "arch${kerneltype}.conf" "$kerneltype" "" "$UUID" "" "$CRYPTUUID"
            #fi
            systemdboot_config "arch${kerneltype}-fallback.conf" "$kerneltype" "-fallback" "$UUID" "" "$CRYPTUUID"
            #multi-user.target (all services, no display manager, no network)
            systemdboot_config "arch${kerneltype}-fallback-rescue.conf" "$kerneltype" "-fallback-rescue" "$UUID" "systemd.unit=rescue-user.target" "$CRYPTUUID"
            serial_console_enable "/boot/loader/entries/archlinux.conf"
        else
            if [[ "$(uname -m)" != "aarch64" ]]; then
                local file="/etc/default/grub"
                if ( ! $quietBoot ); then
                    config_replacevalue "$bootParLoc" "" "$file"
                fi
                serial_console_enable "$file"
                if [[ "$kerneltype" == "linux-lts" ]]; then
                    config_setvalue "GRUB_DEFAULT=" "\"Advanced options for Arch Linux>Arch Linux, with Linux linux\"" "$file"
                elif [[ "$kerneltype" == "linux-hardened" ]]; then
                    config_setvalue "GRUB_DEFAULT=" "\"Advanced options for Arch Linux>Arch Linux, with Linux linux-hardened\"" "$file"
                fi
                sudo grub-mkconfig -o /boot/grub/grub.cfg
            else
                echo "[ Info ] - No bootloader installed"
            fi
        fi
    else
        echo "$(tput setaf 3)[  ERROR ]$(tput sgr0) Please enter a valid kernel package name"
    fi
}

create_crypttab() {
    #Create Crypttab for encrypted home
    echo "
#home" | sudo tee -a "/etc/crypttab"
    #Performance SSD/NVME https://wiki.archlinux.org/title/Dm-crypt/Specialties#Disable_workqueue_for_increased_solid_state_drive_(SSD)_performance
    #ATTENTION TO BE CHECKED if VARIABLE IS STILL SET
    if [[ "$diskType" == "nvme" ]] || [[ "$diskType" == "ssd" ]]; then
        echo "$encryptedNameHome  UUID=$UUIDhome  none    luks,no-read-workqueue,no-write-workqueue" | sudo tee -a "/etc/crypttab"
    else
        echo "$encryptedNameHome  UUID=$UUIDhome  none    luks" | sudo tee -a "/etc/crypttab"
    fi
}

systemdboot_config() {
    local file="/boot/loader/entries/$1"
    local kerneltype="$2"
    local fallback="$3"
    local UUIDLoc="$4"
    local quiet="$5"
    local CRYPTUUIDLoc="$6"
    #local UUIDhomeLoc="$7"
    local label="rootlks"
    #HiDPI small font during boot
    #local font="fbcon=font:TER16x32"
    echo "title    arch${kerneltype}$fallback" | sudo tee "$file"
    echo "linux    /vmlinuz-$kerneltype" | sudo tee -a "$file"
    #BUG systemd-boot upstream: Boot fails if the *-ucode.img files or lines in .conf file are not available!!!
    #Microcode is selected now automatically via /etc/mkinitcpio.conf in hooks
    #echo "initrd  /amd-ucode.img" | sudo tee -a "$file"
    #echo "initrd  /intel-ucode.img" | sudo tee -a "$file"
    #fi
    if [[ "$fallback" == "-fallback-rescue" ]]; then
        fallback="-fallback"
    fi
    if [[ "$initramfs" == "dracut" ]]; then
        local initramfsLoc="-$initramfs"
    fi
    #    echo "initrd   /initramfs-$kerneltype$fallback${initramfsLoc}.img" | sudo tee -a "$file"
    #else
        echo "initrd   /initramfs-$kerneltype$fallback${initramfsLoc}.img" | sudo tee -a "$file"
    #fi
    #No resume resume=$UUIDLoc
    local bootParLoc=""
    if ( $quietBoot ); then
        bootParLoc="$bootPar quiet"
    else
        bootParLoc="$bootPar"
    fi
    if ( $encryptRoot ); then
        echo "options  cryptdevice=UUID=${UUIDLoc}:$label root=UUID=$CRYPTUUIDLoc $bootParLoc" | sudo tee -a "$file"
    #If home is encrypted, it must be opened with /etc/crypttab instead of a kerel parameter!
    #elif ( $encryptHome ); then
    #    label="homelks"
        ### BUG Is this correct for open and mount home???????
    #    echo "options  root=UUID=$UUIDLoc cryptdevice=UUID=${UUIDhomeLoc}:$label home=UUID=$CRYPTUUIDLoc $bootParLoc" | sudo tee -a "$file"
    else
        echo "options  root=UUID=$UUIDLoc $bootParLoc" | sudo tee -a "$file"
    fi

    echo "${file}:Boot entry created"
    echo "---------"
}

systemdboot_loader() {
    local file="/boot/loader/loader.conf"
    if [[ "$kerneltype" != "" ]]; then
        echo "default   arch${kerneltype}.conf" | sudo tee "$file"
        echo "timeout   3" | sudo tee -a "$file"
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) missing kernel info: $(tput setaf 1)ERROR$(tput sgr0)"
    fi
}

serial_console_enable(){
    #Enable Serial Console in VM
    local file="$1"
    if [[ "$computerType" == "vm" ]]; then
        config_replacevalue "$bootPar" "$bootPar console=tty0 console=ttyS0,115200" "$file"
    fi
}

root_account() {
    if ( $enableRoot ); then
        user_passwd_set "root"
    else
        #Disable root account!!!! Important step, otherwise you can login with user root but without a password, DANGER!
        #Disable shell login, this will show "Login incorrect" when entering user root. Affects login, kdm/gdm, su, ssh, scp, sftp, not affected sudo, ftp, email (setuid)
        #https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Security_Guide/s2-wstation-privileges-noroot.html
        config_replacevalue "root:x:0:0:root:\/root:\/bin\/bash" "root:x:0:0:root:\/root:\/usr\/bin\/nologin" "/etc/passwd"
        #lock root account, this replaces the password in /etc/shadow with "!". This will show a password request even though nologin has been defined in previous step
        #https://wiki.archlinux.org/index.php/Sudo#Disable_root_login
        passwd -l root
        #SSH login for root is disallowed automatically by upstream, no need to set "PermitRootLogin no" in /etc/ssh/sshd_config
        #https://wiki.archlinux.org/index.php/OpenSSH#Limit_root_login
    fi
}

archinstallbot_conf() {
    # Create information about system archlinux_installer.
    # CUSTOMIZED INSTALLATION
    local file="/etc/archinstallbot.conf"
    echo "#!/bin/bash
export installedByArchinstallbot=true
export installPacbot=$installPacbot
export enableHomed=$enableHomed" | tee "$file" &> /dev/null
    if  ( $installPacbot ); then
        if [[ "$(uname -m)" == "aarch64" ]]; then
            #Workaround: On aarch64 this is called as a function and doesn't run as a skript with --chroot parameter, so set it manually to use userNameNew!!!
            runParameterLoc="--chroot"
        fi
        pacbot_update_git
        bashrc_first_boot
        echo "[  info  ] pacbot installed. archbot will be loaded on first boot!"
    fi
}

archinstallbot_conf_arm() {
    # Create information about system archlinux_installer.
    # CUSTOMIZED INSTALLATION
    local file="$installDir/etc/archinstallbot.conf"
    echo "#!/bin/bash
export installedByArchinstallbot=true
export installPacbot=$installPacbot
export enableHomed=$enableHomed" | sudo tee "$file" &> /dev/null
    if  ( $installPacbot ); then
        bashrc_first_boot_arm
        echo "[  info  ] pacbot installed. archbot will be loaded on first boot!"
    fi
}

geo_location_service() {
    #BUG Proprietary service is used here, privacy issue?
    local ipinfo=$(curl https://ipinfo.io)
    local ipPcCity=$(echo $ipinfo | cut -d "," -f3 | cut -d ":" -f2 | cut -c 3- | rev | cut -c 2- | rev)
    local ipRegionCity=$(echo $ipinfo | cut -d "," -f10 | cut -d ":" -f2 | cut -c 3- | rev | cut -c 2- | rev)
    local ipRegion=$(echo $ipRegionCity | cut -d "/" -f1)
    local ipCity=$(echo $ipRegionCity | cut -d "/" -f2)
    local ipCountryCode=$(echo $ipinfo | cut -d "," -f5 | cut -d ":" -f2 | cut -c 3- | rev | cut -c 2- | rev)
    local ipCountryCodeLow=$(echo $ipCountryCode | awk '{print tolower($0)}')
    config_setvalue "pcCity=" "\"${ipPcCity}\"" "./archinstallbot.sh"
    #BUG Doesn't work in all countries
    config_setvalue "localeConf=" "\"${ipCountryCodeLow}_${ipCountryCode}.UTF-8\"" "./archinstallbot.sh"
    config_setvalue "region=" "\"${ipRegion}\"" "./archinstallbot.sh"
    config_setvalue "city=" "\"${ipCity}\"" "./archinstallbot.sh"
}

####################################
############## AARCH64 #############
####################################

archlinuxarm_installer() {
    taskText="install Arch Linux ARM for $soc on a automatically partitioned+formatted disk"
    gpt=false
    # BUG Install on btrfs???
    # https://gist.github.com/XSystem252/d274cd0af836a72ff42d590d59647928
    archlinux_preparedisks
    archlinuxarm_install
}

archlinuxarm_install() {
    if ( $diskPrepared ); then
        if [[ "$soc" == "rp4" ]]; then
            #Step 5
            iso="ArchLinuxARM-rpi-aarch64-latest.tar.gz"
        elif [[ "$soc" == "oc2" ]]; then
            #6. Download and extract the root filesystem (as root, not via sudo):
            iso="ArchLinuxARM-odroid-c2-latest.tar.gz"
        fi
        #BUG No SSL and no file verification!!!
        local linkIso="http://os.archlinuxarm.org/os/$iso"
        wget --show-progress --quiet --continue "${linkIso}" --output-document="$iso"
        echo "$(tput setaf $color)[  flash  ]$(tput sgr0) Writing files to the SD-Card can take several minutes"
        sudo bsdtar -xpf $iso -C "$installDir"

        if [[ "$soc" == "oc2" ]]; then
            #7. Flash the bootloader files:
            echo "[    Info    ] Flash bootloader files for oc2"
            cd "$installDir"/boot/
            sudo sh "$installDir"/boot/sd_fusing.sh /dev/"$target"
            cd ../..

            #8. (Optional) Set the screen resolution for your monitor:
            #Open the file root/boot/boot.ini with a text editor.
            #Uncomment the line with the desired resolution, and ensure all others are commented.
            #Save and close the file.
        fi
        sync

        #Step 6
        #mv "$installDir"/boot/* boot

        #AArch64 Replace drive name
        if [[ "$soc" == "rp4" ]] && ( $workaroundrp4 ); then
            sudo sed -i 's/mmcblk0/mmcblk1/g' "$installDir"/etc/fstab
        fi

        #Workaround... doesn't work? for USB support
        #Set in boot/config.txt total_mem=2048

        #If encrypted home is used, first run archbot install doesn't work, so umount home now
        if ( $encryptHome ); then
            sudo umount "$installDir"/home &> /dev/null
            sudo mkdir -p "$installDir"/home/alarm
            sudo chown -R nobody:nobody "$installDir"/home/alarm
            sudo chmod -R 777 "$installDir"/home/alarm
            profile_init "alarm"
        fi
        #sudo cryptsetup close "$encryptedNameHome"
        #Pacbot
        sudo git clone https://gitlab.com/rainer.f/pacbot.git "$installDir"/home/alarm/pacbot
        archinstallbot_conf_arm

        #Step 7
        #sudo umount "$installDir"/boot
        #sudo umount "$installDir"
        cleanup_task

        #Cleanup
        rm "$iso"

        #Boot to new systemchecks
        echo "$(tput setaf 2)>>>>>>>>>>>>>> INSTALLATION FINISHED <<<<<<<<<<<<<<<$(tput sgr0)"
        echo ""
        echo "Please insert the SD card now to the AARCH64 system."
        echo "Plugin the power supply to boot into the new system."
        echo ""
        echo "Default user and password: $(tput setaf 2)alarm/alarm$(tput sgr0) and $(tput setaf 2)root/root$(tput sgr0)."
        echo ""
        if [[ "$soc" == "rp4" ]]; then
            echo "$(tput setaf $color)Attention:$(tput sgr0) Please connect to the $(tput setaf $color)Rasperry Pi 4$(tput sgr0) via $(tput setaf 2)ssh alarm@IPADDRESS$(tput sgr0) as USB doesn't work with the mainline kernel."
        else
            echo "You can connect to your device via $(tput setaf 2)ssh alarm@IPADDRESS$(tput sgr0)"
        fi
        echo "You can scan the local network with (sudo arp-scan 192.168.1.1/24) or look on your router for the connected devices."
        if ( $encryptHome ); then
            echo "Note: Encrypted home partition needs to be added to fstab manually on aarch64 as USB keyboards do not always work!"
        fi
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Disk is not prepared, $(tput setaf $color)ERROR$(tput sgr0)"
        cleanup_task
    fi
}

####################################
############# DUPLICATE ############
####################################

check_target_disk() {
    #Used as well in archinstallbot, usbstickbot
    testRootDiskNVME=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -7)
    testRootDiskSSD=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -3)
    usbRootDisk=$(mount | grep "archiso/boot" | awk '{print $1}' | cut -c 6- | cut -c -3)
    exclude0BDisks=$(lsblk --nodeps | grep " 0B " | awk '{ print $1}')
    local count=0
    for i in ${allDisks[*]}; do
        if [[ "$i" != "$testRootDiskNVME" ]] && [[ "$i" != "$testRootDiskSSD" ]] && [[ "$i" != "$usbRootDisk" ]] && [[ "$i" != "$exclude0BDisks" ]]; then
            availableDisks[count]=$i
            ((count++))
        fi
    done
    # Show available disks and partitions. lsblk requires no root, but doesn't show partitions
    #sudo fdisk -l
    echo " "
    lsblk --nodeps | awk '!/loop/ {print}' | awk '!/archiso/ {print}' | awk '!/ 0B / {print}'
    echo " "

    checkArray emptyArray availableDisks
    if ( ! $emptyArray ); then
        echo ">>>>>> Available Disks <<<<<<"
        echo "$(tput setaf 2)${availableDisks[*]}$(tput sgr0)"
        # Read disk name
        read -r -p "$(tput setaf $color)Please type the name of the $(tput setaf 2)target disk:$(tput sgr0)"
        local target="$REPLY"
        read -r -p "$(tput setaf $color)Do you really want to $taskText $(tput setaf 2)$target$(tput sgr0)$(tput setaf $color) and $(tput setaf 1)DELETE ALL DATA$(tput sgr0) $(tput setaf $color)?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            if [[ "$testRootDisk" != "$target" ]]; then
                local check=0
                for i in ${availableDisks[*]}; do
                    if [[ "$i" == "$target" ]]; then
                        prepare_disk_tasks
                        check=1
                    fi
                done
                if [[ "$check" == "0" ]]; then
                    echo "$(tput setaf $color)Unknown disk >$target<.$(tput sgr0) Close."
                fi
            else
                echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) $(tput setaf $color)Cannot overwrite root >$testRootDisk< of running system.$(tput sgr0) ERROR"
            fi
        else
            echo "$(tput setaf $color)No target disk selected.$(tput sgr0) Close."
        fi
    else
        echo "$(tput setaf $color)No ${targetText}.$(tput sgr0) Close."
        exit
    fi
}

checkArray() {
    #Used as well in pacbot, archinstallbot, systemchecks, helpers
    #Check if Array is empty, returnVar is the name of the boolean variable, arrayLoc is the name of the Array
    local -n returnVar=$1
    local -n arrayLoc=$2
    if [[ ${#arrayLoc[@]} -gt 0 ]] && [[ "${arrayLoc[*]}" = *[![:space:]]* ]]; then
        #echo "FunctionArrayNOTEmpty"
        returnVar=false
    else
        #echo "FunctionArrayEmpty"
        returnVar=true
    fi
}

####################################
############# Helper ###############
####################################

config_comment() {
    #Info https://stackoverflow.com/questions/55933895/how-to-uncomment-the-next-line-with-sed
    local comment=$1
    local commentChar=$2
    local filename=$3
    sudo sed -i -e "/$comment/s/^${commentChar}*/${commentChar}/" "$filename"
}

config_uncomment() {
    local comment=$1
    local commentChar=$2
    local filename=$3
    sudo sed -i -e "/$comment/s/^${commentChar}//" "$filename"
}

config_commentmutliline() {
    local comment=$1
    local commentChar=$2
    local filename=$3
    sudo sed -i -e "/^${comment}/{N;s/\n${commentChar}/\n//}" "$filename"
}

config_uncommentmutliline() {
    local comment=$1
    local commentChar=$2
    local filename=$3
    sudo sed -i -e "/^${comment}/{N;s/\n${commentChar}/\n/}" "$filename"
}

config_replacevalue() {
    local oldValue="$1"
    local newValue="$2"
    local filename="$3"
    sudo sed -i "s/${oldValue}/${newValue}/g" "$filename"
}

config_setvalue() {
    local setting="$1"
    local newValue="$2"
    local filename="$3"
    sudo sed -i "s/\(^${setting}\).*/\1${newValue}/" "$filename"
}

bashrc_output_default() {
    local userNameLoc="$1"
    if [[ "$userNameLoc" == "" ]]; then
        local file="${HOME}/.bashrc"
    elif [[ "$userNameLoc" == "alarm" ]]; then
        local file="$installDir/home/alarm/.bashrc"
    elif ( $enableHomed ); then
        local file="/root/.bashrc"
    else
        #chroot, no user
        local file="/home/$userNameLoc/.bashrc"
    fi
    more /etc/skel/.bashrc | sudo tee "$file" &> /dev/null
}

bashrc_first_boot() {
    if ( $enableHomed ) && ( $chrootActive ); then
        profile_init "root"
        bashrc_output_default "root"
        local file="/root/.bashrc"
    else
        bashrc_output_default "$userNameNew"
        local file="/home/$userNameNew/.bashrc"
    fi
    #Run archbot after login, will be replaced by init step 3
    echo "EDITOR=/usr/bin/nano
#First Boot Helper
archbot" | tee -a "$file" &> /dev/null
}

bashrc_first_boot_arm() {
    bashrc_output_default "alarm"
    local file="$installDir/home/alarm/.bashrc"
    #Run archbot after login, will be replaced by init step 3
    echo "EDITOR=/usr/bin/nano
#First Boot Helper
echo \"----------------------------------------------------------------------------------------- \"
echo \"----------------------------------------------------------------------------------------- \"
echo \"------ Please login as root by running the $(tput setaf $color)su$(tput sgr0) command. This will run archbot setup ------ \"
echo \"----------------------------------------------------------------------------------------- \"
echo \"----------------------------------------------------------------------------------------- \"" | sudo tee -a "$file"  &> /dev/null

    local file="$installDir/root/.bashrc"
    echo "bash /home/alarm/pacbot/src/pacbot/archbot.sh --local --firstbootarm" | sudo tee "$file" &> /dev/null
}

profile_init() {
    #This will run the .bashrc file automatically on login (required if user home folder is created manually or if missing e.g. in root folder in some cases)
    local userNameLoc="$1"
    if [[ "$uuserNameLoc" == "" ]]; then
        local file="${HOME}/.bash_profile"
    elif [[ "$userNameLoc" == "alarm" ]]; then
        local file="$installDir/home/alarm/.bash_profile"
    elif ( $enableHomed ) || [[ "$uuserNameLoc" == "root" ]]; then
        local file="/root/.bash_profile"
    else
        #chroot, no user
        local file="/home/$userNameLoc/.bash_profile"
    fi
    more /etc/skel/.bash_profile | sudo tee "$file" &> /dev/null
}

pacbot_update_git() {
    # Install/update pacbot
    local instDir="/tmp/pacbot-git-tmp"
    if ( $chrootActive ); then
        if ( $enableHomed ); then
            local userName="nobody"
        else
            local userName="$userNameNew"
        fi
    else
        local userName="nobody"
    fi
    sudo -u "$userName" mkdir "$instDir"
    #su -c 'mkdir $instDir' - $userName
    if sudo -u "$userName" bash -c "[[ -w $instDir ]]"; then
    #if su -c 'bash -c "[[ -w $instDir ]]"' - $userName; then
        cd "$instDir" || exit
        sudo -u "$userName" git clone https://gitlab.com/rainer.f/pacbot.git
        #su -c 'git clone https://gitlab.com/rainer.f/pacbot.git' - $userName
        cd pacbot || exit
        local tmpVersion="$(grep -i '^pkgver=' PKGBUILD | cut -c 8-)"
        #BUG: no checking for minor relase => release="$(grep -i '^pkgrel=' PKGBUILD | cut -c 8-)"
        local installedVersion="$(pacman -Q pacbot 2> /dev/null | grep pacbot | cut -c 8- | cut -f1 -d '-')"
        if [[ "$installedVersion" < "$tmpVersion" ]]; then
            #makepkg -sirc
            if [[ "$userName" == "nobody" ]]; then
                #NOTE Dependencies for pacbot are installed in pacstrab with parameter dependsPacbotPKGBUILD, but reinstall to make sure it works here
                #WARNING Parameter -s will tell makepkg to resolves dependencies and ask for a not existing password of nobody
                sudo pacman -Sy --noconfirm $dependsPacbotPKGBUILD
                sudo -u "$userName" makepkg -fc --noconfirm
            else
                sudo -u "$userName" makepkg -sfc --noconfirm
                #su -c 'makepkg -sfc --noconfirm' - $userName
            fi
            #if ( $updatePacbot ); then
                sudo pacman -U --noconfirm $(sudo -u $userName makepkg --packagelist)
            #else
            #    sudo pacman -U --noconfirm $(sudo -u $userName makepkg --packagelist)
            #fi
            if [[ "$?" == 1 ]]; then
                echo "[  ERROR  ] Check if debug packages are enabled in /etc/makepkg.conf"
            fi
            pacbotUpdateText="Update done of pacbot"
            sudo systemctl daemon-reload
        else
            pacbotUpdateText="Latest version is already installed"
        fi
    fi
    install_aur_cleanup "$userName"
}

install_aur_cleanup() {
    userNameLoc="$userName"
    delDir=$(echo "$instDir" | cut -c -5)
    if [[ "$delDir" == "/tmp/" ]]; then
        cd "$delDir" || return
        sudo -u "$userNameLoc" rm -R --force "$instDir"
    fi
}

####################################
############ Interface #############
####################################

runParameterLoc=$1
runParameterLoc2="$2"
archinstallbot_init

if [[ "$runParameterLoc" == "--chroot" ]]; then
    chrootActive=true
    if [[ "$runParameterLoc2" == "--enableRoot" ]]; then
        enableRoot=true
        #WARNING Assumes that root account is only active if user wants homed, otherwise sudo only!
        enableHomed=true
    else
        enableRoot=false
        enableHomed=false
    fi
    archlinux_install_chroot
elif [[ "$runParameterLoc" != "--source-only" ]]; then
    #Upstream added 2 lines of comments before hostname archiso
    installMedia=$(more /etc/hostname | grep arch)
    if [[ "$installMedia" == "archiso" ]]; then
        loadkeys "$loadKeyboard"
    fi

    if [[ "$EUID" -eq 0 ]] && [[ "$installMedia" != "archiso" ]]; then
        printf "archinstallbot should not run directly with root privileges, it works as well with sudo in existing Arch Linux!\n"
    else
        if [[ "$runParameterLoc" == "--aarch64" ]]; then
            if [[ "$runParameterLoc2" == "--rp4" ]]; then
                #Rasperry Pi 4 B
                ##https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4
                soc="rp4"
                workaroundrp4=true
            elif [[ "$runParameterLoc2" == "--rp3" ]]; then
                #Raspberry Pi 3
                #https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3
                soc="rp4"
            elif [[ "$runParameterLoc2" == "--oc2" ]]; then
                #Odroid-C2
                #https://archlinuxarm.org/platforms/armv8/amlogic/odroid-c2
                soc="oc2"
            fi
            archlinuxarm_installer
        else
            if [[ "$runParameterLoc" == "--enableHomed" ]]; then
                enableRoot=true
                enableHomed=true
                echo "$(tput setaf $color)[  EXPERIMENTAL  ]$(tput sgr0) Use systemd-homed"
            else
                enableRoot=false
                enableHomed=false
            fi
            archlinux_installer
        fi
    fi
fi
