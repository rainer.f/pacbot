#!/bin/bash
# pacbot - Automated Pacman and System Maintenance, author Rainer Finke, license GPLv3

####################################
######## Global Variables ##########
####################################

declare -a smartHealthText
declare -a smartInfoText2
declare -a smartCheckText2
declare -a scrubStatusText
declare -a scrubFinishedText
declare -a balanceFinishedText
declare -a balanceLogText
declare -a btrfsMountPoint
declare -a btrfsFilesystemShowText
declare -a btrfsFilesystemDFText
declare -a btrfsFilesystemUsageText
declare -a btrfsDeviceStatsText
declare -a btrfsDevicesArr
export smartHealthText
export smartInfoText2
export smartCheckText2
export scrubStatusText
export scrubFinishedText
export balanceFinishedText
export balanceLogText
export btrfsMountPoint
export btrfsFilesystemShowText
export btrfsFilesystemDFText
export btrfsFilesystemUsageText
export btrfsDeviceStatsText
emptyArray=false

####################################
########## System Checks ###########
####################################

system_errors() {
	failed_services
	journal_errors
	printf "\n"
}

failed_services() {
	printf "\nFAILED SYSTEMD SERVICES:\n"
	systemctl --failed
}

journal_errors() {
	printf "\nHIGH PRIORITY SYSTEMD JOURNAL ERRORS:\n"
	journalctl -p 3 -xb
}

####################################
############## Btfrs ###############
####################################

btrfs_devices() {
    #Examples: /dev/mapper/crypthome or /dev/nvme0n1p2
    btrfsDevicesArr=$(grep '\<btrfs\>' /proc/mounts | awk '{ print $1 }' | sort -u)
}

btrfs_scrub_status() {
    # Scrub status, based on http://marc.merlins.org/linux/scripts/btrfs-scrub
    local deviceCount=0
    btrfs_devices
    for btrfs in $btrfsDevicesArr
    do
        local mountpoint=""
        mountpoint=$(grep "$btrfs" /proc/mounts | awk '{ print $2 }' | sort | head -1)
        btrfsMountPoint[$deviceCount]="$mountpoint"
        scrubStatusText[$deviceCount]="$(btrfs scrub status $mountpoint)"
        btrfsDeviceStatsText[$deviceCount]="$(btrfs device stats $mountpoint)"
        ((deviceCount++))
    done
    checkArray emptyArray scrubStatusText
    if ( $emptyArray ); then
        scrubStatusText[0]="Btrfs not used/support!"
    fi
}

btrfs_scrub_status_show() {
    local showDetails="$1"
    if [[ "$showDetails" == "" ]]; then
        showDetails=true
    fi
    local deviceCount=0
    btrfs_scrub_status
    for partition in "${btrfsMountPoint[@]}"; do
        if ( $showDetails ); then
            btrfs_scrub_status_notification

            printText "$debugName btrfs scrub status ${btrfsMountPoint[$deviceCount]}" 0 1 "v"
            #echo "${scrubFinishedText[$deviceCount]}"
            #echo "${scrubStatusText[$deviceCount]}"
            echo "${scrubStatusText[$deviceCount]} $(tput setaf 2)${scrubFinishedText[$deviceCount]}$(tput sgr0)"

            printText "$debugName btrfs device stats ${btrfsMountPoint[$deviceCount]}" 1 1 "v"
            echo "${btrfsDeviceStatsText[$deviceCount]}"

            echo "
----------------------------------------------------------------"
        else
            btrfs_scrub_status_notification
        fi
        ((deviceCount++))
    done
}

btrfs_scrub_status_notification() {
    local btrfsScrubError=$(echo "${scrubStatusText[$deviceCount]}" | grep "Error summary:" | awk '{ print $3$4$5}')

    local -a btrfsStatsError
    btrfsStatsError=$(echo "${btrfsDeviceStatsText[$deviceCount]}" | awk '{ print $2 }')
    local btrfsStatsOK=true
    for i in ${btrfsStatsError[@]}; do
        #echo "------$i------"
        if [[ "$i" == "0" ]] && ( $btrfsStatsOK ) ; then
            btrfsStatsOK=true
        else
            btrfsStatsOK=false
        fi
    done

    #echo "--------------${btrfsMountPoint[$deviceCount]}"
    if [[ "$btrfsScrubError" == "noerrorsfound" ]] && ( $btrfsStatsOK ); then
        if ( $showDetails ); then
            echo "
Btrfs health for ${btrfsMountPoint[$deviceCount]}: $(tput setaf 2)Scrub Checksums Verified$(tput sgr0) & $(tput setaf 2)IO-Statistic Good$(tput sgr0)
"
        else
            btrfsHealthStatusARR["${btrfsMountPoint[$deviceCount]}"]="$(tput setaf 2)Scrub-Checksums-Verified$(tput sgr0) $(tput setaf 2)IO-Statistic-Good$(tput sgr0)"
        fi
    elif [[ "$btrfsScrubError" == "noerrorsfound" ]] && ( ! $btrfsStatsOK ); then
        if ( $showDetails ); then
            echo "
Btrfs health for ${btrfsMountPoint[$deviceCount]}: $(tput setaf 2)Scrub Checksum verified$(tput sgr0) & $(tput setaf 1)IO-Statistic Errors$(tput sgr0) recorded. Please check!!!
"
        else
            btrfsHealthStatusARR["${btrfsMountPoint[$deviceCount]}"]="$(tput setaf 2)Scrub-Checksum-Verified$(tput sgr0) $(tput setaf 1)IO-Statistic-Errors$(tput sgr0)"
        fi
    elif [[ "$btrfsScrubError" != "noerrorsfound" ]] && ( $btrfsStatsOK ); then
        if ( $showDetails ); then
            echo "
Btrfs health for ${btrfsMountPoint[$deviceCount]}: $(tput setaf 1)Scrub Checksum Errors$(tput sgr0) & $(tput setaf 2)IO-Statistics Good$(tput sgr0)
"
        else
            btrfsHealthStatusARR["${btrfsMountPoint[$deviceCount]}"]="$(tput setaf 1)Scrub-Checksum-Errors$(tput sgr0) $(tput setaf 2)IO-Statistics-Good$(tput sgr0)"
        fi
    else
        if ( $showDetails ); then
            echo "
Btrfs health for ${btrfsMountPoint[$deviceCount]}: $(tput setaf 1)Scrub Checksum Errors$(tput sgr0) & $(tput setaf 1)IO-Statistic Errors$(tput sgr0). Please check urgently the Btrfs volume!!!
"
        else
            btrfsHealthStatusARR["${btrfsMountPoint[$deviceCount]}"]="$(tput setaf 1)Scrub-Checksum-Errors$(tput sgr0) $(tput setaf 1)IO-Statistic-Errors$(tput sgr0). Please check urgently the Btrfs volume!!!"
        fi
    fi
    #echo "-X------------------${btrfsHealthStatusARR[$partition]}"
    #echo "-X------------------${btrfsHealthStatusARR[@]}"
    #echo "-X-------------------"${btrfsHealthStatusARR["/home"]}
}

btrfs_scrub_start() {
    # Scrub (weekkly/onACPower), detect bitrot via checksum, repair automatically in RAID 1/5/6/10
    # Is btrfs-scrub@.timer offering the same functionality? The e2scrub_all.timer could check ext4 metadata as well.
    local deviceCount=0
    btrfs_devices
    for btrfs in $btrfsDevicesArr
    do
        local mountpoint=""
        mountpoint=$(grep "$btrfs" /proc/mounts | awk '{ print $2 }' | sort | head -1)
        #Allow scrub to run in the background as it could take hours, do not wait for output
        btrfs scrub start "$mountpoint"

        ((deviceCount++))
    done
}

btrfs_scrub_start_foreground_reset_statistic() {
    read -r -p "Running btrfs scrub in foreground can take $(tput setaf $color)minutes or hours depending from the amount of data stored$(tput sgr0) on the device!!! If you did use $(tput setaf 2)ssh$(tput sgr0) for the connection, use $(tput setaf 2)tmux$(tput sgr0) to get a console that can be reopened even if the connection was lost. Continue? [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
          btrfs_scrub_start_log_errors
        declare -A btrfsHealthStatusARR
        btrfs_scrub_status_show false
        btrfs_statistic_reset_manual
    fi
}

btrfs_scrub_start_log_errors() {
    local file="/home/$userName/btrfs_checksum_errors.txt"
    echo "--------------------- START    $(date +%Y-%m-%d:%H-%M) ---------------------" | grep "" >> $file
    sudo chmod 777 $file
    #Run dmesg in background in parrael
    sudo dmesg --follow-new  | grep --line-buffered "checksum error at" >> $file &

    local deviceCount=0
    btrfs_devices
    for btrfs in $btrfsDevicesArr
    do
        local mountpoint=""
        mountpoint=$(grep "$btrfs" /proc/mounts | awk '{ print $2 }' | sort | head -1)
        #Run Scrub in Foreground
        echo "$(tput setaf $color)Running btrfs scrub for $mountpoint can take minutes or hours...$(tput sgr0) Please wait..."
        btrfsSrubForeground=$(btrfs scrub start "$mountpoint")
        local scrubStatus=$?
        # Details https://btrfs.wiki.kernel.org/index.php/Manpage/btrfs-scrub
        if [[ "$scrubStatus" == 0 ]]; then
            scrubFinishedText[$deviceCount]="ScrubRun=OK"
        elif [[ "$scrubStatus" == 1 ]]; then
            scrubFinishedText[$deviceCount]="ScrubRun=Error"
        elif [[ "$scrubStatus" == 2 ]]; then
            scrubFinishedText[$deviceCount]="ScrubRun=NoResume"
        elif [[ "$scrubStatus" == 3 ]]; then
            scrubFinishedText[$deviceCount]="ScrubRun=UncorrectableErrors"
        else
            scrubFinishedText[$deviceCount]="ScrubRun=UnknownError"
        fi
        scrubStatusText[$deviceCount]="[$mountpoint] $(btrfs scrub status $mountpoint)"

        echo "$btrfsSrubForeground" | grep "" >> $file
        echo "${scrubFinishedText[$deviceCount]}" | grep "" >> $file
        echo "--------------------- END $mountpoint $(date +%Y-%m-%d:%H-%M) ---------------------" | grep "" >> $file

        ((deviceCount++))
    done

    btrfs_scrub_status_show true

    echo "Please check the log file $file for errors of files!"
}

btrfs_statistic_reset_manual() {
    echo ""
    local file="/home/$userName/btrfs_checksum_errors.txt"
    read -r -p "Opening the btrfs checksum error log. $(tput setaf $color)Please check for files that couldn't be corrected. Continue$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        nano "$file"
    fi

    echo ""
    local file="/home/$userName/btrfs_checksum_errors.txt"
    read -r -p "$(tput setaf $color)If scrub showed no uncorrectable error, the statistic should be reset as the file that didn't match the checksum is not available anymore! Do you CONFIRM that scrub shows NO ERRORS and that you want to reset the btrfs statistic?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        btrfs_statistic_reset_volume
    fi
}

btrfs_statistic_reset_volume() {
    echo ""
    read -r -p "$(tput setaf 1)Please enter the btrfs mountpoint manually to confirm the reset of the statistic$(tput sgr0): "
    echo ""
    if [[ "$REPLY" != "" ]]; then
        btrfs_statistic_reset "$REPLY"
        btrfs_statistic_reset_volume_tryagain
    else
        echo "[   Error  ] No data entered!"
        btrfs_statistic_reset_volume_tryagain
    fi
}

btrfs_statistic_reset() {
    local btrfsVolume=$1
    echo "${btrfsHealthStatusARR[$btrfsVolume]}"
    if [[ "${btrfsHealthStatusARR[$btrfsVolume]}" != "" ]]; then
        if [[ "$(echo ${btrfsHealthStatusARR[$btrfsVolume]} | grep 'Scrub-Checksum-Verified' | grep 'IO-Statistic-Errors' )" != "" ]]; then
            sudo btrfs device stats --reset "$btrfsVolume"
            echo "
#####################################
THE STATISTIC ABOVE WAS SET BACK TO 0
#####################################

"
            btrfs_scrub_status_show true
            btrfs_scrub_status_show false
        else
            echo "[ ERROR ] $btrfsVolume had no matching Scrub-Checksum-Verified IO-Statistic-Errors"
        fi
    else
        echo "[ ERROR ] Cannot access array for $btrfsVolume"
    fi
}

btrfs_statistic_reset_volume_tryagain() {
    echo ""
    read -r -p "$(tput setaf $color)If scrub showed no uncorrectable error, the statistic should be reset as the file that didn't match the checksum is not available anymore! Do you CONFIRM that scrub shows NO ERRORS and that you want to reset the btrfs statistic?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        btrfs_statistic_reset_volume
    else
        echo "[ Exit ]"
    fi
}

btrfs_balance_start() {
    # Start balance, meta data balance is not recommended, data balance for empty chunks 0 is done automatically
    # https://btrfs.wiki.kernel.org/index.php/Manpage/btrfs-balance
    local rootOnly=$1
    local deviceCount=0
    local FILTER="'(^Dumping|balancing, usage)'"
    btrfs_devices
    for btrfs in $btrfsDevicesArr
    do
        local mountpoint=""
        mountpoint=$(grep "$btrfs" /proc/mounts | awk '{ print $2 }' | sort | head -1)
        #btrfs balance start -dusage=20 -v "$mountpoint" 2>&1 | grep -Ev "$FILTER" | systemd-cat
        #Relocate if filled with less than 60%?
        if [[ "$rootOnly" == "rootOnly" ]]; then
            if [[ "$mountpoint" == "/" ]]; then
                echo "[  INFO  ] Run $rootOnly balance for $mountpoint to avoid partially upgraded system"
                btrfs_balance_mountpoint
            #else
                #DO NOT BALANCE OTHER MOUNTPOINTS EXCET / WHEN RUNNING PACMAN UPGRADE
            fi
        else
            btrfs_balance_mountpoint
        fi
    done
}

btrfs_balance_mountpoint() {
    #Start first with the most empty blocks, sometomes going directly to 60 doesn't work
    if [[ "$mountpoint" == "/" ]]; then
        btrfs balance start -dusage=10 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
        btrfs balance start -dusage=40 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
    else
        btrfs balance start -dusage=10 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
        btrfs balance start -dusage=60 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
    fi
    if [[ "$?" == 0 ]]; then
        balanceFinishedText[$deviceCount]="BalanceDU60=OK[$mountpoint]"
    else
        balanceFinishedText[$deviceCount]="BalanceDU60=Error[$mountpoint]"
    fi
    balanceLogText[$deviceCount]="[$mountpoint] $(btrfs balance status $mountpoint)"
    ((deviceCount++))
}

btrfs_balance_mountpoint_meta() {
    #Balance meta data as well, critical?
    #https://www.reddit.com/r/btrfs/comments/n1oyu5/the_final_word_about_btrfs_balance_metadata/
    btrfs balance start -dusage=10 -musage=10 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
    btrfs balance start -dusage=60 -musage=10 "$mountpoint" 2>&1 | grep -Ev "$FILTER"
}

btrfs_balance_root() {
    balance_start "rootOnly"
}

btrfs_filesystem_show() {
    local deviceCount=0
    btrfs_devices
    for btrfs in $btrfsDevicesArr
    do
        local mountpoint=""
        mountpoint=$(grep "$btrfs" /proc/mounts | awk '{ print $2 }' | sort | head -1)
        btrfsMountPoint[$deviceCount]="$mountpoint"
        btrfsFilesystemShowText[$deviceCount]="$(btrfs filesystem show $mountpoint)"
        btrfsFilesystemDFText[$deviceCount]="$(btrfs filesystem df $mountpoint)"
        btrfsFilesystemUsageText[$deviceCount]="$(btrfs filesystem usage $mountpoint)"
        ((deviceCount++))
    done
    checkArray emptyArray btrfsFilesystemShowText
    if ( $emptyArray ); then
        btrfsFilesystemShowText[0]="No btrfs support!"
    fi
}

btrfs_mount_options_deprecated() {
    #https://wiki.tnonline.net/w/Btrfs/Space_Cache
    local spaceCache=$(mount | grep -i btrfs | grep "space_cache,")
    if [[ "$spaceCache" != "" ]]; then
        echo "$(tput setaf $color)[  WARNING ]$(tput sgr0) Deprecated mount option for btrfs. Please change in $(tput setaf $color)/etc/fstab$(tput sgr0) the $(tput setaf $color)space_cache$(tput sgr0) to $(tput setaf $color)space_cache=v2$(tput sgr0). If you want to change this for a root disk, then you need to boot with a $(tput setaf $color)Live-USB$(tput sgr0) disk and delete the old space cache with $(tput setaf $color)btrfs check --clear-space-cache v1 /dev/device$(tput sgr0) and then mount it with the new space cache $(tput setaf $color)mount /dev/device /mnt/btrfs -o space_cache=v2$(tput sgr0). The conversion can take some minutes and will delay the next mount!"
    fi
}

btrfs_sector_size_show() {
    sudo btrfs inspect-internal dump-super /dev/XXXXXX | grep "sectorsize"
}

####################################
############ GlusterFS #############
####################################

glusterfs_status() {
    if pacman -Q glusterfs; then
        if systemctl status glusterd.service &> /dev/null; then
            echo "-----------gluster pool list-----------"
            gluster pool list
            echo "-----------gluster peer status-----------"
            gluster peer status
            echo "-----------gluster volume info-----------"
            gluster volume info
            echo "-----------gluster volume status-----------"
            gluster volume status
            echo "-----------gluster volume heal VOLUMENAME info-----------"
            local glusterVolumeNames=""
            glusterVolumeNames=$(gluster volume status | grep "Status of volume" | cut -c 19-)
            for volName in $glusterVolumeNames
            do
                gluster volume heal "$glusterVolumeNames" info
            done
        else
            printText "GlusterFS is not running" 1 2 "v"
        fi
    else
        printText "GlusterFS is not installed" 1 2 "v"
    fi
}

glusterfs_scrub() {
    if pacman -Q glusterfs; then
        if systemctl status glusterd.service &> /dev/null; then
            local glusterVolumeNames=""
            glusterVolumeNames=$(gluster volume status | grep "Status of volume" | cut -c 19-)
            for volName in $glusterVolumeNames
            do
                gluster volume bitrot "$glusterVolumeNames" enable
                gluster volume bitrot "$glusterVolumeNames" scrub ondemand
            done
            printText "Log /var/log/glusterfs/bitd.log" 1 2 "v"
            printText "and /var/log/glusterfs/scrub.log" 1 2 "v"
        else
            printText "GlusterFS is not running" 1 2 "v"
        fi
    else
        printText "GlusterFS is not installed" 1 2 "v"
    fi
}

####################################
############## Disks ###############
####################################

check_disks_alignment() {
    printf "\nUse Command: blockdev --getalignoff /dev/sdXY0 \n\n"
    DEVS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
    for blkdisk in $DEVS
    do
        check_disk_alignment
    done

    printf "\nUse Command: lsblk -t\n\n"

    lsblk -t
}

check_disk_alignment() {
    diskAlignment=$(sudo blockdev --getalignoff /dev/$blkdisk)
    if [[ "$diskAlignment" == "0" ]]; then
        echo "$(tput setaf 2)Disk aligned$(tput sgr0) ($diskAlignment) on $blkdisk"
    elif [[ "$diskAlignment" == "" ]]; then
        echo "Disk aligned (unknown) on $blkdisk"
    else
        echo "$(tput setaf 1)Wrong disk alignment$(tput sgr0) ($diskAlignment) on $blkdisk"
    fi
}

smart_health() {
    # Show Smart Health of all connected discs
    local deviceCount=0
    if [[ "$statusVM" == "none" ]]; then
        local -a DEVS
        DEVS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
        for blkdisk in $DEVS
        do
            smart_health_disk
            local smartFamilyText1=""
            smartFamilyText1=$(smartctl --info /dev/"$blkdisk" | grep 'Model' | awk '{ print $3 " " $4 }' | awk 'NR==1')
            local smartModelText1=""
            smartModelText1=$(smartctl --info /dev/"$blkdisk" | grep 'Model' | awk '{ print $3 " " $4 }' | awk 'NR==2')
            local smartSerialText1=""
            smartSerialText1=$(smartctl --info /dev/"$blkdisk" | grep 'Serial' | awk '{ print $3 }')
            local smartFirmwareText1=""
            smartFirmwareText1=$(smartctl --info /dev/"$blkdisk" | grep 'Firmware' | awk '{ print $3 }')
            local smartDiskCapacity=""
            smartDiskCapacity=$(smartctl --info /dev/"$blkdisk" | grep 'Capacity' | awk '{ print $5 $6 }' | awk 'NR==1')
            smartHealthText[$deviceCount]="[$blkdisk] Smart $smartHealthText2+$smartEnabledText1 $smartDiskCapacity $smartFamilyText1 $smartModelText1 $smartSerialText1 $smartFirmwareText1"
            ((deviceCount++))
        done
    else
        smartHealthText[$deviceCount]="VM NoSmartHealth"
    fi
}

smart_health_disk() {
    if [[ "$statusVM" == "none" ]]; then
        local sudoLoc=""
        if [[ "$EUID" -ne 0 ]]; then
            sudoLoc="sudo"
        fi
        smartEnabledText1=""
        smartEnabledText1=$($sudoLoc smartctl --info /dev/"$blkdisk" | grep 'SMART support is: E' | cut -c 19-)
        smartHealthText2=""
        smartHealthText2=$($sudoLoc smartctl -H /dev/"$blkdisk" | grep "SMART overall" | cut -c 51-)
        if [[  "$smartEnabledText1" == "" ]]; then
            smartEnabledText1="NotAvailable"
        fi
        if [[  "$smartHealthText2" == "" ]]; then
            smartHealthText2="Unsupported"
        fi
    else
    #    echo "[  Skipped  ] Smart is not available/useful in a VM (smartmontools)!"
        smartEnabledText1="NotAvailable"
        smartHealthText2="OnlyOutsideOfVM"
    fi
}

smart_info_xall() {
    # Show Smart Health of all connected discs
    local deviceCount=0
    if [[ "$statusVM" == "none" ]]; then
        local -a DEVS
        DEVS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
        for blkdisk in $DEVS
        do
            local smartInfoText1=""
            smartInfoText1=$(smartctl -x /dev/"$blkdisk")
            if [[  "$?" == 1 ]]; then
                smartInfoText1="Smart not supported"
            fi
            smartInfoText2[$deviceCount]="[$blkdisk] $smartInfoText1"
            ((deviceCount++))
        done
    else
        smartInfoText2[$deviceCount]="VM NoSmartInfo"
    fi
}

smart_check() {
    # Show Smart Health of all connected discs
    local deviceCount=0
    local test="$1"
    if [[ "$statusVM" == "none" ]]; then
        local -a DEVS
        DEVS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
        for blkdisk in $DEVS
        do
            local smartCheckText1=""
            smartCheckText1=$(smartctl -t "$test" /dev/"$blkdisk")
            if [[  "$?" == 1 ]]; then
                smartCheckText1="Smart not supported"
            fi
            if [[  "$smartCheckText1" == "" ]]; then
                smartCheckText1="Smart not supported"
            fi
            smartCheckText2[$deviceCount]="[$blkdisk] $smartCheckText1"
            ((deviceCount++))
        done
    else
        smartCheckText2[$deviceCount]="VM NoSmartCheck"
    fi
}

disk_info_list() {
    #This info is required when checking the mounted partition or cryptpartitions
    #NOTE The associative Array must be declared at the place where it needs to be used later! This is not possible inside of the function for whatever reason!!!
    declare -A btrfsHealthStatusARR
    btrfs_scrub_status_show false

    local -a DISKS
    DISKS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)

    #BUG Inside of a VM the partition can look like a blkdisk!!!
    for blkdisk in $DISKS
    do
        local diskType
        local uuidDisk
        local partMountInfoLoc
        local ataNo
        local -a PARTITIONS
        local capacityLoc
        local firmwareLoc
        local cryptNotMounted
        local formFactorLoc
        local rotationRateLoc
        local diskSizeLoc
        local diskLayoutLoc
        local partitionSize
        local partitionSizeFree
        ataDisk=""
        #nvmeDisk=""
        uuidDisk=""
        partMountInfoLoc=""
        ataNo=""
        capacityLoc=""
        firmwareLoc=""
        cryptNotMounted=""
        formFactorLoc=""
        rotationRateLoc=""
        diskSizeLoc=""
        diskSizeLoc=$(sudo fdisk -l /dev/$blkdisk 2> /dev/null | grep "dev" | awk 'NR==1{ print $3 " " $4 }' | rev | cut -c 2- | rev)
        if [[ "$diskSizeLoc" != "" ]]; then
            ataNo=$(ls -alh /dev/disk/by-path/ | grep "$blkdisk" | awk '{ print $9 }' | cut -c 18- | cut -c -7 | awk 'NR==1')
            if [[ "$ataNo" == "" ]]; then
                #VM
                ataNo=$(ls -alh /dev/disk/by-path/ | grep "$blkdisk" | grep "virtio" | awk '{ print $9 }' | awk 'NR==1' | cut -c -18)
            fi

            #This can contain sometimes the manufacturer, disk type, size and serial no. except for USB and VirtualDisks
            diskType=$(ls -al /dev/disk/by-id/ | grep "$blkdisk" | awk '{ print $9 }' | grep "ata" | awk 'NR==1' | cut -c 5-)
            if [[ "$diskType" == "" ]]; then
                diskType=$(ls -al /dev/disk/by-id/ | grep "$blkdisk" | awk '{ print $9 }' | grep "nvme" | grep -v "-eui" | awk 'NR==1' | cut -c 6-)
                if [[ "$diskType" == "" ]]; then
                    if [[ "$statusVM" == "none" ]]; then
                        #USB
                        diskType=$(sudo fdisk -l /dev/$blkdisk | awk 'NR==2 { print $2}')
                        if [[ "$diskType" == "" ]]; then
                            echo "[  ERROR ] $blkdisk Unknown diskType"
                        fi
                        diskType="${diskType}_SerialNo-Unkown"
                    else
                        diskType="VirtIO-Disk"
                        diskType="${diskType}_SerialNo-None"
                    fi
                fi
            fi

            diskLayoutLoc=$(sudo fdisk -l /dev/$blkdisk | awk 'NR==6 { print $2}')
            if [[ "$statusVM" != "none" ]]; then
                if [[ "$diskLayoutLoc" == "" ]]; then
                    diskLayoutLoc="PhysicalLayoutOnlyOutsideOfVM"
                else
                    diskLayoutLoc="QemuImage"
                fi
            fi

            if [[ "$statusVM" == "none" ]]; then
                capacityLoc=$(sudo smartctl -a /dev/$blkdisk | grep "User Capacity" | awk '{ print $5 " " $6 }')
                if [[ "$capacityLoc" == "" ]]; then
                    #NVME
                    capacityLoc=$(sudo smartctl -a /dev/$blkdisk | grep "Total NVM Capacity" | awk '{ print $5 " " $6 }')
                    if [[ "$capacityLoc" == "" ]]; then
                        #USB desvice
                        capacityLoc="[$diskSizeLoc]"
                    fi
                fi

                firmwareLoc=$(sudo smartctl -a /dev/$blkdisk | grep "Firmware Version" | awk '{ print $3 }')
                if [[ "$firmwareLoc" == "" ]]; then
                    firmwareLoc="unknown"
                fi

                rotationRateLoc=$(sudo smartctl -a /dev/$blkdisk | grep "Rotation Rate" | awk '{ print $3$4 }')
                if [[ "$rotationRateLoc" == "" ]]; then
                    rotationRateLoc="Flash"
                fi

                formFactorLoc=$(sudo smartctl -a /dev/$blkdisk | grep "Form Factor" | awk '{ print $3 "" $4 }')
                if [[ "$formFactorLoc" == "" ]]; then
                    formFactorLoc="Card"
                fi
            else
                capacityLoc="$diskSizeLoc"
                firmwareLoc="none"
                rotationRateLoc="Virtual"
                formFactorLoc="Volume"
            fi

            smart_health_disk

            echo ""
            echo "$(tput setaf 2)$blkdisk$(tput sgr0) $ataNo $(tput setaf 2)$diskType$(tput sgr0) [$rotationRateLoc] [$formFactorLoc] $capacityLoc $diskLayoutLoc Firmware_$firmwareLoc Smart_$smartEnabledText1&$smartHealthText2"

            disk_sector_size_check $blkdisk
            check_disk_alignment

            disk_scheduler_show

            #Sometimes blkdisk prints "crypt" devices. Only if mounted manually? Skip them!
            #PARTITIONS=$(lsblk | grep "$blkdisk" | awk 'NR>1{ print $1 }' | cut -c 7- | sort -u)
            PARTITIONS=$(lsblk | grep "$blkdisk" | grep "part" | awk '{ print $1 }' | cut -c 7- | sort -u)
            if [[ "$statusVM" != "none" ]]; then
                #Inside of a VM the disk can be the partition
                checkArray emptyArray PARTITIONS
                if ( $emptyArray ); then
                    PARTITIONS="$blkdisk"
                fi
            fi
            for part in $PARTITIONS; do
                uuidDisk=$(ls -alh /dev/disk/by-uuid/ | grep "$part" | awk '{ print $9 }')
                partitionSize=$(df -h | grep "$part" | awk 'NR==1 { print $2 }')
                partitionSizeFree=$(df -h | grep "$part" | awk 'NR==1 { print $4 }')
                if [[ "$partitionSize" == "" ]] && [[ "$partitionSizeFree" == "" ]]; then
                    echo "'-> $part uuid $uuidDisk "
                else
                    echo "'-> $part uuid $uuidDisk FreeDiskSpace=$partitionSizeFree/$partitionSize"
                fi

                #Only direct mounted if no cryptsetup. Hide lines that are available due to late automounted disks (autofs) and hide mounted encrypted folders with the same name as they will be checked later
                PARTMOUNTED=$(mount | grep "$part" | grep -v autofs | grep -v "/dev/mapper/" | awk '{ print $3 }')
                for mountPnt in $PARTMOUNTED
                do
                    if [[ "$mountPnt" = "/" ]] || [[ "$mountPnt" = "/.snapshots" ]]; then
                        mountPnt2="$mountPnt "
                    else
                        mountPnt2="$mountPnt"
                    fi
                    partMountInfoLoc=$(mount | grep "$part" | grep -v autofs | grep -v "/dev/mapper/" | grep "$mountPnt2" | awk '{ print $3 " (" $5 ")"}')
                    if [[ "$partMountInfoLoc" != "" ]]; then
                        echo "  '-> $partMountInfoLoc ${btrfsHealthStatusARR[$mountPnt]}"
                    fi
                done
                local cryptPartDetails
                local cryptPartMounted
                cryptPartID=$(sudo more /etc/crypttab | grep "$uuidDisk" | awk '{ print $1 }')
                cryptPartDetails=$(sudo more /etc/crypttab | grep "$uuidDisk" | awk '{ print "(key=" $3 " encryption=" $4 ")"}')
                cryptPartMounted=$(sudo dmsetup table | grep "$uuidDisk" | awk '{ print "encrypted by " $5 ", mounted with " $11 "," $12 }')
                #Btrfs scrub can show fixed errors, but it will only show the DM device name like dm-2, so add this info!
                cryptDeviceDM=$(ls -alh /dev/disk/by-id/ | grep "dm-name-$cryptPartID" | awk '{ print $11 }' | rev | cut -c -4 | rev)
                local cryptFlashDiskWithWorkqueuePerfWarningOK=""
                local cryptFlashDiskWithWorkqueuePerfWarningError=""
                if [[ "$(echo $cryptPartMounted | grep 'no_read_workqueue,no_write_workqueue' )" == "" ]] && [[ "$rotationRateLoc" == "Flash" ]]; then
                    cryptFlashDiskWithWorkqueuePerfWarningError="Performance Warning - Disable Read & Write Workqueue in Crypttab for NVME/SSD"
                elif [[ "$(echo $cryptPartMounted | grep 'no_read_workqueue,no_write_workqueue' )" != "" ]] && [[ "$rotationRateLoc" == "Flash" ]]; then
                    cryptFlashDiskWithWorkqueuePerfWarningOK="Direct IO-Mode"
                elif [[ "$(echo $cryptPartMounted | grep 'no_read_workqueue,no_write_workqueue' )" == "" ]] && [[ "$rotationRateLoc" != "Flash" ]]; then
                    cryptFlashDiskWithWorkqueuePerfWarningOK="Buffered IO-Mode"
                else
                    cryptFlashDiskWithWorkqueuePerfWarningError="Read & Write Workqueue activ for slow devices like HDD's"
                fi
                if [[ "$cryptPartID" != "" ]]; then
                    partMountLoc=$(mount | grep "/dev/mapper/$cryptPartID" | awk '{ print $3 }')
                    echo "  '-> $(tput setaf 5)$cryptPartID$(tput sgr0) $cryptDeviceDM $cryptPartDetails $(tput setaf 1)$cryptFlashDiskWithWorkqueuePerfWarningError$(tput sgr0)$(tput setaf 2)$cryptFlashDiskWithWorkqueuePerfWarningOK$(tput sgr0)"
                    if [[ "$partMountLoc" != "" ]]; then
                        for mountPnt in $partMountLoc
                        do
                            #if [[ "$mountPnt" = "/XXXXXXXXXX" ]]; then
                            #    mountPnt2="$mountPnt "
                            #else
                                mountPnt2="$mountPnt"
                            #fi
                            partMountInfoLoc=$(mount | grep "/dev/mapper/$cryptPartID" | grep "$mountPnt" | awk '{ print $3 " (" $5 ")"}')
                            #if [[ "$partMountInfoLoc" != "" ]]; then
                                #local mountPntCrypt=$(echo $partMountInfoLoc | awk '{ print $1 }')
                                echo "    '-> $partMountInfoLoc ${btrfsHealthStatusARR[$mountPnt]} FreeDiskSpace=$(df -h | grep "/dev/mapper/$cryptPartID" | awk 'NR==1 { print $4 }')/$(df -h | grep "/dev/mapper/$cryptPartID" | awk 'NR==1 { print $2 }')"
                            #fi

                            #Btrfs additional Volume/Raid details
                            cryptMountPoint=$(more /etc/fstab | grep "$mountPnt" | grep -v "#" | grep -v "bind" | awk '{ print $2 }')
                            #If the device is mounted by the UUID, then the mountpoint will not show the mount info like /dev/mapper/cryptdevice
                            cryptDeviceUUID=$(ls -l /dev/disk/by-uuid/ | grep "$cryptDeviceDM" | awk '{ print $ 9 }')
                            #echo "cryptDeviceUUID $cryptDeviceUUID"
                            if [[ "$cryptMountPoint" == "" ]] && [[ "$cryptDeviceUUID" != "" ]]; then
                                cryptMountPoint=$(more /etc/fstab | grep "$cryptDeviceUUID" | grep -v "#" | grep -v "bind" | awk '{ print $2 }')
                            fi
                            #echo "cryptMountPoint $cryptMountPoint"
                            if [[ "$cryptMountPoint" != "" ]]; then
                                cryptBtrfsMount=$(sudo btrfs filesystem show "$cryptMountPoint" | grep "devid" | awk '{ print $1 "_" $2 "," $8 ":(total=" $4 ",used=" $6 ")" }' )
                                for cryptBtrfsMountPnt in ${cryptBtrfsMount[*]}; do
                                    #if [[ "$cryptBtrfsMount" != "" ]]; then
                                        echo "        |___+ Btrfs devices $cryptBtrfsMountPnt"
                                    #fi
                                done

                                #Show btrfs device count and available storage
                                cryptBtrfsMountInfo=$(sudo btrfs filesystem show "$cryptMountPoint" | grep "Total devices" | awk '{ print $2 " " $3 ":(allocated=" $7 ")" }' )
                                if [[ "$cryptBtrfsMountInfo" != "" ]]; then
                                    echo "        |___o $(tput setaf 4)Btrfs volume $cryptBtrfsMountInfo$(tput sgr0)"
                                fi

                                #RAID Levels
                                btrfsVolumeRaidLevels=$(sudo btrfs filesystem df "$cryptMountPoint" | grep -v "GlobalReserve" | awk '{ print $1 $2 "(" $3 $4 ")" }')
                                for btrfsDataType in ${btrfsVolumeRaidLevels[*]}; do
                                    echo "        |____ $btrfsDataType"
                                done
                            fi
                        done
                    else
                        #Btrfs additional Volume/Raid details
                        cryptMountPoint=$(more /etc/fstab | grep "$cryptPartID" | grep -v "#" | awk '{ print $2 }')
                        cryptIndirectBtrfsMount=$(sudo btrfs filesystem show "$cryptMountPoint" | grep "$cryptPartID" | awk '{ print $1 "_" $2 "," $8 ":(total=" $4 ",used=" $6 ")" }')
                        #Not Mounted Devices?
                        cryptNotMountedInfo=$(more /etc/fstab | grep "$cryptPartID" | grep -v "#" | awk '{ print $2 " (" $3 ")"}')
                        if [[ "$cryptIndirectBtrfsMount" != "" ]]; then
                            echo "    '-> $cryptNotMountedInfo mounted as internal btrfs volume $cryptIndirectBtrfsMount"
                        else
                            echo "    '-> $cryptNotMountedInfo not mounted (only on demand)"
                        fi
                    fi
                fi
            done
        #else
        #    echo ""
        #    echo "$blkdisk is not a disk!"
        fi
    done

    echo ""
    btrfs_mount_options_deprecated
    echo ""
}
