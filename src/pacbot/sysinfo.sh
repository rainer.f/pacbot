#!/bin/bash
# pacbot - Automated Pacman and System Maintenance, author Rainer Finke, license GPLv3
# Dependency: archey3

####################################
######### Global Variables #########
####################################

export diskSchedulerCheckText=""

####################################
############ Functions #############
####################################

### CPU
sensors_show() {
    # Show CPU temp
    sensors
}

governor_show() {
    # Show CPU governor per CPU core
    declare -a schedulerArr
    declare -a schedulerAllArr
    local schedulerInfo
    local colorLoc
    colorLoc=2
    #cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    #schedulerAllArr=$(cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor), hide error output in vm (2> /dev/null)
    mapfile -t schedulerAllArr < <(cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_driver 2> /dev/null)
    cpu_governor_get_details
    if ( $cpuAMD ); then
        if [[ "$schedulerInfo" == " acpi-cpufreq" ]]; then
            colorLoc=3
        fi
    fi
    mapfile -t schedulerAllArr < <(cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor 2> /dev/null)
    cpu_governor_get_details
    echo "CPU: Power Management & Scheduler$(tput setaf $colorLoc)${schedulerInfo} $(tput sgr0)"
}

cpu_governor_get_details() {
    local count=0
    for scheduler in "${schedulerAllArr[@]}"; do
        if [[ "$scheduler" != "$schedulerArr" ]]; then
            schedulerArr[count]="$scheduler"
            ((count++))
        fi
    done
    schedulerInfo="$schedulerInfo ${schedulerArr[*]}"
}

cpu_frequencies_current() {
    watch cat /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq
}

mglru_show() {
    local mglruLoc=$(cat /sys/kernel/mm/lru_gen/enabled 2> /dev/null)
    if [[ "$mglruLoc" != "" ]]; then
        local mglruMinMSLoc=$(cat /sys/kernel/mm/lru_gen/min_ttl_ms 2> /dev/null)
        echo "Linux: $(tput setaf 2)MGLRU$(tput sgr0) activated [$mglruLoc] with min. ttl ms [$mglruMinMSLoc]"
    else
        echo "Linux: $(tput setaf 3)MGLRU$(tput sgr0) not available - requires Linux 6.1 and compile build option LRU_GEN in Kconfig"
    fi
}

### RAM
ram_show() {
    # Memory Banks used
    sudo dmidecode -t memory | grep Size

    # Other Memory Info
    sudo dmidecode -t 17
    
    #Archlinux removed edac-utils from repository => use rasdaemon from AUR? But rasdaemon requires debugfs which is not available on linux-hardened!
    #Install ECC Ram Info Packages (edac-utils)
    #printf "\nECC support is available if edac-util doesn't show an error\n"
    #edac-util -v
    #printf "\nECC support is available if you see usually in the second line DRAM ECC enabled.\n"
    #sudo dmesg | grep -i edac
}

ram_use() {
    echo "RAM Usage"
    free -m
}

### HDD
# Show Disk Scheduler
disks_scheduler_show() {
    # Show Disk Health of all connected discs
    local deviceCount=0
    local -a DEVS
    DEVS=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
    for blkdisk in $DEVS
    do
        local diskSchedulerCheckTextLoc=""
        diskSchedulerCheckTextLoc=$(cat /sys/block/"$blkdisk"/queue/scheduler)
        if [[  "$?" == 1 ]]; then
            diskSchedulerCheckTextLoc="ERROR1"
        fi
        if [[  "$diskSchedulerCheckTextLoc" == "" ]]; then
            diskSchedulerCheckTextLoc="ERROR2"
        fi
        diskSchedulerCheckText2[$deviceCount]="Disk: $(tput setaf 2)$blkdisk$(tput sgr0) scheduler $diskSchedulerCheckTextLoc"
        echo "${diskSchedulerCheckText2[$deviceCount]}"
        ((deviceCount++))
    done
}

disk_scheduler_show() {
    # Show Disk Health of a connected discs

    local diskSchedulerCheckTextLoc=""
    diskSchedulerCheckTextLoc=$(cat /sys/block/"$blkdisk"/queue/scheduler | awk -F '[' '{ print $2}' | awk '{ print $1 }' | rev | cut -c 2- | rev)
    if [[  "$?" == 1 ]]; then
        diskSchedulerCheckTextLoc="ERROR1"
    fi
    if [[  "$diskSchedulerCheckTextLoc" == "" ]]; then
        diskSchedulerCheckTextLoc="ERROR2"
    fi
    diskSchedulerCheckText2[$deviceCount]="Scheduler $(tput setaf 2)${diskSchedulerCheckTextLoc}$(tput sgr0) in use on disk $blkdisk"
    echo "${diskSchedulerCheckText2[$deviceCount]}"
    ((deviceCount++))
}

usb_info() {
    echo "Show USB device tree (lsusb -t)"
    lsusb -t
}

pci_info() {
    if pacman -Q lspci &> /dev/null; then
        echo "Show PCI device tree (lspci -tv)"
        lspci -tv
    else
        echo "lspci not installed"
    fi
}

firmware_info() {
    fwupdmgr get-devices
}

system_info() {
    fastfetch_show
    mglru_show
    governor_show
    gpu_info
    wine_info
    #ram_use
    #disk_scheduler_show
}

fastfetch_show() {
    if pacman -Q fastfetch &> /dev/null; then
        fastfetch
    else
        echo "fastfetch is not installed"
    fi
}

device_info() {
    pci_info
    usb_info
}

gpu_info() {
    if [[ "$(uname -m)" != "aarch64" ]]; then
        #After a kernel upgrade it could happen that the module is not visible anymore?
        #BUG doesn't work as root
        gpukernel=$(sudo -u "$userName" lspci -vnn 2> /dev/null | grep VGA -A 10 | grep "driver" | awk '{ print $5 }' | awk 'NR==1')
    else
        gpukernel=$(modinfo v3d 2> /dev/null | grep "description:" | awk '{ print $2 " " $3 " " $4 }')
    fi
    #How to check for NIR?
    local opencldrivers=$(clinfo -l 2> /dev/null)
    declare -A gpuopencldriverArr
    local gpuopencldriver=$(clinfo 2> /dev/null | grep "Platform Name" | awk '{ print $3 }')
    local gpuopenclversion=$(clinfo 2> /dev/null | grep "Platform Version" | awk '{ print $4 }')
    for i in ${gpuopencldriver[*]}; do
        gpuopencldriverArr["$i"]="$i"
    done
    #BUG glxinfo works only when there is a graphical desktop
    local gpudriver=$(${override}glxinfo -B 2> /dev/null | grep "OpenGL core profile version string:" | awk '{print $9 " " $10 " " $11}')    
    if [[ "$gpudriver" == "" ]]; then
        gpudriver=$(pacman -Q "mesa")
        
        echo "${gpudriver} - ${gpuopencldriverArr[*]}"
    else
        local gpuvendor=$(${override}glxinfo -B 2> /dev/null | grep "Vendor:" | awk '{print $2'})
        local gpustring=$(${override}glxinfo -B 2> /dev/null | grep "OpenGL renderer string:" | awk -F 'string: ' '{ print $2 }' | awk '{ $1=""; print }' | cut -c 2-)
        #gpuram=$(${override}glxinfo -B 2> /dev/null | grep "Total available memory:" | awk '{print $4}')
        if [[ "$EUID" -eq 0 ]]; then
            #root
            local gpuramdedicated=$(dmesg | grep BAR | grep drm | awk '{ print $6 " " $7}')
            if [[ "$gpuramdedicated" == "" ]]; then
                gpuramdedicated=$(${override}glxinfo -B 2> /dev/null | grep "Video memory:" | awk '{print $3}')
            fi
        else
            local gpuramdedicated=$(${override}glxinfo -B 2> /dev/null | grep "Video memory:" | awk '{print $3}')
            gpuramdedicated="RAM=$gpuramdedicated, BAR=(requires: sudo dmesg | grep BAR)"
        fi
        local gpuopengl=$(${override}glxinfo -B 2> /dev/null | grep "OpenGL core profile version string:" | awk '{print $6}')
        local gpuopengles=$(${override}glxinfo -B 2> /dev/null | grep "OpenGL ES profile version string:" | awk '{print $7 " " $8}')
        #Do not show warnings and error output as of ELFCLASS32 like https://github.com/KhronosGroup/Vulkan-Loader/issues/262
        local gpuvkversion=$(vulkaninfo 2> /dev/null  | grep "Vulkan Instance Version:" | awk '{print $4}')
        local gpuvkdriver=$(vulkaninfo --summary 2> /dev/null | grep "deviceName" | awk -F '=' '{ print $2 }')

        #Video codec support
        local vainfoList=$(vainfo 2> /dev/null | grep VAProfile | awk '{ print $1 }' | cut -c 10- | cut -c -4 | sort )
        declare -A vainfoArr
        for va in $vainfoList; do
            if [[ "$va" != "None" ]]; then
                if [[ "$va" == "VP9P" ]]; then
                    vainfoArr["VP9"]="VP9"
                elif [[ "$va" == "AV1P" ]]; then
                    vainfoArr["AV1"]="AV1"
                else
                    vainfoArr["$va"]="$va"
                fi
            fi
        done
        #echo "Video Codec Support (vainfo): ${vainfoArr[*]}"

        local vdpauList=$(vdpauinfo 2> /dev/null | grep -wv "not supported" | grep -A 99 'Decoder capabilities:'  | grep -B 99 'Output surface:' | head -n -2 | awk '{ print $1 }' | cut -c -4 | sort )
        declare -A vdpauArr
        for vd in $vdpauList; do
            if [[ "$vd" != "Deco" ]] && [[ "$vd" != "name" ]] && [[ "$vd" != "----" ]]; then
                if [[ "$vd" == "AV1_" ]]; then
                    vdpauArr["AV1"]="AV1"
                else
                    vdpauArr["$vd"]="$vd"
                fi
            fi
        done
        checkArray emptyArray vdpauArr
        if ( $emptyArray ); then
            vdpauArr[0]="$(tput setaf 1)Add in /etc/environment for AMD [VDPAU_DRIVER=radeonsi] and for Intel [VDPAU_DRIVER=va_gl]$(tput sgr0)"
        fi
        #echo "Video Codec Support (vdpau): ${vdpauArr[*]}"

        echo "GPU: $(tput setaf 2)${gpudriver}$(tput sgr0)currently using $gpuvendor $gpustring ${gpuramdedicated}, kernel driver $(tput setaf 2)$gpukernel$(tput sgr0)"
        echo "$(tput setaf 2)Video Codecs Support$(tput sgr0)"
        echo " +-- VAinfo   : ${vainfoArr[*]}"
        echo " \`-- VDPauinfo: ${vdpauArr[*]}"
        echo "$(tput setaf 2)OpenCL$(tput sgr0)$(for i in $gpuopenclversion; do printf " [$i]"; done)"
        echo "$opencldrivers"
        echo "$(tput setaf 2)OpenGL$(tput sgr0) [$gpuopengl] [$gpuopengles]"
        echo "$(tput setaf 2)Vulkan$(tput sgr0) [$gpuvkversion]"
        echo "${gpuvkdriver[*]}"
        
        zink_info

    fi
}

zink_info() {
    #BUG cannot use variable
    #local zink="MESA_LOADER_DRIVER_OVERRIDE=zink"
    #local zinkgpuvendor=$($zink glxinfo -B 2> /dev/null | grep "Vendor:" | awk '{print $2}')
    local zinkgpu=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "OpenGL renderer string:" | awk '{print $4}')
    #cut -d' ' -f3- can do what awk can't, this allow a from $x..$y print range
    local zinkgpuvulkan=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "OpenGL renderer string:" | awk -F 'zink ' '{ print $2 }')
    local zinkgpuopengl=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "OpenGL version string:" | awk '{print $1}')
    local zinkgpuopenglmax=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "Max core profile version:" | awk '{print $5}')
    local zinkgpuopengles=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "OpenGL ES profile version string:" | awk '{print $7 " " $8}')
    local zinkversion=$(MESA_LOADER_DRIVER_OVERRIDE=zink glxinfo -B 2> /dev/null | grep "Version" | awk '{ print $2 }')
    if [[ "$zinkgpu" == "" ]]; then
        echo " --- OpenGL/OpenGL ES    -> Vulkan $(tput setaf $color)***ERROR***$(tput sgr0) Zink is not available for this GPU in this Mesa build $zinkgpu $zinkversion $zinkgpuvulkan"
    else
        echo " +-- ${zinkgpuopengl} ${zinkgpuopenglmax}/$zinkgpuopengles   -> Vulkan $(tput setaf 2)$zinkgpu $zinkversion$(tput sgr0) currently using $zinkgpuvulkan"
    fi
}

wine_info() {
    local wine=$(pacman -Q wine-staging 2> /dev/null)
    if [[ "$?" == 1 ]]; then
        wine=$(pacman -Q wine 2> /dev/null)
    fi
    local proton=$(pacman -Q proton 2> /dev/null)
    local wineInstall=""
    if [[ "$wine" != "" ]] || [[ "$proton" != "" ]]; then
        wineInstall="on $(tput setaf 2)$wine $proton$(tput sgr0)"
    else
        wineInstall="missing $(tput setaf 3)wine/proton(tput sgr0)"
    fi
    local dxvk=$(pacman -Q dxvk-bin 2> /dev/null)
    if pacman -Q dxvk-bin &> /dev/null; then
        echo " +-- Direct3D 8/9/10/11  -> Vulkan $(tput setaf 2)$(pacman -Q dxvk-bin)$(tput sgr0) $wineInstall"
    elif pacman -Q dxvk-async-git &> /dev/null; then
        echo " +-- Direct3D 8/9/10/11  -> Vulkan $(tput setaf 2)$(pacman -Q dxvk-async-git)$(tput sgr0) $wineInstall"
    else
        echo " --- Direct3D 8/9/10/11  -> Vulkan $(tput setaf 3)NOT INSTALLED DXVK$(tput sgr0) $wineInstall"
    fi
    if pacman -Q vkd3d-proton-bin &> /dev/null; then
        echo " \`-- Direct3D 12         -> Vulkan $(tput setaf 2)$(pacman -Q vkd3d-proton-bin 2> /dev/null)$(tput sgr0) $wineInstall"
    else
        if pacman -Q vkd3d &> /dev/null; then
            echo " \`-- Direct3D 12         -> Vulkan $(tput setaf 2)$(pacman -Q vkd3d 2> /dev/null)$(tput sgr0) $wineInstall"
        else
            echo " --- Direct3D 12         -> Vulkan $(tput setaf 3)NOT INSTALLED VKD3D-Proton (fast) / VKD3D (slow)$(tput sgr0) $wineInstall"
        fi
    fi
}

security_audit() {
    #https://wiki.archlinux.org/title/Security
    echo "-----------------Secure Systems------------------------"
    echo "There are only very few  free and open source down to the firmware software supporting hardware vendors:"
    echo "+ Server/Workstation/Desktop  -> Raptor Systems provides a almost fully open mainboard with a OpenPower 9 CPU"
    echo "+ Notebook/Mini-PC            -> Purism and Starlabs replaced UEFI with coreboot and disabled the Intel ME. Plus they do work on replacing at least some closed firmware blobs in their Notebook's and PC's"
    echo "+ Phone                       -> Purism provides an almost open phone with hardware kill switches, but there are still firmware blobs used (but not updated without user consent)"
    echo "-----------------Security Risks------------------------"
    echo "Almost all standard hardware on the market enforces you to use and trust fully closed software blobs."
    echo "- proprietary firmware & microcode cannot be audited, verfified and improved"
    echo "- they contain security bugs and potentially backdoors"
    echo "- it is artificially restricted to be redistributed, modified, reverse engineered, decompiled and disassembled"
    echo "- they are used for planned obsolescence"
    echo "-----------------UEFI/BIOS-----------------------------"
    echo "Proprietary software only, security fixes are either not provided or too late. Usually coreboot/linuxboot is not available as a replacement. At least protect the BIOS/UEFI with a admin password and check and update to the latest UEFI/BIOS firmware."
    echo "-----------------CPU-----------------------------------"
    echo "$(tput setaf $color)Active CPU flaw mitigations on your system:"
    grep bugs /proc/cpuinfo | sort -u
    lscpu | grep "Vulnerability"
    echo "-----------------Linux---------------------------------"
    echo "Linux hardened $(uname -r)"
    #Check lockdown mode
    echo "Lockdown Mode: $(cat /sys/kernel/security/lockdown)"
    echo "USBGuard Protection: $(systemctl status usbguard | grep Active | awk '{ print $3 }')"
    echo "Firewalld Protection: $(systemctl status firewalld | grep Active | awk '{ print $3 }')"
    echo "AppArmor Protection: $(systemctl status apparmor | grep Active | awk '{ print $3 }') $(cat /sys/kernel/security/lsm)"
    echo "- AppArmor Enforced profiles: $(sudo aa-status --enforced)"
    #Check is 32bit is disabled
    #???
    printf "$(tput sgr0)"
    echo "- OpenPower - Raptor offers a completly open system with IBM OpenPower CPU's"
    echo "- AMD enforces on it's CPU's a PSP since Family 16H+ systems (started in 2013). The PSP is an additional hidden ARM CPU with hardware (RAM) access and TPM+DRM that could potentially be used to tamper your system. Ryzen loads much more firmware than Epyc. All AMD systems require amd-ucode firmware to function."
    echo "- Intel enforces on it's CPU's with the Intel Management Engine (ME) an additional hidden CPU with hardware access that could potentially be used to tamper your system. Especially if you use a Pro CPU these things are advertised as a feature but they allow to remote access your machine without any notice to the user. Check if ME_cleaner could remove the ME firmware. Purism and System76 sell notebooks with the Intel ME disabled. All Intel systems require intel-ucode firmware to functioning. Intel suffers from severe security bugs in hyper threading."
    echo "- ARM requires almost always proprietary firmware and Linux support is often a mess, there is even no standardized way to boot a generic Linux kernel."
    echo "-----------------GPU----------------------------------"
    echo "- AMD GPU's require firmware blobs to initialize the GPU and for power management"
    echo "- Intel GPU's require firmware blobs to initialize the GPU (display controller, power management) since Skylake/Boxtron"
    echo "- Nvidia GPU's are 100% proprietary and they fight against the open driver nouveau. Get rid of Nvidia cards."
    echo "-----------------RAM DDR4------------------------------"
    echo "RAM (DDR4) requires firmware blobs to be initialized"
    echo "-----------------Cell Modems--------------------------"
    echo "- Cell models are own CPU's with a own OS. They send back regularly data to the cell towers incl. IMEI and hardware id plus leaking the location and potentally even when the mobile is turned off."
    echo "-----------------Other embedded firmwares-------------"
    echo "Harddisks, network and wireless cards, sound cards, keyboards, mouse, usb controller and devices, webcams, headsets, ... require firmware. Most of this proprietary driver firmware is provided in the linux-firmware package. The device internal firmware needs often an additional update. Fwupdate can at least update some devices automatically."
}

secureboot_disable() {
    echo "To install Arch Linux disable secure boot in UEFI/Bios, but boot in UEFI mode and not in legacy CSM"
}

secureboot_check() {
    echo "If secure boot is active, you will see 6 0 0 0 1 in the next line"
    od --address-radix=n --format=u1 /sys/firmware/efi/efivars/SecureBoot*
    echo "Check secure boot with [bootctl status]"
    bootctl status
    echo "Enabling secure boot for Arch Linux is an quite complex process. If you want to do it, use the wiki https://wiki.archlinux.org/index.php/Secure_Boots"
}
