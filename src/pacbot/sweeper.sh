#!/bin/bash
deleteCmd="gio trash"

### Privacy
#Deletion Commands: "rm -r -f" & "gio trash"
#Alternative (but requires 1 additional argument): kioclient move path_to_file_or_directory_to_be_removed trash:/
thumbails_clean_deleted_and_old_files() {
    if pacman -Q imagemagick &> /dev/null; then
        #Idea based on https://www.kubuntuforums.net/forum/general/documentation/how-to-s/12538-howto-cleaning-old-kde-thumbnails
        #EXPIRE=30
        # Path to thumbnails
        thumbmailsDir="${HOME}/.cache/thumbnails"

        thumbnailSubFolders=$(ls -1 "$thumbmailsDir")
        for subFolder in $thumbnailSubFolders; do
            thumbnailsPNG=$(find "$thumbmailsDir/$subFolder/" -name "*.png" -exec ls {} \;)
            for thumbnailFile in $thumbnailsPNG; do
                #  Parsing ImageMagick's output.
                originalFile=$(identify -verbose "$thumbnailFile" | grep -v "Thumbnail of file:" | grep "file:" | sed s/"Thumb::URI:"//g | sed s/"file:\/\/"//g | awk '{ print $1 }')
                #   %20=space   %C3%A4=ä    %C3%84=Ä
                #               %C3%B6=ö    %C3%96=Ö
                #               %C3%BC=ü    %C3%9C=Ü
                #BUG how to replace other special characters???
                if [ -f "$(echo "$originalFile" | sed 's/%20/ /g' | sed 's/%C3%BC/ü/g' | sed 's/%C3%B6/ö/g' | sed 's/%C3%A4/ä/g' | sed 's/%C3%84/Ä/g' | sed 's/%C3%96/Ö/g' | sed 's/%C3%9C/Ü/g')" ]; then
                    #Original file exist.
                    #Removing expired thumbnail.
                    #NOTE ALREADY DONE, see below!!!
                    #find "$thumbnailFile" -mtime +$EXPIRE -exec rm -f {} \;
                    continue
                else
                    #Original file does not exist.
                    #Removing obsolete thumbnail.
                    echo "[ Sweeper.sh ] DELETING obsolete thumbail $thumbnailFile"
                    $deleteCmd "$thumbnailFile"
                fi
            done
        done
    fi
}

#Delete Privacy violating User Feedback files that are created even if KDE User Feedback is disabled by default. These files count application start and time!
$deleteCmd ${HOME}/.config/kde.org/UserFeedback.org.kde.* &> /dev/null

#Thumbnails - Test on system with 250 GB of pictures. Before Cleanup 12,4GB, cleanup in 1 minute to 3GB. Delete thumbails order than 30 days
#find ${HOME}/.cache/thumbnails/ -mtime +30 -exec rm -f {} \; &> /dev/null
find ${HOME}/.cache/thumbnails/ -mtime +30 -exec $deleteCmd \; &> /dev/null
#Alternative, check if file exists and then delete it?
#echo "Start $(timedatectl | grep Local)"
thumbails_clean_deleted_and_old_files
#echo "End $(timedatectl | grep Local)"

### Cache & Temp
#Delete temporary file leftovers of KDE

#Delete debuginfofd leftovers
$deleteCmd ${HOME}/.cache/debuginfod_client/* &> /dev/null

#Delete temporary files from network locations (kioexec)
$deleteCmd ${HOME}/.cache/kioexec/* &> /dev/null

#Delete winetricks cached downloades
$deleteCmd ${HOME}/.cache/protontricks/* &> /dev/null
$deleteCmd ${HOME}/.cache/winetricks/* &> /dev/null

#Delete Wine Temp files, leftovers from .exe installations
$deleteCmd ${HOME}/.wine/drive_c/users/$(logname)/Temp/* &> /dev/null

#Delete pacbot user info files. As Update-OK is created by root, force deletion
$deleteCmd "${HOME}/Desktop/Update-OK" &> /dev/null
$deleteCmd "${HOME}/Desktop/~updating" &> /dev/null

### Crash
#KDE DrKonqi Crash Reports
$deleteCmd ${HOME}/.cache/drkonqi/crashes/* &> /dev/null

#Delete Firefox Crash Reports, one system collected 200GB crash reports without further user knownledge
$deleteCmd "${HOME}/.mozilla/firefox/Crash Reports/pending/" &> /dev/null

### Flatpak
#Delete temporary downloaded files that Librewolf doesn't cleanup right in time
$deleteCmd ${HOME}/.var/app/io.gitlab.librewolf-community/cache/tmp/* &> /dev/null
