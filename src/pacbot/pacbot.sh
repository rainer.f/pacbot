#!/bin/bash
# pacbot - Automated Pacman and System Maintenance, author Rainer Finke, license GPLv3
# Note: Do not run this script with sh as it shows an error with mapfile, use "bash pacbot.sh"
# The main idea is to run pacbot as an automated service, the interface.sh is provided mainly to understand maintenance and to fix errors

#Remove broken symlinks requires read/write access on a very high root level! Remove from service operation??? /etc,/home,/opt,/srv,/usr
#ServiceReadPath="/opt/pacbot/,/usr/bin/,/var/log/"
#ServiceReadWrithPath="/etc/pacman.d/,/var/lib/pacman/,/var/cache/pacman/pkg"

####################################
######### LOCAL ADJUSTMENTS ########
####################################

#For testing
#export pkgPath="."
export pkgPath="/opt/pacbot"

####################################
######## Global Variables ##########
####################################

declare -a criticalInstalledPackage
declare -a restartService
declare -a updateVersions
declare -a updatePackages
declare -a restartSystem
declare -a updateFlatpakArr
declare -a updatePackagesSize
declare -A lostFilesArr
declare -A restartSystemArr

export scriptName="pacbot"
export scriptDescription="Automated Pacman & System Maintenance Tool"
export runParameter=""
runAsService=false
export pacmanDownloadText=""
export paccacheText=""
export pacmanUpgradeText=""
export aurUpgradeText=""
pacmanLockText=""
pacmanLock=false
export updateSmartFirmwareText=""
export updateFlatpakText=""
export orphanPackages=""
export removedOrAurPackages=""
export brokenSymlinksText=""
export fixFolderPermissionsText=""
export fixFolderPermission=""
export lostFilesStatusText=""
export verifyPackageText=""
export lostFiles=""
export mirrorListText=""
export COLUMNS
export updateCount=0
listUpgradedPackages=""
systemdFailedServicesBefore=""
systemdFailedServicesAfter=""
updatesInstalled=false
upgradeAlert=""
upgradeLoop=0
upgradeTooLessDiskSpace=false
# whoami doesn't work during sudo use
export userName=$(logname)
#userNameSession=$(loginctl show-session $XDG_SESSION_ID |  awk -F "=" 'FNR == 3 {print $2}')
emptyArray=false

####################################
########## Init Functions ##########
####################################

check_runParameter_pacbot() {
    if [[ "$runParameter" == "-h" ]] || [[ "$runParameter" == "--help" ]]; then
        printText "- $scriptDescription" 1 0 "v"
        printText "Options:" 1 0 false
        printText "-d --daily          Daily  Automatic Run" 1 0 "v"
        printText "-w --weekly         Weekly Automatic Run" 1 0 "v"
        printText "-m --manual         Manually Run Tasks" 1 1 "v"
        printText "If no option is provided, manual mode will start" 1 2 "v"
        exit
    elif [[ "$runParameter" == "--daily" ]]; then
        runParameter="-d"
    elif [[ "$runParameter" == "--weekly" ]]; then
        runParameter="-w"
    elif [[ "$runParameter" == "" ]] || [[ "$runParameter" == "--local" ]]; then
        runParameter="-m"
    elif [[ "$runParameter" == "--ds" ]] || [[ "$runParameter" == "--daily-service" ]]; then
        runParameter="-d"
        runAsService=true
    elif [[ "$runParameter" == "--ws" ]] || [[ "$runParameter" == "--weekly-service" ]]; then
        runParameter="-w"
        runAsService=true
    fi

    #$TERM is not explicitly set (dumb) when running as a systemd service
    if [[ "$TERM" == "dumb" ]]; then
        export TERM="dumb"
    fi

    #logname shows emty user name when running as a systemd service
    #local loggedInUsers=$(w | grep "/usr/bin/" | awk '{ print $1 }' | awk 'NR==1')
    local loggedInUsers=$(who | awk '{ print $1 }' | awk 'NR==1')
    local allUsers=$(cat /etc/passwd | grep "/home/" | awk 'NR==1' | cut -d ":" -f1)
    if [[ "$userName" == "" ]]; then
        #BUG what if there are multiple users?
        if [[ "$loggedInUsers" == "" ]]; then
            #BUG what if there are multiple users?
            if [[ "$allUsers" != "" ]]; then
                userName="$allUsers"
            else
                #ERROR
                userName="nobody"
            fi
        else
            userName="$loggedInUsers"
        fi
    fi
}

source_dependencies_pacbot() {
    # Get global user settings and dependencies
    source "$pkgPath/settings.sh"
    source "$pkgPath/helpers.sh"
    source "$pkgPath/systemchecks.sh"
    source "$pkgPath/interface.sh"
    source "$pkgPath/usbstickbot.sh" "--source-only"
    source "$pkgPath/archinstallbot.sh" "--source-only"
    source "$pkgPath/sysinfo.sh"
}

####################################
############## Updater #############
####################################

update_time() {
    # Update system time before refresh pacman-keys and before pacman
    systemctl restart systemd-timesyncd.service
}

update_mirrorlist() {
    # Update mirrorlist to avoid using outdated and slow mirrors
    local text0="MirrorListUpdate=OK"
    local text1="MirrorListUpdate=Error"
    if pacman -Q reflector &> /dev/null; then
        #Same reflector command as in archinstallbot
        reflector --verbose --latest 5 --number 5 --age 3 --completion-percent 100 --sort rate --protocol https --save /etc/pacman.d/mirrorlist
        checkCommandOutput mirrorListText $? "$text0" "$text1"
    else
        if ! pacman -Q reflector &> /dev/null && [[ "$machineArch" == "aarch64" ]]; then
            printText "Reflector not avaialble on aarch64" 1 1 "$outputType"
        else
            printText "Please install reflector" 1 1 "$outputType"
        fi
    fi
}

download_updates() {
    # Download the newest mirrorlists and packages
    local refreshMirrors=$1
    local text0="RefreshMirrors=$refreshMirrors DownloadUpdates=OK"
    local text1="RefreshMirrors=$refreshMirrors DownloadUpdates=Error"
    remove_pacmanlock
    if ( ! $pacmanLock ); then
        if ( $refreshMirrors ); then
            pacman -Syyuw --noconfirm
        else
            pacman -Syuw --noconfirm
        fi
        #$? contains the last stderr output, 0=success, 1=error
        checkCommandOutput pacmanDownloadText $? "$text0" "$text1"
    fi
}

diskspace_cleanup() {
    clean_paccache
    btrfs_snapshot_cleanup
    #Sometimes there are too many empty chungs, so balance will get rid of them before pacman takes the wrong free space disk into account.
    #BUG This will fix some of the cases where the system cannot boot anymore due to partially upgraded packages or partially written packages on a full disk!
    btrfs_balance_root
}

btrfs_snapshot_cleanup() {
    #The snapper-cleanup.timer is activated only 1 time per day and this might leave many too many old snapshots on disk.
    #Pacman can and will fail to calculate correctly the available disk space on btrfs with snapshots, so it is important to truncate the old snapshots before upgrading the system.
    #BUG This will fix some of the cases where the system cannot boot anymore due to partially upgraded packages or partially written packages on a full disk!
    local snapperCleanupActiv="$(systemctl list-timers | grep snapper-cleanup.timer)"
    if [[ "$snapperCleanupActiv" != "" ]]; then
        echo "[  INFO  ] Run snapper-cleanup.service to avoid partially upgraded system"
        systemctl restart snapper-cleanup.service
    fi
}

clean_paccache() {
    # Remove all cached versions of uninstalled packages in /var/cache/pacman/pkg/
    # Is paccache.timer offering the same functionality?
    printf "\\n"

    paccache -ruvk0
    printf "\\n"
    local paccacheCleanDeinstalled=$?
    # Delete packages older than 2 versions of applications in /var/cache/pacman/pkg/
    paccache -rvk2
    printf "\\n"
    local paccacheRemoveOldVersions=$?

    flatpak_cache_check

    aur_clean
    printf "\\n"

    if [ "$paccacheCleanDeinstalled" == 0 ] && [ "$paccacheRemoveOldVersions" == 0 ] && [ "$paccacheCleanDeinstalledAUR" == 0 ] && [ "$paccacheRemoveOldVersionsAUR" == 0 ]; then
        paccacheText="CleanPaccache=Yes"
    else
        paccacheText="CleanPaccache=0"
    fi
}

flatpak_cache_check() {
    #Clean Flatpak cache
    if pacman -Q "flatpak" &> /dev/null; then
        flatpak_cache_clean
    fi
}

upgrade_show() {
    local syncRepo=$1
    local updatesText=""
    remove_pacmanlock
    if ( ! $pacmanLock ); then
        # Aur helper are slower than pacman only, so skip it if nor required
        if pacman -Q "$aurHelper" &> /dev/null && [[ "$runParameter" == "-m" ]] && ( $syncRepo ); then
            #This doesn't work in service mode /as of sudo
            #Do not hide output with &> /dev/null
            #NOTE As pacbot runs as root, we need to switch back to the normal user with sudo -u user!
            sudo -u "$userName" "$aurHelper" -Sy
            sudo -u "$userName" "$aurHelper" -Qu
            #updateVersions=$(sudo -u "$userName" "$aurHelper" -Qu)
            mapfile -t updateVersions < <(sudo -u "$userName" "$aurHelper" -Qu)
            mapfile -t updatePackages < <(sudo -u "$userName" "$aurHelper" -Qqu)
        else
            #Do not hide output with &> /dev/null
            if ( $syncRepo ); then
                pacman -Sy
            fi
            pacman -Qu
            #updateVersions=$(pacman -Qu)
            mapfile -t updateVersions < <(pacman -Qu)
            mapfile -t updatePackages < <(pacman -Qqu)
        fi
    else
        unset updateVersions
        unset updatePackages
    fi
    #checkArray emptyArray updateVersions
    #if ( $emptyArray ); then
    if ( $pacmanLock ); then
        updatesText="Cannot check for updates, another pacman instance is active"
    else
        # Show Number of Updates echo "the array contains ${#my_array[@]} elements", array starts at 0 so add 1
        updateCount="${#updateVersions[@]}"
        updatesText="$updateCount Updates available"
    fi
    printText "$updatesText" 0 1 "$outputType"
}

upgrade_prepare_keyring() {
    #Run this only after upgrade_show
    for update in ${updatePackages[*]}; do
        if [[ "$update" == "archlinux-keyring" ]] || [[ "$update" == "parabola-keyring" ]]; then
            #Not considered as partial upgrade, see https://wiki.archlinux.org/title/Pacman/Package_signing#Upgrade_system_regularly
            echo "[  info  ] Update keyring before updating system to avoid potential unkown keys!"
            keyring_reinstall
        fi
    done
}

upgrade_system() {
    # Autoupgrade System, but disallow upgrade for PRODUCTION SERVERs or user mangaged systems
    local acceptReplacements=$1
    local pacmanUpgradeCommand="pacman -Syuq --noconfirm --needed"
    remove_pacmanlock
    if ( ! $pacmanLock ); then
        fetch_warnings
        systemdFailedServicesBefore=$(systemctl --failed --plain)
        upgrade_show false
        upgrade_prepare_keyring
        if [[ "$upgradeAlert" == "0" ]] || [[ "$upgradeAlert" == "ignore" ]]; then
            #notify_user "Upgrading system now ..." "normal"
            if [[ "$runParameter" == "-m" ]]; then
                pacman_lowdiskspace
                if ( $acceptReplacements ); then
                    yes | $pacmanUpgradeCommand
                else
                    $pacmanUpgradeCommand
                fi
                upgrade_system_status_diskspacecheck "$?"
            else
                if ( ! $prodServer ) && ( ! $userManaged ); then
                    pacman_lowdiskspace
                    if ( $acceptReplacements ); then
                        yes | $pacmanUpgradeCommand --noprogressbar
                    else
                        $pacmanUpgradeCommand --noprogressbar
                    fi
                    upgrade_system_status_diskspacecheck "$?"
                #else
                #    echo "Automatic Upgrade disabled - Production Server or User Managed"
                fi
            fi
        else
            upgrade_system_status "1"
        fi
    fi
}

upgrade_system_status_diskspacecheck() {
    local status="$1"
    if [ "$status" == 1 ]; then
        if [[ "$upgradeLoop" == 1 ]]; then
            #Failed upgrade reasons
            #1. Unknown key => probably solved by upgrade_prepare_keyring
            #keyring_reinstall
            #3. Low disk space
            pacman_lowdiskspace
            upgrade_system
        elif [[ "$upgradeLoop" == 2 ]]; then
            #2. Missing pacman database file /var/lib/pacman/local/PACKAGENAME/desc
            reinstall_broken_packages_missing_file ""
            upgrade_system
        elif [[ "$upgradeLoop" == 3 ]]; then
            pacman_lowdiskspace
            if ( $upgradeTooLessDiskSpace ); then
                echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Pacman couldn't upgrade your system after trying it $upgradeLoop times. Too less disk space Error!"
            else
                echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Pacman couldn't upgrade your system after trying it $upgradeLoop times. Unkown Error!"
            fi
            #return
            #exit
            upgrade_system_status "$status"
        else
            upgrade_system_status "$status"
        fi
    else
        upgrade_system_status "$status"
    fi
}

upgrade_system_status() {
    local status="$1"
    #NOTE Make sure that after a upgrade, that all data is written do disk especially on btrfs before restart services that might potentially crash the system
    sync

    if [ "$status" == 0 ]; then
        #notify_user "... System Upgrade Finished" "normal"
        # Reload daemons, is this done NOW already automatically by pacman??? systemd-daemon-reload.hook
        systemctl daemon-reload
        restart_critical_services
    elif [ "$status" == 1 ]; then
        upgrade_error_fixlinux
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) UNKNOWN Pacman Error $status. Please check what happened. Fatal ERROR !!!"
    fi
    systemdFailedServicesAfter=$(systemctl --failed --plain)
    local text0="AutoUpgrade=Completed"
    local text1="[AutoUpgrade=Error]"
    if [[ "$systemdFailedServicesBefore" != "$systemdFailedServicesAfter" ]]; then
        text0="systemd services FAILED"
    fi
    text0="[$text0] [DaemonReload]"
    checkCommandOutput pacmanUpgradeText "$status" "$text0" "$text1"

    #Try to fix failed services by restarting them
    restart_failed_services
}

restart_critical_services() {
    # Restart CRITICAL services after upgrade
    service_requires_restart
    service_restart

    #Do it only after upgrading all packages including AUR, see in interface.sh
    #restart_upgraded_running_applications

    restart_system_recommended
}

service_restart() {
    checkArray emptyArray restartService
    if ( ! $emptyArray ); then
        #Remove "" as it will recognize multiple services only as a text
        systemctl restart ${restartService[*]}
        printText "Services restarted --> ${restartService[*]}" 0 1 "$outputType"
    fi
}

restart_failed_services() {
    #Restart failed services
    systemctl reset-failed
}

restart_upgraded_running_applications() {
    restart_upgraded_services
    service_restart

    #Run this only when normal user is active
    restart_upgraded_applications
    #restart_applications_recommended
}

restart_applications_recommended() {
    # Restart system recommended after upgrade
    checkArray emptyArray restartApplication
    if ( ! $emptyArray ); then
        #Remove "" as it will recognize multiple services only as a text
        printText "Applications using upgraded libraries should be restarted manually by the user --> ${restartApplication[*]}" 0 1 "v"
    fi
}

restart_system_recommended() {
    # Restart system recommended after upgrade
    checkArray emptyArray restartSystem
    if ( ! $emptyArray ); then
        for i in ${restartSystem[*]}; do
            restartSystemArr["$i"]="$i"
        done
        #Remove "" as it will recognize multiple services only as a text
        printText "Restart the system is recommended after upgrading --> ${restartSystemArr[*]}" 0 1 "$outputType"
    fi
}

service_requires_restart() {
    # Check for packages that are updated and require critical service restart
    getInstalledPackages criticalInstalledPackage CRITICALPACKAGES
    local count=0
    local countRestart=0
    unset restartService
    for package in ${criticalInstalledPackage[*]}; do
        for update in ${updatePackages[*]}; do
            if [[ "$package" == "$update" ]]; then
                for service in ${runningCriticalServices[*]}; do
                    if [[ "$package" == "openssh" ]] && [[ "$service" == "sshd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "openssl" ]] && [[ "$service" == "sshd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "openssl" ]] && [[ "$service" == "httpd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "openssl" ]] && [[ "$service" == "dovecot.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "apache" ]] && [[ "$service" == "httpd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "php" ]] && [[ "$service" == "php-fpm.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "mariadb" ]] && [[ "$service" == "mariadb.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "nfs-utils" ]] && [[ "$service" == "nfs-server.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "glusterfs" ]] && [[ "$service" == "glusterd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "dovecot" ]] && [[ "$service" == "dovecot.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "postfix" ]] && [[ "$service" == "postfix.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    elif [[ "$package" == "fail2ban" ]] && [[ "$service" == "fail2ban.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    # Restarting of libvirtd shouldn't affect running VM's!
                    elif [[ "$package" == "libvirt" ]] && [[ "$service" == "libvirtd.service" ]]; then
                        restartService[$count]="$service"
                        ((count++))
                    #This is to collect packages where the system should be restarted. Only the server linux-hardened requires a restart as it is critical.
                    elif [[ "$package" == "linux" ]] || [[ "$package" == "linux-hardened" ]] || [[ "$package" == "linux-lts" ]] || [[ "$package" == "linux-aarch64" ]] || [[ "$package" == "linux-raspberrypi4" ]] || [[ "$package" == "linux-odroid-c2" ]] || [[ "$package" == "linux-aarch64-chromebook" ]] || [[ "$package" == "glibc" ]] || [[ "$package" == "systemd" ]]; then
                        restartSystem[$countRestart]="$package"
                        ((countRestart++))
                    elif [[ "$package" == "mesa" ]]; then
                        #Mesa updates shouldn't matter as on a prodServer as there should be no graphical user interface
                        if ( ! $prodServer ); then
                            restartSystem[$countRestart]="$package"
                            ((countRestart++))
                        fi
                    fi
                done
            fi
        done
    done
}

restart_upgraded_services() {
    #TEST: Is this save on a production system??? As long as the user is active, it should be ok.
    if [[ "$runParameter" == "-m" ]]; then
        printText "Trying to restart running services that are using upgraded libraries..." 0 1 "v"
        #Extract services to restart?
        #lsof +c 0 | grep 'DEL.*lib' | awk '1 { print $1 ": " $NF }' | sort -u | awk '{print $1}' | rev | cut -c 2- | rev
        declare -A upgradedServiceArr
        #declare -A activeServiceArr
        # BUG 2020-11 This got now extremly slow and is sometimes not finishing
        # Use tracer from AUR??? tracer -ae
        local listUpgradedServices=$(tracer --services-only | awk 'NR>2' | awk '{ print $4 }')
        #local listUpgradedServices=$(lsof +c 0 2> /dev/null | grep 'DEL.*lib' | awk '1 { print $1 ": " $NF }' | sort -u | awk '{print $1}' | rev | cut -c 2- | rev)
        for i in $listUpgradedServices; do
            upgradedServiceArr["$i"]="$i"
        done
        #Get services
        #All services - systemctl | grep service | grep -e running -e exited -e active |awk '{print $1 " " $4}'
        #local listActiveServices=$(systemctl | grep service | grep -e running -e exited -e active |awk '{print $1}')
        #for i in $listActiveServices; do
        #    activeServiceArr["$i"]="$i"
        #done

        #Try to restart systemd pid1 itself. Is this always safe? There was one BUG report for systemd 231 that it crashed the system.
        systemctl daemon-reexec

        #local count=0
        #unset restartService
        #checkArray emptyArray activeServiceArr
        #if ( ! $emptyArray ); then
            checkArray emptyArray upgradedServiceArr
            if ( ! $emptyArray ); then
                #for i in ${activeServiceArr[*]}; do
                    #activeServicePIDName=$(systemctl status "$i" | grep "PID" | awk '{print $4}' | cut -c 2- | rev | cut -c 2- | rev)
                    for j in ${upgradedServiceArr[*]}; do
                        #if [[ "$j" == "$activeServicePIDName" ]]; then
                            #if [[ "$activeServicePIDName" == "systemd-logind" ]] || [[ "$activeServicePIDName" == "sddm" ]] || [[ "$activeServicePIDName" == "dbus-broker-lau" ]] || [[ "$activeServicePIDName" == "cockpit-tls" ]] || [[ "$activeServicePIDName" == "cockpit.service" ]]; then
                            if [[ "$j" == "systemd-logind" ]] || [[ "$j" == "sddm" ]] || [[ "$j" == "dbus-broker" ]] || [[ "$j" == "cockpit-tls" ]] || [[ "$j" == "cockpit" ]] || [[ "$(echo '$j' 2> /dev/null | cut -c -24)" == "cockpit-wsinstance-https" ]]; then
                                printText "Service NOT restarted (to avoid logout of a desktop/cockpit session) --> $j" 0 0 "$outputType"
                            elif [[ "$j" == "pacbot-daily" ]] || [[ "$j" == "pacbot-weekly" ]]; then
                                printText "[ skip ] pacbot should not restart itself --> $j" 0 0 "i"
                            else
                                #BUG How to restart --user system services like pipewire???
                                #restartService[$count]="$i"
                                restartService[$count]="$j"
                                ((count++))
                            fi
                        #fi
                    done
                #done
            fi
        #fi
    fi
}

restart_upgraded_applications() {
    printText "Searching for upgraded applications that should be restarted..." 1 1 "v"
    #Extract services to restart?
    #listUpgradedPackages=$(lsof +c 0 2> /dev/null | grep 'DEL.*lib' | awk '1 { print $1 " " $NF }' | sort -u)
    listUpgradedPackages=$(tracer -ae | grep -A 9999 'manually:' | awk ' NR>1' | grep -B 9999 'session:' | head -n -2)
    #TESTING
    #listUpgradedPackages="telegram-desktop"
    #BUG unset will destroy the assoziative array!
    #unset updatedApplicationsArr
    declare -A updatedApplicationsArr
    #lsof +c 0 | grep 'DEL.*lib' | awk '1 { print $1 ": " $NF }' | sort -u | awk '{print $1}' | rev | cut -c 2- | rev
    #local listUpgradedServices=$(lsof +c 0 | grep 'DEL.*lib' | awk '1 { print $1 ": " $NF }' | sort -u | awk '{print $1}' | rev | cut -c 2- | rev)
    #local listUpgradedServices=$(lsof +c 0 | grep 'DEL.*lib' | awk '1 { print $1 " " $NF }' | sort -u | awk '{ print $1 }')
    #local listUpgradedServices=$(echo "$listUpgradedPackages" | awk '{ print $1 }')
    #for i in $listUpgradedServices; do
    for i in $listUpgradedPackages; do
        updatedApplicationsArr["$i"]="$i"
    done


    #local uid=$(loginctl show-session $XDG_SESSION_ID |  awk -F "=" 'FNR == 2 {print $2}')
    local count=0
    unset restartApplication
    checkArray emptyArray updatedApplicationsArr
    if ( ! $emptyArray ); then
        for i in ${updatedApplicationsArr[*]}; do
            #Check for Wayland?
            #sesssionType=$(loginctl show-session $(loginctl | grep $userName | awk '{print $1}') -p Type | cut -c 6-)
            #if [[ "$sesssionType" == "wayland" ]]; then
                #get PID of running process and kill it, both work only with the short name "telegram-deskto" and not the full application name "telegram-desktop"
                #applicationPID=$(pgrep -a $i | awk '{ print $1 }')
                applicationPID=$(pidof "$i")
                applicationName="$i"
                #How to get full application name so than it can be restarted
                #The second awk is required to remove from telegram-desktop -- the characters -- at the end
                #applicationName=$(pgrep -a $i | awk -F 'bin/' '{ print $2 }' | awk '{ print $1 }')
                #Some applications can be killed by root and will restart automatically on demand for the user in the background like xdg-desktop-portal xdg-desktop-portal-kde. Doesn't work for pipewire!
                if [[ "$i" == "xdg-desktop-portal" ]] || [[ "$i" == "xdg-desktop-portal-kde" ]] || [[ "$i" == "xdg-desktop-portal-gtk" ]] || [[ "$i" == "xdg-document-portal" ]] || [[ "$i" == "xdg-permission-store" ]] || [[ "$i" == "krunner" ]]; then
                    kill $applicationPID
                    echo "... restart application on demand by the system $applicationName"
                #Restarting applications is safe if there is no data loss, OK for Telegram, Firefox, Plasmashell
                elif [[ "$i" == "telegram-desktop" ]] || [[ "$i" == "neochat" ]]  || [[ "$i" == "firefox" ]] || [[ "$i" == "plasmashell" ]] || [[ "$i" == "nextcloud" ]] || [[ "$i" == "maliit-keyboard" ]]; then
                    if [[ "$runParameter" == "-m" ]] && [[ "$EUID" -ne 0 ]]; then
                        #Do this only in Desktop mode
                        case "$(tty)" in
                        "/dev/pts"*)
                            #Some applicaitons can be restartet for the user without user intervention
                            if [[ "$i" == "maliit-keyboard" ]]; then
                                kill $applicationPID
                                if [[ "$EUID" -ne 0 ]]; then
                                    "$applicationName" &> /dev/null &
                                fi
                                echo "... restarted application $applicationName"
                            else
                                read -r -p "Restart $applicationName $applicationPID ? [y/N]"
                                if [[ "$REPLY" =~ [yY] ]]; then
                                    #Question: Limit this to applications of the userName? ps -u "$userName"

                                    #pkill requires the real process name which is in case of "telegram-desktop" -> "telegram-deskto", so it can't be used here
                                    #pkill -x $i
                                    kill $applicationPID
                                    #kill -19 $applicationPID # It stops! nice, we are reaching just what we wanted.
                                    #sleep 2
                                    #kill -18 $applicationPID # Ok... it continues to death... that isn't funny though.

                                    #XDG_RUNTIME_DIR is required to start the application on Wayland.
                                    #BUG What else is required to run it on Wayland for other user? Maybe XDG_SESSION_TYPE=wayland => not enough
                                    #XDG_RUNTIME_DIR=/run/user/$uid sudo -u "$userName" "$applicationName" &
                                    if [[ "$EUID" -ne 0 ]]; then
                                        "$applicationName" &> /dev/null &
                                    fi
                                    echo "... restarted application $applicationName"
                                else
                                    restartApplication[$count]="$i"
                                    ((count++))
                                fi
                            fi
                        ;;
                        "/dev/tty"*) echo "tty" ;;
                        *) echo "Not suitable tty" > /dev/stderr ;;
                        esac
                    else
                        restartApplication[$count]="$i"
                        ((count++))
                    fi
                else
                    restartApplication[$count]="$i"
                    ((count++))
                fi
            #else
            #    restartApplication[$count]="$i"
            #    ((count++))
            #fi
        done
    fi

    #if [[ "$runParameter" == "-m" ]]; then
        #Only worked with lsof
        #restart_upgraded_libraries
        #updated_libraries_running_applications
    #fi
}

restart_upgraded_libraries() {
    #Do not unset, it will destroy the assoziative array!
    #unset updatedLibrariesArr
    declare -A updatedLibrariesArr
    #local listUpgradedLibraries=$(lsof +c 0 | grep 'DEL.*lib' | awk '1 { print $1 " " $NF }' | sort -u | awk ' {print $2} ')
    local listUpgradedLibraries=$(echo "$listUpgradedPackages" | awk '{ print $2 }')
    for i in $listUpgradedLibraries; do
        updatedLibrariesArr["$i"]="$i"
    done

    local count=0
    unset restartUsedLibrary
    checkArray emptyArray updatedLibrariesArr
    if ( ! $emptyArray ); then
        for i in ${updatedLibrariesArr[*]}; do
            restartUsedLibrary[$count]="$i"
            ((count++))
        done
    fi
}

updated_libraries_running_applications() {
    # Restart system recommended after upgrade
    checkArray emptyArray restartUsedLibrary
    if ( ! $emptyArray ); then
        #Remove "" as it will recognize multiple services only as a text
        printText "INFO: Applications should be restarted when using the following upgraded libraries --> ${restartUsedLibrary[*]}" 0 1 "i"
    fi
}

upgrade_aur_automatically() {
    #As AUR packages are installed and trusted by the user, upgrading them can be automated and it will security issues
    if [[ "$runParameter" == "-m" ]]; then
        upgrade_aur
    else
        printText "$aurHelper not available in service mode (not allowed for root)" 1 1 "i"
        #BUG logname => $userName is not set/empty, cannot run this automatically
        #if ( ! $prodServer ) && ( ! $userManaged ); then
        #    upgrade_aur
        #fi
    fi
}

upgrade_aur() {
    # Upgrade AUR packages
    local text0="AurUpgrade=OK"
    local text1="AurUpgrade=Error"
    remove_pacmanlock
    if ( ! $pacmanLock ); then
        if pacman -Q "$aurHelper" &> /dev/null; then
            #--rebuildtree will build all dependencies in aur before upgrading an aur package
            #BUG What about git packages? Add --devel? But this requires some time to build these packages.
            sudo -u "$userName" "$aurHelper" -Syua --rebuildtree --noconfirm
            checkCommandOutput aurUpgradeText $? "$text0" "$text1"
        else
            echo "Package $aurHelper not installed"
        fi
    fi
}

aur_clean() {
    #To avoid to delete all, do this with paccache
    #Source https://gist.github.com/luukvbaal/2c697b5e068471ee989bff8a56507142
    #WARNING Do not use ${HOME} here, it would be root
    local yaycacheArr="$(find /home/$userName/.cache/yay -maxdepth 1 -type d | awk '{ print "-c " $1 }' | tail -n +2)"
    # Clean Package Download folder (output -v)
    local yayremoved=$(/usr/bin/paccache -ruvk0 $yaycacheArr | sed '/\.cache\/yay/!d' | cut -d \' -f2 | rev | cut -d / -f2- | rev)
    #[ -z $yayremoved ] || echo "==> Remove all uninstalled package folders" &&
    #echo $yayremoved | xargs -rt rm -r
    checkArray emptyArray yayremoved
    if ( ! $emptyArray ); then
        echo $yayremoved | xargs -rt rm -r
        paccacheCleanDeinstalledAUR=$?
    fi

    # Delete packages older than 2 versions of applications
    /usr/bin/paccache -rvk2 -c /var/cache/pacman/pkg $yaycacheArr
    paccacheRemoveOldVersionsAUR=$?
}

show_upgraded_packages() {
    # Show upgraded/installed packages from pacman.log
    #local actualdate1=$(date +%F)
    #local actualdate2=$(date +%Y-%m-%d)
    #sed - replace " " with "T" as otherwise time is cut off from date
    local uptimeSince=$(uptime -s | sed ''/' '/s//$(printf "T")/'')
    printText "Upgraded packages since last boot:" 0 1 "v"
    paclog --after=$uptimeSince 2> /dev/null | paclog --caller ALPM 2> /dev/null | paclog --grep='upgraded' 2> /dev/null | awk '{print $4 " " $5 " " $6 " " $7}'
    if [[ "$?" == 1 ]]; then
        printText "0" 0 2 "v"
        updatesInstalled=false
    else
        printf "\n"
        updatesInstalled=true
    fi
}

remove_pacmanlock() {
    #Packagekit is a little black box regarding it's activity, but try to stop it. If it is running, do not remove pacman lock.
    systemctl stop packagekit.service &> /dev/null
    # Remove pacman.db lock before Upgrade, sometimes previous unsuccesful updates block later updates
    local text0="[PacmanLock=Deleted]"
    #local text1="[PacmanLock=No]"
    local text1=""
    #pgrep "pacman" will list all processes with the same name, do not use it
    #Replace with "fuser /var/lib/pacman/db.lck"?
    if [[ $(pidof "pacman") == "" ]] && [[ $(pidof "packagekitd") == "" ]]; then
        find /var/lib/pacman/db.lck -exec rm           {} \;  -print &> /dev/null
        checkCommandOutput pacmanLockText $? "$text0" "$text1"
        pacmanLock=false
    else
        pacmanLock=true
        pacmanLockText="[PacmanLock=PacmanIsRunning]"
    fi
    if [[ "$pacmanLockText" != "" ]]; then
        printText "$pacmanLockText" 0 1 "$outputType"
    fi
}

refresh_keyring() {
    # Update Keyrings for pacman, sometimes you cannot run pacman updates due to outdated signatures
    #This takes a lot of time and shows a lot of confusing messages!
    #https://wiki.archlinux.org/title/Pacman/Package_signing#Adding_developer_keys
    #BUG When running this command, it doesn't block the pacman db and therefore another pacman could destroy somehow in a special situation the database???
    #NOTE Can this be replaced by archlinux-keyring-wkd-sync.service that runs automatically now once per week since 2022-07???
    #sudo nano /usr/bin/archlinux-keyring-wkd-sync
    #pacman-key --refresh-keys

    # Add new/unkown keys that are maybe provided only with the new keyring. WARNING this takes now a lot of time and shows a lot of error verbose output
    keyring_reinstall
}

keyring_reinstall() {
    #https://wiki.archlinux.org/title/Pacman/Package_signing#Upgrade_system_regularly
    #This is NOT a partial upgrade accoring to the Wiki
    pacman -Sy --needed --noconfirm "archlinux-keyring"
    #pacman -Q "parabola-keyring" &> /dev/null
    #if [[ "$?" == 0 ]]; then
    if pacman -Q "parabola-keyring" &> /dev/null; then
        pacman -Sy --needed --noconfirm "parabola-keyring"
    fi
    if pacman -Q "chaotic-keyring" &> /dev/null; then
        pacman -Sy --needed     --noconfirm "chaotic-keyring"
    fi
}

update_firmware() {
    #local smartUpdate=$1
    if [[ $statusVM == "none" ]]; then
        # Is fwupd-refresh.timer offering the same functionality to just check for new updates?
        fwupd_showupdates_firmware

        smart_getupdates
    else
        updateSmartFirmwareText="VM NoFWUpdate"
    fi
}

fwupd_showupdates_firmware() {
    if pacman -Q fwupd &> /dev/null; then
        #Show firmware updates
        #fwupdmgr get-updates
        fwupdmgr get-upgrades
        updateSmartFirmwareText="[FWUpdate]"
    else
        updateSmartFirmwareText="Package fwupd not installed"
    fi
}

fwupd_getupdates_firmware() {
    # Update firmware database, NO INSTALLATION
    if pacman -Q fwupd &> /dev/null; then
        fwupdmgr refresh
    else
        echo "[  INFO  ] fwupd is not installes, firmware upgrade skipped"
    fi
}

fwupd_update_firmware() {
    fwupd_getupdates_firmware
    fwupd_showupdates_firmware
    if pacman -Q fwupd &> /dev/null; then
        fwupdmgr upgrade
    else
        echo "[  INFO  ] fwupd is not installes, firmware upgrade skipped"
    fi
}

firmware_arm_update() {
    if [[ "$machineArch" == "aarch64" ]]; then
        if pacman -Q rpi-eeprom || pacman -Q rpi-eeprom-git &> /dev/null; then
            #Update firmware Raspberry Pi 4
            rpi-eeprom-update -d -a
            #Reboot
        fi
    fi
}

firmware_updates_install() {
    fwupd_update_firmware
    firmware_arm_update
}

smart_getupdates() {
    if pacman -Q smartmontools &> /dev/null; then
        # Update Smart SDD/HDD database, only update firmware info database
        # *not required in VM*
        if [[ "$smartUpdate" == "true" ]]; then
            updateSmartFirmwareText=$(update-smart-drivedb)
            updateSmartFirmwareText="[SmartDB] $updateSmartFirmwareText"
        fi
    else
        updateSmartFirmwareText="Package smartmontools not installed"
    fi
}

update_flatpak() {
    if pacman -Q flatpak &> /dev/null; then
        # Download only
        #flatpak_update_download

        # Install new packages
        #flatpak_update
        flatpakUpdateText=$(flatpak update --noninteractive --assumeyes | awk '{ print $1 }')
        #Remove some redundant text like /x86_64/stable
        flatpakUpdateText=$(echo "$flatpakUpdateText" | sed "s/\/x86_64\/stable//g")
        flatpakUpdateText=$(echo "$flatpakUpdateText" | sed "s/\/x86_64/g")
        flatpakUpdateText=$(echo "$flatpakUpdateText" | sed "s/Info://")
        flatpakUpdateText=$(echo "$flatpakUpdateText" | sed "s/Nothing//")
        flatpakUpdateText=$(echo "$flatpakUpdateText" | sed "s/Nichts//")

        updateFlatpakText="[FlatpakUpdate=OK] ${flatpakUpdateText[*]}"
    else
        updateFlatpakText="NoFlatpak"
    fi
}

show_flatpak_updates() {
    local updateFlatpakText=""
    if pacman -Q flatpak &> /dev/null; then
        #flatpak_update_show
        updateFlatpakArr=$(flatpak remote-ls --updates --all | awk 'NR>=0')
        checkArray emptyArray updateFlatpakArr
        if ( ! $emptyArray ); then
            if [[ "$updateFlatpakText" == "" ]]; then
                updateFlatpakText="0 Flatpak Updates available"
            fi
            printText "$updateFlatpakText" 0 1 "$outputType"
        else
            printText "0 Flatpak Updates available" 0 1 "$outputType"
        fi
    else
        updateFlatpakText="Flatpak not used/installed!"
    fi
}

upgrade_error_fixlinux() {
    #Sometimes updates are installed, but the upgrade process failed. In some conditions, this can lead to deleted initramfs-linux.img  and vmlinuz-linux beeing deleted from /boot
    reboot_check_boot ""

    #BUG Deleted initramfs-linux.img/vmlinuz-linux due to low disk space upgrade error of pacman
    echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Pacman Error. Trying to reinstall linux to prevent an empty kernel file on /boot (maybe btrfs related disk space problem)!!!"
    kernel_ucode_reinstallation_and_verification
}

####################################
######### Verify and Clean #########
####################################

orphan_packages() {
    #Removing flatpak orphans doesn't require user intervention, so do it first
    printText "Remove unused Flatpak packages" 0 1 "$outputType"
    oprhan_packages_flatpak_remove

    # Show ophan/not required packages
    printText "$debugName pacman -Qtd" 1 0 "v"
    #-q will remove the version
    orphanPackages=$(pacman -Qtdq)
    checkArray emptyArray orphanPackages
    if ( ! $emptyArray ); then
        printText "[OrphanPackages]" 0 0 "$outputType"
        printText "${orphanPackages[*]}" 0 1 "$outputType"
        orphan_packages_remove
    else
        printText "[NoOrphanPackages]" 0 1 "$outputType"
    fi

    #BUG logname/$userName is empty in service mode
    if pacman -Q "$aurHelper" &> /dev/null && [[ "$runParameter" == "-m" ]]; then
        # Show removed from official sources & AUR packages
        printText "$debugName yay -Ps | grep Missing" 1 0 "v"
        #-q will remove the version, use sed to print everything only after ":"
        missingPackageSource=$(sudo -u "$userName" "$aurHelper" -Ps | grep 'Missing' | sed -e 's#.*:\(\)#\1#')
        declare -A missingPackageSourceChecked
        for i in ${missingPackageSource[*]}; do
            if [[ "$i" != "pacbot" ]]; then
                missingPackageSourceChecked[$i]="$i"
            fi
        done
        checkArray emptyArray missingPackageSourceChecked
        if ( ! $emptyArray ); then
            printText "[MissingPackageSource]" 0 0 "$outputType"
            printText "${missingPackageSourceChecked[*]}" 0 1 "$outputType"
            missing_packages_remove
        else
            printText "[NoMissingPackageSource]" 0 1 "$outputType"
        fi

    #Clean optionally dependencies via yay seems to find less packages then -Qttd, is there a blacklist?
    #sudo -u "$userName" "$aurHelper" -Yc --noconfirm

    else
        printText "Cannot check for missing package sources in AUR in service mode" 1 1 "i"
    fi

    #Clean optionally dependencies
    printText "$debugName pacman -Qttd" 1 0 "v"
    optionalPackages=$(pacman -Qqttd)
    checkArray emptyArray optionalPackages
    if ( ! $emptyArray ); then
        printText "[OptionalPackages]" 0 0 "$outputType"
        printText "${optionalPackages[*]}" 0 1 "$outputType"
    else
        printText "[NoOptionalPackages]" 0 1 "$outputType"
    fi

    printText "$debugName pacman -Qm" 1 0 "v"
    removedOrAurPackages=$(pacman -Qqm)
    #Show pacbot and dependencies separate
    declare -A removedOrAurPackagesChecked
    for i in ${removedOrAurPackages[*]}; do
        if [[ "$i" != "pacbot" ]]; then
            removedOrAurPackagesChecked["$i"]="$i"
        fi
    done
    for j in ${PACKAGESAUR[*]}; do
        unset removedOrAurPackagesChecked["$j"]
    done
    for k in ${PACKAGESARCHBOT[*]}; do
        unset removedOrAurPackagesChecked["$k"]
    done
    printText "INFO: AUR packages installed by pacbot  -> ${PACKAGESAUR[*]}" 0 0 "$outputType"
    printText "INFO: AUR packages installed by archbot -> ${PACKAGESARCHBOT[*]}" 0 1 "$outputType"
    checkArray emptyArray removedOrAurPackagesChecked
    if ( ! $emptyArray ); then
        printText "[AUR-Packages or removed from official repository]" 0 0 "$outputType"
        #Not shown as a list? Just as text?
        printText "${removedOrAurPackagesChecked[*]}" 0 1 "$outputType"
    else
        printText "[NoPackageInstalledFromAUR]" 0 1 "$outputType"
    fi
}

orphan_packages_remove() {
    if [[ "$runParameter" == "-m" ]]; then
        printText "Use [pacman -D --asexplicit] or [pacman -D --asdeps] to change the installation reason of orphan packages or manually install them again after removal" 1 1 "v"
        #Removing orphaned packages automatically should be save as these are not required anymore by any other package
        #THIS REQUIRES USER INTERVENTION
        read -r -p "Do you want to delete the not required packages automatically (orphans)? [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            #Do not add "" to variable!
            pacmanRscNC "${orphanPackages[*]}"
            printText "[RemovedOrphanPackages]" 1 0 "$outputType"
            printText "${orphanPackages[*]}" 1 1 "$outputType"
        fi
    fi
}

oprhan_packages_flatpak_remove() {
    #Is this maybe done automatically by Flatpak?
    if pacman -Q flatpak &> /dev/null; then
        flatpak_uninstall_unused
    fi
}

missing_packages_remove() {
    if [[ "$runParameter" == "-m" ]]; then
        #Removing missing packages automatically should be save as these are not required anymore by any other package
        #THIS REQUIRES USER INTERVENTION
        read -r -p "Do you want to delete the not required packages automatically (missing package)? [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            #Do not add "" to variable!
            #Attention:  -Rsc/-Rc is dangerous here as it could remove essential packages if combined with --noconfirm as AUR offers packages with many dependencies like libkscreen-git
            pacman -R --noconfirm ${missingPackageSourceChecked[*]}
            printText "[RemovedMissingPackagesSource]" 1 0 "$outputType"
            printText "${missingPackageSourceChecked[*]}" 0 1 "$outputType"
        fi
    fi
}

verify_installedpackages() {
    # Verify installed or missing files of packages
    # pacman -Qkk PACKAGENAME can show you which files have been modified!
    local packageChecksumIssue=""
    local text0="[VerifyPackages=OK]"
    local text1="[VerifyPackages=Error]"
    #Only show broken packages, md5sum takes ~ 10 seconds more than sha256sum
    #packageChecksumIssue=$(paccheck --sha256sum --quiet)
    #Do we need to add here --noextract --noupgrade --db-files to catch all errors?
    #NOTE --file-properties will show too many false positives like amd-ucode and intel-ucode on /boot and systemd due to /var/log/journal GID mismatch
    packageChecksumIssue=$(paccheck --sha256sum --list-broken --noextract --noupgrade --db-files --files 2> /dev/null)
    checkCommandOutput verifyPackageText $? "$text0" "$text1 ${packageChecksumIssue[*]}"
    printText "$verifyPackageText" 0 1 "$outputType"
    reinstall_broken_packages "${packageChecksumIssue[*]}"

    #Check, verify and repair as well flatpak packages
    flatpak_verify_repair
}

reinstall_broken_packages() {
    local brokenPackages="$1"
    local -n brokenPackagesMain
    local count=0
    unset brokenPackagesMain
    #BUG AUR packages cannot be installed automatically and this will fail.
    #BUG VLC is creating /usr/lib/vlc/plugins/plugins.dat on package installation and is always listed as a checksum mismatch, package will be reinstalled always
    #BUG smartmontools drive database can be upgraded via update-smart-drivedb and it would be considered then as a broken package
    checkArray emptyArray brokenPackages
    if ( ! $emptyArray ); then
        for i in ${brokenPackages[*]}; do
            if [[ "$i" == "vlc" ]] || [[ "$i" == "vlc-git" ]] || [[ "$i" == "smartmontools" ]] || [[ "$i" == "brscan4" ]]; then
                echo "[  INFO  ] $i is probably not a broken package as it is normal that it modifies its own files. But reinstall it anyway!"
            fi
            if pacman -Si $i &> /dev/null; then
                #Info do not use quotes here for variable!
                brokenPackagesMain[$count]="$i"
                ((count++))
            elif pacman -Q "$aurHelper" &> /dev/null && [[ "$runParameter" == "-m" ]]; then
                #BUG logname/$userName is empty in service mode
                if sudo -u "$userName" "$aurHelper" -Si $i &> /dev/null; then
                    sudo -u "$userName" "$aurHelper" -Sy --noconfirm $i
                    reinstall_broken_packages_info
                else
                    printText "Cannot install broken AUR package $i automatically in service mode" 1 1 "$outputType"
                fi
            else
                printText "Unkown broken package $i" 1 1 "$outputType"
            fi
        done
    fi
    reinstall_packagages
}

reinstall_packagages() {
    local getPacmanErrorText=""
    brokenPackageText=""
    checkArray emptyArray brokenPackagesMain
    if ( ! $emptyArray ); then
        # Do not use here -Sy as it will install updates as well???
        if [[ "$EUID" -ne 0 ]]; then
            local installCmd="sudo pacman -Sy --noconfirm"
        else
            local installCmd="pacman -Sy --noconfirm"
        fi
        #Forward as well errors to the output with 2>&1
        getPacmanErrorText=$($installCmd ${brokenPackagesMain[*]} 2>&1)
        if [[ "$?" == 1 ]]; then
            echo "#-#1---------------$getPacmanErrorText----------------"
            brokenPackageText="[BrokenPackageNotInstalledAutomatically] ${brokenPackagesMain[*]}"
            reinstall_broken_packages_missing_file "$getPacmanErrorText"
            #Try to reinstall packages again if the DESC error was fixed
            if ( $errorDescIssueFixed ); then
                getPacmanErrorText=$($installCmd ${brokenPackagesMain[*]} 2>&1)
            fi
            if [[ "$runParameter" == "-m" ]]; then
                reinstall_broken_packages_file_exists_fix "$getPacmanErrorText"
            fi
        else
            echo "#-#2--------------$getPacmanErrorText"
            brokenPackageText="[ReinstalledBrokenPackage] ${brokenPackagesMain[*]}"
        fi
        sync

        #i="${brokenPackagesMain[*]}"
        reinstall_broken_packages_info
    fi
}

reinstall_broken_packages_info() {
    #TOO LATE TO CATCH ERROR?
    #if [[ "$?" == 0 ]]; then
    #    local brokenPackageText="[ReinstalledBrokenPackage] $i"
    #else
    #    local brokenPackageText="[BrokenPackageNotInstalledAutomatically] $i"
    #fi
    printText "$brokenPackageText" 0 1 "$outputType"
    #notify_user "$brokenPackageText" "critical"
}

reinstall_broken_packages_missing_file() {
    local getPacmanErrorText="$1"
    errorDesc=""
    errorDescIssueFixed=false
    # BUG Try to fix missing /var/lib/pacman/local/PACKAGENAME/desc errors

    if [[ "$getPacmanErrorText" == "" ]]; then
        #NOTE 2>&1 1> This stores as well the error output of a command including the normal output in the variable
        getPacmanErrorText=$(pacman -Syuq --noconfirm --needed 2>&1 1>/dev/null)
    fi
    #echo "---------------$getPacmanErrorText----------------"

    reinstall_broken_packages_missing_file_fix "/desc"
    reinstall_broken_packages_missing_file_fix "/files"
}

reinstall_broken_packages_missing_file_fix() {
    local file="$1"
    errorDesc=$(echo "$getPacmanErrorText" | grep "/var/lib/pacman/local/" | grep "$file" | awk '{ print $4 }')
    #echo "$(tput setaf $color)[  MISSING $file FILE ]$(tput sgr0) $errorDesc"

    if [[ "$errorDesc" != "" ]]; then
        errorDescIssueFixed=true
        echo "$(tput setaf $color)[  MISSING $file FILE ]$(tput sgr0) $errorDesc"
            #read -r -p "$(tput setaf 1)[     ERROR     ]$(tput setaf $color) Pacbot found a broken pacman database package file like $(tput setaf 2)/var/lib/pacman/local/PACKAGENAME/$file$(tput setaf $color)!!! Create empty file to fix this pacman error?$(tput sgr0) [y/N]"
            #if [[ "$REPLY" =~ [yY] ]]; then

                for i in ${errorDesc[*]}; do
                    # NOTE Uncrititcal! Creating an empty /desc or /files file for this package allows pacman to continue with upgrading packages and fixing the error by installing the update package including the missing meta desc data file!
                    touch "$i"
                    printText "[  Error ] Pacbot fixed a missing pacman database package by creating an empty file for $i" 0 1 "$outputType"
                #    pacman -S --noconfirm "$i"
                done

                # Testing
                #exit
            #fi
    else
        #echo "--------------$errorDesc-------------------"
        echo "$(tput setaf 2)[  MISSING $file FILE ]$(tput sgr0) 0 errors $errorDesc"
    fi
}

reinstall_broken_packages_file_exists() {
    local getPacmanErrorText=""
    #NOTE 2>&1 1> This stores as well the error output of a command including the normal output in the variable, but somehow this doesn't work as expected ... BUG
    #getPacmanErrorText=$(pacman -Syuq --noconfirm --needed 2>&1 1>/dev/null)
    getPacmanErrorText=$(pacman -Syuq --noconfirm --needed)
    if [[ "$?" == 1 ]]; then
        reinstall_broken_packages_file_exists_fix "$getPacmanErrorText"
    else
        echo "Nothing to do, there is no package installation issue due to already existing files."
    fi
}

reinstall_broken_packages_file_exists_fix() {
    #WARNING Either the function to overwrite existing files from the interface or the broken package check can run this code. So far, it will not run during a normal package upgrade!!!
    local getPacmanErrorText="$1"
    local errorDescEN=""
    local errorDescDE=""
    declare -A brokenPackgesWithExisitngFiles

    #Hide existing file name
    errorDescEN=$(echo "$getPacmanErrorText[*]" | awk '{ print $1 " " $3 " "  $4 " " $5}' | grep -i "exists in filesystem")
    errorDescDE=$(echo "$getPacmanErrorText[*]" | awk '{ print $1 " " $3 " "  $4 " " $5}' | grep -i "existiert im Dateisystem")
    #echo "------$errorDescDE------"
    if [[ "$errorDescDE" != "" ]] || [[ "$errorDescEN" != "" ]]; then
        if [[ "$errorDescDE" != "" ]]; then
            errorDesc=$(echo "$errorDescDE[*]" | awk '{ print $1}' | rev | cut -c 2- | rev)
        elif [[ "$errorDescEN" != "" ]]; then
            errorDesc=$(echo "$errorDescEN[*]" | awk '{ print $1}' | rev | cut -c 2- | rev)
        else
            echo "Not supported language"
        fi
        for i in ${errorDesc[*]}; do
            brokenPackgesWithExisitngFiles["$i"]="$i"
        done

        #read -r -p "$(tput setaf 1)[    WARNING   ]$(tput sgr0) Do you want to $(tput setaf $color)overwrite existing files$(tput sgr0) when installing [$(tput setaf $color)${brokenPackgesWithExisitngFiles[*]}$(tput sgr0)]? [y/N]"
        #Attention ! $REPLY will ALWAYS to the exit exept for yY
        #if [[ "$REPLY" =~ [yY] ]]; then
            echo "$(tput setaf 3)[ EXISTING FILE NOT OWNED ]$(tput sgr0) There are packages with a 'file already exists' problem [${brokenPackgesWithExisitngFiles[*]}]"
            enforce_package_install_overwrite "${brokenPackgesWithExisitngFiles[*]}"
        #fi
    else
        echo "$(tput setaf 2)[ EXISTING FILE NOT OWNED ]$(tput sgr0) There is no package with a 'file already exists' problem"
    fi
}

remove_broken_symlinks() {
    # Check for broken symlinks
    # Check as well entries that usually do not have this issue
    # Home/mnt/run/media shows many issues due to broken links of */steamapps/common/SteamLinuxRuntime_soldier/, .local/share/Steam/steamapps/common/SteamLinuxRuntime_sniper/, or if Games are installed in steamapps/compatdata (Proton .dll's) => self-managed
    # Home .local/var/pmbootstrap (postmarketOS) => self-managed
    # /var contains many issues due to /var/lib/flatpak
    # /run, /proc, /tmp contain only runtime data
    # /usr/share/cockpit/branding is a packaging issue that exists for years
    #local SYMLINKS_CHECK=("/etc" "/home" "/opt" "/srv" "/usr" "/var")
    #local SYMLINKS_CHECK_USALLY_OK=("/boot" "/dev" "/efi" "/root" "/srv" "/swap" "/sys")
    #local SYMLINKS_EXCLUDE="-not \( -path /home/*/.local/var/pmbootstrap -prune \) -not \( -path /home/*/.local/share/Steam/steamapps -prune \) -not \( -path /var/lib/flatpak -prune \)"
	#mapfile -t broken_symlinks < <(find "${SYMLINKS_CHECK[@]}" $SYMLINKS_EXCLUDE -xtype l -print)
	mapfile -t broken_symlinks < <(find "/" -not \( -path /home/*/.local/var/pmbootstrap -prune \) -not \( -path /proc -prune \) -not \( -path /run/systemd -prune \) -not \( -path /run/udev -prune \) -not \( -path /run/user -prune \) -not \( -path /.snapshots -prune \) -not \( -path /tmp -prune \) -not \( -path /usr/share/cockpit/branding -prune \) -not \( -path /var/lib/flatpak -prune \) -not \( -path */steamapps -prune \) -xtype l -print)
	if [[ "${broken_symlinks[*]}" ]]; then
        rm "${broken_symlinks[@]}"
		#find "${broken_symlinks[@]}" -xtype l -delete
		brokenSymlinksText="[BrokenSymlinksRemoved=Yes]"
	else
		brokenSymlinksText="[BrokenSymlinksRemoved=No]"
	fi
	#SYMLINKS_CHECK=("/home")
	#mapfile -t broken_symlinks < <(find "${SYMLINKS_CHECK[@]}" -xtype l -print)
	#for i in ${broken_symlinks[@]}; do
    #    read -r -p "$(tput setaf $color)Do you want to delete $(tput setaf 2)$i $(tput setaf $color)?$(tput sgr0)  [y/N]"
    #    if [[ "$REPLY" =~ [yY] ]]; then
            # BUG This doesn't work for Steam/Wine/Proton directories
    #        rm $i
    #    fi
	#done
}

fix_permissions() {
    # Show and fix file permission issues after upgrade (this needs all packages downloaded to pacman cache)
    # https://github.com/droserasprout/pacman-fix-permissions
    #ATTENTION pacman-fix-permissions does a system UPGRADE (pacman -Syu)
    # Source https://github.com/droserasprout/pacman-fix-permissions
    #BUG how to ignore file systems without permissions like /boot fat32?
    #BUG pacman-fix-permission cannot hide the text output and when donloading a lot of packages in can result in an overflow of the fixFolderPermission variable, so grep the output based on the character "/"
    local text0="FixFolderPermissions=OK"
    local text1="FixFolderPermissions=Error"
    if [[ "$runParameter" == "-m" ]]; then
        fixFolderPermission=$(yes | pacman-fix-permissions | grep "/")
    else
        if ( ! $prodServer ) && ( ! $userManaged ); then
            fixFolderPermission=$(yes | pacman-fix-permissions | grep "/")
        fi
    fi
    checkCommandOutput fixFolderPermissionsText $? "$text0" "$text1"
}

show_unowned_files() {
    # Check for Lost files, Source https://github.com/graysky2/lostfiles
    # or https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Removing_unused_packages_.28orphans.29
    #Note: if there are too many files not installed by any package (e.g. proton-ge-custom) lostfiles output is too long for the bash script. Replace with "pacreport --unowned-files | grep -B 9999 "Explicitly:" | head -n -1 | sed -e '/Files:/,+0d'"?
    #INFO lostfiles shows all files where pacreport can only shows the unkown directory
    #lostFiles=$(lostfiles)
    #BUG spaces between text is not considered as one line
    #WORKAROUND replace BLANKS with ???
    #SOLUTION: https://alvinalexander.com/linux-unix/bash-shell-script-for-loop-iterate-blank-spaces-in-file/ ==> Declare NEWLINE instead of BLANK as a character to get the next element!!!
    #NOTE Remove 2 leading spaces with cut -c 3-
    lostFiles=$(pacreport --unowned-files | grep -B 9999 "Explicitly:" | head -n -1 | sed -e '/Files:/,+0d' | cut -c 3-)
    #mapfile -t lostFiles < <(pacreport --unowned-files | grep -B 9999 "Explicitly:" | head -n -1 | sed -e '/Files:/,+0d')
    for i in ${lostFiles[*]}; do
        lostFilesArr["$i"]="$i"
    done

    #Exclude system generated and config files
    for j in ${SYSTEMGENERATEDFILES[*]}; do
        unset lostFilesArr["$j"]
    done

    #Merge lost files in lost system directories
    show_unowned_directories

    checkArray emptyArray lostFilesArr
    if ( $emptyArray ); then
        lostFilesStatusText="LostFiles=No"
    else
        lostFilesStatusText="LostFiles=Yes"
    fi
}

show_unowned_directories() {
    #Get and check content of the system generared directories to identify additional unkown files
    for k in ${SYSTEMGENERATEDDIRECTOY[*]}; do
        for o in ${lostFilesArr[*]}; do
            if [[ "$k" == "$o" ]]; then
                #Remove Directories Managed By Applications
                #NOTE .snapshots contains million of files and directories as it contains the root file system multiple times => SKIP
                if [[ "$k" == "/.snapshots/" ]] || [[ "$k" == "/var/lib/flatpak/" ]]; then
                    lostFilesArr["$o"]="${o}______ContentNotListed-TooManyEntries-SystemGeneratedDirectoryManagedByApplication______"
                else
                    #read -r -p "DEBUG ###########$k####$o###################################"
                    #Get Content of the System Generated Directory from the File System
                    #NOTE Skip first line as this is the directory itself with awk
                    #find "$k" -print | awk NR\>1
                    #timedatectl | head -n -6
                    listDirectory=$(find "$k" -print | awk NR\>1)
                    unset listDirectoryArr
                    declare -A listDirectoryArr
                    IFS=$'\n'
                    for l in ${listDirectory[*]}; do
                        listDirectoryArr["$l"]="$l"
                    done
                    #DEBUG Show length of Array
                    #echo "DEBUG ARRAY ELEMTS: ${#listDirectoryArr[@]}"
                    #echo "DEBUG ARRAY CONTENT: ${listDirectoryArr[@]}"
                    #timedatectl | head -n -6
                    for m in ${SYSTEMGENERATEDFILES[*]}; do
                        unset listDirectoryArr["$m"]
                    done
                    #echo "DEBUG ARRAY ELEMTS AFTER REMOVAL OF SYSTEMGENERATEDFILES: ${#listDirectoryArr[@]}"
                    #timedatectl | head -n -6
                    checkArray emptyArray listDirectoryArr
                    if ( $emptyArray ); then
                        #NOTE: Empty Folder Content, remove System Generated Directory from Lost Files array
                        unset lostFilesArr["$k"]
                        #timedatectl | head -n -6
                    else
                        for n in ${listDirectoryArr[*]}; do
                            #NOTE: This adds the lost files in the system generated directory directory to the lost files
                            lostFilesArr["$n"]="${n}______UnknownFileInSystenGeneratedDirectory______"
                            #timedatectl | head -n -6
                        done
                    fi
                    #timedatectl | head -n -6
                    #echo "${#lostFilesArr[@]}"
                fi
            fi
        done
    done
    #read -r -p "DEBUG: END"
}

show_missing_files() {
    # Show lost/missing files from installed packages, print lines only before and only after text
    pacreport --missing-files | grep -A 9999 'Files:'  | grep -B 9999 'Size:' | awk 'NR>1' | head -n -1
}

review_pacfiles() {
    #https://wiki.gentoo.org/wiki/Handbook:Parts/Portage/Tools#etc-update
	# Find and act on any .pacnew or .pacsave files
	printText "etc-update" 1 1 "v"
	delete_notrequired_pacfiles
	#pacdiff
	#Use etc-update package instead of pacdiff from Gentoo to manually merge pacnew files
	if [[ "$runParameter" == "-m" ]]; then
        etc-update
    fi
	#printText "...Done checking for pacfiles" 1 2 "v"
}

delete_notrequired_pacfiles() {
    for file in ${DELETEPACNEWFILES[*]}; do
        rm "$file" &> /dev/null
        if [[ "$?" == 0 ]]; then
            printText "$file not required -> removed" 0 1 "$outputType"
        fi
    done
}

delete_notrequired_cachefiles() {
    for file in ${DELETECACHEFILES[*]}; do
        rm "$file" &> /dev/null
        if [[ "$?" == 0 ]]; then
            printText "Cache $file not required -> removed" 0 1 "$outputType"
        fi
    done
}

cups_delete_cached_files() {
    #Cups can leave information about printed files, so delete them
    echo "[  INFO  ] Delete cached files in /var/spool/cups/c*"
    sudo find /var/spool/cups -type f '(' -name 'c*' ')' -delete &> /dev/null
}

####################################
### Rescue and Manual Maintenance ##
####################################

install_package() {
    printf "\\n"
    read -r -p "Which package do you want to install? Package name: "
    #pacman -Q "$aurHelper" &> /dev/null
    #if [[ "$?" == 0 ]]; then
    if pacman -Q "$aurHelper" &> /dev/null; then
        "$aurHelper" "$REPLY"
    else
        #pacman -S "$REPLY"
        #if [[ "$?" == 1 ]]; then
        if ! pacman -S "$REPLY" &> /dev/null; then
            # First install AUR support with YAY
            install_aur "yay-bin"
            # Now retry to install
            "$aurHelper" "$REPLY"
        fi
    fi
}

reinstall_allpackages() {
    # This will reinstall all packages keeping the install reason
    local installAllPackages=""
    local text0="All packages reinstalled"
    local text1="Error during reinstalling all packages"
    printText "Reinstall all packages (pacman -Qnq | pacman -S -)" 1 0 "v"
    pacman -Qnq | pacman -S -
    checkCommandOutput installAllPackages $? "$text0" "$text1"
    printText "$installAllPackages" 1 2 "$outputType"
}

enforce_package_install() {
    local installPackage="$1"
    if [[ "$installPackage" == "" ]]; then
        reinstall_broken_packages_file_exists
    else
        enforce_package_install_overwrite "$installPackage"
    fi
}

enforce_package_install_overwrite() {
    local installPackage="$1"
    if [[ "$installPackage" == "" ]]; then
        echo "[ ERROR ] No package provided <$installPackage>"
        exit
    else
        #NOTE If upgrading just one package it leaves the risk of a broken system as of a partial upgrade, e.g. if you overwrite gpgme with a new version without upgrading anything else it probably disables the possibility to use pacman unless you added before this procedure pacman-static.
        echo "$(tput setaf 1)[   WARNING  ]$(tput sgr0) Enforcing the update of $(tput setaf 1)one package$(tput sgr0) has the $(tput setaf 1)risk of a unusable system due to a partial upgrade$(tput sgr0). Therefore make at least sure that $(tput setaf 2)pacman-static$(tput sgr0) is available in case you need to repair the system afterwards!"
        pacman-static_install
        pacman -Sy --noconfirm --overwrite "*" "$installPackage"
    fi
}

reset_keyring() {
    #https://wiki.archlinux.org/index.php/Pacman/Package_signing#Resetting_all_the_keys
    rm -R /etc/pacman.d/gnupg
    pacman-key --init
    pacman-key --populate archlinux
    printText "Reset Pacman Keyring" 1 2 "$outputType"
}

show_local_packages() {
    printText "Explicit installed local packages" 1 1 "v"
    #Only show package name and not the version, so -Qq instead of Q-
    pacman -Qqe
}

review_pacbot_log() {
    # Use LastTriggerUSec, ActiveEnterTimestamp is incorrect
    local lastPacbotRun=""
    lastPacbotRun=$(systemctl show -p LastTriggerUSec pacbot-weekly.timer | awk '{print $2 " " $3}')
    #BUG, seems to not work anymore via unit
    #journalctl --since "$lastPacbotRun" -u pacbot-weekly.service | grep " "
    journalctl --since="$lastPacbotRun" | grep "pacbot-s"
}

arch_news() {
	# Grab the latest Arch Linux news, Source https://gitlab.com/mgdobachesky/ArchSystemMaintenance/blob/master/src/maint/run.sh
	python "$pkgPath/archNews.py" | less
}

fetch_warnings() {
	# Fetch and warn the user if any known problems have been published since the last upgrade, Source https://gitlab.com/mgdobachesky/ArchSystemMaintenance/blob/master/src/maint/run.sh
	printText "Check Arch Linux news..." 1 1 "v"
	local lastUpgrade=""
	lastUpgrade=$(sed -n '/pacman -Syu/h; ${x;s/.\([0-9-]*\).*/\1/p;}' /var/log/pacman.log)

	if [[ -n "$lastUpgrade" ]]; then
		python "$pkgPath/archNews.py" "$lastUpgrade"
	else
		python "$pkgPath/archNews.py"
	fi
	upgradeAlert="$?"

	if [[ "$upgradeAlert" == "1" ]]; then
		printText "WARNING - Due to $upgradeAlert Upgrade Alert(s) this upgrade requires out-of-the-ordinary user intervention." 1 0 "$outputType"
		printText "Continue only after fully resolving the above issue(s)." 1 1 "v"
        if [[ "$runParameter" == "-m" ]]; then
            read -r -p "Are you ready to continue? [y/N]"
            #Attention ! $REPLY will ALWAYS to the exit exept for yY
            if [[ "$REPLY" =~ [yY] ]]; then
                upgradeAlert="ignore"
            fi
        else
            local disabledUpgradeText="DISABLED Automatic Upgrade as of $upgradeAlert Upgrade Alerts from Arch Linux News"
            printText "$disabledUpgradeText" 2 2 "$outputType"
            #notify_user "$disabledUpgradeText"
        fi
	else
		printText "$upgradeAlert Upgrade Alerts" 0 1 "v"
	fi
}

downgrade_package() {
    read -r -p "Which package do you want to downgrade? Package name: "
    if pacman -Q "$REPLY" &> /dev/null; then
        downgrade $REPLY
        read -r -p "Do you want to edit pacman.conf and add manually $REPLY to $(tput setaf $color)IgnorePkg$(tput sgr0)?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            nano /etc/pacman.conf
        fi
    else
        printText "Package $REPLY unkown" 0 2 "v"
    fi
}

pacman_clean_cache() {
    local keepOldPackages="$1"
    if [[ "$keepOldPackages" == "" ]]; then
        keepOldPackages=0
    fi
    #pacman -Scc --nocofirm doesn't delete anything
    paccache -rvk"$keepOldPackages"

    clean_flatpakcache

    if pacman -Q "$aurHelper" &> /dev/null && [[ "$runParameter" == "-m" ]]; then
        sudo -u "$userName" "$aurHelper" -Scca --noconfirm
    fi
}

pacman_lowdiskspace() {
    local updateSize=0
    local rootDisk=$(mount | grep " / " | awk '{print $1}' | cut -c 6-)
    #If btrfs subvolumes are used, there are multiple lines printed, so use only first line (all sizes are the same)
    #MB * 1024 = size in byte
    local rootSizeFree=$(df -l | grep "$rootDisk" | awk '{ print $4 }' | awk ' NR==1 ')
    #Doesn't work if package was already downloaded
    #updatePackagesSize=$(pacman -Qqu | pacman -Sp --print-format '%s' -)
    updatePackagesSize=$(pacman -Qqui | grep "KiB" | awk '{ print $3 }' | tr ',' '.')
    #BUG : is printed sometimes
    if [[ $updatePackagesSize[0] == ":" ]]; then
        updatePackagesSize=$(pacman -Qqui | grep "KiB" | awk '{ print $4 }' | tr ',' '.')
    fi
    for packageSizeKiB in ${updatePackagesSize[*]}; do
        updateSize=$(echo "$updateSize + $packageSizeKiB" | bc)
    done
    unset updatePackagesSize
    updatePackagesSize=$(pacman -Qqui | grep "MiB" | awk '{ print $3 }' | tr ',' '.')
    #BUG : is printed sometimes
    if [[ $updatePackagesSize[0] == ":" ]]; then
        updatePackagesSize=$(pacman -Qqui | grep "MiB" | awk '{ print $4 }' | tr ',' '.')
    fi
    for packageSizeMiB in ${updatePackagesSize[*]}; do
        packageSizeKiB=$(echo "$packageSizeMiB * 1000" | bc )
        updateSize=$(echo "$updateSize + $packageSizeKiB" | bc)
    done
    #Add buffer = 100MB = 100000KB
    updateSize=$(echo "$updateSize" + 100000 | bc)
    #Remove comma and its decimal value, print text befefore special character .
    updateSize=$(echo "$updateSize" | cut -d . -f 1)
    #read -r -p "RootSizeFree $rootSizeFree < updateSizeRquired $updateSize"
    if (( $rootSizeFree < $updateSize )); then
        echo "[ info ] Free space on / is $rootSizeFree but $updateSize are required for updating. Deleting pacman cache now!!!"
        if [[ "$upgradeLoop" == 0 ]]; then
            pacman_clean_cache "1"
        else
            pacman_clean_cache "0"
        fi
        upgradeTooLessDiskSpace=true
    fi
    ((upgradeLoop++))
}

maintenance_nonroot_tasks() {
    #WARNING only use printText in verboose mode otherwise a new updating desktop notification file would be created
    show_upgraded_packages
    if ( $updatesInstalled ); then
        restart_upgraded_applications
        restart_applications_recommended
    fi
}

####################################
############# Interface ############
####################################

runParameter="$1"
if [[ "$runParameter" == "--local" ]]; then
    export pkgPath="."
fi


if [[ "$runParameter" != "--source-only" ]]; then
    # Make sure script is running as root
    if [[ "$EUID" -ne 0 ]]; then
        #Normal User
        #notify_user "----------Start1 user---------" "normal"
        #printf "This script runs as root with sudo\n"
        #exit 1
        userMode=true
        sudo bash $pkgPath/pacbot.sh "$runParameter"
    fi

    source_dependencies_pacbot
    check_runParameter_pacbot

    if [[ "$EUID" -eq 0 ]]; then
        #notify_user "----------Start2 root---------" "critical"
        # Import dependencies
        notifyUserDesktopNewFile
        init_tasks
        if [[ "$runParameter" == "-d" ]]; then
            maintenance_daily_tasks
        elif [[ "$runParameter" == "-w" ]]; then
            maintenance_weekly_tasks
        else
            maintenance_manual
            #printf "\n\n ------ TEST for syntax errors TEST ------\n\n"
        fi
        notifyUserDesktopDone
    #else
    #	printText "$scriptName runs only with root privileges!" 1 1 "v"
    fi

    if [[ "$EUID" -ne 0 ]]; then
        #Normal User - Post Upgrade hook probably from desktop
        if [[ "$runParameter" == "-m" ]]; then
            maintenance_nonroot_tasks
        fi
    fi
fi
