#!/bin/ash
#OpenWRT
#Based on https://blog.christophersmart.com/2018/03/18/auto-apply-latest-package-updates-on-openwrt-lede-project/

####################################
######### Global Variables #########
####################################

logFile="/www/update.txt"
localBin="/usr/local/sbin"
updateSystem="$localBin/update-system.sh"
version="$(uname -srmn)"
distribution="$(cat /etc/os-release | grep "PRETTY_NAME" | cut -c 14- | cut -c -15)"

####################################
############## General #############
####################################

opkgbot_init() {
    settings_local
    opkgbot_install
    ipv6-pd_hotfix
}

opkgbot_install() {
    #Make a directory to hold the script.
    mkdir -p "$localBin"
    
    #cat > /usr/local/sbin/update-system.sh << \EOF
    echo "#!/bin/ash
opkg update
# upgrade netifd first as it causes drop out and system upgrade fails
opkg upgrade netifd
# install luci-ssl, so we get web back after upgrades => not required anymore since 21.02.0 https://openwrt.org/releases/21.02/notes-21.02.0
#opkg install luci-ssl
#/etc/init.d/uhttpd restart
# do package upgrades
PACKAGES=\"\$(opkg list-upgradable | awk '{print \$1}')\"
if [ -n \"\${PACKAGES}\" ]; then
    opkg upgrade \${PACKAGES}
if [ \"\$?\" -eq 0 ]; then
    echo \"\$(date -I\"seconds\") - \$(cat /etc/os-release | grep "PRETTY_NAME" | cut -c 14- | cut -c -15) \$(uname -srmn) - Update success (\${PACKAGES[*]}), rebooting services only (rpcd dnsmasq odhcpd firewall ddns uhttpd adguardhome) if updated\" >> $logFile
        #exec reboot
        service_requires_restart
    else
        echo \"\$(date -I\"seconds\") - \$(cat /etc/os-release | grep "PRETTY_NAME" | cut -c 14- | cut -c -15) \$(uname -srmn) - Update failed\" >> $logFile
    fi
#Debug
#else
#    echo \"\$(date -I\"seconds\") - \$(cat /etc/os-release | grep "PRETTY_NAME" | cut -c 14- | cut -c -15) \$(uname -srmn) - Nothing to update\" >> $logFile
fi" | tee "$updateSystem" &> /dev/null
    #EOF
    
    #Make the script executable and touch the log file.
    chmod u+x "$updateSystem"
    touch "$logFile"

    #Make sure the script and results are kept when upgrading the firmware.
    echo "$localBin" >> /etc/sysupgrade.conf
    echo "$logFile" >> /etc/sysupgrade.conf

    #Next schedule the script in cron.
    #crontab -e
    #My cron entry looks like this, to run at 2am every day.
    #    0 3 * * * /usr/local/sbin/update-system.sh
    echo "opkgbot runs every day at 03:00"
    echo "    0 3 * * * /usr/local/sbin/update-system.sh" | tee "/etc/crontabs/root" &> /dev/null

    #Now just start and enable cron.
    /etc/init.d/cron start
    /etc/init.d/cron enable

    #Give it a run manually, if you want.
    #root@firewall:~# /usr/local/sbin/update-system.sh

    #Download a copy of the log from another machine (once the router has finished rebooting).
    #curl http://192.168.1.1/update.result
    #2018-03-18T10:14:49+1100 - nothing to update
}

package_update_reboot() {
    opkg update
    # upgrade netifd first as it causes drop out and system upgrade fails
    opkg upgrade netifd
    # install luci-ssl, so we get web back after upgrades => not required anymore since 21.02.0 https://openwrt.org/releases/21.02/notes-21.02.0
    #opkg install luci-ssl
    #/etc/init.d/uhttpd restart
    # do package upgrades
    PACKAGES="$(opkg list-upgradable | awk '{print $1}')"
    if [ -n "${PACKAGES}" ]; then
        opkg upgrade ${PACKAGES}
        if [ "$?" -eq 0 ]; then
            echo "$(date -I"seconds") - $distribution $version - Update success (${PACKAGES[*]}), rebooting services only (rpcd dnsmasq odhcpd firewall ddns uhttpd adguardhome) if updated" >> "$logFile"
            #exec reboot
            service_requires_restart
        else
            echo "$(date -I"seconds") - $distribution $version - Update failed" >> "$logFile"
        fi
    #Debug
    #else
    #    echo "$(date -I"seconds") - $distribution $version - Nothing to update" >> "$logFile"
    fi
}

service_requires_restart() {
    # Check for packages that are updated and require critical service restart
    for update in ${PACKAGES[*]}; do
        if [[ "$update" == "rpcd" ]]; then
            /etc/init.d/rpcd restart
        elif [[ "$update" == "dnsmasq" ]]; then
            /etc/init.d/dnsmasq restart
        elif [[ "$update" == "odhcpd-ipv6only" ]]; then
            /etc/init.d/odhcpd restart
        elif [[ "$update" == "firewall" ]]; then
            /etc/init.d/firewall restart
        elif [[ "$update" == "ddns-scripts-services" ]]; then
            /etc/init.d/ddns restart
        elif [[ "$update" == "uhttpd" ]]; then
            /etc/init.d/uhttpd restart
        elif [[ "$update" == "adguardhome" ]]; then
            /etc/init.d/adguardhome restart
        fi
    done
}

package_reinstall_after_upgrade() {
    opkg update
    #DynDNS ddns-scripts-nsupdate???
    opkg install luci-app-ddns ca-certificates wget-ssl
    #Statistic
    opkg installluci-app-statistics collectd-mod-ping
    #Sysugrade Tool
    opkg install luci-app-attendedsysupgrade

    #(Re-)start services
    /etc/init.d/ddns restart
    /etc/init.d/collectd restart
}

ipv6-pd_hotfix() {
local file="/etc/hotplug.d/iface/99-ipv6_fix_prefix_delegation"
echo "#!/bin/sh
#https://forum.openwrt.org/t/delegated-ipv6-prefix-not-updated/56135/6

__INTERFACE__="wan6"
__DEBUG__="TRUE"

#WARNING: Only to see how often this is triggered, it is not filtered on the interface!!!
if [ "$__DEBUG__" == "TRUE" ]; then
    #date >> /tmp/debug-hotplug-iface.log
    #date >> /www/update.txt
    #env  >> /tmp/debug-hotplug-iface.log
    #env >> /www/update.txt
    echo "$(date -I"seconds") - $INTERFACE - IPv6-PD Hotfix" >> /www/update.txt
fi


if [ "$INTERFACE" = "$__INTERFACE__" -a "$IFUPDATE_ADDRESSES" = "1" ]; then
    message="***[ IPV6-PD Hotfix ]*** Address change detected, resetting interface $__INTERFACE__"
    #/usr/bin/logger -p user.notice -t \
    #                hotplug "$message"
    #echo "$(date -I"seconds") - $message" >> /www/update.txt
    /sbin/ifup "$__INTERFACE__"       # ifup implies ifdown

    sleep 30s

    /etc/init.d/ddns restart

    #LAN => f0:: contains even after 30 seconds sometimes multiple ipv6 addresses, so use full address of wan6 now
    #ipv6LAN=$(ip a | grep inet6 | grep 2003 | grep f0:: | awk '{ print $2 }')
    #cut first 15 digits as this is the /56 prefix of the Telekom DSL provider in the FritzBox
    #Filter on noprefixroute as this is IPv6 only and it exceludes depracted addresses, then filter exclude ::1 as these are all LAN interfaces, filter as well local fd00:: address
    #If the DSL provider (Telekom) changes the old IPV6-PD the old one is still valid for 2 hours and then there are still 2 addresses shown???
    ipv6LAN=$(ip a | grep inet6 | grep noprefixroute | grep -v ::1 | grep -v fd00:: | awk '{ print $2 }' | cut -c -15)
    #Reconstruct provider subnet
    providerSubnet="00::/56"
    ipv6LAN="${ipv6LAN}$providerSubnet"
    /usr/bin/logger -p user.notice -t "hotplug $ipv6LAN $message"
    echo "$(date -I"seconds") - $ipv6LAN $message" >> /www/update.txt
fi
" | tee "$file" &> /dev/null

    #Make the script executable
    chmod u+x "$file"

    #Make sure the script and results are kept when upgrading the firmware.
    #In LUCI you can find this in System / Backup & FlashFirmware / Configuration
    echo "$file" >> /etc/sysupgrade.conf
}

settings_local() {
    #In LUCI this is visible in Network / DHCP and DNS / Resolv and Hosts Files
    echo "/etc/hosts.local" >> /etc/sysupgrade.conf
}

sysupgrade() {
    #https://openwrt.org/docs/guide-user/installation/generic.sysupgrade#upgrade_procedure
    # example downloading the OpenWrt 15.05 upgrade image for a TP-LINK TL-WR1043ND ver. 1.x router
    cd /tmp
    wget http://downloads.openwrt.org/chaos_calmer/15.05/ar71xx/generic/openwrt-15.05-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin
    
    wget http://downloads.openwrt.org/chaos_calmer/15.05/ar71xx/generic/sha256sums
    sha256sum -c sha256sums 2> /dev/null | grep OK
    
    opkg save
    
    # Initiate sysupgrade with your desired options
    # by default ( no -n ) settings are kept
    sysupgrade -v /tmp/openwrt-15.05-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin
}

sysupgrade_todos() {
    #https://openwrt.org/docs/guide-user/installation/generic.sysupgrade
    #Reinstall all previously installed packages
    opkg restore
    #Make config again if necessary
    #Update configs based on upstream settings
}

####################################
############ Interface #############
####################################

