#!/bin/bash
# usbstickbot - Arch Linux + openSUSE Tumbleweed USB Stick creation tool, author Rainer Finke, license GPLv3
# Precondition: Linux system with wget + sha256sum + dd

####################################
######### Global Variables #########
####################################

scriptNameLoc="usbstickbot"
declare -a allDisks
declare -a availableDisks
allDisks=$(lsblk --nodeps | awk 'NR>1{ print $1 }' | sort -u)
targetText="USB Stick inserted"
distribution="archlinux"
taskText="write the latest $distribution iso"
emptyArray=false
efi=false
color=3
iso=""
linkIso=""
linkChecksum=""
checksumFile=""
matchedIsoChecksum=""
disableChecksum=false

####################################
############ USB Stick #############
####################################

download_write_iso() {
    echo ">>>>>>>>>>>> usbstickbot $distribution <<<<<<<<<<<<"
    echo "Download and verify $distribution ISO image..."
    read -r -p "$(tput setaf $color)Do you want to write the ISO image directly to an USB Stick?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        check_target_disk
    else
        download_iso
    fi
}

prepare_disk_tasks() {
    download_iso
    write_image
    cleanup_task
}

download_iso() {
    error404NotFound=false
    errorCnt=0
    iso_link
    download_iso_link
}

download_iso_link() {
    echo ">>>>>> Download ${distribution} <<<<<<"
    if wget --show-progress --https-only --continue "${linkIso}" --output-document="$iso"; then
        if ( ! $disableChecksum ); then
            wget --https-only -nv "$linkChecksum"  --output-document="$checksumFile"
            local checksumOfficial=""
            checksumOfficial=$(awk 'NR==1 {print $1}' "$checksumFile")
            local checksumIso=""
            checksumIso=$(${checksum}sum "$iso" | awk 'NR==1 {print $1}')
            if [[ "$checksumOfficial" == "$checksumIso" ]]; then
                echo "$(tput setaf 2)$checksum of iso is correct!$(tput sgr0)"
                matchedIsoChecksum=true
            else
                echo "$checksum doesn't match. $(tput setaf $color)ERROR$(tput sgr0)"
                matchedIsoChecksum=false
            fi
        else
            matchedIsoChecksum=false
        fi
    else
        error404NotFound=true
        ((errorCnt++))
        if [[ "$distribution" == "" ]] || [[ "$distribution" == "archlinux" ]]; then
            #Sometimes during the first 5 days of a month the new build is not available yet, so use the one from the previous month
            iso_archlinux
            if (( $errorCnt <= 1 )); then
                download_iso_link
            else
                echo "Download didn't work, check your network and try again. ERROR"
            fi
        else
            echo "Download didn't work, check your network and try again. ERROR"
            matchedIsoChecksum=false
        fi
    fi
}

iso_link() {
    if [[ "$distribution" == "" ]] || [[ "$distribution" == "archlinux" ]]; then
        iso_archlinux
    elif [[ "$distribution" == "openSUSE" ]]; then
        iso_opensuse
    elif [[ "$distribution" == "netboot" ]]; then
        iso_netboot
    fi
}

iso_archlinux() {
    iso="${distribution}-latest.iso"
    checksum="sha256"
    checksumFile="${checksum}sums.txt"
    local year
    year=$(date '+%Y')
    local month
    if ( ! $error404NotFound ); then
        month=$(date '+%m')
    else
        month=$(date '+%m')
        ((month--))
        if (( $month < 10 )); then
            month="0$month"
        fi
    fi
    local day
    #BUG sometimes it is not the first day of the month. Extract from website???
    iso_archlinux_getlatestreleasedate
    local latestArch="${year}.${month}.${day}"
    local linkArchLinux="https://ftp.halifax.rwth-aachen.de/archlinux/iso/"
    local arch="x86_64"
    #isoName="${distribution}-${latestArch}-${arch}.iso"
    linkIso="${linkArchLinux}${latestArch}/${distribution}-${latestArch}-${arch}.iso"
    linkChecksum="${linkArchLinux}${latestArch}/${checksumFile}"
}

iso_archlinux_getlatestreleasedate() {
    day=$(curl --silent https://archlinux.org/download/ | grep "Current Release" | awk '{ print $ 3 }' | cut -c 9- | rev | cut -c 6- | rev)
    if [[ "$day" == "" ]]; then
        day="01"
    fi
}

iso_opensuse() {
    iso="${distribution}-latest.iso"
    checksum="sha256"
    checksumFile="${checksum}.txt"
    local linkOpenSuseLinux="https://download.opensuse.org/"
    local channel="tumbleweed"
    local channelTmp=$(echo "$channel" | cut -c 2-)
    local channel2="T${channelTmp}"
    local net="NET"
    if [[ "$distType" != "" ]]; then
        net="$distType"
    fi
    local arch="x86_64"
    linkIso="${linkOpenSuseLinux}${channel}/iso/${distribution}-${channel2}-${net}-${arch}-Current.iso"
    linkChecksum="${linkIso}.${checksum}"
}


iso_netboot() {
    #BUG Works in VM, but doesn't boot from USB!
    iso="${distribution}.xyz.iso"
    #checksum=""
    disableChecksum=true
    #checksumFile="${checksum}sums.txt"
    #local year
    #local year=$(date '+%Y')
    #local month
    #local month=$(date '+%m')
    #local latestArch="${year}.${month}.01"
    local linkArchLinux="https://boot.${distribution}.xyz/ipxe"
    #local arch="x86_64"
    linkIso="${linkArchLinux}/${iso}"
    #linkChecksum="${linkArchLinux}${latestArch}/${checksumFile}"
}

write_image() {
    if ( $matchedIsoChecksum ) || ( $disableChecksum ); then
        echo "...writing image to >$(tput setaf $color)$target$(tput sgr0)< will take several minutes... $(tput setaf $color)Password required!$(tput sgr0)"
        sudo dd if="$iso" of=/dev/"$target" status=progress bs=4M
        sync
        echo "$distribution USB stick created on >$target<. You can boot to the USB stick at any time later by pressing DEL/F2 key several times during the boot logo."
        read -r -p "$(tput setaf $color)Finished. Do you want to reboot to UEFI Bios now?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            local efi=""
            if ( $efi ); then
                systemctl reboot --firmware-setup
            else
                systemctl reboot
            fi
        else
            echo "No reboot requested"
        fi
        echo ""
    fi
}

cleanup_task() {
    if [[ "$iso" != "/" ]] &&  [[ "$checksumFile" != "/" ]]; then
        rm "$iso"
        rm "$checksumFile"
    fi
}

####################################
############# DUPLICATE ############
####################################

check_target_disk() {
    #Used as well in archinstallbot, usbstickbot
    testRootDiskNVME=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -7)
    testRootDiskSSD=$(mount | grep " / " | awk '{print $1}' | cut -c 6- | cut -c -3)
    usbRootDisk=$(mount | grep "archiso/boot" | awk '{print $1}' | cut -c 6- | cut -c -3)
    exclude0BDisks=$(lsblk --nodeps | grep " 0B " | awk '{ print $1}')
    local count=0
    for i in ${allDisks[*]}; do
        if [[ "$i" != "$testRootDiskNVME" ]] && [[ "$i" != "$testRootDiskSSD" ]] && [[ "$i" != "$usbRootDisk" ]] && [[ "$i" != "$exclude0BDisks" ]]; then
            availableDisks[count]=$i
            ((count++))
        fi
    done
    # Show available disks and partitions. lsblk requires no root, but doesn't show partitions
    #sudo fdisk -l
    echo " "
    lsblk --nodeps | awk '!/loop/ {print}' | awk '!/archiso/ {print}' | awk '!/ 0B / {print}'
    echo " "

    checkArray emptyArray availableDisks
    if ( ! $emptyArray ); then
        echo ">>>>>> Available Disks <<<<<<"
        echo "$(tput setaf 2)${availableDisks[*]}$(tput sgr0)"
        # Read disk name
        read -r -p "$(tput setaf $color)Please type the name of the $(tput setaf 2)target disk:$(tput sgr0)"
        local target="$REPLY"
        read -r -p "$(tput setaf $color)Do you really want to $taskText and $(tput setaf 1)DELETE ALL DATA$(tput sgr0) $(tput setaf $color)on $(tput setaf 2)$target$(tput sgr0)?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            if [[ "$testRootDisk" != "$target" ]]; then
                local check=0
                for i in ${availableDisks[*]}; do
                    if [[ "$i" == "$target" ]]; then
                        prepare_disk_tasks
                        check=1
                    fi
                done
                if [[ "$check" == "0" ]]; then
                    echo "$(tput setaf $color)Unknown disk >$target<.$(tput sgr0) Close."
                fi
            else
                echo "$(tput setaf $color)Cannot overwrite root >$testRootDisk< of running system.$(tput sgr0) ERROR"
            fi
        else
            echo "$(tput setaf $color)No target disk selected.$(tput sgr0) Close."
        fi
    else
        echo "$(tput setaf $color)No ${targetText}.$(tput sgr0) Close."
        exit
    fi
}

checkArray() {
    #Used as well in pacbot, archinstallbot, systemchecks, helpers
    #Check if Array is empty, returnVar is the name of the boolean variable, arrayLoc is the name of the Array
    local -n returnVar=$1
    local -n arrayLoc=$2
    if [[ ${#arrayLoc[@]} -gt 0 ]] && [[ "${arrayLoc[*]}" = *[![:space:]]* ]]; then
        #echo "FunctionArrayNOTEmpty"
        returnVar=false
    else
        #echo "FunctionArrayEmpty"
        returnVar=true
    fi
}

####################################
############ Interface #############
####################################

runParameterLoc=$1
downloadIso=false
if [[ "$runParameterLoc" == "archlinux" ]]; then
    distribution="archlinux"
elif [[ "$runParameterLoc" == "opensuse" ]] || [[ "$runParameterLoc" == "openSUSE" ]]; then
    distribution="openSUSE"
elif [[ "$runParameterLoc" == "opensuse-kde" ]] || [[ "$runParameterLoc" == "openSUSE-kde" ]]; then
    distribution="openSUSE"
    distType="KDE-Live"
elif [[ "$runParameterLoc" == "netboot" ]]; then
    distribution="netboot"
elif [[ "$runParameterLoc" == "--help" ]] || [[ "$runParameterLoc" == "-h" ]]; then
    echo "$scriptNameLoc: - Download Linux ISO, verify and write it to USB Stick"
    echo ""
    echo "archlinux             Download latest Arch Linux"
    echo "opensuse              Download latest openSUSE network boot minimal"
    echo "opensuse-kde          Download latest openSUSE KDE full live ISO"
    echo "netboot               Download PXE netboot image"
    echo "-d --download-only    Download only"
    echo ""
    echo "If no distribution is provided, Arch Linux will be downloaded"
    exit
elif [[ "$runParameterLoc2" == "--download-only" ]] || [[ "$runParameterLoc2" == "-d" ]]; then
    downloadIso=true
fi

runParameterLoc2=$2
if [[ "$runParameterLoc2" == "--download-only" ]]; then
    downloadIso=true
fi

if [[ -d "/sys/firmware/efi" ]]; then
    efi=true
fi
if [[ "$runParameterLoc" != "--source-only" ]]; then
    if [[ "$EUID" -eq 0 ]]; then
        printf "usbstickbot should not run with root privileges, it works only with sudo!\n"
    else
        if ( $downloadIso ); then
            download_iso
        else
            download_write_iso
        fi
    fi
#else
    #Required to not run the archinstallbot automatically when sourcing this file
    #continue or break not allowed here
    #echo "usbstickbot nothing to do --source-only"
fi
