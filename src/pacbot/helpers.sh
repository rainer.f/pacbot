#!/bin/bash
# pacbot - Automated Pacman and System Maintenance, author Rainer Finke, license GPLv3

####################################
######## Global Variables ##########
####################################

declare -a missingPackage
declare -a missingPackageAur
declare -a unkownPackage
declare -a runningCriticalServices
declare -a runningCriticalServicesShort
declare -a installPackages

export prodServer=false
export updatePacbot=true
export machineArch=""
export computerType=""
export imageOnly=false
export userManaged=false
export onBattery=false
export powerText=""
export statusVM=""
export pacbotUpdateText=""
#hostname is not available in new installed Arch System, used in archinstallbot
#hostName=$(hostname)
export hostName=""
hostName=$(hostnamectl | grep "Static hostname:" | sed 's/ Static hostname: *//')
export distributionActual=$(more /etc/os-release | grep "NAME" | sort -u | awk ' NR==1 ' | cut -c 7- | rev | cut -c 2- | rev)

####################################
############ Functions #############
####################################

init_tasks() {
    archinstallbot_conf_check
    archinstallbot_conf_check_user
    init_local_exceptions
    init_pc_state
    check_preconditions
    check_critical_services
    show_development_version
    set_hello
}

archinstallbot_conf_check() {
    installedByArchinstallbot=false
    installPacbot=false
    local file="/etc/archinstallbot.conf"
    test -f "$file"
    if [[ "$?" == 0 ]]; then
        source "$file"
    else
        archinstallbot_conf_create
    fi
}

archinstallbot_conf_check_user() {
    #Non-root
    if [[ "$EUID" -ne 0 ]]; then
        local file="${HOME}/.archbotuser.conf"
        test -f "$file"
        if [[ "$?" == 0 ]]; then
            source "$file"
        else
            archinstallbot_conf_create_user
        fi
    fi
}

archinstallbot_conf_create() {
    # Create information about system archlinux_installer.
    # CUSTOMIZED INSTALLATION
    local file="/etc/archinstallbot.conf"
    local pacbotConfigVersionRelease=$(pacman -Q pacbot | awk '{ print $2 }')
    echo "#!/bin/bash" | tee "$file" &> /dev/null
    #echo "export installed=true" | tee -a "$file" &> /dev/null
    #echo "export installPacbot=$installPacbot" | tee -a "$file" &> /dev/null
    echo "export pacbotVersion=$pacbotConfigVersionRelease" | tee -a "$file" &> /dev/null
}

archinstallbot_conf_create_user() {
    # Create archbot user config.
    local file="${HOME}/.archbotuser.conf"
    echo "#!/bin/bash" | tee "$file" &> /dev/null
    echo "export createdProfile=false" | tee -a "$file" &> /dev/null
}

init_local_exceptions() {
    # Check for PROPUCTION SERVER
    # THIS IS SPECIFIC TO YOUR HOSTNAME, in my case the second digit marks the production server, PLEASE ADJUST as necessary
    local hostNameText=""
    hostNameText=$(echo "$hostName" | cut -c -2 | cut -c 2-)
    if [[ "$hostNameText" == "$HOSTNAMEID_PROD1" ]]; then
        prodServer=true
        updatePacbot=false
    fi
    if [[ "$hostNameText" == "$HOSTNAMEID_PROD2" ]]; then
        prodServer=true
        updatePacbot=false
    fi

    # Add option to allow user to manage updates on its own, disable automatic system update
    if [[ "$hostNameText" == "$HOSTNAMEID_USERMANAGED" ]]; then
        userManaged=true
        updatePacbot=false
    fi

    # Add option to allow user to manage updates on its own, but AUTO UPDATE PACBOT, disable automatic system update
    if [[ "$hostNameText" == "$HOSTNAMEID_USERMANAGED_UPDATEPACBOT" ]]; then
        userManaged=true
        updatePacbot=true
    fi

    # Check if system is ONLY an IMAGE (e.g. copy via RSYNC or btrfs send/receive)
    if [[ "$hostNameText" == "$HOSTNAMEID_IMAGE" ]]; then
        imageOnly=true
        updatePacbot=false
    fi
}

init_pc_state() {
    #x86_64 | aarch64
    machineArch="$(uname -m)"
    # Computer type desktop|laptop|vm|convertible
    computerType=$(hostnamectl | grep "Chassis:" | sed 's/         Chassis: *//' | awk '{print $1}')
    if [[ "$computerType" == "" ]] && [[ "$machineArch" == "aarch64" ]]; then
        computerType="desktop"
    elif [[ "$computerType" == "" ]]; then
        #BUG systemd???
        computerType="unknown"
    fi

    # Battery status if available
    local bat0=""
    bat0=$(cat /sys/class/power_supply/BAT0/status 2> /dev/null)
    local bat1=""
    bat1=$(cat /sys/class/power_supply/BAT1/status 2> /dev/null)
    if [[ "$bat0" == "Discharging" ]] || [[ "$bat1" == "Discharging" ]]; then
        onBattery=true
        powerText="battery$bat0"
    elif [[ "$bat0" == "Charging" ]] || [[ "$bat1" == "Charging" ]]; then
        onBattery=false
        powerText="battery$bat0"
    else
        onBattery=false
        powerText="power"
    fi

    # Virtual machine or real hardware
    statusVM=$(systemd-detect-virt)
}

check_preconditions() {
    # Check if all packages required to run this script are installed
    getMissingPackages missingPackage PACKAGES
    getMissingPackages missingPackageAur PACKAGESAUR

    checkArray emptyArray missingPackage
    if ( $emptyArray ); then
        missingPackage[0]="0"
    fi

    checkArray emptyArray missingPackageAur
    if ( $emptyArray ); then
        missingPackageAur[0]="0"
    fi
}

getMissingPackages() {
    local -n returnVariable=$1
    local -n packageArray=$2
    local count=0
    #Make sure array is emtpy, is this necessary?
    unset returnVariable
    for packageName in ${packageArray[*]}; do
        if [[ $statusVM != "none" ]] && [[ "$packageName" == "smartmontools" ]]; then
            continue
        elif [[ $statusVM != "none" ]] && [[ "$packageName" == "fwupd" ]]; then
            continue
        else
            packages_is_known
        fi
    done
}

packages_is_known() {
    #Check here with pacman -Si if package is known in remote database? Or in AUR with yay -Si?
    if ! pacman -Q "$packageName" &> /dev/null; then
        if pacman -Si "$packageName" &> /dev/null; then
            returnVariable[$count]="$packageName"
            ((count++))
        else
            #yay -Si shows packages available from AUR but pacman -Si is not!
            if pacman -Q "$aurHelper" &> /dev/null; then
                if "$aurHelper" -Si "$packageName" &> /dev/null; then
                    returnVariable[$count]="$packageName"
                    ((count++))
                else
                    echo ">>>>>>>>> WARNING unkown package $packageName <<<<<<<<<<"
                    unkownPackage["$packageName"]="$packageName"
                fi
            else
                "[  WARNING  ] AUR support not installed yet. Cannot check for missing packages."
            fi
        fi
    #else
    #    echo "[   INFO   ] Arch Linux package $packageName is already installed. Skipping!"
    fi
}

packages_is_known_slow() {
    #Check here with pacman -Si if package is known in remote database? Or in AUR with yay -Si?
    if pacman -Si "$packageName" &> /dev/null; then
        if ! pacman -Q "$packageName" &> /dev/null; then
            returnVariable[$count]="$packageName"
            ((count++))
        #else
        #    echo "[   INFO   ] Arch Linux package $packageName is already installed. Skipping!"
        fi
    else
    #yay -Si shows packages available from AUR but pacman -Si is not!
        if pacman -Q "$aurHelper" &> /dev/null; then
            if "$aurHelper" -Si "$packageName" &> /dev/null; then
                if ! pacman -Q "$packageName" &> /dev/null; then
                    returnVariable[$count]="$packageName"
                    ((count++))
                #else
                #    echo "[   INFO   ] AUR package $packageName is already installed. Skipping!"
                fi
            else
                echo ">>>>>>>>> WARNING unkown package $packageName <<<<<<<<<<"
                unkownPackage["$packageName"]="$packageName"
            fi
        else
            "[  WARNING  ] AUR support not installed yet. Cannot check for missing packages."
        fi
    fi
}

check_critical_services() {
    # Check if some critical packages are installed that may require restart after upgrade
    getCriticalServices runningCriticalServices CRITICALSERVICES
    checkArray emptyArray runningCriticalServices
    if ( $emptyArray ); then
        runningCriticalServices[0]="none"
    fi
}

getCriticalServices() {
    local -n returnVariable="$1"
    local -n serviceArray="$2"
    local count=0
    #Make sure array is emtpy, is this necessary?
    unset returnVariable
    for serviceName in ${serviceArray[*]}; do
        #systemctl status "$serviceName" &> /dev/null
        #if [[ "$?" == 0 ]]; then
        if systemctl status "$serviceName" &> /dev/null; then
            returnVariable[$count]=$serviceName
            # Only for short list of running services
            runningCriticalServicesShort[$count]=$(echo "$serviceName" | sed '$s/.service$//')
            ((count++))
        fi
    done
}

getInstalledPackages() {
    local -n returnVariable="$1"
    local -n packageArray="$2"
    local count=0
    #Make sure array is emtpy, is this necessary?
    unset returnVariable
    for packageName in ${packageArray[*]}; do
        #pacman -Q "$packageName" &> /dev/null
        #if [[ "$?" == 0 ]]; then
        if pacman -Q "$packageName" &> /dev/null; then
            returnVariable[$count]=$packageName
            ((count++))
        fi
    done
}

install_missingpackages() {
    # This will install the missing packages for this script
    checkArray emptyArray missingPackage
    if ( ! $emptyArray ); then
        if [[ "$missingPackage" != "0" ]]; then
            printText "Install missing packages (pacman -Sy ${missingPackage[*]})" 1 1 "v"
            #Do not add "" around missingPackage as it doesn't work as one string
            #sudo pacman -Sy --noconfirm ${missingPackage[*]}
            #As it is only one variable, "" are required
            pacmanSyNC "${missingPackage[*]}"
        fi
    fi

    local emptyArray2=false
    checkArray emptyArray2 missingPackageAur
    if ( ! $emptyArray2 ); then
        if [[ "$missingPackageAur" != "0" ]]; then
            printText "Install missing packages from AUR ($aurHelper ${missingPackageAur[*]})" 1 1 "v"
            for packageName in ${missingPackageAur[*]}; do
                sudo -u "$userName" "$aurHelper" -aS --noconfirm "$packageName"
            done
        fi
    fi
    if ( $emptyArray ) && ( $emptyArray2 ); then
        printText "0 missing packages" 1 2 "v"
    fi
}

pacmanSyNC() {
    local iPackageAll="$1"
    local reinstall="$2"
    if [[ "$reinstall" == "reinstall" ]]; then
        #Check for known packages
        getInstalledPackages installPackages iPackageAll
    else
        #Check for Packages that are currently NOT installed
        getMissingPackages installPackages iPackageAll
    fi
    checkArray emptyArray installPackages
    if ( ! $emptyArray ); then
        #Do not add "" to variable as otherwise this is recognized as one string
        #echo "------ ${installPackages[*]} -------"
        sudo pacman -Sy --noconfirm ${installPackages[*]}
    #else
        #echo "Packages are installed already"
    fi
    checkArray emptyArray unkownPackage
    if ( ! $emptyArray ); then
        echo "[ Warning ] Unkown packages: ${unkownPackage[*]}"
    fi
}

pacmanRscNC() {
    local rscPackagesAll="$1"
    unset rscPackages
    getInstalledPackages rscPackages rscPackagesAll
    checkArray emptyArray rscPackages
    if ( ! $emptyArray ); then
        sudo pacman -Rsc --noconfirm ${rscPackages[*]}
    #else
    #    echo "No package is installed => Nothing to uninstall"
    fi
}

checkCommandOutput() {
    local -n returnVariable="$1"
    if [ "$2" == 0 ]; then
        returnVariable="$3"
    else
        returnVariable="$4"
    fi
}

printText() {
    #echo does automatically a line break after text output. It can be removed with echo -n
    #printf and then echo makes a line break before echo
    #echo and then printf makes a line break before printf
    #To combine the output correctly is too complicated, remove therefore echo from this output function!
    local text="$1"
    local lineBreakBefore="$2"
    local lineBreakAfter="$3"
    local outputType="$4"
    local verboseOutput=false
    local journalScriptNameFilter="${scriptName}-$outputType"
    local color=7
    if [[ "$outputType" == "v" ]]; then
        verboseOutput=true
    fi
    #Write non-verboose output to log file
    if ( ! $verboseOutput ); then
        printf "\\n"
        printf "%s" "$text" | systemd-cat -t "$journalScriptNameFilter"
        #WARNING This creates a desktop notification file
        notifyUserDesktop
    fi
    #Show all output to user
    if ( ! $runAsService ); then
        if ( $verboseOutput ); then
            color=3
        fi
        for (( c=1; c<="$lineBreakBefore"; c++ )); do
            printf "\\n"
        done
        printf "%s: %s" "$journalScriptNameFilter" "$(tput setaf $color)$text$(tput sgr0)"
        for (( c=1; c<="$lineBreakAfter"; c++ )); do
            printf "\\n"
        done
    fi
}

notifyUserDesktopNewFile() {
    local folder="/home/$userName/Desktop"
    local file="/home/$userName/Desktop/~updating"
    local file2="/home/$userName/Desktop/Update-OK"
    mkdir -p "$folder" &> /dev/null
    chown -R "$userName:$userName" "$folder" &> /dev/null
    chmod 777 "$folder" &> /dev/null
    #echo "" | tee "$file" &> /dev/null
    mv "$file2" "$file" &> /dev/null
    chown "$userName:$userName" "$file" &> /dev/null
    chmod 777 "$file" &> /dev/null
    echo "--------------------- $(date +%Y-%m-%d:_%H:%M) -------------------------" | tee -a "$file" &> /dev/null
    #rm "$file2" &> /dev/null
}

notifyUserDesktop() {
    if [[ "$scriptName" == "pacbot" ]]; then
        local file="/home/$userName/Desktop/~updating"
        echo "$text" | tee -a "$file" &> /dev/null
        #echo "text" | sh /home/rainer/matrix.sh/matrix.sh --homeserver=https://matrix.org --token=xxx --room=\!xxx:matrix.org -
        #echo "$text" | sh /home/rainer/matrix.sh/matrix.sh --homeserver=https://matrix.org --token=syt_cmY_WIOCcuHnuzdhhXOqALHz_4BQYIy --room=\!aBAQPnFDljnyFzmJzj:matrix.org -
    fi
}

notifyUserDesktopDone() {
    printText "/\ Ciao" 1 2 "$outputType"

    local file="/home/$userName/Desktop/~updating"
    local file2="/home/$userName/Desktop/Update-OK"
    mv "$file" "$file2" &> /dev/null
}

install_aur() {
    # Install AUR support or package
    local aurPackage=$1
    #The source section allows you to install as well from third parties on your own risk
    local source=$2
    if [[ "$2" == "" ]]; then
        source="https://aur.archlinux.org/${aurPackage}.git"
    else
        read -r -p "$(tput setaf $color)ARE YOU SURE YOU WANT TO INSTALL A PACKAGE FROM THIS UNKOWN SOURCE [$soure]?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            source="${source}/${aurPackage}.git"
        else
            exit
        fi
    fi
    sudo pacman -S --noconfirm --needed base-devel

    if ! pacman -Q $aurPackage &> /dev/null; then
        local instDir="/tmp/pacbot-$aurPackage-tmp"
        sudo -u nobody mkdir "$instDir"
        if sudo -u nobody bash -c "[[ -w $instDir ]]"; then
            cd "$instDir" || exit
            sudo -u nobody git clone "$source"
            cd "$aurPackage" || exit
            sudo -u nobody makepkg -fc --noconfirm
            #pacman -U "$(sudo -u nobody makepkg --packagelist)" --noconfirm
            sudo pacman -U --noconfirm $(sudo -u nobody makepkg --packagelist)
        fi

        install_aur_cleanup "nobody"
    else
        echo "$aurPackage is already installed"
    fi
}

pacbot_systemd_timer() {
    local systemdServiceName=$1
    if [[ "$systemdServiceName" == "daily" ]]; then
        sudo systemctl enable pacbot-daily.timer
    elif [[ "$systemdServiceName" == "weekly" ]]; then
        sudo systemctl enable pacbot-weekly.timer
    else
        exit
    fi
    sudo systemctl restart timers.target
}

show_timers() {
    systemctl list-timers | grep ""
}

show_development_version() {
    if [[ "$pkgPath" == "." ]]; then
        scriptVersion="${scriptVersion}${pkgPath}DEV"
    fi
}

kexec_reboot() {
    if pacman -Q kexec-tools &> /dev/null; then
        #In a VM rebooting with Bios/UEFI is quite fast, no need for now to use kexec
        if [[ $statusVM == "none" ]]; then
            #Only kexec -l is required if booting to another kernel, but also if there are multiple entries (bug) :-(
            echo "Fast reboot available, using kexec now :-)"
            local kernel=$(uname -r)
            #For the standard kernel this might be unreliable as of something like arch1-1
            local linux=$(uname -r | rev | cut -c 4- | cut -c -4 | rev)
            local linuxHardened=$(uname -r | rev | cut -c -8 | rev)
            local linuxLts=$(uname -r | rev | cut -c -3 | rev)
            if [[ "$linux" == "arch" ]]; then
                sudo kexec -l /boot/vmlinuz-linux --initrd=/boot/initramfs-linux.img --reuse-cmdline
                systemctl_kexec
            elif [[ "$linuxHardened" == "hardened" ]]; then
                #sudo kexec -l /boot/vmlinuz-linux-hardened --initrd=/boot/initramfs-linux-hardened.img --reuse-cmdline
                #systemctl_kexec
                echo "Kernel $kernel doesn't support kexec due to security restrictions, executing normal reboot!"
                systemctl_reboot
            elif [[ "$linuxLts" == "lts" ]]; then
                sudo kexec -l /boot/vmlinuz-linux-lts --initrd=/boot/initramfs-linux-lts.img --reuse-cmdline
                systemctl_kexec
            else
                echo "Kernel $kernel unkown, executing normal reboot!"
                systemctl_reboot
            fi
        else
            echo "Inside of a $statusVM, executing normal reboot!"
            systemctl_reboot
        fi
    else
        echo "Package kexec is not installed, executing normal reboot!"
        systemctl_reboot
    fi
}

systemctl_kexec() {
        read -r -p "Do you want to $(tput setaf $color)fast reboot system$(tput sgr0) skipping the UEFI/BIOS?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            #sudo systemctl kexec
            reboot_check_boot "kexec"
        else
            systemctl_reboot
        fi
}

systemctl_reboot() {
        read -r -p "Do you want to $(tput setaf $color)reboot system$(tput sgr0)?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            #systemctl reboot
            reboot_check_boot "reboot"
        fi
}

systemctl_reboot_firmware() {
        read -r -p "Do you want to $(tput setaf $color)reboot to UEFI/Bios$(tput sgr0)?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            #systemctl reboot --firmware-setup
            reboot_check_boot "reboot --firmware-setup"
        fi
}

sudo_loop() {
    #Use a sudo loop to avoid that the user has to type in the password again for sudo if it times out e.g. during a long complilation?
    #https://gitlab.com/postmarketOS/pmbootstrap/-/issues/1677
    #while true; do sudo -v >> /dev/null; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
    echo "Not tested yet"
}

reboot_check_boot() {
    local rebootCmd="$1"
    local bootFiles=true

    boot_files_exists "linux"
    boot_files_exists "linux-hardened"
    boot_files_exists "linux-lts"
    boot_files_exists "linux-libre"
    boot_files_exists "amd-ucode"
    boot_files_exists "intel-ucode"

    if ( ! $bootFiles ); then
        echo "[    ERROR   ] Fatal error, do not try to boot without first installing the required boot files!"
        read -r -p "Do you want to $(tput setaf $color)reinstall linux incl. ucode and verifiy installed packages$(tput sgr0)?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            kernel_ucode_reinstallation_and_verification
        else
            echo "[  FATAL ERROR  ] Missing boot files might require a $(tput setaf $color)LIVE-USB stick$(tput sgr0) and $(tput setaf $color)arch-chroot$(tput sgr0) to make the system work again!"
        fi
    else
        if [[ "$rebootCmd" != "" ]]; then
            #NOTE Do not use quotes for $rebootCmd!
            systemctl $rebootCmd
        fi
    fi
}

boot_files_exists() {
    local pkg1="$1"
    if pacman -Q "$pkg1" &> /dev/null; then
        if [[ "$pkg1" == "linux" ]] || [[ "$pkg1" == "linux-hardened" ]] || [[ "$pkg1" == "linux-lts" ]] || [[ "$pkg1" == "linux-libre" ]]; then

            if [[ $(ls -alh "/boot/vmlinuz-$pkg1" | awk '{ print $9 }') != "/boot/vmlinuz-$pkg1" ]]; then
                echo "ERROR - linux is installed but vmlinuz-$pkg1 file cannot be found in /boot !!!"
                bootFiles=false
            fi

            local pkg2="initramfs-$pkg1-fallback"
            boot_files_img_exists

            local pkg2="initramfs-$pkg1"
        else
            local pkg2="$pkg1"
            boot_files_img_exists
        fi
    fi
}

boot_files_img_exists() {
    if [[ $(ls -alh "/boot/$pkg2.img" | awk '{ print $9 }') != "/boot/$pkg2.img" ]]; then
        echo "[    ERROR   ] $pkg1 is installed but $pkg2.img file cannot be found in boot !!!"
        bootFiles=false
    fi
}

kernel_ucode_reinstallation_and_verification() {
    if [[ "$EUID" -ne 0 ]]; then
        local installCmd="sudo pacman -Sy  --noconfirm"
    else
        local installCmd="pacman -Sy  --noconfirm"
    fi
    #xz is essential, it can break as well if the disk is full, then linux cannot create initrd
    pacmanSyNC "linux linux-hardened linux-libre linux-lts amd-ucode intel-ucode xz" "reinstall"
    if [ $? == 0 ]; then
        echo "$(tput setaf 2)[    OK    ]$(tput sgr0) Rebooting linux should work! Except for broken packages like systemd. They will be verified now just to make sure noting went wrong (especialy on btrfs due to possible incorrect file size checks)"
        #BUG Getting this parth doesn't work because $pkgPath is somehow empty, but pacbot function is already loaded. So not an issue?
        #source "$pkgPath/pacbot.sh" "--source-only"
        verify_installedpackages_manual
    else
        echo "$(tput setaf 1)[    ERROR    ]$(tput sgr0) Please check MANUALLY if <initramfs-linux.img> and <vmlinuz-linux> exists in /boot partition. Fatal ERROR !!! "
    fi
}

packages_installed_show_size() {
    echo "----------PACMAN----------"
    pacman_packages_installed_show_size
    echo "---------FLATPAK---------"
    flatpak_packages_installed_show_size
}

pacman_packages_installed_show_size() {
    if ! pacman -Q "expac" &> /dev/null; then
        echo "[  Missing Package  ] Installing expac now..."
        pacmanSyNC "expac"
    fi
    expac -s "%-30n %m" | sort -hk 2 | awk ' {size=$2/1024;print $1 " ",size,"MB"}'
}

flatpak_repositories_add() {
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    #Not required anymore?
    #sudo flatpak remote-add --if-not-exists kdeapps --from https://distribute.kde.org/kdeapps.flatpakrepo
}

flatpak_packages_privacy() {
    #Gnome/Gtk dependencies
    flatpak install --noninteractive io.gitlab.librewolf-community
    #Tor not available on aarch64?
}

flatpak_packages_privacy_tor() {
    if [[ "$machineArch" != "aarch64" ]]; then
        #Qt5 dependencies
        flatpak install --noninteractive com.github.micahflee.torbrowser-launcher
    fi
}

flatpak_packages_messenger() {
    #KDE/Qt 6.5 dependencies
    flatpak install --noninteractive org.telegram.desktop
    flatpak_packages_messenger3
}


flatpak_packages_messenger2() {
    #KDE/Qt 6.5 dependencies
    flatpak install --noninteractive net.jami.Jami
}

flatpak_packages_messenger3() {
    if [[ "$machineArch" != "aarch64" ]]; then
        #Element not available for aarch64? gtk
        flatpak install --noninteractive im.riot.Riot
    fi
}

flatpak_packages_desktop() {
    #Use flaptak packages instead of AUR as they can be updated with less user interaction
    flatpak install --noninteractive org.mozilla.Thunderbird

    #Flatpak app permission manager. Use Plasma's flatpak-kcm instead
    #flatpak install --noninteractive com.github.tchx84.Flatseal

    #Gnome 43 dependencies
    flatpak install --noninteractive me.kozec.syncthingtk
}

flatpak_packages_android() {
    #Connect to Android device with USB-C cable and output on screen (Qt+KDE)
    flatpak install --noninteractive in.srev.guiscrcpy
}

flatpak_packages_proton() {
    flatpak install --noninteractive org.winehq.Wine

    flatpak install --noninteractive org.winehq.Wine.DLLs.dxvk

    flatpak install --noninteractive com.valvesoftware.Steam.CompatibilityTool.Proton
    flatpak install --noninteractive com.valvesoftware.Steam.CompatibilityTool.Proton-Exp
}

flatpak_packages_dosbox() {
    flatpak install --noninteractive io.github.dosbox-staging
}

flatpak_packages_emulation() {
    #Use Emudeck instead of own installer?
    #https://www.emudeck.com

    #KDE/Qt 6.5 dependencies
    echo "Dolphin-Emu - Gamecube/Wii (Vulkan Game Emulator"
    flatpak install --noninteractive org.DolphinEmu.dolphin-emu
    #Dolpin-Emu fork for Metroid Prime triology
    flatpak install --noninteractive io.github.shiiion.primehack
    echo "RPCS3 - Playstation 3 (Vulkan) Game Emulator"
    flatpak install --noninteractive net.rpcs3.RPCS3
    echo "PCSX2 - Playstation 2 Game Emulator"
    flatpak install --noninteractive net.pcsx2.PCSX2
    echo "RPCS3 - Playstation 1 Game Emulator"
    flatpak install --noninteractive org.duckstation.DuckStation
    echo "Citra - Nintento 3DS Game Emulator (Vulkan)"
    flatpak install --noninteractive org.citra_emu.citra

    #KDE/Qt 5.15 dependencies
    echo "Yuzu - Nintendo Switch (Vulkan) Game Emulator"
    flatpak install --noninteractive org.yuzu_emu.yuzu
    echo "MelonDS - Game Emulator for Nintendo DS"
    flatpak install --noninteractive net.kuribo64.melonDS

    #?
    echo "PPSSPP - Playstation Portable (Vulkan) Game Emulator"
    flatpak install --noninteractive org.ppsspp.PPSSPP

    #?
    echo "MelonDS - Nintendo Switch Game Emulator"
    flatpak install --noninteractive org.ryujinx.Ryujinx

    #Use Flatpak???? org.libretro.RetroArch
}

#flatpak_packages_game() {
    #flatpak install --noninteractive com.github.Matoking.protontricks

    #flatpak install --noninteractive com.valvesoftware.Steam.Utility.gamescope

    #flatpak install --noninteractive org.freedesktop.Platform.VulkanLayer.MangoHud

    #flatpak install --noninteractive org.freedesktop.Platform.VulkanLayer.vkBasalt

    #flatpak install --noninteractive com.valvesoftware.Steam.Utility.steamtinkerlaunch

    #flatpak install --noninteractive io.github.dosbox-staging

    #flatpak install --noninteractive org.openrgb.OpenRGB

    #flatpak install --noninteractive com.valvesoftware.Steam.CompatibilityTool.Proton-GE

#    flatpak_packages_game2
#}

#flatpak_packages_game2() {
    #Athenaeum => Use and activate Flathub in Lutris
    #echo "Athenaeum Open Source Game Library - Uses outdated KDE packages!"
    #flatpak install --noninteractive com.gitlab.librebob.Athenaeum

    #ProtonUp-Qt
    #echo "ProtonUp-Qt - Install Proton Addons like GE-Proton/SteamTinkerLaunch/Luxtorpeda/Roberta/Boxtron"
    #flatpak install --noninteractive net.davidotek.pupgui2

    #Heroic
    #echo "Heroic Game Launcher - GOG/Epic"
    #flatpak install --noninteractive com.heroicgameslauncher.hgl
    #yay -S --noconfirm heroic-games-launcher-bin

    #Gnome 44 dependencies - Lutris - Requires python-protobuf for Battle.net -> Will it be provided in Flatpak?
    #echo "Lutris Game Launcher - Flathub/Itch.io/HumbleBundle/Battle.net/Origin/Ubisoft/Amazon/Steam/GOG/Epic/Steam"
    #flatpak install --noninteractive net.lutris.Lutris

    #Gnome 44 dependencies - Bottles Wine manager (gtk4)
    #flatpak install --noninteractive com.usebottles.bottles
#}

flatpak_update_show() {
    flatpak remote-ls --updates --all
}

flatpak_update_download() {
    flatpak update --no-deploy --assumeyes
}

flatpak_update() {
    flatpak update --noninteractive --assumeyes
}

flatpak_packages_installed_show_size() {
    flatpak --columns=app,size,name,branch,version list | sort -hk 2
}

flatpak_uninstall_unused() {
    flatpak uninstall --unused -y
}

flatpak_cache_clean() {
    #BUG https://github.com/flatpak/flatpak/issues/1119
    bash -c "! pgrep -x flatpak && rm -r /var/tmp/flatpak-cache-*"
}

flatpak_verify_repair() {
    flatpak repair
}

####################################
######### ARCHBOT UPDATER ##########
####################################

archbot_config_updater() {
    local file="/etc/archinstallbot.conf"
    local oldVersion=$(echo $pacbotVersion | cut -f1 -d '-')
    if [[ "$oldVersion" == "" ]]; then
        local pacbotConfigVersionRelease="21-1"
        echo "export pacbotVersion=$pacbotConfigVersionRelease" | tee -a "$file"
        echo "[  FIX  ] pacbotVersion was empty in $file"
        oldVersion=$(echo $pacbotConfigVersionRelease | cut -f1 -d '-')
    else
        echo "[  Info  ] pacbotVersion was $oldVersion from $file"
    fi
    local newVersion=$(pacman -Q pacbot | awk '{ print $2 }' | cut -f1 -d '-')
    if ( ! $userManaged ); then
        #while $do || conditions; do
        while (( $oldVersion < $newVersion )); do
            #BUG no minor releases used, but wouldn't work with with this code
            archbot_auto_upgrade
        done
    fi
}

archbot_auto_upgrade() {
    #Get Config/Setup functions from Archbot
    source "$pkgPath/archbot.sh" "--source-only"
    echo "------------ CHECKING SYSTEM CONFIG BASED ON PACBOT $oldVersion ---------"

    if [[ "$oldVersion" == "29" ]]; then
        echo "------------ UPDATING SYSTEM CONFIG BASED ON PACBOT $oldVersion ---------"

        local file="/etc/archinstallbot.conf"
        config_replacevalue "installed=true" "installedByArchinstallbot=true" "$file"
        #WARNING Do not use ${HOME} here, it would be the wrong user root instead of a normal user
        mkdir -p /home/$userName/.config/autostart-scripts/
        ln -sf "/opt/pacbot/sweeper.sh" "/home/$userName/.config/autostart-scripts/"
        rm "/home/$userName/.config/autostart/kde-sweeper.sh.desktop"
        rm "/home/$userName/.local/bin/kde-sweeper.sh"

        if pacman -Q "plymouth" &> /dev/null || pacman -Q "plymouth-git" &> /dev/null; then
            if pacman -Q plymouth-git &> /dev/null; then
                #Install plymouth-git as recommended in the Wiki
                pacmanSyNC "plymouth"
            fi
            local file="/etc/mkinitcpio.conf"
            config_replacevalue "sd-plymouth" "plymouth" "$file"
            if pacman -Q "linux" &> /dev/null; then
                sudo mkinitcpio -p "linux"
            elif pacman -Q "linux-hardened" &> /dev/null; then
                sudo mkinitcpio -p "linux-hardened"
            elif pacman -Q "linux-libre" &> /dev/null; then
                sudo mkinitcpio -p "linux-libre"
            elif pacman -Q "linux-lts" &> /dev/null; then
                sudo mkinitcpio -p "linux-lts"
            fi
            sudo plymouth-set-default-theme -R bgrt
        fi

        #SDDM mainline supports Wayland now
        if pacman -Q "sddm-git" &> /dev/null; then
            yes | sudo pacman -Sy --noconfirm sddm
        fi

        if pacman -Q "opencl-clover-mesa" &> /dev/null; then
            pacmanSyNC "opencl-rusticl-mesa"
        fi

        if pacman -Q "radeon-profile-daemon-git" &> /dev/null; then
            sudo systemctl stop radeon-profile-daemon.service
            sudo systemctl disable radeon-profile-daemon.service
            rm "${HOME}/.config/autostart/radeon-profile.desktop"
            #pacmanSyNC "corectrl"
        fi

        #Haveged is obsolete since kernel 5.4
        sudo systemctl disable haveged.service
        sudo systemctl stop haveged.service

        if ( ! $prodServer ); then
            if ! pacman -Q "jamesdsp" &> /dev/null; then
                yay -Sy --noconfirm jamesdsp
            fi
            if ! pacman -Q "noisetorch" &> /dev/null; then
                yay -Sy --noconfirm noisetorch
            fi
        fi

        pacmanRscNC "iotop bpytop neofetch edac-utils systemd-kcm khotkeys ksysguard plasma-wayland-session mesa-demos autoupgrader-git steam-fast-login latte-dock memtest86-efi plasma-thunderbolt plasma-sdk haveged pulseaudio-equalizer-ladspa easyeffects lsp-plugins pulseaudio-bluetooth opencl-mesa opencl-clover-mesa radeon-profile-daemon-git plymouth-theme-arch-breeze"
        pacmanSyNC "fastfetch btop mesa-utils base-devel"
        config_replacevalue "neofetch" "fastfetch" "${HOME}/.bashrc"

        #BUG Workaround, used as well in archinstallbot to install linux
        #https://gitlab.archlinux.org/archlinux/mkinitcpio/mkinitcpio/-/issues/238
        local file="/etc/mkinitcpio.d/linux.preset"
        config_setvalue "fallback_options=" "\"-S autodetect -S kms\"" "$file"
        local file="/etc/mkinitcpio.d/linux-lts.preset"
        config_setvalue "fallback_options=" "\"-S autodetect -S kms\"" "$file"
        local file="/etc/mkinitcpio.d/linux-hardened.preset"
        config_setvalue "fallback_options=" "\"-S autodetect -S kms\"" "$file"
        local file="/etc/mkinitcpio.d/linux-zen.preset"
        config_setvalue "fallback_options=" "\"-S autodetect -S kms\"" "$file"
        #Export is not allowed anymore in /etc/environment
        local file="/etc/environment"
        config_setvalue "export " "" "$file"
        config_setvalue "#export " "#" "$file"

        flatpak uninstall --assumeyes com.github.tchx84.Flatseal
    fi

    ((oldVersion++))
    local file="/etc/archinstallbot.conf"
    echo "---------- UPDATED SYSTEM CONFIG TO $oldVersion in $file ----------"
    config_setvalue "export pacbotVersion=" "${oldVersion}-1" "$file"
}
