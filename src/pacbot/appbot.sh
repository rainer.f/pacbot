#!/bin/bash
# Package & Update/Upgrade Manager
# PureOS (Debian, Ubuntu, Mint), PostmarketOS (Alpine)

export pkgPath="."
#export pkgPath="/opt/pacbot"

targetVendor=""
targetDevice=""
scriptName="appbot"

####################################
########## Init Functions ##########
####################################

source_dependencies_appbot() {
	#Get global user settings and dependencies
    #source "$pkgPath/settings.sh"
	source "$pkgPath/helpers.sh"
	#source "$pkgPath/systemchecks.sh"
	source "$pkgPath/interface.sh"
	#source "$pkgPath/usbstickbot.sh" "--source-only"
    #source "$pkgPath/archinstallbot.sh" "--source-only"
    #source "$pkgPath/sysinfo.sh"
	###### PACBOT ERROR MUST RUN AS ROOT, BUG #########
	#source "$pkgPath/pacbot.sh" "--source-only"
}

appbot_init() {
    source_dependencies_appbot
    init_pc_state #helper.sh
    init_distro
    init_sudo_command
    init_pac_commands
}

init_distro() {
    #pac is short for package manager in honor of pacman
    export pac=""
    export pacInstall=""
    export pacUpdates=""
    export pacUpgrade=""
    export pacCleanCache=""
    export pacVerifyRepair=""
    export pacRemoveObsolete=""
    export pacUninstall=""
    export pacSearch=""
    export pacInstalled=""
    export distro="$(cat /etc/os-release | grep "ID" | awk 'NR==1' | cut -c 4-)"
    distroFamilyLoc=""
    export systemManager="systemd"
    export fastfetch="fastfetch"

    if [[ "$distro" == "arch" ]]; then
        distroFamilyLoc="arch"
    elif [[ "$distro" == "postmarketos" ]]; then
        distroFamilyLoc="alpine"
        systemManager="rc"
    elif [[ "$distro" == "pureos" ]]; then
        distroFamilyLoc="debian"
        systemManager="rc"
    elif [[ "$distro" == "debian" ]]; then
        distroFamilyLoc="debian"
    elif [[ "$distro" == "ubuntu" ]]; then
        distroFamilyLoc="debian"
    elif [[ "$distro" == "opensuse" ]]; then
        distroFamilyLoc="suse"
    fi

    if [[ "$distroFamilyLoc" == "arch" ]]; then
        pac="pacman"
        pacInstall="$pac -Sy"
        pacUpdates="$pac -Su"
        pacUpgrade="$pac -Syu"
        pacCleanCache="$pac -Scc"
        pacVerifyRepair="echo \"pacman cannot repair itself and requires external tool paccheck\""
        pacRemoveObsolete="echo unkown command"
        pacUninstall="$pac -Rsc"
        pacSearch="$pac -Ss"
        pacInstalled="$pac -Q"
    elif [[ "$distroFamilyLoc" == "alpine" ]]; then
        pac="apk"
        pacInstall="$pac add"
        pacUpdates="$pac update"
        pacUpgrade="$pac upgrade -a"
        pacCleanCache="rm /var/cache/apk/*"
        pacVerifyRepair="$pac fix"
        pacRemoveObsolete="echo unkown command"
        pacUninstall="$pac del"
        pacSearch="$pac search"
        pacInstalled="$pac info"
    elif [[ "$distroFamilyLoc" == "debian" ]]; then
        pac="apt"
        pacInstall="$pac install"
        pacUpdates="$pac update"
        pacUpgrade="$pac upgrade -a"
        pacCleanCache="echo unknown command"
        pacVerifyRepair="echo unknown comman"
        pacRemoveObsolete="apt autoremove"
        pacUninstall="$pac del"
        pacSearch="$pac search"
        pacInstalled="$pac info"
        fastfetch="neofetch"
    elif [[ "$distroFamilyLoc" == "suse" ]]; then
        pac="zypper"
        pacInstall="$pac install"
        pacUpdates="$pac update"
        pacUpgrade="$pac dist-upgrade"
        pacCleanCache="echo unknown command"
        pacVerifyRepair="echo unknown comman"
        pacRemoveObsolete="apt autoremove"
        pacUninstall="$pac del"
        pacSearch="$pac search"
        pacInstalled="echo unkown command"
    else
        echo "[ ERROR ] unkown distro family for <$distro>"
        exit
    fi
}

init_pac_commands() {
    if [[ "$EUID" -ne 0 ]] && [[ "$sudoCmd" == "" ]] ; then
        echo "[ ERROR ] Cannot continue with normal user and empty sudo command. Fatal Error!!!"
        exit
    else
        pacInstall="$sudoCmd $pacInstall"
        pacUpdates="$sudoCmd $pacUpdates"
        pacUpgrade="$sudoCmd $pacUpgrade"
        pacCleanCache="$sudoCmd $pacCleanCache"
        pacVerifyRepair="$sudoCmd $pacVerifyRepair"
        pacRemoveObsolete="$sudoCmd $pacRemoveObsolete"
        pacUninstall="$sudoCmd $pacUninstall"
    fi
}


init_sudo_command() {
    #As sudo means su do or doas run0 so is tuas just translated
    export sudoCmd=""
    if [[ "$EUID" -eq 0 ]]; then
        #root => empty skip sudo
        sudoCmd=""
    else
        #normal user
        if $pacInstalled "sudo" &> /dev/null; then
            sudoCmd="sudo"
        elif $pacInstalled "doas" &> /dev/null; then
            sudoCmd="doas"
        elif $pacInstalled "systemd" &> /dev/null; then
            sudoCmd="run0"
        else
            #echo "[ ERROR ] No sudo command available"
            #Run all as root?
            sudoCmd="su -c"
        fi
    fi
}

####################################
########## Package Manager #########
####################################

appbot_install() {
    $pacInstall git
    git clone "https://gitlab.com/rainer.f/pacbot.git"
}

pac_upgrade_show() {
    $pacUpdates
}

pac_upgrade_system() {
    #sudo $pac update
    $pacUpgrade
    #pac_verifiy_repair
    #$sudoCmd pmos-update-kernel postmarketos-qcom-sdm845
    echo "Updating Flatpkak packages..."
    flatpak update
    #flatpak update --noninteractive --assumeyes
    #echo "$(flatpak update --noninteractive --assumeyes | awk '{ print $1 }')"
    echo "Flatpkak are up-to-date"
    #flatpak_uninstall_unused
}

pac_remove_lock() {
    echo "[  info  ] remove apk lock not implemented"
}

pac_verifiy_repair() {
    $pacVerifyRepair
}

pac_cache_clean() {
    $pacCleanCache
}

pac_remove_notrequired() {
    $pacRemoveObsolete
    #Config files deletion
    $sudoCmd aptitude purge ~c
}

fetch_warnings() {
    echo ""
}

pac_bad_signature_fix() {
    local package="$"
    if [[ "$distro" == "postmarketos" ]]; then
        curl -X PURGE https://dl-cdn.alpinelinux.org/alpine/edge/testing/aarch64/$package.apk
    fi
}

pac_upgrade_system_status() {
    echo "[  info  ]  upgrade system status not implemented"
}

####################################
############## Service #############
####################################

systemManager_enable_start() {
    local service="$1"
    if [[ "$service" == "" ]]; then
        echo "ERROR - Please provide a service to run this function"
        exit
    fi
    if [[ "$systemManager" == "systemd" ]]; then
        $sudoCmd systemctl enable "$service".service
        $sudoCmd systemctl start "$service".service
    elif [[ "$systemManager" == "rc" ]]; then
        $sudoCmd rc-update add "$service" default
        $sudoCmd rc-service "$service" start
    fi
}

systemManager_disable_stop() {
    local service="$1"
    if [[ "$service" == "" ]]; then
        echo "ERROR - Please provide a service to run this function"
        exit
    fi
    if [[ "$systemManager" == "systemd" ]]; then
        $sudoCmd systemctl disable "$service".service
        $sudoCmd systemctl stop "$service".service
    elif [[ "$systemManager" == "rc" ]]; then
        $sudoCmd rc-update delete "$service"
        $sudoCmd rc-service "$service" stop
    fi
}

####################################
############## TESTING #############
####################################

tinydm_switch_session() {
    local session="$1"
    tinydm_change_session "$session"
    tinydm_restart
}

tinydm_restart() {
    $sudoCmd rc-service tinydm restart
}

tinydm_logs() {
    more ${XDG_STATE_HOME:-~/.local/state}/tinydm.log
}

tinydm_change_session(){
    local sessionType="$1"
    #https://wiki.postmarketos.org/wiki/Configure_postmarketOS_for_multiple_UIs_or_users
    #$sudoCmd tinydm-set-session -f -s /usr/share/wayland-sessions/session.desktop
    #$sudoCmd tinydm-set-session -f -s /usr/share/xsessions/session.desktop
    $sudoCmd tinydm-set-session -f -s /usr/share/$sessionType/session.desktop
}

qt6_fastpath() {
    $sudoCmd apk del angelfish
}

####################################
########### Applications ###########
####################################

wireless_iwd_install() {
    local file="/etc/NetworkManager/conf.d/wifi_backend.conf"
    echo "[device]
wifi.backend=iwd" | $sudoCmd tee "$file" &> /dev/null

    echo "Install iwd for wlan: Enable in /etc/NetworkManager/conf.d/wifi_backend.conf iwd"
    if ! $pacInstalled iwd &> /dev/null; then
        $pacInstall "iwd"
    fi
    systemManager_disable_stop "wpa_supplicant"
    systemManager_enable_start "iwd"

    echo "Please set up IWD by configuring a Wifi connections!"
}

frpd_install() {
    $pacInstall frp
    local file="/etc/frp/frpc.toml"
    config_setvalue "serverAddr = " "\"$serverTarget\"" "$file"
    echo "
[[proxies]]
name = \"${hostName}-ssh\"
type = \"tcp\"
localIP = \"127.0.0.1\"
localPort = 22
remotePort = $serverForwardPort" | tee -a "$file"
    systemManager_enable_start "frpc"
}

####################################
############## pmosbot #############
####################################

packages_base_install() {
    #postmarketos-update-kernel is required as otherwise you cannot update the kernel directly on the device without reflashing boot
    $pacInstall postmarketos-update-kernel htop $fastfetch git dialog nano flatpak
    #Not available anymore??? bpytop
}

plasma_mobile_install() {
    #If e.g. phosh or a shell only was installed, add plasma-mobile later
    $pacInstall plasma-phone-components
}

phosh_mobile_install() {
    $pacInstall postmarketos-ui-phosh phosh-mobile-settings
}

packages_mobile_install() {
    # phonebook weather
    #Video Acceleration: libva-utils for vainfo; vdpauinfo
    $pacUninstall firefox-esr
    $pacInstall discover-backend-apk discover-backend-flatpak ksysguard calindori kamoso elisa vvave pix okular kweather neochat mesa mesa-utils mesa-vulkan-swrast vulkan-tools mesa-vulkan-freedreno mesa-rusticl mesa-va-gallium mesa-vdpau-gallium libva-utils vdpauinfo nextcloud-client element-desktop
    if [[ "$device" == "librem5" ]]; then
        $pacInstall megapixels-purism
    fi
}

pmos_init_step1() {
    wireless_iwd_install
    install_packages_base
    install_packages_mobile
}

uboot_update(){
    #https://wiki.postmarketos.org/wiki/Purism_Librem5_(purism-librem5)
    if [[ "$device" == "librem5" ]]; then
        update-u-boot -n
        $sudoCmd update-u-boot
    fi
}

flatpak(){
    flatpak_repositories_add
    flatpak install --noninteractive com.github.tchx84.Flatseal
    flatpak_packages_privacy
    flatpak install --noninteractive org.libreoffice.LibreOffice
    flatpak_packages_messenger
    flatpak_packages_messenger2
}

####################################
############ MAINTENANCE ###########
####################################

ssh_connect() {
    ip a
    echo "Please connect a USB cable between the computer and the phone to connect with SSH!"
    ssh 172.16.42.1
}

wifi_scan_connect() {
    #From terminal if there is no gui
    $sudoCmd nmcli device wifi list ifname wlan0
    read -r -p "$(tput setaf $color)To connect to a WiFi network, enter the name of the $(tput setaf 2)SSID$(tput sgr0):"
    if [[ "$REPLY" != "" ]] && [[ "$REPLY" != " " ]]; then
        #$sudoCmd nmcli device wifi connect "$SSID" password "$PASSWORD" ifname "wlan0"
        $sudoCmd nmcli device wifi connect "$SSID" ifname "wlan0"
    else
        echo "Please enter a SSID"
    fi

}

share_network_host() {
    #First add fixed ip address to interal device e.g. usb network

    #https://wiki.archlinux.org/title/Internet_sharing#With_firewalld
    #One Time Setup, define internal & exteral device!
    ip a
    firewall-cmd --zone=external --change-interface=XXXXXX --permanent
    firewall-cmd --zone=internal --change-interface=YYYYYY --permanent
    #Enable NAT to forward traffic from internal to external zone
    $sudoCmd firewall-cmd --permanent --new-policy int2ext
    $sudoCmd firewall-cmd --permanent --policy int2ext --add-ingress-zone internal
    $sudoCmd firewall-cmd --permanent --policy int2ext --add-egress-zone external
    $sudoCmd firewall-cmd --permanent --policy int2ext --set-target ACCEPT
    $sudoCmd firewall-cmd --reload
}

share_network_client_temp() {
    #https://wiki.postmarketos.org/wiki/USB_Internet
    $sudoCmd ip route add default via 172.16.42.2 dev usb0
    #echo "nameserver 1.1.1.1" | $sudoCmd tee -a /etc/resolv.conf
}

share_network_client_fix() {
    #https://wiki.postmarketos.org/wiki/USB_Internet
    echo 'ip route add default via 172.16.42.2 dev usb0' | $sudoCmd tee -a /etc/local.d/usb_internet.start
    $sudoCmd chmod +x /etc/local.d/usb_internet.start
    $sudoCmd rc-update add local

}

####################################
########## pmosinstallbot ##########
####################################

pmbootstrap_install() {
    #https://wiki.postmarketos.org/wiki/Installation_guide
    if ! $pacInstalled "pmbootstrap" &> /dev/null; then
        #yay -aS --noconfirm pmbootstrap
        $pacInstall pmbootstrap
    fi

    postmarketos_prepare
}

postmarketos_prepare() {
    pmbootstrap init
    pmbootstrap status
    pmbootstrap pull
}

postmarketos_install() {
    #pmbootstrap install
    #Try to use full disk encrpytion on a mobile device!
    if [[ "$targetDevice" == "oneplus3" ]]; then
        pmbootstrap install --android-recovery-zip
    else
        pmbootstrap install --fde
    fi
    #Full disk encryption and flashing to SD card
    #pmbootstrap install --fde --sdcard=/dev/mmcblk

    #In case of errors, use images via fastboot
    if [[ "$?" == "1" ]]; then
        echo "[ ERROR ] manual mode. Try again to prepare the image!"
        #pmbootstrap export
    fi
    #pmbootstrap shutdown
}

pmbootstrap_flash() {
    if [[ "$targetDevice" == "fajita" ]]; then
        echo "Installing PostMarketOS on $targetDevice is a little challenging due to bad connections via USB and needs several tries!"
        #Try multiple times?
        echo "$(tput setaf 3)Erase dtbo on $targetDevice$(tput sgr0)"
        fastboot erase dtbo
        if [[ "$?" == "1" ]]; then
            read -r -p "$(tput setaf $color)Please restart your device into (tput setaf 2)BOOTLOADER$(tput sgr0). Restarted? [y/N]:"
            if [[ "$REPLY" =~ [yY] ]]; then
                fastboot erase dtbo
            fi
        fi
        echo "$(tput setaf 3)Flash rootfs$(tput sgr0)"
        pmbootstrap flasher flash_rootfs --partition userdata
        if [[ "$?" == "1" ]]; then
            echo "WARNING: If your device hangs during uploading the data for more than 5 minutes without any progress, you need to restart your phone!!!"
            pmbootstrap flasher flash_rootfs --partition userdata
            if [[ "$?" == "1" ]]; then
                pmbootstrap flasher flash_rootfs --partition userdata
            fi
        fi
        echo "$(tput setaf 3)Flash kernel$(tput sgr0)"
        pmbootstrap flasher flash_kernel
        if [[ "$?" == "1" ]]; then
            echo "WARNING: If your device hangs during uploading the data for more than 5 minutes without any progress, you need to restart your phone!!!"
            pmbootstrap flasher flash_kernel
            if [[ "$?" == "1" ]]; then
                echo "If it fails during flashing boot, the mobile was anyway able to boot to the new system! Maybe it works already."
                pmbootstrap flasher flash_kernel
            fi
        fi
    elif [[ "$targetDevice" == "oneplus3" ]]; then
        fastboot reload recovery
        #Enable ADB sideload manually
        pmbootstrap flasher --method=adb sideload
    elif [[ "$targetDevice" == "librem5" ]]; then
        mbootstrap flasher flash_rootfs
    elif [[ "$targetDevice" == "sdcard" ]]; then
        pmbootstrap install --sdcard=/dev/sdXXXXXXXX
    else
        echo "$(tput setaf 3)Flash rootfs$(tput sgr0)"
        pmbootstrap flasher flash_rootfs
        echo "$(tput setaf 3)Flash boot$(tput sgr0)"
        pmbootstrap flasher boot
    fi
    echo ""
    echo "Finished: If there was an error, use the original USB cable from your mobile phone (e.g. for OnePlus)!"
    echo "If it still fails, run the following commands manually"
    #/home/rainer/.local/var/pmbootstrap/chroot_native/home/pmos/rootfs/
    echo "--- fastboot flash userdata /tmp/postmarketOS-export/$targetVendor-$targetDevice.img"

    #Does this work for fajita as well?
    echo "--- fastboot flash boot /tmp/postmarketOS-export/boot.img-postmarketOS-XXX-XXX"
}

####################################
############# INTERFACE ############
####################################

interface_dialog_appbot() {
    if [[ "$distro" == "postmarketos" ]]; then
        interface_dialog_pmos
    else
        interface_dialog_connect_pmos
    fi
}

interface_dialog_connect_pmos() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName on $distro ($sudoCmd $pac):" --menu "\nTasks:" 13 60 8 \
            1 "Update system" \
            2 "Install postmarketOS" \
            3 "SSH to postmarketOS (connect USB cable)" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) interface_dialog_generic;;
        2) pmos_dialog_pmos_install;;
        3) ssh_connect; check_exit;;
        *) clear; exit;;
    esac
    done
}

interface_dialog_generic() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName on $distro ($sudoCmd $pac):" --menu "\nTasks:" 13 60 8 \
            1 "Upgrade $distro" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) pac_upgrade_system; check_exit;;
        *) clear; break;;
    esac
    done
}

pmos_dialog_pmos_install() {
    while true; do
    REPLY=$(dialog --stdout --title "postmarketOS install to mobile device helper" --menu "\nInstaller:" 15 60 8 \
            1 "pmbootstrap init" \
            2 "pmbootstrap install" \
            3 "pmbootstrap flash" \
            4 "pmbootstrap log" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) pmbootstrap_install; check_exit;;
        2) postmarketos_install; check_exit; check_exit;;
        3) pmbootstrap_flash; check_exit;;
        4) pmbootstrap log; check_exit;;
        *) clear; break;;
    esac
    done
}

interface_dialog_pmos() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName on $distro ($sudoCmd $pac):" --menu "\nTasks:" 14 60 8 \
            1 "Upgrade system" \
            2 "Setup postmarketOS (Mobile)" \
            3 "Wifi connect (Mobile)" \
            4 "TinyDM: Switch to Wayland Session" \
            5 "TinyDM: Switch to X Session" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) interface_dialog_generic;;
        2) init_pmos_step1; check_exit;;
        3) wifi_scan_connect; check_exit;;
        4) tinydm_switch_session "wayland-"; check_exit;;
        5) tinydm_switch_session "x"; check_exit;;
        *) clear; exit;;
    esac
    done
}

maintenance_daily_tasks() {
    pac_upgrade_system
}

####################################
################ RUN ###############
####################################

runParameter="$1"
if [[ "$runParameter" == "--local" ]]; then
    export pkgPath="."
fi

if [[ "$runParameter" == "fajita" ]]; then
    targetDevice="$runParameter"
    targetVendor="oneplus"
fi

if [[ "$runParameter" == "-s" ]] || [[ "$runParameter" == "--service" ]]; then
    appbot_init
    if [[ "$distro" == "postmarketos" ]]; then
        pac_upgrade_system
    else
        echo "No service available for $distro"
    fi
else
    appbot_init
    interface_dialog_appbot
fi
