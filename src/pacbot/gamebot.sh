#!/bin/bash
# gamebot - Steam sync & update launcher settings, mount network library, author Rainer Finke, license GPLv3
# EXPERIMENTAL This script is only used to play around with the possibilities to manage a central Steam library and many games

####################################
######### LOCAL ADJUSTMENTS ########
####################################

#For testing
export pkgPath="."
#export pkgPath="/opt/$scriptName"

#Options: local|nfs
filesystem="local"
encrypted=true
localDisk="sdb1"
remoteSteamFolder="/mnt/steam"
remoteSteamLibrary="$remoteSteamFolder/library"
steamPath="Steam"
steamUser="rainer"

#Settings for nfs
server="192.168.1.21"

####################################
######## Global Variables ##########
####################################

mountedSteamFolder="/mnt/$filesystem"
mountedSteamLibrary="$mountedSteamFolder/$steamPath"
#mountedSteamLibrary="${HOME}/LocalOnly/Steam1/library/steamapps"
localSteam="${HOME}/.local/share/Steam/steamapps"
localShaderCache="$localSteam/shadercache"
localDownloadCache="$localSteam/downloading"
localCompatdata="$localSteam/compatdata"
steamLauncher="/usr/bin/steam-runtime -newbigpicture"

#Static
scriptName="gamebot"
#filesystemParameter="-o auto_cache -o reconnect -o noatime"
filesystemParameter="-oauto_cache,reconnect,noatime"

####################################
######### Steam Functions ##########
####################################

source_dependencies3() {
	#Get global user settings and dependencies
    #source "$pkgPath/settings.sh"
	#source "$pkgPath/helpers.sh"
	#source "$pkgPath/systemchecks.sh"
	#source "$pkgPath/sysinfo.sh"
	###### PACBOT ERROR MUST RUN AS ROOT, BUG #########
	#source "$pkgPath/pacbot.sh" "--source-only"
	#source "$pkgPath/usbstickbot.sh" "--source-only"
    source "$pkgPath/archinstallbot.sh" "--source-only"
    source "$pkgPath/interface.sh"
    source "$pkgPath/archbot.sh" "--source-only"
}

protonaddons_install() {
    #Flatpak?
    #ProtonUp-Qt allows the user to choose proton apps like GE-Proton, Proton-TKG, Luxtorpeda, Boxtron, Roberta, SteamTinkerLaunch
    #Qt6
    yay -S --noconfirm  protonup-qt

    #mkdir -p ${HOME}/.local/.share/Steam/compatibilitytools.d/
    #cd ~/.local/share/Steam/compatibilitytools.d/ || cd ~/.steam/root/compatibilitytools.d/ || return

    #Luxtorpeda: Use native open source Linux game engines
    #rm -R "luxtorpeda"
    #curl -L https://luxtorpeda.gitlab.io/luxtorpeda/master/luxtorpeda.tar.xz | tar xJf -
    #Requires rust
    #There is as well a non git version in AUR, but fixes and new engines are probably only available in git
    #yay -S --noconfirm luxtorpeda-git

    #Boxtron: Use native Linux dosbox for Wine/Proton games so it needs to be installed
    #dosboxsdl2_install
    #Add optional dependency for boxtron
    #pacmanSyNC "soundfont-fluid"

    #Dosbox-staging can be installed directly with luxtorpeda, so Boxtron is not required anymore
    #How to get the latest release? => AUR package
    #curl -L https://github.com/dreamer/boxtron/releases/download/v0.5.3/boxtron.tar.xz | tar xJf -
    #yay -S --noconfirm boxtron

    #Scummvm can be installed directly with luxtorpeda, so Roberta is not required anymore
    #Roberta: Use native Linux ScummVM for Wine/Proton games
    #BUG How to get the latest release?
    #rm -R "roberta"
    #curl -L https://github.com/dreamer/roberta/releases/download/v0.1.0/roberta.tar.xz | tar xJf -
    #Installs scummvm fluidsynth libinstpatch sdl2_net
    #Roberta has been deleted again from AUR
    #yay -S --noconfirm roberta

    #Use ProtonUp-Qt to download manually ProtonGE
    #Proton-GE: Based on latest Wine, D9VK enabled by default
    #How to get the latest release?
    #local protonge="Proton-5.2-GE-2"
    #local protongeVersion=$(echo "$protonge" | cut -c 8-)
    #rm -R "$protonge"
    #curl -L https://github.com/GloriousEggroll/proton-ge-custom/releases/download/"$protongeVersion"/"$protonge".tar.gz | tar xzf -
    #yay -S --noconfirm proton-ge-custom-bin

    #ProtonGE is now as well just based on 5.11, so add proton-tkg
    #https://github.com/Frogging-Family/wine-tkg-git
    # Removed???
    #yay -S --noconfirm proton-tkg-git
    #yay -S --noconfirm vkd3d-proton-tkg-git
    #yay -S --noconfirm lib32-vkd3d-proton-tkg-git

    #Use git package as it is available as well in chaotic-aur as compiled
    yay -S --noconfirm protontricks-git
    echo "To use protontricks:  [protontricks GAMEID --gui]"
}

game_manager_install() {
    #Game Manager
    ##Gnome 44 dependencies - Lutris - Requires python-protobuf for Battle.net
    echo "Lutris Game Launcher - Flathub/Itch.io/HumbleBundle/Battle.net/Origin/Ubisoft/Amazon/Steam/GOG/Epic/Steam"
    #pacmanSyNC python-protobuf
    pacmanSyNC lutris
    ##Heroic
    echo "Heroic Game Launcher - GOG/Epic"
    yay -S --noconfirm heroic-games-launcher-bin

    #Gamehub available as well in chaotic-aur
    #echo "Gamehub - Game Launcher for GOG/HumbleBundle/itch.io/Steam"
    #yay -S --noconfirm gamehub

    #flatpak_packages_game2

    #NOTE Use Emudeck/Emustation instead
    #gameemulation_install
}

proton_standalone_install() {
    #Requires lib32
    #Not required anymore lib32-faudio (AUR)
    local winePkgs="lib32-gtk3 wine-gecko wine-mono faudio lib32-libpulse lib32-mpg123 lib32-gnutls lib32-libldap ocl-icd lib32-ocl-icd lib32-v4l-utils lib32-libxslt lib32-gst-plugins-base-libs lib32-libcanberra lib32-openal lib32-giflib lib32-libva samba"
    read -r -p "$(tput setaf $color)Do you want to use Proton standalone instead of Wine-staging?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        #read -r -p "$(tput setaf $color)Do you want to use Proton-git instead of Proton stable?$(tput sgr0)  [y/N]"
        #if [[ "$REPLY" =~ [yY] ]]; then
            #Proton-git (for Steam) will install as well wine-valve-git which can be used separatly instead of wine/wine-staging
            #Proton-git package is outdated, won't compile
            #yay -S --noconfirm proton-git
        #    yay -S --noconfirm wine-valve-git
            #https://github.com/ValveSoftware/vkd3d
            #VKD3D seems quite outdated, replace with vkd3d-proton? Only for proton??? https://github.com/HansKristian-Work/vkd3d-proton
            #yay -S --noconfirm vkd3d-valve-git
        #    yay -S --noconfirm vkd3d-proton-bin
            #BUG no lib32 package, is this required???
            #if ! pacman -Q lib32-vkd3d-git &> /dev/null; then
            #    yay -S --noconfirm lib32-vkd3d-valve-git
            #else
            #    yay -S lib32-vkd3d-valve-git
            #fi
            #DXVK is used by default in Proton, no need for a separate package
        #    if pacman -Q dxvk-bin &> /dev/null; then
        #        sudo pacman -Rsc dxvk-bin
        #    fi
        #    pacmanSyNC "$winePkgs"
            #dosboxsdl2_install
        #else
            #Proton (for Steam) will install as well wine-valve which can be used separatly instead of wine/wine-staging
            # Wine-valve will replace wine and vkd3d-proton will replace vkd3d, needs confirmation, no not use --noconfirm
            yay -S --noconfirm proton
            #yay -S wine-valve
            yay -S --noconfirm vkd3d-proton-bin

            # Missing package lib32-vkd3d-valve????
            #if ! pacman -Q lib32-vkd3d-git &> /dev/null; then
            #    yay -S --noconfirm lib32-vkd3d-valve
            #else
            #    yay -S lib32-vkd3d-valve
            #fi
            #DXVK is used by default in Proton, no need for a separate package
            if pacman -Q dxvk-bin &> /dev/null; then
                sudo pacman -Rsc dxvk-bin
            fi
            pacmanSyNC "$winePkgs"
            #dosboxsdl2_install
        #fi
    else
        #Wine. How to remove gtk2?
        sudo pacman -Sy "wine-staging"
        pacmanSyNC "$winePkgs vkd3d lib32-vkd3d"
        yay -S --noconfirm dxvk-bin
        setup_dxvk install --symlink
        #dosboxsdl2_install
    fi
}

dosboxsdl2_install() {
    #Switch to Flatpak
    flatpak_packages_dosbox

    #BUG dosbox stable doesn't support Wayland, use dosbox-sdl2 therefore instead!
    #if ! pacman -Q sdl_sound-hg &> /dev/null; then
    #    yay -S --noconfirm sdl2_sound-hg
    #else
    #    sudo pacman -Rsc --noconfirm sdl_sound-hg
    #    if ! pacman -Q sdl2_sound-hg &> /dev/null; then
    #        yay -S --noconfirm sdl2_sound-hg
    #    fi
    #fi
    #yay -S --noconfirm dosbox-staging
}

mount_steam_networkfs() {
    #Steam on a network folder is really slow as high IO when creating the shader cache, so use only a local shadercache and this makes the library as well independent from the GPU. Otherwise Steam deletes the shadercahce of another GPU when library is mounted!!!

    #Mount Steam Remote Library
    #"$filesystem" "${server}:$remoteSteamLibrary" "$mountedSteamLibrary" "$filesystemParameter"
    if [[ "$filesystem" == "local" ]] && ( $encrypted ); then
        echo "Please make sure to have a mountpount in /etc/fstab. If not, you need to mount the cryptdevice manually!!! Example [/dev/mapper/csteam1                             /mnt/Steam   btrfs defaults,noatime,compress=zstd:3,space_cache,subvolid=5,subvol=/,noauto,x-systemd.automount,nofail,_netdev 0 0]"
        sudo cryptsetup open /dev/"$localDisk" csteam1
        ###sudo mount -o compress=zstd /dev/mapper/csteam1 /mnt/Steam
        ###sudo mount --bind /mnt/Steam/library/ /mnt/nfs/Steam/
    elif [[ "$filesystem" == "nfs" ]]; then
        #sudo mkdir -p "$remoteSteamFolder"
        sudo mkdir -p "$mountedSteamFolder"
        sudo mount -t nfs ${server}:/ "$mountedSteamFolder"
    fi

    #Mount Local ShaderCache
    #mount "$localShaderCache" "$mountedSteamLibrary/shadercache"

    #Start Steam launcher
    if [[ "$?" == "0" ]]; then
        $steamLauncher
    else
        #BUG: "sudo modprobe nfsd" doesn't work after kernel upgrade due to error message "modprobe: FATAL: Module nfsd not found in directory /lib/modules/5.12.12-arch1-1"
        echo "Plase restart your computer"
    fi

    #exit
}

unmount_steam() {
    #Close Steam Client
    closeSteam

    #Unmount ShaderCache
    #unmount "$mountedSteamLibrary/shadercache"

    #Unmount Remote Steam Library
    #umount "$mountedSteamLibrary"
    #umount /home/rainer/LocalOnly/Steam1
    if [[ "$filesystem" == "nfs" ]]; then
        sudo umount "$mountedSteamFolder"
    fi

    exit
}

clean_link_shadercache_local() {
    #WARNING This was done only for performance reasons. But this is against the Steam Removable Disk idea!!!

    rm -R "$localShaderCache/"* &> /dev/null
    rm -R "$mountedSteamLibrary/steamapps/shadercache"
    ln -s "$localShaderCache" "$mountedSteamLibrary/steamapps/"

    #Temp Download folder
    rm -R "$localDownloadCache/"* &> /dev/null
    rm -R "$mountedSteamLibrary/steamapps/downloading"
    ln -s "$localDownloadCache" "$mountedSteamLibrary/steamapps/"

    #What about compatdata (proton)? Installing Windows games takes a lot of time on HDD, but you would do it just once then!
    mkdir -p "$localCompatdata"
    rm -R "$localCompatdata/"* &> /dev/null
    rm -R "$mountedSteamLibrary/steamapps/compatdata"
    ln -s "$localCompatdata" "$mountedSteamLibrary/steamapps/"

    #BUG Steam will not run steamlinux runtime games from nobody:nobody!!! So do not use it from a shared drive!
    rm -R "$mountedSteamLibrary/steamapps/common/SteamLinuxRuntime"
    rm -R "$mountedSteamLibrary/steamapps/common/SteamLinuxRuntime_sniper"
    rm -R "$mountedSteamLibrary/steamapps/common/SteamLinuxRuntime_soldier"
    echo ""
    echo "Please install SteamLinuxRuntime now manually to your local drive and not to the shared drive!"
}

steam_proton_update_launch_option() {
    #Update Proton version => Use ProtonUp Qt?
    local file="${HOME}/.local/share/Steam/config/config.vdf"
    cp "$file" "${file}.bak"
    #local date=$(date +"%Y-%m-%d")
    #cp "$file" "${file}.bak.$date"
    oldVersion="$1"
    newVersion="$2"
    declare -A protonArr
    #examples proton_5 proton_411 proton_42 proton_316 proton_37
    #Case insensitive Proton/proton
    #Question: Add grep -e steamlinuxruntime -e Boxtron -e Luxtorpeda -e Roberta? What happens in steam if this is changed from e.g. linux to Proton?
    #local listProtonUsed=$(more ${HOME}/.local/share/Steam/config/config.vdf | grep -iF proton | awk '{ print $2 }' | cut -c 2- | rev | cut -c 2- | rev)
    local listProtonUsed=$(more ${HOME}/.local/share/Steam/config/config.vdf | grep -iF name | cut -d "\"" -f4)
    #Keep "" at beginning and end
    #local listProtonUsed=$(more ${HOME}/.local/share/Steam/config/config.vdf | grep -iF name | cut -d "\"" -f3-)
    #Is it possible to create an array with all proton versions? And ask user to select what should be replaced?
    for i in ${listProtonUsed[*]}
    do
        protonArr["$i"]="$i"
    done
    checkArray emptyArray protonArr
    if ( ! $emptyArray ); then
        closeSteam
        echo "Currently used versions:"
        printf '%s\n' "${protonArr[@]}"
        read -r -p "$(tput setaf $color)Please type the name of the OLD Proton version:$(tput sgr0)"
        local base="$REPLY"
        for i in ${protonArr[*]}; do
            if [[ "$i" == "$base" ]]; then
                read -r -p "$(tput setaf $color)Please type the name of the NEW Proton version:$(tput sgr0)"
                local target="$REPLY"
                for j in ${protonArr[*]}; do
                    if [[ "$j" == "$target" ]]; then
                        config_replacevalue "$base" "$target" "$file"
                    fi
                done
            fi
        done
    else
        echo "Nothing to do, no Proton in use"
    fi
}

steam_update_launch_option() {
    #steamLaunchOptions="gamemoderun ENABLE_VKBASALT=1 DXVK_HUD=api mangohud %command%"
    #BUG FSR - Use parameter -F fsr in gamescope and WINE_FULLSCREEN_FSR=1, upscaling can be used in fullscreen mode e.g. from 1080p to 4k. FSR can crash gamescope now somehow, so do not use it anymore?????
    #BUG since Proton 7.0 no mouse input anymore, it requires on a desktop -e to accept input from the composit manager
    # SDL_VIDEODRIVER=wayland mostly work in Proton, needs to be removed in OpenGL Linux
    #mangohud can see only gamemoderun and vkbasalt if they run after mangohud
    #The order of the commands below is quite important, do change them only for testing, e.g. mangohud will not see gamescope+vkbasalt if loaded after them or gamescope is not able to launch if opened after zink and SDL wayland.
    steamLaunchOptions="DRI_PRIME=1 SDL_VIDEODRIVER=wayland gamescope -w 3840 -h 2160 -W 1920 -H 1080 -r 60 -o 60 -- env MANGOHUD=1 ENABLE_VKBASALT=1 DXVK_HUD=api MESA_LOADER_DRIVER_OVERRIDE=zink mangohud %command%"
    local steamUsers=$(ls ${HOME}/.local/share/Steam/userdata/)
    read -r -p "Do you want to set the Steam launch options in $(tput setaf $color)ALL GAMES and ALL PROFILES$(tput sgr0) to $steamLaunchOptions  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        closeSteam
        local date=$(date +"%Y-%m-%d")
        for steamUserID in $steamUsers
        do
            if [[ "$steamUserID" != "ac" ]] && [[ "$steamUserID" != "0" ]]; then
                echo "Steam Profile: $steamUserID"
                local file="${HOME}/.local/share/Steam/userdata/$steamUserID/config/localconfig.vdf"
                cp "$file" "${file}.bak.$date"
                # Bug: This seems to modify some special characters (unicode???) in the file but Steam will fix it automatically
                # Bug: This doesn't work if the game was not installed/played yet
                config_setvalue "						\"LaunchOptions\"		" "\"${steamLaunchOptions}\"" "$file"
            else
                echo "User $steamUserID unkown --- TO BE CHECKED"
            fi
        done
    fi
}

steamcmd_game_install() {
    steamcmd_install

    read -r -p "$(tput setaf $color)To install a game type the Steam ID:$(tput sgr0)"
    local gameID="$REPLY"
    if [[ "$gameID" != "" ]] && (( $gameID > 0 )); then
        #BUG steam game folder is not created automatically with force_install_dir, so use default in home directory
        #+force_install_dir "$mountedSteamLibrary/steamapps/common/"
        steamcmd +login "$steamUser" +app_update "$gameID" validate +quit
        #@sSteamCmdForcePlatformType windows
    else
        echo "Error: GameID is empty"
    fi
}

steamcmd_install() {
    if ! pacman -Q steamcmd &> /dev/null; then
        yay -aS --noconfirm steamcmd
    fi
}

closeSteam() {
    if [[ $(pidof "steam") == "" ]]; then
        echo "Steam is not running and it is safe to modify the config files"
    else
        steam -shutdown
        echo "... closing Steam now!"
    fi
}

steam_sync_config() {
    local steamConfig
    local steamUserConfig
    steamConfig="${HOME}/.local/share/Steam/config/"
    steamUserConfig="${HOME}/.local/share/Steam/userdata/"
    echo "1.) Please add the folder $(tput setaf 2)$steamConfig$(tput sgr0) and $(tput setaf 2)$steamUserConfig$(tput sgr0) as a new seperate folder synchronization to e.g. $(tput setaf 2)Nextcloud$(tput sgr0) (no subfolders required)"

    read -r -p "Do you want to delete now the local Steam config & userdata files before synchronizing an existing sync folder?  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        closeSteam
        rm -R "$steamConfig"*
        rm -R "$steamUserConfig"*
    else
        echo "Please delete the files in $(tput setaf 2)$steamConfig$(tput sgr0) and $(tput setaf 2)$steamUserConfig$(tput sgr0) when setting up an existing sync folder"
    fi

    echo "2.) Sync this folder into a Backup folder on Nextcloud"
    echo "3.) Add the same sync to other computers that should share the same Steam Library"
}

flatpak_steamdeck_packages() {
    #How to source from archbot?
    flatpak install --noninteractive com.github.tchx84.Flatseal
    flatpak_desktop_packages
    flatpak_game2_packages
    flatpak_privacy_packages

    #Packages that need to be installed on Steam Deck via Flatpak

    #Only X11, as soon as KDE switches to Wayland on the Steam Deck, Maliit will probably be required
    #Not required anymore, STEAM + X will open the Steam keyboard when Steam is running
    #flatpak install --noninteractive org.cubocore.CoreKeyboard

    flatpak install --noninteractive tv.kodi.Kodi net.jami.Jami org.telegram.desktop com.usebottles.bottles org.mozilla.Thunderbird org.libreoffice.LibreOffice com.nextcloud.desktopclient.nextcloud
}

steam_library_sdcard() {
    #Go to mounted media, otherwise Steam will not recognize the library from the SteamDeck
    mountedDisk="/run/media/${userName}/$diskName"
    rm -R $mountedDisk/SteamLibrary/steamapps/
    ln -s $mountedDisk/steamapps $mountedDisk/SteamLibrary/
}

steamos_dev_mode() {
    if [[ "$1" == "true" ]]; then
        sudo steamos-readonly disable
    else
        sudo steamos-reanonly enable
    fi
}

steamos_set_password() {
    passwd
}

steamos_plasma_wayland() {
    # Require writeable file systemchecks
    steamos_dev_mode "true"
    config_setvalue "Session=" "plasma-steamos-wayland-oneshot.desktop" "/etc/sddm.conf.d/zz-steamos-autologin.conf"
}

emudeck_install_update() {
    local source="https://www.emudeck.com/EmuDeck.desktop"
    local file="${HOME}/EmuDeck.desktop"
    wget --show-progress --quiet --continue "${source}" --output-document="$file"
    kioclient exec "$file"
}

nonsteamlauncher_install_update() {
    #https://github.com/moraroy/NonSteamLaunchers-On-Steam-Deck
    local source="https://github.com/cchrkk/NSLOSD-DL/releases/download/DlLinkFix/NonSteamLaunchers.desktop"
    local file="${HOME}/NonSteamLaunchers.desktop"
    wget --show-progress --quiet --continue "${source}" --output-document="$file"
    kioclient exec "$file"
}

cryoutilities_install_update() {
    #CryoUtilities - Improve Steam Deck performance, swap, spappiness, hute pages
    #https://github.com/CryoByte33/steam-deck-utilities
    #Explanation video https://www.youtube.com/watch?v=C9EjXYZUqUs
    local source="https://raw.githubusercontent.com/CryoByte33/steam-deck-utilities/main/InstallCryoUtilities.desktop"
    local file="${HOME}/CryoUtilities.desktop"
    wget --show-progress --quiet --continue "${source}" --output-document="$file"
    kioclient exec "$file"
}

nexusmodsapp_install() {
    #Nexus Mods App (new instead of Vortex in Wine)
    #https://github.com/Nexus-Mods/NexusMods.App
    yay -S --noconfirm nexusmods-app-bin
}

#DuckyDeck?

performance_manual() {
    echo "BUG - Disable scaling in KDE Plasma as this will create blurry images in games!"
    echo "Please increase UMA Frame Buffer Size from 1GB to 4GB in BIOS/UEFI after making sure you have at least 4GB swap space on a 16GB device!"
}

####################################
############# OBSOLETE #############
####################################

gamemode_install() {
    echo "Gamemode: Please set in Steam the launch parameter [gamemoderun %command%]"
    pacmanSyNC "gamemode lib32-gamemode"
    systemctl --user enable gamemoded
    systemctl --user start gamemoded
}

game_controller_helper() {
    yay -S --noconfirm oversteer
    echo "Oversteer: Setup Logitech G29/27/25/920 wheels"
}

game_system_rgb() {
    if [[ "$computerType" == "desktop" ]]; then
    #Flatpak?
        yay -S --noconfirm openrgb
    fi
}

steamtinkerlauch_install() {
    #Use ProtonUp
    yay -S --noconfirm steamtinkerlaunch
    echo "Steamtinkerlaunch: Run it with the following Steam command line [steamtinkerlaunch %command%]"
}

amd_radeonprofile_install() {
    #Not maintained anymore, there are many errors in the journal
    echo "Radeon-Profile: Adjust voltage and fan curve for AMD GPU's"
    yay -S --noconfirm radeon-profile-daemon-git
    sudo systemctl enable radeon-profile-daemon.service
    sudo systemctl start radeon-profile-daemon.service
    radeonprofile_autostart
}

radeonprofile_autostart() {
    local path="${HOME}/.config/autostart"
    mkdir -p $path
    local file="${path}/radeon-profile.desktop"
    #Autostart radeon-profile
    echo "[Desktop Entry]
Categories=System;Monitor;HardwareSettings;TrayIcon;
Comment[$languageCodeLong]=Monitor Radeon GPU parameters and switch power profiles
Comment=Monitor Radeon GPU parameters and switch power profiles
Exec=radeon-profile
GenericName[$languageCodeLong]=Control panel
GenericName=Control panel
Icon=radeon-profile
MimeType=
Name[$languageCodeLong]=Radeon Profile
Name=Radeon Profile
Path=
StartupNotify=false
Terminal=false
TerminalOptions=
Type=Application=
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-KDE-SubstituteUID=false
X-KDE-Username=" | tee "$file" &> /dev/null
}

gameemulation_install() {
    read -r -p "$(tput setaf $color)Do you want to install game emulators (EmulationStation, Dolphin-Emu, Yuzu, ...)?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
        libretro_install

        #NOTE Use EMUDECK instead
        #flatpak_packages_emulation
        #echo "Install Emulation Station Game Launcher"
        #pacmanSyNC "emulationstation"

        #echo "Ludo - Game Emulation Frontend"
        #Use Emulation Station instead
        #yay -S --noconfirm ludo

        #Flatpak?
        #echo "Dolphin-Emu - Game Emulator for Gamecube (Vulkan support)"
        #pacmanSyNC "dolphin-emu"

        #Flatpak?
        #echo "PPSSPP - Game Emulator for Playstation Portable (Vulkan support)"
        #pacmanSyNC "ppsspp"

        #Flatpak?
        #echo "Yuzu - Game Emulator for Nintendo Switch (Vulkan support)"
        #yay -S --noconfirm yuzu-mainline-git

        #Flatpak?
        # Takes too long to build, disabled
        #echo "RPCS3 - Game Emulator for Playstation 3 (Vulkan support)"
        #yay -S --noconfirm rpcs3-git

        #Use RetroArch
        #Game Emulators OpenGL
        #echo "Mupen64 Plus - Nintento 64 Game Emulator"
        #pacmanSyNC "mupen64plus"

        #Flatpak?
        #echo "PCSX2 - Playstation 2 Game Emulator"
        #pacmanSyNC "pcsx2 lib32-jack2"
    else
        echo "Skipping installation of game emulators!"
    fi
}

libretro_install() {
    #Game Emulators Vulkan
    echo "RetroArch - Game Emulator front-end (Vulkan supported) for Nintendo 64, Sega Dreamcast, PSP"
    pacmanSyNC "retroarch retroarch-assets-xmb libretro-dolphin libretro-ppsspp libretro-mupen64plus-next libretro-flycast"
    #yay retroarch-autoconfig-udev-git

    #kodi libretro addons
    #media_center_install
    kodi_install
    #pacmanSyNC "kodi-addon-game-libretro kodi-addon-game-libretro-mupen64plus kodi-addon-game-libretro-flycast"
}

####################################
### gamebot Interface Functions ####
####################################

interface_steamdeck() {
    while true; do
    REPLY=$(dialog --stdout --title "SteamDeck" --menu "\nSteam Deck Task:" 20 100 8 \
            1 "Set new Password for Steam Deck user" \
            2 "Flatpak Software (Librewolf, Flatseal, Kodi, Bottles, Telegram, Thunderbird, Nextcloud)" \
            3 "Enable DEV mode (NOT RECOMMENDED UNLESS YOU KNOW WHAT YOU DO)" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) steamos_set_password; check_exit;;
        2) flatpak_steamdeck_packages; check_exit;;
        3) steamos_dev_mode; check_exit;;
        *) clear; break;;
    esac
    done
}

interface_gamebot() {
    while true; do
    REPLY=$(dialog --stdout --title "gamebot" --menu "\nGamebot Task:" 21 100 8 \
            1 "Archbot - Setup Gaming System - Mesa, Multilib, Steam, Gamesope, Manghud, GOverlay" \
            2 "How To Setup Steam Config (Proton Version) & Sync (Launch Parameter) for Nextcloud" \
            3 "Set Default Launch Options" \
            4 "Update Proton Version for all Games" \
            5 "Game Manager - Lutris & Heroic" \
            6 "NonSteamLaunchers: Install Game Launchers directly to Steam like Battle.net" \
            7 "EmuDeck: Install and setup Game Emulation" \
            8 "CryoUtilities: Performance Optimizations" \
            9 "Proton UP: Manage Steam Launcher like GE-Proton & Luxtorpeda and add Protontricks" \
            10 "Nexus Mods App: Manage Game Mods (Replaces Vortex App in Wine)" \
            11 "Performance - Manual Tasks Info" \
            12 "Steam Deck Opions" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) gaming_install; check_exit;;
        2) steam_sync_config; check_exit;;
        3) steam_update_launch_option; check_exit;;
        4) steam_proton_update_launch_option; check_exit;;
        5) game_manager_install; check_exit;;
        6) nonsteamlauncher_install_update;;
        7) emudeck_install_update; check_exit;;
        8) cryoutilities_install_update; check_exit;;
        9) protonaddons_install; check_exit;;
        10) nexusmodsapp_install; check_exit;;
        11) performance_manual; check_exit;;
        12) interface_steamdeck;;
        *) clear; break;;
    esac
    done
}

####################################
############ Interface #############
####################################

runParameter=$1

if [[ "$EUID" -eq 0 ]] && [[ "$EUID" -ne 0 ]]; then
	printf "bot should not run with root privileges, it works only with sudo!\n"
else
	# Import settings
    source_dependencies3

    interface_gamebot
fi
