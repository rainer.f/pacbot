#!/bin/bash
# archbot - Arch Linux Initialization and Setup Tool, author Rainer Finke, license GPLv3
# User Profile

####################################
######### Global Variables #########
####################################

runParameter=$1
if [[ "$runParameter" == "--source-only" ]]; then
    runParameter=""
else
    export pkgPath="/opt/pacbot"
fi

####################################
########## Init Functions ##########
####################################

source_dependencies_archbot() {
	#Get global user settings and dependencies
    #source "$pkgPath/settings.sh"
	#source "$pkgPath/helpers.sh"
	#source "$pkgPath/systemchecks.sh"
	#source "$pkgPath/interface.sh"
	#source "$pkgPath/usbstickbot.sh" "--source-only"
    source "$pkgPath/archinstallbot.sh" "--source-only"
    #source "$pkgPath/sysinfo.sh"
    #source "$pkgPath/userprofile.sh"
	###### PACBOT ERROR MUST RUN AS ROOT, BUG #########
	#source "$pkgPath/pacbot.sh" "--source-only"
}

####################################
############# Functions ############
####################################

user_profile_create() {
    #New additional archbot user or homed user
    echo "[  archbot  ] Setting up $(tput setaf 2)new user profile$(tput sgr0) for your user..."
    read -r -p "$(tput setaf $color)Do you want to setup the user profile based on archbot settings?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        bashrc_first_boot
    fi

    config_setvalue "export createdProfile=" "true" "${HOME}/.archbotuser.conf"
}

user_profile_init() {
    bashrc_output_add
    user_profile_kde
}

bashrc_output_add() {
    echo "[  $(tput setaf 2).bashrc$(tput sgr0)  ] Show default system information when opening a konsole"
    bashrc_output_default
    local file="${HOME}/.bashrc"
    #Use fastbatch instead of neofetch/archey3
    echo "EDITOR=/usr/bin/nano
#Show Logged Errors
journalctl -p 3 -xb | grep \"\"
fastfetch
#Show pacbot service output since last boot
journalctl -b | grep pacbot-s" | tee -a "$file" &> /dev/null
}

user_profile_kde() {
    echo "[    $(tput setaf 2)KDE$(tput sgr0)    ] Setting up KDE Profile for user"
    #User Profile KDE Init
    kde_init
    #printf "\nInit firefox for KDE\n"
    #firefox_kde_init firefox-developer-edition
    nextcloud_autostart
    nextcloud_sync
    kde_sweeper
    user_profile_afterinit
}

kde_init() {
    mkdir -p ${HOME}/.config/
    #Set krunner on meta key
    #kwriteconfig5 --file ~/.config/kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.krunner,/App,,display"
    #qdbus org.kde.KWin /KWin reconfigure

    #BUG? Wayland: set keyboard layout
    echo "[Layout]
DisplayNames=
LayoutList=$languageCode
LayoutLoopCount=-1
Model=pc101
ResetOldOptions=false
ShowFlag=false
ShowLabel=true
ShowLayoutIndicator=true
ShowSingle=false
SwitchMode=Global
Use=true" | tee "${HOME}/.config/kxkbrc" &> /dev/null

    #Touchpad: Settings do depent from the touchpad id, so cover only the basics
    echo "[Keyboard]
NumLock=0

[Mouse]
#Use Adwaita instead of breeze_cursors as it is more crisp
cursorTheme=Adwaita" | tee "${HOME}/.config/kcminputrc" &> /dev/null

    #kdeglobals
#    echo "[QtQuickRendererSettings]
#SceneGraphBackend=opengl" | tee "${HOME}/.config/kdeglobals" &> /dev/null

    #Kwinrc
    #Privacy - Do not use geolocation (default by KDE) to set NightColor!
    #WindowEffects
    #Vulkan backend? In development https://invent.kde.org/plasma/kwin/-/issues/169
    echo "[$Version]
update_info=kwin.upd:replace-scalein-with-scale,kwin.upd:port-minimizeanimation-effect-to-js,kwin.upd:port-scale-effect-to-js,kwin.upd:port-dimscreen-effect-to-js,kwin.upd:auto-bordersize

[Compositing]
Backend=OpenGL
GLCore=true
GLPreferBufferSwap=a
GLTextureFilter=2
HiddenPreviews=5
OpenGLIsUnsafe=false

[Desktops]
Number=2
Rows=2

[Effect-MagicLamp]
AnimationDuration=130

[Effect-wobblywindows]
Drag=85
Stiffness=10
WobblynessLevel=1

[ElectricBorders]
TopLeft=

[NightColor]
Active=true
EveningBeginFixed=2100
Mode=Times
MorningBeginFixed=0630
NightTemperature=4800

[Plugins]
kwin4_effect_dimscreenEnabled=true
kwin4_effect_fadeEnabled=false
kwin4_effect_scaleEnabled=true
squashEnabled=false
magiclampEnabled=true
wobblywindowsEnabled=true

[Effect-PresentWindows]
TouchBorderActivate=2

[TouchEdges]
Bottom=KRunner
Top=KRunner

[Wayland]
InputMethod=/usr/share/applications/com.github.maliit.keyboard.desktop

[Windows]

[org.kde.kdecoration2]
ButtonsOnLeft=MF
ButtonsOnRight=IAX" | tee "${HOME}/.config/kwinrc" &> /dev/null

    #Turn off annoying Plasma sounds
    echo "[Event/Trash: emptied]
Action=
Execute=
Logfile=
TTS=

[Event/beep]
Action=
Execute=
Logfile=
TTS=

[Event/catastrophe]
Action=
Execute=
Logfile=
TTS=

[Event/fatalerror]
Action=
Execute=
Logfile=
TTS=

[Event/messageCritical]
Action=
Execute=
Logfile=
TTS=

[Event/messageInformation]
Action=
Execute=
Logfile=
TTS=

[Event/messageWarning]
Action=
Execute=
Logfile=
TTS=

[Event/messageboxQuestion]
Action=
Execute=
Logfile=
TTS=

[Event/notification]
Action=
Execute=
Logfile=
TTS=

[Event/printerror]
Action=
Execute=
Logfile=
TTS=

[Event/warning]
Action=
Execute=
Logfile=
TTS=" | tee "${HOME}/.config/plasma_workspace.notifyrc" &> /dev/null

    #Disable Baloo as it is crashing quite a lot, still in 2019!
    echo "[Basic Settings]
Indexing-Enabled=false

[General]
only basic indexing=false" | tee "${HOME}/.config/baloofilerc" &> /dev/null

    #Plasma Discover - Disable update notifier => pacbot runs automatic updates
    echo "[Global]
RequiredNotificationInterval=-1" | tee "${HOME}/.config/PlasmaDiscoverUpdates" &> /dev/null

    #Privacy - Delete Used Files History
    echo "[Plugin-org.kde.ActivityManager.Resources.Scoring]
keep-history-for=1" | tee "${HOME}/.config/kactivitymanagerd-pluginsrc" &> /dev/null

    #Privacy - Delete Trash Files after some Times
    echo "[${HOME}/.local/share/Trash]
Days=14
LimitReachedAction=0
Percent=10
UseSizeLimit=true
UseTimeLimit=true" | tee "${HOME}/.config/ktrashrc" &> /dev/null
}

firefox_kde_init() {
    local firefoxVersion="$1"
    #firefox firefox-developer-edition firefox-nightly. This can be done as well with /etc/environment
    #cp "/usr/share/applications/$firefoxVersion.desktop" "${HOME}/.local/share/applications/"
    #kwriteconfig5 --file="${HOME}"/.local/share/applications/"$firefoxVersion".desktop --group="Desktop Entry" "Exec" "GTK_USE_PORTAL=1 /usr/lib/${firefoxVersion}/firefox %u"
    #kwriteconfig5 --file="${HOME}"/.local/share/applications/"$firefoxVersion".desktop --group="Desktop Action new-private-window" "Exec" "GTK_USE_PORTAL=1 /usr/lib/${firefoxVersion}/firefox %u"
    #kwriteconfig5 --file="${HOME}"/.local/share/applications/"$firefoxVersion".desktop --group="Desktop Action new-window" "Exec" "GTK_USE_PORTAL=1 /usr/lib/${firefoxVersion}/firefox %u"
    #kbuildsycoca5

    #Integrate mime types https://wiki.archlinux.org/index.php/Firefox#KDE/GNOME_integration. Doesn't work?
    ln -s ~/.config/mimeapps.list ~/.local/share/applications/mimeapps.list
    #ln -s ~/.local/share/applications/mimeapps.list ~/.config/mimeapps.list

    #Special settings should be done directly in your profile and then use Firefox sync. Otherwise it must be copied to every profile
    #echo "" | tee "${HOME}/.mozilla/firefox/your-profile/user.js"
}

nextcloud_autostart() {
    mkdir -p ${HOME}/.config/autostart/
    echo "[Desktop Entry]
Name=Nextcloud
GenericName=File Synchronizer
Exec=/usr/bin/nextcloud --background
Terminal=false
Icon=nextcloud
Categories=Network
Type=Application
StartupNotify=false
X-GNOME-Autostart-enabled=true" | tee "${HOME}/.config/autostart/com.nextcloud.desktopclient.nextcloud" &> /dev/null
}

nextcloud_sync() {
    mkdir -p ${HOME}/Desktop/
    mkdir -p ${HOME}/.config/
#    echo "Nextcloud Sync Manual by $scriptName
#
#1. Add your account in the Nextcloud client
#   - Change seetings like sync folders < 5MB without asking
#2. Only enable folders that you want to sync
#
#Attention: $scriptName added to Nextcloud an ignored file list like .cache, .mozilla, .ssh, .wine, libvirt, sddm, Trash and Steam
#
#End-to-end Encryption: Use Plasma Vaults to store secret information (performance can be slow when storing thousands of files)." | tee "${HOME}/Desktop/Nextcloud-Sync-Setup.txt" &> /dev/null

    #Maybe Backup and restore manually important folders? .aqbanking .bogofilter .config .kitch .kodi .mozilla .local .ssh .wine
    #+ .config/... calibre itch kitch kmymoney PhilippSchmieder Element SteamFastLogin
    #+ .local/share/... Steam TelegramDesktop

    #Run nextcloud for user from command line to create default config?
    #nextcloudcmd

    #Doesn't cover SYNC Folders
    #mkdir -p "${HOME}"/.config/Nextcloud
    #file="${HOME}/.config/Nextcloud/nextcloud.cfg"
    #echo "[General]
#confirmExternalStorage=true
#newBigFolderSizeLimit=500
#optionalServerNotifications=true
#useNewBigFolderSizeLimit=true
#
#[Accounts]
#0\Folders\1\ignoreHiddenFiles=false
#
#0\Folders\1\localPath=/home/rainer/
#0\Folders\1\paused=false
#0\Folders\1\targetPath=/
#0\authType=webflow
#0\user=@Invalid()
#0\webflow_user=rainer" | tee "$file" &> /dev/null

    mkdir -p "${HOME}"/.config/Nextcloud
    file="${HOME}/.config/Nextcloud/sync-exclude.lst"
    cp /etc/Nextcloud/sync-exclude.lst $file
    #Syntax is important!!! /.cache is not root!, only .cache
    #Cargo was created when installing luxtorpeda which requires rust to build
    #.kodi Thumbnails are a real performance issue
    #Steam Config Folder Sync if you plan to use this folder on multiple systems with the same Steam Remote library (sync if Linux/Proton game)
    echo ".bash_history
.cache/
.cargo/
.config/itch/Cache/
.config/kitch/Cache/
.config/itch/Partitions/
.config/kitch/Partitions/
.config/falkon/profiles/default/Service Worker/CacheStorage/
.dbus/
.gradle/
.kde4/
.kodi/
.mono/
.mozilla/
.pki/
.ssh/
.tor-browser/
.wine/
.java/
.jd/
.kodi/userdata/Thumbnails/
.local/share/flatpak/
.local/share/libvirt/
.local/share/Nextcloud/
.local/share/pikaur/
.local/share/sddm/
.local/share/Steam/
.local/share/Trash/
Local/
Lokal/
LocalOnly/
Steam/config/avatarcache/
Steam/config/htmlcache
Steam/config/widevine
config/librarycache/
config/shaderhitcache/" | tee -a "$file" &> /dev/null
}

kde_sweeper() {
    local file="/opt/pacbot/sweeper.sh"
    autostart_link_script ""
}

autostart_link_script() {
    local initProfile="$1"
    if [[ "$initProfile" == "" ]]; then
        mkdir -p ${HOME}/.config/autostart-scripts/
        ln -sf "$file" "${HOME}/.config/autostart-scripts/"
    else
        sudo mkdir -p /home/${initProfile}/.config/autostart-scripts/
        sudo ln -sf "$file" "/home/${initProfile}/.config/autostart-scripts/"
        sudo chown -R "$initProfile":"$initProfile" "/home/${initProfile}/.config/"
    fi
}

user_profile_afterinit() {
    local file="${HOME}/.local/bin/kde-init.sh"
    config_comment "gsettings" "#" "$file"
    config_comment "bash /opt/pacbot/" "#" "$file"
}

####################################
############ Interface #############
####################################

#See beginning of script
#runParameter=$1
if [[ "$runParameter" == "--inituser" ]]; then
    user_profile_init
fi
