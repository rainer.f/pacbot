#!/bin/bash
# backup - Only backup files that are important and unique per installation, author Rainer Finke, license GPLv3

####################################
######## Global Variables ##########
####################################

export backupDir="/home/BACKUP/"
export backupDirUser="${backupDir}$userName"

####################################
############ Functions #############
####################################

backup_system() {
    mkdir -p $backupDir
    cp /etc/fstab $backupDir
    cp /etc/crypttab $backupDir
    cp /boot/loader/entries/*.conf $backupDir
    # MariaDB
    cp /etc/my.cnf $backupDir 2> /dev/null
    # Apache
    cp /etc/httpd/conf/httpd.conf $backupDir 2> /dev/null
    cp /etc/httpd/conf/extra/* $backupDir 2> /dev/null
    # PHP
    cp more /etc/php/php.ini $backupDir 2> /dev/null
    cp more /etc/php/php.ini $backupDir 2> /dev/null
    cp /etc/php/conf.d/* $backupDir 2> /dev/null
    # Nextcloud
    cp /usr/share/webapps/nextcloud/config/config.php $backupDir 2> /dev/null
    # Letsencrypt
    cp /etc/letsencrypt/cli.ini $backupDir 2> /dev/null
    # Postfix
    cp /etc/postfix/main.cf $backupDir 2> /dev/null
    cp /etc/postfix/aliases $backupDir 2> /dev/null
    cp /etc/postfix/virtual_alias $backupDir 2> /dev/null
    cp /etc/postfix/mysql-*.cf $backupDir 2> /dev/null
    cp /usr/share/webapps/postfixAdmin/.htaccess $backupDir 2> /dev/null
    cp /usr/share/webapps/postfixAdmin/config.inc.php $backupDir$backupDirUser/.ssh
    # Dovecot
    cp /etc/dovecot/conf.d/10-ssl.conf $backupDir 2> /dev/null
    cp /etc/pam.d/dovecot $backupDir 2> /dev/null
    cp /etc/dovecot/conf.d/10-auth.conf $backupDir 2> /dev/null
    cp /etc/dovecot/dovecot.conf $backupDir 2> /dev/nul
    cp /etc/dovecot/conf.d/80-sieve.conf $backupDir 2> /dev/null
    cp /etc/dovecot/conf.d/10-mail.conf $backupDir 2> /dev/null
    cp /etc/dovecot/dovecot-sql.conf $backupDir 2> /dev/null
    # Getmail
    cp /etc/.getmail/getmailrc $backupDir 2> /dev/null
    # NFS
    cp /etc/exports $backupDir 2> /dev/null
}

backup_user() {
    mkdir -p $backupDirUser
    chown "$userName":users $backupDirUser
    # SSH
    mkdir -p $backupDirUser/.ssh
    cp $HOME/.ssh $backupDirUser/.ssh $backupDirUser/.ssh
    #Steam
    #cp /home/$userName/.local/share/Steam/userdata/*/7/remote/sharedconfig.vdf $backupDirUser
}
