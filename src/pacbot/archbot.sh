#!/bin/bash
# archbot - Arch Linux Initialization and Setup Tool, author Rainer Finke, license GPLv3
# Precondition: Arch Linux based system, installation without multiboot on disk + gpt + uefi + systemd + btrfs + zstd
# Note: Do not run this script with sh as it shows an error with mapfile, use "bash archbot.sh"

####################################
######### Global Variables #########
####################################

archinstallbot=false
declare -a iPackages
declare -a rPackages
declare -a rscPackages
gpuAMD=false
gpuIntel=false
gpuBroadcom=false
cpuIntel=false
cpuAMD=false
#Green=2, Orange=3
color=3
soc=""
userHome=""
createdProfile=false

#Package Groups - vulkan-swrast contains lavapipe
mesaPkg="mesa libdrm mesa-utils vulkan-icd-loader vulkan-tools vulkan-radeon vulkan-intel vulkan-mesa-layers vulkan-swrast llvm-libs opencl-rusticl-mesa clinfo"
mesaPkgX86="lib32-mesa lib32-libdrm lib32-mesa-utils lib32-vulkan-icd-loader lib32-vulkan-radeon lib32-vulkan-intel lib32-vulkan-mesa-layers lib32-llvm-libs lib32-opencl-mesa"
#Replaced by mesa: libva-mesa-driver mesa-vdpau
mesaVideoAccelerationPkg="libva-intel-driver libvdpau-va-gl libva libva-utils vdpauinfo"
#Replaced by mesa
#mesaVideoAccelerationPkgX86="lib32-libva-mesa-driver lib32-mesa-vdpau"
mesaPkgAll="$mesaPkg $mesaVideoAccelerationPkg"
pipewirePackages="pipewire pipewire-pulse pipewire-alsa pipewire-jack gst-plugin-pipewire wireplumber"
pipewirePackagesX86="lib32-pipewire lib32-pipewire-jack pipewire-v4l2 lib32-pipewire-v4l2"
plasmaMinimal="plasma-desktop plasma-workspace-wallpapers konsole discover flatpak-kcm ${pipewirePackages}"

####################################
########## Init Functions ##########
####################################

init_archbot() {
    export scriptName="archbot"
    export scriptDescription="Automated Arch Linux Installation & Management Tool"
    userName=$(logname)
	#Import settings
    source_dependencies_archbot

    init_tasks
    init_tasks_archbot
    init_CPU_GPU
}

source_dependencies_archbot() {
	#Get global user settings and dependencies
    source "$pkgPath/settings.sh"
	source "$pkgPath/helpers.sh"
	source "$pkgPath/systemchecks.sh"
	source "$pkgPath/interface.sh"
	source "$pkgPath/usbstickbot.sh" "--source-only"
    source "$pkgPath/archinstallbot.sh" "--source-only"
    source "$pkgPath/sysinfo.sh"
    #source "$pkgPath/gamebot.sh"
    source "$pkgPath/userprofile.sh" "--source-only"
	#source "$pkgPath/pacbot.sh" "--source-only"
}

init_tasks_archbot() {
    #Global variables
    languageCode=$(echo "$localeConf" | cut -c -2)
    languageCodeLong=$(echo "$localeConf" | cut -c -5)
}

init_CPU_GPU(){
    #lspci | grep ' VGA ' | cut -d" " -f 1 | xargs -i lspci -v -s {}
    local gpuAMDText=$(lspci | grep VGA | cut -d ":" -f3 | cut -c 2- | cut -c -22)
    if [[ "$gpuAMDText" == "Advanced Micro Devices" ]]; then
        gpuAMD=true
    else
        gpuAMD=false
    fi
    #local gpuIntel=$(lspci | grep VGA | cut -d ":" -f3 | cut -c 2- | cut -c -17)
    # Grep for " VGA" as on some devices 2 lines are returned, on a tablet was an NON-VGA devices
    local gpuIntelText=$(lspci | grep " VGA" | awk '{print $5}')
    #if [[ "$gpuIntelText" == "Intel Corporation" ]]; then
    if [[ "$gpuIntelText" == "Intel" ]]; then
        gpuIntel=true
    else
        gpuIntel=false
    fi

    local cpuText=$(lscpu | grep "GenuineIntel" | awk '{print $2}')
    if [[ "$cpuText" == "Intel" ]]; then
        cpuIntel=true
    else
        cpuIntel=false
    fi
    local cpuText=$(lscpu | grep "AuthenticAMD" | awk '{print $2}')
    if [[ "$cpuText" == "AuthenticAMD" ]]; then
        cpuAMD=true
    else
        cpuAMD=false
    fi

    #local cpuBroadcomText=$(lscpu | grep "GenuineIntel" | awk '{print $2}')
    #if [[ "$cpuBroadcomText" == "Intel" ]]; then
        soc="rp4"
    #fi
}

####################################
############ Init Arch #############
####################################

archlinux_homed_after_install_root() {
    echo "$(tput setaf $color)[  User  ] No $(tput setaf 2)normal user account$(tput setaf $color). Please setup a new user now...$(tput sgr0) "
    user_init "wheel"
    #bashrc_first_boot
    config_replacevalue "archbot" "#archbot" "/root/.bashrc"

    read -r -p "$(tput setaf $color)Reboot to login with new user $(tput setaf 2)${userNameNew}$(tput setaf $color)?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        echo "Please run $(tput setaf 2)archbot$(tput sgr0) after login with new user to continue with the system setup."
        systemctl reboot
    fi
}

archlinuxarm_after_install_root() {
    #Only possible from aarch and not from x64!
    printf "[  pacman-key ] Init Arch Linux ARM keys \n\n"
    pacman-key --init
    pacman-key --populate archlinuxarm
    printf "\n\n[   upgrade   ] Install system upgrades \n\n"
    pacman -Syu --noconfirm
    if [[ "$soc" == "rp4" ]] || [[ "$soc" == "rp3" ]]; then
        #BUG USB doesn't work with 5.11.4, video acceleration not available
        echo "[  workaround ] Please use on the Raspberry Pi 3/4 aarch64 the Raspberry Pi 4 linux package to be able to use USB, video acceleration and to start kodi"
        pacman -Sy linux-raspberrypi4 raspberrypi-firmware

        if [[ "$?" == 0 ]]; then
            #Workaround for raspberrypi4 kernel
            sed -i 's/mmcblk1/mmcblk0/g' /etc/fstab
        fi

        if [[ "$soc" == "rp4" ]]; then
            #Workaround...Set in boot/config.txt, only required on Rasperry kernel?
            echo "[  workaround ] Fullscreen without black borders around the picture on linux-raspberrypi4"
            echo "disable_overscan=1" | tee -a "/boot/config.txt"
        fi
    fi
    #base-devel contains fakeroot and strip which is required for makepkg
    pacman -Sy --noconfirm git base-devel

    #Create locale, user, hostname, block root account and install pacbot
    #BUG source lost somehow?
    source "$pkgPath/archinstallbot.sh" "--source-only"
    archlinux_install_chroot

    #This contained the first run archbot installer
    rm "/root/.bashrc"

    #Logout of root account and reboot
    read -r -p "$(tput setaf 2)[  finished  ]$(tput sgr0) Please $(tput setaf $color)reboot$(tput sgr0) your system now and login with you new user $(tput setaf 2)$userNameNew$(tput sgr0). Continue?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        reboot
    fi
}

archlinux_init_step1() {
    #Done as well by archinstallbot.sh
    enable_default_services

    if [[ "$machineArch" != "x86_64" ]]; then
        #Required for ARM machines that contain an image only
        locale_init

        #disable alarm user
        sudo userdel -r alarm
    fi

    #ATTENTION Some packages fail to install sometimes in step3
    sudo systemctl restart systemd-timesyncd.service
    sudo pacman -Sy --noconfirm "archlinux-keyring"

    linux_check_install "-lts"
    linux_32bitsupport_disable ""
    linux_32bitsupport_disable "-lts"
    fstab_config

    echo "$scriptName    [Arch Linux Init 1 - DONE]"
    #if  ( $installedByArchinstallbot ); then
    #    archlinux_init_step2
    #else
    #    installPacbot=false
    #    archinstallbot_conf
    #    fstab_changed_reboot
    #fi
}

archlinux_init_step2() {
    btrfs_setup
    mkinitcpio_compression
    packages_base_install
    udisks2_removable_disk_config
    enable_dotanddnssec
    ipv6_privacy_enable
    environment_variables_default
    makepkg_config
    pacman_config_yay_install
    systemd_pacman_hook
    packages_base_hardware_install

    if ( $installedByArchinstallbot ) && ( $installPacbot ); then
        pacbot_install
    else
        read -r -p "$(tput setaf $color)Do you want to enable [pacbot] update services and add fastfetch to bashrc?$(tput sgr0) [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            pacbot_install
        fi
    fi

    echo "$scriptName    [Arch Linux Init 2 - DONE]"
}

archlinux_init_step3_desktop() {
    desktop_install
	echo "$scriptName    [Arch Linux Init 3 Desktop - DONE]"
}

archlinux_init_step3_mediacenter() {
    media_center_install
    echo "$scriptName    [Arch Linux Init 3 Media Center - DONE]"
}

archlinux_init_step3_server() {
    server_install
    echo "$scriptName    [Arch Linux Init 3 Server - DONE]"
}

archlinux_optimize() {
    mkinitcpio_arm
    configure_services
    dbusbroker_install
    swappiness_usage false

    #No automatic change of disk scheduler, so deactivate this step, user can view it in the system info menu
    #printf "\nShow disk scheduler info\n"
    #check_ioscheduler

    systemd_config
    mkinitcpio_systemd
    mesa_specialflags

    pacman-static_install

    #osk_sdl
    systemd_boot_config_earlykms

    echo "$scriptName    [Arch Linux Optimization - DONE]"
}

####################################
######### Init Functions ###########
####################################

fstab_config() {
    local file="/etc/fstab"

	printf "\n$file: Do not write file access time to disk. Replaced [relatime] with $(tput setaf 2)noatime$(tput sgr0) \n"
    config_replacevalue "relatime" "noatime" "$file"

    #Add for SSD's DISCARD with discard=async. Option seems to help avoiding run out of disk space scenarios when weekly trim job is not enough.
    printf "\n$file: Set $(tput setaf 2)discard=async$(tput sgr0) for ssd's \n"
    #Not required anymore in Linux 6.2 ???
    config_replacevalue "ssd" "ssd,discard=async" "$file"
    #BUG Avoid double entries
    config_replacevalue "discard=async,discard=async" "discard=async" "$file"

    #printf "\n$file: Enable [autodefrag] \n"
    #Add autodefrag. Not recommended when using snapshots? But recommended for HDD's? On SSD it might reduce life time due to additional writes.
    #BUG DO NOT ADD THIS TO A NON BTRFS LINE otherwise you cannot boot anymore !!!!!
    #config_replacevalue "discard=async" "discard=async,autodefrag" "$file"
    #BUG Avoid double entries
    #config_replacevalue "autodefrag,autodefrag" "autodefrag" "$file"

    printf "\n$file: Changed btrfs compression from [lzo] to $(tput setaf 2)zstd$(tput sgr0) \n"
 	#BUG How to change this only for btrfs lines?
 	config_replacevalue "lzo" "zstd" "$file"

    fstab_apply_mount
}

fstab_apply_mount() {
    if ( $installedByArchinstallbot ); then
        printf "\nSystem installed by archinstallbot - no restart required. On x64_86 btrfs is already compressed with zstd"
        printf "\n"
    else
        fstab_changed_reboot
    fi
}

fstab_changed_reboot() {
    printf "\n"
    read -r -p "$(tput setaf $color)Please reboot the system to mount btrfs partitions with zstd compression. Afterwards continue with the setup procedure. Continue?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        sudo systemctl reboot
    else
        printf "\nTo apply btrfs compression as a mount option please reboot the system!\n"
    fi
}

udisks2_removable_disk_config() {
    #udisks2 must be installed as otherwise the folder will not exist
    local file="/etc/udisks2/mount_options.conf"
    echo "[defaults]" | sudo tee "$file" &> /dev/null
    #Use compression level 12 as this should max out every SD-card/HDD/SSD-QLC with 160MB/s write
    #https://docs.google.com/spreadsheets/d/1x9-3OQF4ev1fOCrYuYWt1QmxYRmPilw_nLik5H_2_qA/edit#gid=0
    echo "btrfs_defaults=noatime,compress-force=zstd:12,discard=async" | sudo tee -a "$file" &> /dev/null
    echo "ext4_defaults=noatime,discard=async" | sudo tee -a "$file" &> /dev/null
}

linux_check_install() {
    local linuxType="$1"
    if ! pacman -Q "linux$linuxType" &> /dev/null; then
        local freeBootMB=$(df -lh 2> /dev/null | grep "boot" | awk '{print $4}' | rev | cut -c 2- | rev)
        #Why is it sometimes changing to efi?
        if [[ "$freeBootMB" == "" ]]; then
            freeBootMB=$(df -lh 2> /dev/null | grep "efi" | awk '{print $4}' | rev | cut -c 2- | rev)
        fi
        if (( $freeBootMB > 100 )); then
            if [[ "$machineArch" == "aarch64" ]] && [[ "$linuxType" == "-lts" ]]; then
                echo "[ Info ] linux-$linuxType is not available on aarch64"
            else
                echo "Install $(tput setaf 2)Linux$linuxType$(tput sgr0)..."
                install_linux "$linuxType"
            fi
        else
            # Install additionally LTS kernel as an additional fallback, ask user as boot partition might be smaller than 300MB
            df -lh
            read -r -p "Do you want to install Linux-$linuxType as a fallback (RECOMMENDED). $(tput setaf $color)Too less space on boot partition, continue on own risk?$(tput sgr0)  [y/N]"
            if [[ "$REPLY" =~ [yY] ]]; then
                install_linux "$linuxType"
            fi
        fi
    else
        echo "Linux-$linuxType is already installed"
    fi
}

btrfs_setup() {
    if ( $installedByArchinstallbot ); then
        printf "\nSystem installed by archinstallbot. No re-compression required on x64_86 as btrfs is already compressed with zstd \n"
    else
        #printf "\nCompress all files on Btrfs Root partition. Not required if system was installed by archinstallbot! \n"
        read -r -p "$(tput setaf $color)Do you want to re-compress all files with zstd on the Btrfs root partition?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sudo btrfs filesystem defragment -r -v -czstd /
        else
            echo "Btrfs filesystem not compressed with zstd"
        fi
    fi

    btrfs_snapper_config
}

btrfs_snapper_config() {
    #Setup snapshots with snapper. If the /.snapshot exists already, this comman "snapper -c root create-config /" will fail, therefore copy the template manually
    sudo snapper -c root create-config / &> /dev/null
    if [[ "$?" == "1" ]]; then
        sudo cp /usr/share/snapper/config-templates/default /etc/snapper/configs/root
        # Activate config
        sudo sed -i "s/\=\"\"/\=\"root\"/"  /etc/conf.d/snapper
    fi

    #Keep only 10 instead of 50 snapshots is it requires to much space
    config_setvalue "NUMBER_LIMIT=" "\"10\"" "/etc/snapper/configs/root"
}

mkinitcpio_compression() {
    #2> /dev/null is required to hide this message "df: /run/user/1000/doc: Operation not permitted"
    local freeBootMB=$(df -lh 2> /dev/null | grep "boot" | awk '{print $4}' | rev | cut -c 2- | rev)
    #Why is it sometimes changing to efi?
    if [[ "$freeBootMB" == "" ]]; then
        freeBootMB=$(df -lh 2> /dev/null | grep "efi" | awk '{print $4}' | rev | cut -c 2- | rev)
    fi
    if (( $freeBootMB > 50 )); then
        local file="/etc/mkinitcpio.conf"
        printf "\nEnable $(tput setaf 2)zstd$(tput sgr0) for compressed Linux kernel images\n"
        config_comment "COMPRESSION=" "#" "$file"
        # zstd is supported since >= Linux 5.9
        #echo "COMPRESSION=\"zstd\"" | sudo tee -a "$file"
        config_uncomment "COMPRESSION=\"zstd\"" "#" "$file"
        #config_uncomment "COMPRESSION=\"lz4\"" "#" "$file"
        # mkinitcpio not required, it will happen automatically in next step by installing base packages
        #sudo mkinitcpio -p linux
    else
        printf "\nNOT possible to enable [zstd] compressed Linux image, to less space on boot partition!!!\n"
    fi
}

mkinitcpio_systemd() {
    file="/etc/mkinitcpio.conf"
    echo "Replace legacy udev with $(tput setaf 2)systemd$(tput sgr0)..."
    config_replacevalue "udev" "systemd" "$file"
    config_replacevalue " encrypt" " sd-encrypt" "$file"
    config_replacevalue "keymap" " sd-vconsole" "$file"
    #echo "$(tput setaf $color)Attention: If booting linux fails$(tput sgr0), please select in the boot menu $(tput setaf $color)linux-lts$(tput sgr0). Systemd will be only integrated into mkinitcpio of linux-lts as soon as you install the next update."
}

mkinitcpio_arm() {
    if [[ "$machineArch" == "aarch64" ]]; then
        #As of direct kernel boot, disable mkinitcpio creation on aarch64
        yay -Sy --noconfirm raspberrypi-stop-initramfs
    fi
}

onscreenkeyboard_boot_unl0kr() {
    #https://gitlab.com/cherrypicker/unl0kr
    #osk-sdl is replaced in PostmarketOS by unl0kr
    if [[ "$computerType" == "convertible" ]]; then
        yay -Sy --noconfirm unl0kr
        #Add to initramfs: evdev hid_multitouch i2c_hid_acpi intel_lpss_pci intel-lpss uhid uinput mac_hid i2c_smbus i2c_i801 hid_multitouch i2c_hid_acpi usbhid wacom ideapad_laptop lenovo_ymc think_lmi hid_generic mousedev
    fi
}

systemd_boot_config_earlykms() {
    echo "Adjust $(tput setaf 2)boot menu waiting time$(tput sgr0) and show log messages during boot"
    if ( $efi ); then
        #ATTENTION Timeout of 0 looks nicer during boot but hides the menu options which is not good in case when boot doesn't work, on a desktop some monitors turn on quite slow and this combined with a UEFI that shows only shortly a boot picture, it can happen that the boot menu is not visible, 3 seconds means it was visible only for less than one second, so use 4 seconds here. On a laptop 2 seconds are enough!
        if [[ "$computerType" == "desktop" ]]; then
            config_setvalue "timeout " "  3" "/boot/loader/loader.conf"
        else
            config_setvalue "timeout " "  2" "/boot/loader/loader.conf"
        fi

        #Disabled support for quiet boot as it feels faster even if it is slower and user can see error messages
        local file="/boot/loader/entries/archlinux.conf"
        config_replacevalue "$bootPar $bootPar" "$bootPar" "$file"
        if ( $quietBoot ); then
            kernel_boot_parameter_config "quiet"
        else
            config_replacevalue "quiet" "" "$file"
        fi

        #Do not set boot parameter in linux-lts as this is used as a fallback in case of boot errors!!!
        kernel_boot_parameter "/boot/loader/entries/archlinux.conf"
        if pacman -Q linux-hardened &> /dev/null; then
            kernel_boot_parameter "/boot/loader/entries/archlinux-hardened.conf"
        fi
    else
        if [[ "$machineArch" != "aarch64" ]]; then
            echo "Legacy Bios Grub mode"
            local file="/etc/default/grub"
            if [[ "$computerType" == "desktop" ]]; then
                config_setvalue "GRUB_TIMEOUT=" "3" "$file"
            else
                config_setvalue "GRUB_TIMEOUT=" "2" "$file"
            fi
            sudo grub-mkconfig -o /boot/grub/grub.cfg
        else
            echo "[ Info ] Quiet boot not enabled on aarch64"
        fi
    fi

    earlyKMS_enable

    #Enable pcie_aspm=force? Does it work with btrfs?

    if ( ! $prodServer ); then
        watchdog_disable ""

        #This is not helping much, so disable it
        #read -r -p "$(tput setaf $color)Please enter [loglevel=3 rd.udev.log_priority=3 rd.systemd.show_status=auto vt.global_cursor_default=0] to [$file] to enforce silent boot (ONLY IF udev replaced with systemd in previous step). Only for desktop+laptop! Continue?$(tput sgr0)  [y/N]"
        #if [[ "$REPLY" =~ [yY] ]]; then
            #sudo nano $file
        #fi

        plymouth_enable
    else
        echo "[   skip  ] Plymouth not installed on production server!"
        mknitcpio_build
    fi
}

mknitcpio_build() {
    if pacman -Q "linux" &> /dev/null; then
        sudo mkinitcpio -p linux
    fi
    if pacman -Q "linux-hardened" &> /dev/null; then
        sudo mkinitcpio -p linux-hardened
    fi
}

kernel_boot_parameter() {
    local file="$1"
    if [[ "$file" == "" ]]; then
        echo "ERROR no /boot/loader/entries/XXXXXXXX.conf file name provided"
    else
        #Loglevel 3 will hide unnecessary warning and info messages during boot, they can be viewed later in dmesg
        kernel_boot_parameter_config "loglevel=3"

        #Consoleblank will turn off the terminal output after the defined period of seconds to save power when no desktop run. In a low power system it went from 7,2W to 6.9W saving 0,3 Watt.
        kernel_boot_parameter_config "consoleblank=360"

        #Spin up all hard drive at the same time is normal on desktop hardware, so no need to query them after each other. This makes the boot slow if there are multiple hdd's installed
        #https://wiki.archlinux.org/index.php/Improving_performance/Boot_process#Staggered_spin-up
        #local sss=$(sudo dmesg | grep SSS | awk '{ print $5 }')
        #if [[ "$sss" == "SSS" ]]; then
            echo "Spin up all hard drives at the same time by disabling $(tput setaf 2)ahci SSS$(tput sgr0)"
            kernel_boot_parameter_config "libahci.ignore_sss=1"
        #fi

        if ( $cpuAMD ); then
            #Enable EPP for AMD (not always the default yet for Zen)
            kernel_boot_parameter_config "amd_pstate=active"
            #Active in linux >= 6.7 for RDNA2 and RDNA3
            #https://www.phoronix.com/news/AMD-More-Seamless-Boot
            #kernel_boot_parameter_config "amdgpu.seamless=1"
        fi
    fi
}

kernel_boot_parameter_config() {
    local addParameter="$1"
    config_replacevalue "$bootPar" "$bootPar $addParameter" "$file"
    #BUG remove double entries
    config_replacevalue "$addParameter $addParameter" "$addParameter" "$file"
}

packages_base_install() {
    #nano btrfs-progs openssh git from archinstallbot
    #cockpit needs udisks2 firewalld and it requires kexec-tools (why?)
    #progress required for dd to show progress, rsync is required for backups
    #Add asp to download package sources (debug).
    #nvme-cli is required to align some nvme's to 4096 instead if 512 bytes (LBA)
    #htop & btop offers I/O info from iotop as well in a second tab
    local basicPackages="less nano btrfs-progs snapper duperemove openssh git htop btop ncdu tmux reflector cockpit cockpit-storaged udisks2 firewalld bash-completion unrar dialog fastfetch wget bind progress rsync arch-install-scripts"
    local basicPackagesHW="smartmontools nvme-cli hdparm fwupd powertop tlp dmidecode lm_sensors"
    if [[ "$statusVM" == "none" ]]; then
        printf "\nInstall $(tput setaf 2)basic packages for hardware devices$(tput sgr0) (VM=$statusVM)\n"
        if ( $prodServer ); then
            pacmanSyNC "$basicPackages $basicPackagesHW"
        else
             pacmanSyNC "$basicPackages $basicPackagesHW fping arp-scan ddrescue"
        fi
    else
        printf "\nInstall $(tput setaf 2)basic packages$(tput sgr0) for all systems\n"
        pacmanSyNC "$basicPackages"
    fi

    bug_workarounds

    packages_base_cleanup
}

packages_base_hardware_install() {
    if [[ "$statusVM" == "none" ]]; then
        memtest_install

        tlp_config

        # Detect all sensors, not recommended to run this interactively, but it checks more sensos with this
        echo "Checking for system sensors..."
        yes yes | sudo sensors-detect &> /dev/null
        #sudo sensors-detect

        firmware_tools_arm
    fi
}

packages_base_cleanup() {
    #printf "\nRemove not required Arch Linux Packages\n"
    #sudo pacman -Rsc ***
    pacmanRscNC "man-db man-pages"

    if [[ "$statusVM" != "none" ]]; then
        # Remove Linux firmware in a virtual server environement
        sudo pacman -Rsc linux-firmware
    fi
}

minimal_system() {
    #Attenton: Only remove base on systemd-boot system, it is still required for grub to boot the system
    #Remove base package as it is the dependecy for many packages
    sudo pacman -R base
    pacmanRscNC "licenses xfsprogs"
    #Required as otherwise e.g. ping might be removed
    sudo pacman -D --asexplicit iputils iproute2
    if ( $efi ); then
        sudo pacman -R base
        pacmanRscNC "systemd-sysvcompat"
    fi
    packages_base_cleanup
    #sudo pacman -Rsc

    #POSSIBLE CLEANUPS
    #file="/etc/pacman.conf"
    #config_uncomment "#NoExtract" "#" "$file"
    #config_setvalue "NoExtract   = " "NoExtract =  usr/share/doc/* usr/share/man/* usr/share/gtk-doc/* usr/lib/libreoffice/help/* usr/share/help/* !usr/share/help/C/*" "$file"

    #Delete locale - 578MB
    #sudo rm -R "/usr/share/locale/"
    #Delete doc  - 569MB
    #sudo rm -R "/usr/share/doc/"
    #Delete man-pages - 110MB, remove man-db 109MB
    #sudo rm -R "/usr/share/man/"
    #Delete gtk-doc (even if not installed) - 36MB
    #sudo rm -R "/usr/share/gtk-doc/"
    #Delete licenses - 9MB, remove licenses 7MB
    #sudo rm -R "/usr/share/licenses/"

    #https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Installing_only_content_in_required_languages
    #yay localepurge
    #file="/etc/locale.nopurge"
    #config_uncomment "NEEDCONFIGFIRST" "#" "$file"
    #echo "en_US" | sudo tee -a "$file"
    #echo "en_US.UTF-8" | sudo tee -a "$file"
    #echo "de_DE" | sudo tee -a "$file"
    #echo "de_DE.UTF-8" | sudo tee -a "$file"
    #sudo localpurge-config
    #sudo localpurge
}

systemd_pacman_hook() {
    if ( $efi ); then
        # Install pacman.hook to automatically update systemd-boot after kernel update
        printf "\n$(tput setaf 2)Auto update systemd-boot$(tput sgr0) after kernel updates...\n"
        yay -S --noconfirm systemd-boot-pacman-hook
    fi
}

pacman-static_install() {
    #WARNING This requires a lot of time to compile the package, only install after chaotic-aur? Not available anymore in chaotic-aur???
    echo "(tput setaf $color)pacman-static will require a lot of compile time if chaotic-aur is not in use!!!$(tput sgr0)"
    #If pacman doesn't work anymore, run "sudo ./pacman-static -Syu"
    yay -S --noconfirm --needed pacman-static
}

pacbot_install() {
    pacbot_update_git
    bashrc_output_add
    pacbot_systemd_timer "daily"
    pacbot_systemd_timer "weekly"
    show_timers
    install_missingpackages
}

configure_services() {
    printf "\nEnable timers for services like $(tput setaf 2)SMART$(tput sgr0) disk health checks and $(tput setaf 2)fwupd$(tput sgr0) firmware update...\n"
    if [[ $statusVM == "none" ]]; then
        if pacman -Q fwupd &> /dev/null; then
            sudo systemctl enable fwupd-refresh.timer
            sudo systemctl enable smartd.service
        fi
    fi
    #Improve boot time
    sudo systemctl disable lvm2-monitor.service
    sudo systemctl mask lvm2-monitor.service
    sudo systemctl disable ModemManager.service
    sudo systemctl mask ModemManager.service
    #Several mainboards do have now wireless and it is enabled even if not required, so disable it. User should install iwd to get wireless
    sudo systemctl disable wpa_supplicant.service
    sudo systemctl mask wpa_supplicant.service
    #Recommended start early as possible to speed up boot. Is this true??? Service takes longer but does it improve other services?
    #sudo systemctl enable upower.service
    sudo systemctl disable upower.service
    #Systend.hwdb-update slows down boot from time to time, not required on installed systems, so disable it!
    sudo systemctl mask systemd-hwdb-update.service
    sudo systemctl enable systemd-networkd-wait-online.service
    sudo systemctl disable NetworkManager-wait-online.service
    #Disable NFSv3 automatic services due to security issues
    sudo systemctl mask rpcbind.service
    sudo systemctl mask rpcbind.socket
    systemd-networkd-wait-for-only-one-interface
}

systemd-networkd-wait-for-only-one-interface() {
    #Workaround so that networkd waits only for a bring up of just one interface
    #https://wiki.archlinux.org/title/Systemd-networkd#systemd-networkd-wait-online
    sudo mkdir -p "/etc/systemd/system/systemd-networkd-wait-online.service.d/"
    file="/etc/systemd/system/systemd-networkd-wait-online.service.d/wait-for-only-one-interface.conf"
    echo "[Service]
ExecStart=
ExecStart=/usr/lib/systemd/systemd-networkd-wait-online --any" | sudo tee -a "$file" &> /dev/null
}

configure_services_desktop() {
    #Low Memory App Killer, oomd is now available by default systemd package
    sudo systemctl enable systemd-oomd.service
    sudo systemctl start systemd-oomd.service
}

pacman_config_yay_install() {
    printf "\nInstall $(tput setaf 2)AUR$(tput sgr0) support and AUR installer $(tput setaf 2)yay$(tput sgr0)...\n"
    install_aur "yay-bin"
    echo "Set pacman & yay defaults..."
    pacman_config
    yay_config
    #install_pikaur
    console_colors
}

pacman_config() {
    echo "/etc/pacman.conf: Enable color in pacman output..."
    config_uncomment "Color" "#" "/etc/pacman.conf"
    #Increase parallel download (pacman 6)
    config_uncomment "ParallelDownloads" "#" "/etc/pacman.conf"
}

yay_config() {
    #To get color output, enable it in pacman.conf
    #BUG combinedupgrade cannot be used as it will not replace a package and try to build it from AUR!!! But leave it activated as it is more convenient for a user. Pacbot daily/weekly automatic runs will fix most of these issues.
    #BUG --editmenu is in yay --help, but shows error message???
    #yay --combinedupgrade --editmenu --nodiffmenu --save
    yay --combinedupgrade --nodiffmenu --save
    #Generate development package database used for devel update.
    yay -Y --gendb
}

console_colors() {
    #Maybe add this to ~/.config/nano/nanorc to avoid modified system file?
    config_uncomment "\/usr\/share\/nano\/\*.nanorc" "#" "/etc/nanorc"
}

desktop_install() {
    #Desktop
    #Task without a important GUI like YAST?
    printf "\n: KDE Desktop\n"
    read -r -p "$(tput setaf $color)Do you want to install KDE, Librewolf (Flatpak), LibreOffice, MPV and Nextcloud-Client?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        desktop_preconditions

        #BUG packagekit-qt6 will install packagekit which is not fully secure as it can update the system without root/password. Install on desktop should be fine as it is used based on user interaction. Avoid to use it with cockpit on a server???
        #INFO firewalld requires python-pyqt6 to show the applet, Jami requires qt6-shadertools, kimageformats is required for JPEG-XL support in KDE/Gwenview
        local plasmaMeta="$plasmaMinimal kdeplasma-addons kgamma kwallet-pam kwrited plasma-disks plasma-pa plasma-vault plasma-nm bluedevil drkonqi hspell kinfocenter ksshaskpass powerdevil"
        local desktopMeta="sshfs libreoffice-fresh libreoffice-fresh-${languageCode} hunspell-en_us hunspell-${languageCode} nmap libnfs nfs-utils xdg-desktop-portal libdecor android-tools android-udev mpv cups sane sane-airscan"
        local desktopGtkMeta="xdg-desktop-portal-gtk"
        local desktopQt6Meta="telegram-desktop jami-qt qt6-shadertools python-pyqt6 nextcloud-client"
        local desktopQt5Legacy="maliit-keyboard"
        local desktopOptionalQt5Legacy="marble kdevelop kdevelop-php pacmanlogviewer kompare krdc rapid-photo-downloader clipgrab elisa vlc"

        #Install virt-manager, android-udev directly here to improve installation speed
	    local plasmaDesktop="${mesaPkgAll} ${plasmaMeta} p7zip gstreamer-vaapi kimageformats kio-fuse sddm-kcm okular gwenview svgpart colord-kde spectacle kdegraphics-thumbnailers ffmpegthumbs plasma-systemmonitor plasma-firewall kget ktorrent kdiff3 kwalletmanager partitionmanager appmenu-gtk-module kde-gtk-config breeze-gtk kscreen kate kfind kcalc ktimer sweeper print-manager system-config-printer filelight dolphin-plugins kio-admin ark kdialog plasma-browser-integration kaccounts-providers $desktopQt6Meta krfb xdg-desktop-portal-kde $desktopMeta $desktopGtkMeta $desktopQt5Legacy"
        pacmanSyNC "$plasmaDesktop"

	    pipewire_enable

	    kde_cleanup

	    configure_services_desktop

	    #mail_install
	    #Use librewolf from Flatpak
	    #firefox_install
	    #virtmanager_install
	    #pacmanSyNC syncthing
	    cups_install

	    #User Profile
	    kde_init_autostart "--now"

	    #Questions:
	    #1. Change language in SDDM <> US? Only by adding xkeyboard layout?
	    #2. Disable Sounds

	    #Required for lan to show network symbol in task panel
	    printf "\nLaptop mode, enable Wifi and Wifi power management...\n"
        sudo systemctl enable NetworkManager.service
        ipv6_privacy_networkmanager_enable

        #Enable as well on desktop as some devices have built-in wifi or to use a USB wifi stick
        if [[ "$computerType" == "laptop" ]] || [[ "$computerType" == "convertible" ]] || [[ "$computerType" == "desktop" ]]; then
            wireless_install
        fi

        if [[ "$computerType" == "convertible" ]]; then
            #iio-sensor-proxy is required to automatically turn the screen in KDE if you turn the screen/tablet vertical/horizontal
            #BUG this seems to work only before standby
            pacmanSyNC "iio-sensor-proxy"

            #BUG After installing iio-sensor-proxy tablet ??? is immediately waking after suspending. The soloution is to disable XHC in wake events
            #echo XHC | sudo tee /proc/acpi/wakeup
        fi

        flatpak_packages_desktop_install

        #Enable chaotic-aur to avoid package compilations
	    chaotic_aur_repository

	    #Preparing Qt5 removal, this allows removal of vlc
	    yay -Sy --noconfirm phonon-qt6-mpv

	    #Tor
	    yay -Sy --noconfirm tor-browser-bin

	    #GUI for systemctl, removed in Plasma 6
	    #yay -Sy --noconfirm systemd-kcm

	    #Equalizer for Qt instead fo easyeffects and lsp-plugins (as it requires gnome-desktop-4)
	    yay -Sy --noconfirm jamesdsp

	    #Noise suppression for voice
	    yay -Sy --noconfirm noisetorch

        btrfs_desktop_tools_install

        #Android Phone screen mirroring
        yay -Sy --noconfirm guiscrcpy
        #Android requires android-tools android-udev, installed already above
        #yay -Sy --noconfirm android-completion

        #Fonts
        #pacmanSyNC "ttf-roboto ttf-roboto-mono"

	    #Synthing decentral file synchronizing between multiple devices
	    #yay -Sy --noconfirm syncthingtray
	fi
}

desktop_preconditions() {
    environment_variables_custom
    sddm_install_config
    mesa_install true
}

flatpak_packages_desktop_install() {
    echo "Install $(tput setaf 2)Flatpak$(tput sgr0)..."
    flatpak_install

    flatpak_packages_desktop

    flatpak_packages_privacy

    flatpak_packages_messenger3
}

sddm_install_config() {
    echo "Install and setup $(tput setaf 2)SDDM$(tput sgr0)..."
    sddm_install
    sddm_config
}

sddm_install() {
    if pacman -Q sddm-git &> /dev/null; then
        yes | sudo pacman -Sy --noconfirm "sddm"
    else
        sudo pacman -Sy --noconfirm "sddm"
    fi

    #WARNING Reload deamon otherwise SDDM cannot be enabled autotically?
    sudo systemctl daemon-reload
    sudo systemctl enable sddm.service
}

sddm_config() {
    sudo mkdir -p /etc/sddm.conf.d/
    local file="/etc/sddm.conf.d/kde_settings.conf"
    echo "[General]" | sudo tee "$file" &> /dev/null
    #SDDM 0.20 https://github.com/sddm/sddm/commit/e9e5ca681b164d203dcd7d8cd20679fb2d8ba421, valid options x11 (root), x11-user (rootless), wayland (rootless)
    echo "DisplayServer=wayland" | sudo tee -a "$file" &> /dev/null
    echo "GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[Theme]" | sudo tee -a "$file" &> /dev/null
    echo "Current=breeze" | sudo tee -a "$file" &> /dev/null
    echo "CursorTheme=breeze_cursors" | sudo tee -a "$file" &> /dev/null
    #Use kwin instead of Weston as a compositor
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[Wayland]" | sudo tee -a "$file" &> /dev/null
    #Alternative Input Method: qt6-virtualkeyboard (https://wiki.archlinux.org/title/SDDM#Virtual_keyboards)
    echo "CompositorCommand=kwin_wayland --drm --no-lockscreen --no-global-shortcuts --locale1 --inputmethod maliit-keyboard" | sudo tee -a "$file" &> /dev/null

    checkEncryptionHome=$(mount | grep " /home " | awk '{print $1}' | cut -c 6- | cut -c -6)
    if [[ "$checkEncryptionHome" == "mapper" ]]; then
        sddm_autologin_plasma
    fi

    #xorg_locale
}

sddm_autologin_plasma() {
    local file="/etc/sddm.conf.d/kde_settings.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[Autologin]" | sudo tee -a "$file" &> /dev/null
    echo "Relogin=false" | sudo tee -a "$file" &> /dev/null
    #This is already default in SDDM
    #echo "ReuseSession=true" | sudo tee -a "$file" &> /dev/null
    #Plasma 6 Wayland by default, before plasmawayland, fallback now with plasmax11
    echo "Session=plasma.desktop" | sudo tee -a "$file" &> /dev/null
    echo "User=$userName" | sudo tee -a "$file" &> /dev/null
}

sddm_autologin_kodi() {
    local file="/etc/sddm.conf.d/kodi_settings.conf"
    echo "[Autologin]" | sudo tee "$file" &> /dev/null
    echo "Relogin=true" | sudo tee -a "$file" &> /dev/null
    echo "Session=kodi.desktop" | sudo tee -a "$file" &> /dev/null
    echo "User=$userName" | sudo tee -a "$file" &> /dev/null
}

media_center_install() {
    #Media Center
    printf "\nMedia Center\n"
    read -r -p "$(tput setaf $color)Do you want the Kodi Media Center?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        desktop_preconditions

        kodi_install

        pipewire_enable

        yay -S --noconfirm "kodi-standalone-service"
        read -r -p "$(tput setaf $color)Do you want to login automatically to Kodi after boot?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sddm_autologin_kodi
            #BUG Kodi on Wayland (cage) is not able to turn off the screen to save energy, this means it consumes +0,5W more energy on a 10W system and overall the enery consumptions seems to be slightly higher
            #sudo systemctl disable sddm.service
            #sudo systemctl disable sddm-plymouth.service
            #sudo systemctl enable kodi-wayland.service
        fi

        #ATTENTION Kodi cannot find with libnfs the local NFS servers as of the firewall
        #sudo systemctl disable firewalld.service
        #firewallZone=$(firewall-cmd --get-default-zone)
        #sudo firewall-cmd --zone="$firewallZone" --add-service nfs
        #sudo firewall-cmd --zone="$firewallZone" --add-service rpc-bind
        #BUG NFS is using dynamic ports and therefore it NFS must be setup manually with static ports, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/storage_administration_guide/s2-nfs-nfs-firewall-config

    fi
}

media_center_jellyfin_install() {
    #Media Center
    printf "\nMedia Center\n"
    read -r -p "$(tput setaf $color)Do you want the Jellyfin Media Center?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        desktop_preconditions

        pacmanSyNC "$plasmaMinimal"
        yay -S --noconfirm "jellyfin-media-player"

        pipewire_enable

        yay -S --noconfirm "kodi-standalone-service"
        read -r -p "$(tput setaf $color)Do you want to login automatically to Kodi after boot?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sddm_autologin_plasma
            #autostart_jellifin_media_player
        fi
    fi
}

kodi_install() {
        if [[ "$machineArch" == "aarch64" ]]; then
            pacmanSyNC "${mesaPkgAll} kodi-rpi libnfs cage xorg-xwayland ${pipewirePackages}"
        else
            #kodi-x11 is a new package? Should it be installed by default?
            #NOTE For the standalone kodi-wayland.service cage and xorg-xwayland is required! The kodi-wayland package got removed 2023-04
            pacmanSyNC "${mesaPkgAll} kodi libnfs cage xorg-xwayland ${pipewirePackages}"
        fi

        #JellyCon integrates the remote library to Kodi
        #kodi-addon-jellyfin can create a local librabry, but this might create an issue to high disk space usage on some TV's
        yay -S --noconfirm kodi-addon-jellycon
}

kodi_config_defaults() {
    #WARNING Config file is not overwritten, only created on the first run!
    local link="https://raw.githubusercontent.com/xbmc/xbmc/master/system/keymaps"

    local linkFile="$link/mouse.xml"
    local file="${HOME}/.kodi/userdata/keymaps/mouse.xml"
    mkdir -p "${HOME}/.kodi/userdata/keymaps"
    wget --show-progress --quiet --continue "${linkFile}" --output-document="$file"
    #BUG do not add here </rightclick> as the slash doesn't work in the config_replacevalue function
    config_replacevalue "<rightclick>rightclick<" "<rightclick>back<" "$file"

    #Doesn't work on a Wayland compositor like KDE Kwin, only in standalone
    linkFile="$link/keyboard.xml"
    file="${HOME}/.kodi/userdata/keymaps/keyboard.xml"
    wget --show-progress --quiet --continue "${linkFile}" --output-document="$file"
    config_replacevalue "<power>ActivateWindow(ShutdownMenu)<" "<power>AudioNextLanguage<" "$file"
    config_replacevalue "<sleep>ActivateWindow(ShutdownMenu)<" "<sleep>AudioNextLanguage<" "$file"

    #To reconfigure certain keys, disable the systemd logind control over it
    file="/etc/systemd/logind.conf"
    config_replacevalue "#HandlePowerKey=poweroff" "HandlePowerKey=ignore" "$file"
}

gaming_install() {
    #Gaming System
    printf "\nGaming Center\n"
    read -r -p "$(tput setaf $color)Do you want install a Gaming System?$(tput sgr0) Please note that Steam is proprietary software!  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        linux_32bitsupport_enable ""

        mesa_install true
        #Steam requires still 32 bit packages as Steam is not yet provided as a 64 bit package and there are still 32 bit games
        mesa_install_32bit

        #Steam: Do not install steam-native-runtime, too many dependencies, may not work for several games and Steam provides a container runtime by default
        #SteamNewGamePadUI+Gamescope+Manguhud: glfw-x11 is required, if gamescope-session-steam-git should show the mangohud. glfw-wayland doesn't work yet.
        #GOverlay is a GUI for MangoHud and VKBasalt
        pacmanSyNC "steam lsb-release gamescope mangohud goverlay lib32-mangohud glfw-x11 sdl12-compat lib32-sdl12-compat ${pipewirePackagesX86}"
        echo "Gamescope Wayland & Vulkan Compositor: Please set in Steam the launch parameter [gamescope %command%]"
        echo "MangoHud: Please set in Steam the launch parameter [mangohud %command%]"

        gaming_optimize
        environment_variables_gaming

        #SteamNewGamePadUI in SDDM (based on Gamescope & Mangohuf)
        yay -S --noconfirm gamescope-session-steam-git

        vkbasalt_install_config

        #Use Proton from Steam?
        #proton_standalone_install

        if ( $gpuAMD ); then
            #amd_radeonprofile_install
            #pacmanSyNC "corectrl"
            #TuxClocker 1.3 for AMD? it shows as well CPU scaling / Mhz
            #WattmanGTK
            yay -S --noconfirm "lact"
        fi
	fi
}

server_install() {
	#Server
    printf "\nServer\n"
    read -r -p "$(tput setaf $color)Do you want to install a Server System?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then

        printf "\nCockpit\n"
        read -r -p "Do you want to enable the $(tput setaf $color)Cockpit web frontend$(tput sgr0) to manage the local and remote hosts?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            cockpit_install_config
        fi

        read -r -p "Do you want to install $(tput setaf $color)libvirtd$(tput sgr0) to run Virtual Machines?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            libvirt_install
            network_bridge_install
        fi

        printf "\nWebserver\n"
        read -r -p "Do you want to install a webserver with $(tput setaf $color)Apache, MariaDB, PHP and PHPMyAdmin$(tput sgr0)?  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            lamp_install
        fi

        logrotate_install

        echo "$scriptName    [Arch Linux Init 3 - DONE]"
	fi
}

cockpit_install_config() {
    sudo systemctl enable cockpit.socket
    sudo systemctl start cockpit.socket
    bug_workaroud_cockpit_hosts
}

flatpak_install() {
    if ! pacman -Q flatpak &> /dev/null; then
	    printf "\nInstall $(tput setaf 2)flatpak$(tput sgr0)...\n"
        pacmanSyNC "flatpak"
    fi
    #Default repo's that should be automatically available, run with sudo to avoid user popup when sudo was authenticated already
    #flathub exists by default, but why is it needed again for the user?
    flatpak_repositories_add
}

mesa_install() {
    noConfirm="$1"
    installCommand="sudo pacman -Sy"
    if ( $noConfirm ); then
        installCommand="pacmanSyNC"
    fi
    if [[ "$statusVM" == "none" ]]; then
        repository_enable "multilib"
        #Do not add --noconfirm as it will skip mesa-git to mesa downgrade
        if pacman -Q vulkan-mesa-layer-git &> /dev/null; then
            sudo pacman -Rsc vulkan-mesa-layer-git
        fi
        if pacman -Q lib32-vulkan-mesa-layer-git &> /dev/null; then
            sudo pacman -Rsc lib32-vulkan-mesa-layer-git
        fi
        if pacman -Q vulkan-mesa-layers-git &> /dev/null; then
            sudo pacman -Rsc vulkan-mesa-layers-git
        fi
        if pacman -Q lib32-vulkan-mesa-layers-git &> /dev/null; then
            sudo pacman -Rsc lib32-vulkan-mesa-layers-git
        fi
        if pacman -Q llvm-libs-minimal-git &> /dev/null; then
            sudo pacman -R llvm-libs-minimal-git  &> /dev/null
        fi
        if pacman -Q lib32-llvm-libs-minimal-git &> /dev/null; then
            sudo pacman -R lib32-llvm-libs-minimal-git  &> /dev/null
        fi
        # BUG?
        #if pacman -Q llvm-libs-git &> /dev/null; then
        #    sudo pacman -R llvm-libs-minimal-git
        #fi
        # BUG?
        #if pacman -Q lib32-llvm-libs-git &> /dev/null; then
        #    sudo pacman -R lib32-llvm-libs-minimal-git
        #fi

        echo "Install $(tput setaf 2)Mesa$(tput sgr0) graphic drivers..."

        if [[ "$machineArch" != "aarch64" ]]; then
            $installCommand $mesaPkgAll vulkan-intel
        elif [[ "$machineArch" == "aarch64" ]]; then
            $installCommand $mesaPkgAll vulkan-broadcom
        fi

        #Cleanup
        if pacman -Q llvm-libs-git &> /dev/null; then
            sudo pacman -R llvm-libs-git
        fi
        if pacman -Q lib32-llvm-libs-git &> /dev/null; then
            sudo pacman -R lib32-llvm-libs-git
        fi
    else
        $installCommand virglrenderer vulkan-virtio
    fi
}

mesa_install_32bit() {
    if [[ "$statusVM" == "none" ]]; then
        pacmanSyNC $mesaPkgX86 $mesaVideoAccelerationPkgX86
    else
        pacmanSyNC lib32-vulkan-virtio
    fi
}

mesagit_repository() {
    repository_install "mesa-git"

    sudo nano /etc/pacman.conf

    chaotic_aur_repository
}

repository_mesa-git_add() {
    local file="/etc/pacman.conf"
    #How to add this in position 1???
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[mesa-git]" | sudo tee -a "$file" &> /dev/null
    #Seems like some packages are not signed
    #Not required anymore?
    echo "SigLevel = Optional" | sudo tee -a "$file" &> /dev/null
    echo "Server = https://pkgbuild.com/~lcarlier/\$repo/\$arch" | sudo tee -a "$file" &> /dev/null
}

chaotic_aur_repository() {
    repository_install "chaotic-aur"

    #sudo nano /etc/pacman.conf

    #WARNING Required to sync repository otherwise there are error messages during the installation
    sudo pacman -Syy
}

repository_chaotic-aur_add() {
    #https://aur.chaotic.cx/
    #old -> https://lonewolf.pedrohlc.com/chaotic-aur/
    #sudo pacman-key --keyserver keys.mozilla.org -r 3056513887B78AEB
    #sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
    #sudo pacman-key --lsign-key FBA220DFC880C036
    sudo pacman-key --lsign-key 3056513887B78AEB
    #Not available without chaotic-aur repo
    #yay -S --noconfirm "chaotic-mirrorlist"
    #yay -S --noconfirm "chaotic-keyring"
    #https://aur.chaotic.cx/
    sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    local file="/etc/pacman.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[chaotic-aur]" | sudo tee -a "$file" &> /dev/null
    #echo "Server = http://lonewolf-builder.duckdns.org/\$repo/\$arch" | sudo tee -a "$file" &> /dev/null
    echo "Include = /etc/pacman.d/chaotic-mirrorlist" | sudo tee -a "$file" &> /dev/null
}

testing_enable() {
    repository_enable "core-testing"
    repository_enable "extra-testing"
    repository_enable "multilib-testing"
}

testing_disable() {
    repository_disable "core-testing"
    repository_disable "extra-testing"
    repository_disable "multilib-testing"
}

testingkde_enable() {
    #enable_testing
    repository_install "kde-unstable"
    #BUG only added to last line!!!
    echo "Please move kde-unstable before testing manually in /etc/pacman.conf"
}

testingkde_disable() {
    #Disable_testing
    read -r -p "$(tput setaf $color)Do you want to disable KDE-unstable?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        repository_disable "kde-unstable"
    fi
}

chaotic-aur_disable() {
    #Disable_testing
    read -r -p "$(tput setaf $color)Do you want to disable Chaotic-Aur?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        repository_disable "chaotic-aur"
    fi
}

parabola_enable() {
    #enable_testing
    read -r -p "$(tput setaf $color)Do you want to enable Parabola libre repository? WARNING: This will temporarly disable mandatory package singing to be able to install the Parabola keyring. But it will be enabled afterwards again!$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        repository_parabola_init
    fi
}

parabola_migration_from_arch() {
    local repositoryLibre=$(more "/etc/pacman.conf" | grep "mirrorlist-libre")
    if [[ "$repositoryLibre" != "Include = /etc/pacman.d/mirrorlist-libre" ]]; then
        repository_parabola_init
    fi

    read -r -p "$(tput setaf $color)If you want to migrate from Arch Linux to Parabola GNU/Linux-libre, please move in /etc/pacman.conf the [libre] repository MANUALLY above all others. Continue$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        repository_parabola_migration
    fi
}

parabola_disable() {
    #Disable_testing
    read -r -p "$(tput setaf $color)Do you want to disable Parabola libre repository?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        repository_parabola_delete
    fi
}

debug_enable() {
    #enable_testing
    #BUG only added to last line!!! and addearpscand multiple times
    read -r -p "$(tput setaf $color)Do you want to enable Debug Symbols and Qt5-Debug Repository?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        debug_symbols_enable
        repository_install "qt-debug"
    fi
}

debug_disable() {
    #Disable_testing
    read -r -p "$(tput setaf $color)Do you want to disable Debug Symbols and Qt5-Debug Repository?$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        debug_symbols_disable
        repository_disable "qt-debug"
    fi
}

repository_install() {
    local name=$1
    if [[ "$name" == "" ]]; then
        echo "ERROR no repository name provided"
    else
        local repository=$(more /etc/pacman.conf | grep "$name" | awk 'NR==1')
        local file="/etc/pacman.conf"
        if [[ "$repository" != "[$name]" ]]; then
            read -r -p "$(tput setaf $color)Do you want to enable $name?$(tput sgr0)  [y/N]"
            if [[ "$REPLY" =~ [yY] ]]; then
                if [[ "$repository" == "#[$name]" ]]; then
                    repository_enable "$name"
                    echo "$name enabled"
                else
                    repository_${name}_add
                    echo "$name added"
                fi
            fi
        else
            echo "$name is already activated"
        fi
    fi
}

repository_enable() {
    local name=$1
    if [[ "$name" == "" ]]; then
        echo "ERROR no repository name provided"
    else
        local file="/etc/pacman.conf"
        config_uncomment "\[$name\]" "#" "$file"
        config_uncommentmutliline "\[$name\]" "#" "$file"
    fi
}

repository_disable() {
    local name=$1
    if [[ "$name" == "" ]]; then
        echo "ERROR no repository name provided"
    else
        local file="/etc/pacman.conf"
        config_comment "\[$name\]" "#" "$file"
        #BUG
        config_commentmutliline "\[$name\]" "#" "$file"
    fi
}

repository_kde-unstable_add() {
    #BUG How to avoid to add this twice?
    #BUG How to add this before testing?
    local file="/etc/pacman.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[kde-unstable]" | sudo tee -a "$file" &> /dev/null
    echo "Include = /etc/pacman.d/mirrorlist" | sudo tee -a "$file" &> /dev/null
}

repository_qt-debug_add() {
    #BUG How to avoid to add this twice?
    #BUG How to add this before testing?
    local file="/etc/pacman.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[qt-debug]" | sudo tee -a "$file" &> /dev/null
    echo "Server = https://qutebrowser.org/qt-debug/\$arch" | sudo tee -a "$file" &> /dev/null

    echo "If you install a package from qt5-debug, please add the key with gpg --recv-keys ??????? or install the package locally with pacman -U /var/cache/pacman/pkg/..."
}

repository_parabola_add() {
    #BUG How to avoid to add this twice?
    #BUG How to add this before testing?
    local file="/etc/pacman.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[libre]" | sudo tee -a "$file" &> /dev/null
    echo "Include = /etc/pacman.d/mirrorlist-libre" | sudo tee -a "$file" &> /dev/null
    sudo nano "$file"
}

repository_parabola_init() {
    #https://wiki.parabola.nu/Migration_from_Arch
    local file="/etc/pacman.conf"
    #BUG How to set this only in the first line? And not in the others e.g. of KDE-unstable???
    #disable the package verification for some minutes to install the Parabola keyring
    config_uncomment "RemoteFileSigLevel = " "#" "$file"
    config_setvalue "RemoteFileSigLevel = " "Never" "$file"
    #Download and install the Parabola keyring and then the mirror list
    sudo pacman -U https://www.parabola.nu/packages/core/i686/archlinux32-keyring-transition/download/
    sudo pacman -U https://www.parabola.nu/packages/libre/x86_64/parabola-keyring/download
    sudo pacman -U https://www.parabola.nu/packages/libre/x86_64/pacman-mirrorlist/download
    #Move the new mirrorlist to a separate file so that it doesn't get overwritten by the Arch Linux mirrorlist
    sudo mv /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist-libre
    #Add the parabola repository

    repository_parabola_add

    #Ee-enable now the package verification
    config_comment "RemoteFileSigLevel = " "#" "$file"
    config_setvalue "RemoteFileSigLevel = " "Required" "$file"
    #Clean the pacman cache and afterwards force the package database synchronisation
    sudo pacman -Scc
    sudo pacman -Syy
    #Reinstall the Parabola keyring via pacman and populate the keys
    sudo pacman -Sy --noconfirm parabola-keyring
    sudo pacman-key --populate parabola
    #Re-install Arch Linux mirrorlist package
    sudo pacman -Sy --noconfirm pacman-mirrorlist
}

repository_parabola_migration() {
    local file="/etc/pacman.conf"
    sudo nano "$file"

    config_replacevalue "mirrorlist-libre" "mirrorlist" "$file"
    sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist-archlinux
    sudo mv /etc/pacman.d/mirrorlist-libre /etc/pacman.d/mirrorlist
    #BUG Delete manually conflicting packages that are not handled by Parabola
    #sudo pacman -R kde-graphics-meta kimagemapeditor
    echo "Deinstall bade now as it would require now already your-freedom as a dependency"
    sudo pacman -R base
    #sudo pacman -Rsc nmap ktorrent clibgrab gst-plugins-bad-libs
    sudo pacman -Scc
    sudo pacman -Syy
    sudo pacman -Sy --noconfirm parabola-keyring
    sudo pacman -Sy --noconfirm pacman-mirrorlist
    sudo pacman-key --refresh

    read -r -p "$(tput setaf $color)WARNING: After the switch to Parabole GNU/Linux-libre all non free firmware will be deleted. It is possible that your system cannot boot anymore or that some hardware won't work anymore after booting to Parabola!!!$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        sudo pacman -Suu pacman
        linux_check_install "-libre"
        linux_check_install "-libre-lts"
        linux_32bitsupport_disable "-libre"
        linux_32bitsupport_disable "-libre-lts"
    fi
}

parabola_yourfreedom() {
    #Blacklist see https://git.parabola.nu/blacklist.git/plain/blacklist.txt
    #This is quite restrictive and will remove many "useful" packages for normal users leaving as non-free network services are considered harmful
    # and user shouldn't be able to decide anymore for himself to use e.g. flatpak
    sudo pacman -R kde-graphics-meta kimagemapeditor
    sudo pacman -Rsc nmap ktorrent clibgrab gst-plugins-bad-libs
    sudo pacman -Suu base your-freedom
}

parabola_nonprism() {
    #https://wiki.parabola.nu/Nonprism
    local file="/etc/pacman.conf"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "[nonprism]" | sudo tee -a "$file" &> /dev/null
    echo "Include = /etc/pacman.d/mirrorlist" | sudo tee -a "$file" &> /dev/null

    read -r -p "$(tput setaf $color)If you want to enforce Non-Prism packages, please move in /etc/pacman.conf the [nonprism] repository MANUALLY above all others. Continue$(tput sgr0)  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        sudo nano "$file"
    else
        echo "Aborted"
    fi
    sudo pacman -S your-privacy

    parabola_yourfreedom_emu
}

parabola_yourfreedom_emu() {
    #https://wiki.parabola.nu/Emulator_licensing_issues
    sudo pacman -S your-freedom_emu
}

repository_parabola_delete() {
    sudo pacman -Rsc parabola-keyring
    repository_disable "parabola"
}

mesa_specialflags() {
    if ( $efi ); then
        local file="/boot/loader/entries/archlinux.conf"
        echo "--------Boot parameter--------"
        #Enable overclocking/underclocking, or amdgpu.ppfeaturemask=0xffffffff ???
        mesa_specialflags_parameter

        #local file="/boot/loader/entries/archlinux.conf"
        #echo "Optional add: radeon.si_support=0 amdgpu.si_support=1 radeon.cik_support=0 amdgpu.cik_support=1"
        read -r -p "$(tput setaf $color)Verify boot parameter in $file?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sudo nano "$file"
        fi
    else
        echo "Legacy Bios Grub mode - not supported to addjust kernel boot parameters"
    fi
}

mesa_specialflags_parameter() {
    if ( $gpuAMD ); then
        echo "AMDGPU: Enabled amdgpu.ppfeaturemask=0xfffd7fff (Clock) amdgpu.dcfeaturemask=0x8 (PSR to save power)"
        # psr panel self refresh, sudo dmesg -T | grep -i psr, sudo cat /sys/kernel/debug/dri/0/eDP-1/psr_state
        kernel_boot_parameter_config "amdgpu.dcfeaturemask=0x8"
        # Allow change clock frequency of GPU
        kernel_boot_parameter_config "amdgpu.ppfeaturemask=0xfffd7fff"
    elif ( $gpuIntel ); then
        echo "Intel GPU: Enabled i915.enable_fbc=1 (FBC) i915.enable_psr=1 (PSR) to reduce power consumption"
        #This might lead to flickering, but can reduce power consumption
        kernel_boot_parameter_config "i915.enable_fbc=1"
        #If you want to show only the bios logo, enable fastboot
        #BUG Intel HD Graphics 515 flickering => set intel_idle.max_cstate=1
        #config_replacevalue "$bootPar" "$bootPar i915.fastboot=1" "$file"
        #BUG Intel flickering, Disable panel self refresh on some devices to avoid flickering= i915.enable_psr=0
        kernel_boot_parameter_config "i915.enable_psr=1"
    fi
}

vkbasalt_install_config() {
    #Flatpak?
    yay -S --noconfirm vkbasalt
    yay -S --noconfirm lib32-vkbasalt
    mkdir ${HOME}/.local/share/vkBasalt/ && cp /usr/share/vkBasalt/vkBasalt.conf.example ${HOME}/.local/share/vkBasalt/vkBasalt.conf
    echo "VKBasalt image sharpening: Please set in Steam the launch parameter [ENABLE_VKBASALT=1 %command%] for a 64bit game or [ENABLE_VKBASALT32=1 %command%]] for 32bit"
}

makepkg_config() {
    #Use all cores to compile packages
    local file="/etc/makepkg.conf.d/archbot.conf"
    echo "Autmatically $(tput setaf 2)use all cores to compile$(tput sgr0) native packages $file..."
    #echo "MAKEFLAGS=\"-j\$(grep -c '^processor' /proc/cpuinfo)\"" | sudo tee -a "$file" &> /dev/null
    #echo "MAKEFLAGS=\"-j\$(nproc)\"" | sudo tee -a "$file" &> /dev/null
    #config_uncomment "MAKEFLAGS=" "#" "$file"
    #config_setvalue "MAKEFLAGS=" "\"-j\$(nproc)\"" "$file"
    echo "#Use all cores of the CPU when compiling
MAKEFLAGS=\"-j\$(nproc)\"
#Use x86_64
CFLAGS=\"-march=native -O2 -pipe -fno-plt -fexceptions \\
        -Wp,-D_FORTIFY_SOURCE=3 -Wformat -Werror=format-security \\
        -fstack-clash-protection -fcf-protection \\
        -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer\"
#Disable Debug Symbol Packages
OPTIONS=\"(strip docs !libtool !staticlibs emptydirs zipman purge !debug)\"" | sudo tee "$file" &> /dev/null
    #BUG Compiling qt5-base will exit with an error due to avx, disabling is the best if it doesn't work for all packages!
    #config_replacevalue "-march=x86-64 -mtune=generic" "-march=native" "$file"
    #config_replacevalue "-march=native" "-march=x86-64 -mtune=generic" "$file"
}

wireless_install() {
    local file="/etc/NetworkManager/conf.d/wifi_backend.conf"
    echo "[device]" | sudo tee "$file" &> /dev/null
    echo "wifi.backend=iwd" | sudo tee -a "$file" &> /dev/null

    sudo systemctl enable NetworkManager-dispatcher.service
    sudo systemctl mask systemd-rfkill.service
    sudo systemctl mask systemd-rfkill.socket

    echo "Install Wifi (iwd) and Bluetooth (bluez-utils) and enable iwd in /etc/NetworkManager/conf.d/wifi_backend.conf"
    if ! pacman -Q iwd &> /dev/null; then
        pacmanSyNC "iwd tlp-rdw bluez-utils"
    fi
    sudo systemctl disable wpa_supplicant.service
    sudo systemctl enable iwd.service

    sudo systemctl enable bluetooth.service
}

wireless_wakeonlan_disable() {
    #Already done by TLP by default => Tested this for a device where it doesn't work
    #Disable Wake On Lan for Wifi devices to save power
    echo "Wireless Disable Wake On Lan to save Power"
    local file="/etc/udev/rules.d/81-wifi-powersave.rules"
    echo "ACTION==\"add\", SUBSYSTEM==\"net\", KERNEL==\"wl*\", RUN+=\"/usr/bin/iw dev \$name set power_save on\"" | sudo tee "$file" &> /dev/null
}

dbusbroker_install() {
    #https://wiki.archlinux.org/index.php/D-Bus
    printf "\nInstall $(tput setaf 2)dbus-broker$(tput sgr0) and replace legacy dbus service...\n"
    if ! pacman -Q dbus-broker &> /dev/null; then
        pacmanSyNC "dbus-broker"
    fi
    sudo systemctl disable dbus.service
    sudo systemctl enable dbus-broker.service
    sudo systemctl --global enable dbus-broker.service
    echo "To use <dbus-broker> the system needs a restart"
}

swappiness_usage() {
    #https://wiki.archlinux.org/index.php/Swap#Swappiness
    local gamingSystemLoc="$1"
    local file="/etc/sysctl.d/99-swappiness.conf"
    #if ( $gamingSystemLoc ); then
    #    echo "vm.swappiness=1" | sudo tee "$file" &> /dev/null
    #else
        echo "vm.swappiness=10" | sudo tee "$file" &> /dev/null
    #fi
    echo "vm.vfs_cache_pressure=50" | sudo tee -a "$file" &> /dev/null
}

earlyKMS_enable() {
    #GPU KMS
    echo "Enabled $(tput setaf 2)early kernel modesetting$(tput sgr0) for GPU's..."
    local file="/etc/mkinitcpio.conf"
    if ( $gpuAMD ); then
        config_setvalue "MODULES=" "(amdgpu)" "$file"
    elif ( $gpuIntel ); then
        config_setvalue "MODULES=" "(i915)" "$file"
    fi

    #read -r -p "Replaced [udev] with [systemd] in [$file] to improve boot. $(tput setaf $color)Review mkinitcpio.conf?$(tput sgr0) [y/N]"
    #if [[ "$REPLY" =~ [yY] ]]; then
    #    local file="/etc/mkinitcpio.conf"
    #    sudo nano "$file"
    #fi
}

glusterfs_install() {
    #Glusterfs can create a huge log file (> 1GB), so logrotate is required
    logrotate_install
    #Glusterfs should be used only if you know how to manage the gluster manually in an emergency case
    pacmanSyNC "glusterfs qemu-block-gluster libvirt-storage-gluster"
    #Disable rpcbind dependency due to potential security issues
    #local folder="/etc/systemd/system/glusterd.service.d"
    #local file="$folder/override.conf"
    #sudo mkdir "$folder"
    #sudo cp /usr/lib/systemd/system/glusterd.service "$file"
    #config_replacevalue "Requires=rpcbind.service" "Requires=" "$file"
    #sudo systemctl mask rpcbind.service
    #sudo systemctl mask rpcbind.socket
    sudo systemctl enable glusterd.service
    sudo systemctl start glusterd.service
    read -r -p "$(tput setaf $color)Do you want to disable/stop firewalld and connect this machine to another existing glusterfs server?$(tput sgr0)  [y/N]"
	if [[ "$REPLY" =~ [yY] ]]; then
        sudo systemctl stop firewalld.service
        sudo systemctl disable firewalld.service
        read -r -p "$(tput setaf $color)Please type the name of the glusterfs server you want to connect to:$(tput sgr0)"
        if [[ "$REPLY" != "" ]]; then
            #sudo gluster peer probe "$REPLY"
            #if [[ "$?" == 0 ]]; then
            if sudo gluster peer probe "$REPLY"; then
                ################ BUG Add logic to add a disk ###################
                echo "Please add manually /dev/sdXY /mnt/sdXY btrfs defaults 0 0"
                sudo nano /etc/fstab
                #sudo mkdir -p /mnt/sdXY
                #sudo chattr -R +C /mnt/sdXY/

            else
                echo "Please make sure that the other glusterfs machine is on the same network. Error"
            fi
        fi
    fi
}

install_downgradedpackages() {
    # This will downgrade packages based on the actual repository (helpful if you want to go from testing back to standard repo's)
    printText "Accept downgraded packages and install them (pacman -Syyuu)" 1 0 "v"
    sudo pacman -Syyuu
}

####################################
############ Additions #############
####################################

libvirt_install() {
    #virt-install required to add vm's in cockpit
    #ebtables (required for NAT) removed from Arch Linux, use iptables-nft
    #ovmf provides UEFI support in virtual machines
    pacmanSyNC "libvirt qemu-base dnsmasq bridge-utils openbsd-netcat qemu-block-gluster virt-install cockpit-machines edk2-ovmf"
    sudo pacman -Sy iptables-nft
    sudo gpasswd -a "$userName" libvirt
    sudo gpasswd -a nobody kvm

    local file="/etc/libvirt/libvirtd.conf"
    config_uncomment "unix_sock_group =" "#" "$file"
    config_uncomment "unix_sock_ro_perms =" "#" "$file"
    config_uncomment "unix_sock_rw_perms =" "#" "$file"
    config_uncomment "auth_unix_ro =" "#" "$file"
    config_setvalue "auth_unix_ro =" "\"none\"" "$file"
    config_uncomment "auth_unix_rw =" "#" "$file"
    config_setvalue "auth_unix_rw =" "\"none\"" "$file"
    config_comment "listen_tls =" "#" "$file"
    config_comment "auth_tcp =" "#" "$file"

    local file="/etc/conf.d/libvirtd"
    config_uncomment "LIBVIRTD_ARGS=" "#" "$file"
    config_setvalue "LIBVIRTD_ARGS=" "\"\"" "$file"
    sudo systemctl enable libvirtd.service
    sudo systemctl start libvirtd.service

    #Btrfs disable Copy on Write
    echo "On $(tput setaf $color)btrfs$(tput sgr0) copy on write must be disabled for VM images, if you use other than the standard folder for images check manually with the command $(tput setaf $color)lsattr /var/lib/libvirt/images$(tput sgr0)"
    sudo chattr +C -R /var/lib/libvirt/images
}

network_bridge_install() {
    local interface="br1"
    local file="/etc/systemd/network/$interface.netdev"
    echo "[NetDev]" | sudo tee "$file"
    echo "Name=$interface" | sudo tee -a "$file"
    echo "Kind=bridge" | sudo tee -a "$file"

    sudo systemctl restart systemd-networkd

    file="/etc/systemd/network/wired.network"
    config_comment "DHCP=ipv4" "#" "$file"
    echo "Bridge=$interface" | sudo tee -a "$file"

    file="/etc/systemd/network/$interface.network"
    echo "[Match]" | sudo tee "$file"
    echo "Name=$interface" | sudo tee -a "$file"
    echo "" | sudo tee -a "$file"
    echo "[Network]" | sudo tee -a "$file"
    echo "DHCP=ipv4" | sudo tee -a "$file"
    #ipv6_privacy prefer-public/kernel or no/yes, use default public no privacy for server
    echo "IPv6PrivacyExtensions=no" | sudo tee -a "$file"
    #mDNS
    echo "MulticastDNS=yes" | sudo tee -a "$file"

    #Use private stable address on server instead of ipv6_privacy_enable
    ipv6_private_stable_enable "$interface"
}

ipv6_private_stable_enable() {
    local interface="$1"
    sudo sysctl net.ipv6.conf."$interface".addr_gen_mode=3
    #BUG How to restart the network without disconnecting here from SSH if there is a new IP address assigned?
    sudo systemctl restart systemd-networkd
    local stablesecret=$(sudo sysctl net.ipv6.conf."$interface".stable_secret)
    local file="/etc/sysctl.d/40-ipv6.conf"
    # How is it possible to get here: "net.ipv6.conf.br1.stable_secret = net.ipv6.conf.br1.stable_secret = fbd7:5e19:efeb:e44a:7579:f82d:ada5:98ab"?
    #echo "net.ipv6.conf.${interface}.stable_secret = $stablesecret" | sudo tee -a "$file"
    echo "$stablesecret" | sudo tee -a "$file"
    echo "net.ipv6.conf.${interface}.addr_gen_mode = 2" | sudo tee -a "$file"
}

virtmanager_install() {
    #Gtk3
    # Add as well Vulkan Venus driver support on a desktop
    pacmanSyNC "virt-manager"
    #Workaround required, otherwise Virt-Manager will not ask for ssh password
    sudo ln -s /usr/bin/ksshaskpass /usr/lib/ssh/ssh-askpass
}

environment_variables_default(){
    printf "\nConfigure $(tput setaf 2)/etc/environment$(tput sgr0) and set defaults...\n"
    local file="/etc/environment"
    echo "#" | sudo tee "$file" &> /dev/null
    echo "# This file is parsed by pam_env module" | sudo tee -a "$file" &> /dev/null
    echo "#" | sudo tee -a "$file" &> /dev/null
    echo "# Syntax: simple "KEY=VAL" pairs on separate lines" | sudo tee -a "$file" &> /dev/null
    echo "#" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "EDITOR=/usr/bin/nano" | sudo tee -a "$file" &> /dev/null
}

environment_variables_custom() {
    environment_variables_default
    local file="/etc/environment"
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "### Wayland" | sudo tee -a "$file" &> /dev/null
    echo "# Firefox-Wayland" | sudo tee -a "$file" &> /dev/null
    echo "MOZ_DBUS_REMOTE=1" | sudo tee -a "$file" &> /dev/null
    echo "MOZ_ENABLE_WAYLAND=1" | sudo tee -a "$file" &> /dev/null
    echo "GTK_USE_PORTAL=1" | sudo tee -a "$file" &> /dev/null
    #Enable Firefox touchscreen scrolling
    if [[ "$computerType" == "convertible" ]]; then
        echo "MOZ_USE_XINPUT2=1" | sudo tee -a "$file" &> /dev/null
    else
        echo "#MOZ_USE_XINPUT2=1" | sudo tee -a "$file" &> /dev/null
    fi
    #echo "# LibreOffice Scaling Qt Workaround, change value according to your screen size, libreoffice scales now automatically" | sudo tee -a "$file" &> /dev/null
    #echo "SAL_FORCEDPI=200" | sudo tee -a "$file" &> /dev/null
    #SDL uses Wayland by default since 2.0.22
    echo "# SDL2 Enforce Newer SDL System Package Usage" | sudo tee -a "$file" &> /dev/null
    #SDL is still too buggy on Wayland, enable per game only
    echo "#SDL_VIDEODRIVER=wayland" | sudo tee -a "$file" &> /dev/null
    #BUG Doesn't work, error message in Proton
    #echo "#SDL_DYNAMIC_API=/usr/lib/libSDL2-2.0.so.0" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "### Vulkan" | sudo tee -a "$file" &> /dev/null
    echo "# AMD" | sudo tee -a "$file" &> /dev/null
    #if ( $gpuAMD ); then
    #    echo "#RADV_PERFTEST=aco" | sudo tee -a "$file" &> /dev/null
    #fi
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "# Zink - OpenGL on Vulkan" | sudo tee -a "$file" &> /dev/null
    echo "#MESA_LOADER_DRIVER_OVERRIDE=zink" | sudo tee -a "$file" &> /dev/null
    echo "#GALLIUM_DRIVER=zink" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    #https://www.kdab.com/qt3d-renderer-qt6/
    echo "# Qt on Vulkan - rhi with OpenGL should be the default over only opengl (limited to ES2/GL2) in QT3D_RENDERER" | sudo tee -a "$file" &> /dev/null
    echo "#QT3D_RENDERER=rhi" | sudo tee -a "$file" &> /dev/null
    echo "#QSG_RHI=1" | sudo tee -a "$file" &> /dev/null
    echo "#QSG_INFO=1" | sudo tee -a "$file" &> /dev/null
    #Works/Tested with jami, marble requiered QSG_RHI=1 but only used Vulkan partially
    echo "QSG_RHI_BACKEND=vulkan" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "# Gtk4 on Vulkan" | sudo tee -a "$file" &> /dev/null
    echo "#GSK_RENDERER=vulkan" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "### OpenGL" | sudo tee -a "$file" &> /dev/null
    #NIR for AMD GPU's provides OpenGL4.6 (default since Mesa 20.0 not required anymore)
    #echo "radeonsi_enable_nir=true" | sudo tee -a "$file" &> /dev/null
    echo "# Intel" | sudo tee -a "$file" &> /dev/null
    #Iris for Intel by default in Mesa 20.0 for Gen8+
    if ( $gpuIntel ); then
        echo "#MESA_LOADER_DRIVER_OVERRIDE=iris" | sudo tee -a "$file" &> /dev/null
    #else
        #echo "#MESA_LOADER_DRIVER_OVERRIDE=iris" | sudo tee -a "$file" &> /dev/null
    fi
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "### Video Acceleration - AMD (radeonsi), Intel (va_gl)" | sudo tee -a "$file" &> /dev/null
    echo "# AMD" | sudo tee -a "$file" &> /dev/null
    if ( $gpuAMD ); then
        echo "VDPAU_DRIVER=radeonsi" | sudo tee -a "$file" &> /dev/null
    else
        echo "#VDPAU_DRIVER=radeonsi" | sudo tee -a "$file" &> /dev/null
    fi
    if ( $gpuIntel ); then
        echo "VDPAU_DRIVER=va_gl" | sudo tee -a "$file" &> /dev/null
    else
        echo "#VDPAU_DRIVER=va_gl" | sudo tee -a "$file" &> /dev/null
    fi
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "### OpenCL" | sudo tee -a "$file" &> /dev/null
    echo "RUSTICL_ENABLE=zink,radeonsi,iris,etnaviv,llvmpipe" | sudo tee -a "$file" &> /dev/null
}

environment_variables_gaming() {
    environment_variables_default
    environment_variables_custom
    local file="/etc/environment"
    echo "### HUD" | sudo tee -a "$file" &> /dev/null
    echo "# MangoHud (works only for Vulkan as an environment variable)" | sudo tee -a "$file" &> /dev/null
    echo "#MANGOHUD=1" | sudo tee -a "$file" &> /dev/null
    echo "" | sudo tee -a "$file" &> /dev/null
    echo "# Wine/Proton DXVK Hud (only show what is missing in MangoHud)" | sudo tee -a "$file" &> /dev/null
    #devinfo (e.g. RADV/ACO) is now provided with MangoHud as well, only D3D api version 9, 10, 11 are missing
    echo "DXVK_HUD=api" | sudo tee -a "$file" &> /dev/null
    #https://www.reddit.com/r/SteamDeck/comments/zwvekp/dxvk_async_a_deck_essential_nobody_talks_about/
    #echo "DXVK_ASYNC=1" | sudo tee -a "$file" &> /dev/null
}

gaming_optimize() {
    #https://medium.com/@a.b.t./here-are-some-possibly-useful-tweaks-for-steamos-on-the-steam-deck-fcb6b571b577
    #OPTIONAL => 1000hz hertz tick kernel (https://liquorix.net/) => install linux-lqx (only chaotic-aur) or linux-zen (extra)
    linux_check_install "-zen"
    local file="/boot/loader/entries/archlinux-zen.conf"
    config_replacevalue "archlinux-zen" "archlinux-zen-gamemode-insecure" "$file"
    kernel_boot_parameter "$file"
    kernel_boot_parameter_config "quiet"
    kernel_boot_parameter_config "splash"
    linux_32bitsupport_enable "-zen"
    mesa_specialflags_parameter
    #1. CPU Performance Governor -> Start games with "gamemoderun GAME" => OK
    #2. MGLRU is already in use by Arch Linux with 7 and min_ttl_ms=0 => NOTHING TO DO
    #3. hard+soft memlock 2147484 => ???
    #4. NVME/SSD/M2 IO scheduler kyber instead of mq-deadline => liquorix used bfq OK
    #5. noatime => OK
    #6. Boot with mitigations=off & nowatchdog & nmi_watchdog=0 (https://www.phoronix.com/review/amd-3950x-retbleed) => Boot to gamemode OK

    watchdog_disable "-zen"
    kernel_boot_parameter_config "mitigations=off"

    #OPTIONAL2: Use madvise => ?

    #https://github.com/CryoByte33/steam-deck-utilities (Tweak Details https://github.com/CryoByte33/steam-deck-utilities/blob/main/docs/tweak-explanation.md)
    #1. Create swap file with RAM size => Partially 4GB OK
    #2. Swappiness of 1 => OK
    swappiness_usage true
    #3. Transparent hugepages => Default OK

    #https://wiki.archlinux.org/title/Gaming#Improving_performance
    local file="/etc/tmpfiles.d/consistent-response-time-for-gaming.conf"
    echo "#    Path                  Mode UID  GID  Age Argument" | sudo tee "$file" &> /dev/null

    #4. Shared memory in transparent hugepages
    echo "Enable in transparent hugepage shared memory"
    #Default never
    echo "w /sys/kernel/mm/transparent_hugepage/shmem_enabled - - - - advise" | sudo tee -a "$file" &> /dev/null

    #5. Disabling compaction_ proactiveness => OK
    echo "Diable in VM compaction proactiveness"
    #Default 20
    echo "w /proc/sys/vm/compaction_proactiveness - - - - 0" | sudo tee -a "$file" &> /dev/null

    #6. Disabling khugepage defragmentation => OK
    echo "Disable in transparent hugepage: auto defrag"
    #Default 1
    echo "w /sys/kernel/mm/transparent_hugepage/khugepaged/defrag - - - - 0" | sudo tee -a "$file" &> /dev/null

    #7. Enable page lock unfairness => OK
    echo "Enable in VM compaction proactiveness page lock unfairness"
    #Default 5
    echo "w /proc/sys/vm/page_lock_unfairness - - - - 1" | sudo tee -a "$file" &> /dev/null

    local file="/etc/sysctl.d/80-gamecompatibility.conf"
    #Used by Valve on the Steam Deck https://wiki.archlinux.org/title/Gaming#Game_compatibility
    echo "vm.max_map_count = 2147483642" | sudo tee "$file" &> /dev/null
}

watchdog_disable() {
    #https://wiki.archlinux.org/title/Improving_performance#Watchdogs
    local linuxType="$1"
    local file="/boot/loader/entries/archlinux$linuxType.conf"
    kernel_boot_parameter_config "nowatchdog"
    kernel_boot_parameter_config "nmi_watchdog=0"
    #https://wiki.archlinux.org/title/Power_management#Power_saving
    #Check if available with wdctl
    if ( $cpuIntel ); then
        kernel_boot_parameter_config "modprobe.blacklist=iTCO_wdt"
    elif ( $cpuAMD ); then
        #Only on AMD 700 chipsets & newer?
        kernel_boot_parameter_config "modprobe.blacklist=sp5100_tco"
    fi
}

archlinux_security() {
    #https://wiki.archlinux.org/title/Security
    #Logs are restricted on Arch Linux by default to root
    local rebootNewKernelLoc=false

    printf "\nFirewalld\n"
    read -r -p "Enable firewalld?  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        firewall_config
    fi

    printf "\nLinux-hardened\n"
    #https://github.com/anthraxx/linux-hardened
    read -r -p "Replace linux with linux-hardened? Improved adress space layout randomization (ASR) and hardened BPF. Disables kexec and external kernel modules (nvidia). Restrict kernel pointers in proc and unprivileged user namespace usage. Continue?  [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        linux_hardened_install
        rebootNewKernelLoc=true
    fi

    echo "Enable Linux security restrictions. Kernel lockdown to limit debug output and loading of untrusted modules."
    read -r -p "$(tput setaf $color)Do you want to enable kernel lockdown? $(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        lockdown_enable ""
        if pacman -Q "linux-hardened" &> /dev/null; then
            lockdown_enable "-hardened"
        fi
    fi

    echo "AppArmor enforces mandatory access control"
    read -r -p "$(tput setaf $color)Do you want to install and enable AppArmor?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        apparmor_install ""
        if pacman -Q "linux-hardened" &> /dev/null; then
            apparmor_install "-hardened"
        fi
    fi

    echo "USBGuard"
    read -r -p "$(tput setaf $color)Do you want to block all new attached USB devices until they are manually allowed by the user?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        usbguard_install_config
    fi

    if ( $rebootNewKernelLoc ); then
        read -r -p "$(tput setaf $color)Please reboot now into the new linux-hardened kernel system. If it doesn't work, select linux-lts in the boot manager. Continue?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sudo systemctl reboot
        else
            echo "Please reboot into linux-hardened manually!"
        fi
    fi
}

linux_hardened_install() {
    linux_check_install "-hardened"
    if ( $efi ); then
        local file="/boot/loader/loader.conf"
        config_replacevalue "archlinux" "archlinux-hardened" "$file"
        config_replacevalue "hardened-hardened" "hardened" "$file"
    fi
    linux_32bitsupport_disable "-hardened"
    #On a desktop flatpak needs "sysctl kernel.unprivileged_userns_clone=1" to start LibreWolf
    if ( ! $prodServer ) && pacman -Q "flatpak" &> /dev/null; then
        #sudo sysctl -w kernel.unprivileged_userns_clone=1
        local file="/etc/sysctl.d/userns.conf"
        echo "kernel.unprivileged_userns_clone=1" | sudo tee "$file" &> /dev/null
    fi
    sudo pacman -Rsc --noconfirm linux
}

lockdown_enable() {
    #https://wiki.archlinux.org/title/Security#Kernel_lockdown_mode
    #kernel >= 5.4 parameter:
    #https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=aefcf2f4b58155d27340ba5f9ddbe9513da8286d
    #deactivates hibernate and loading unsigned kernel modules
    local linuxType="$1"
    #LOCKDOWN_MODULE_SIGNATURE
    #LOCKDOWN_DEV_MEM
    #LOCKDOWN_HIBERNATION
    #LOCKDOWN_PERF
    #LOCKDOWN_TRACEFS
    #LOCKDOWN_BPF_READ
    #Options: lockdown={integrity|confidentiality}
    #integrity: kernel features that allow userland to modify the running kernel are disabled
    #confidentiality: kernel features that allow userland to extract confidential information from the kernel are also/additionally disabled
    local lockdownLoc=integrity
    if ( $efi ); then
        local file="/boot/loader/entries/archlinux${linuxType}.conf"

        #Restrict loading unsigned kernel modules by adding kernel parameter
        kernel_boot_parameter_config "module.sig_enforce=1"

        #Lockdown mode to restrict user mode to modify the kernel
        kernel_boot_parameter_config "lockdown=$lockdownLoc"
    else
        echo "Legacy Grub Bios mode - not supported to enable kernel lockdown mode"
    fi
    #Checck mode of operation with: cat /sys/kernel/security/lockdown
}

apparmor_install() {
    ### BUG Not tested ###
    #AppArmor is available in all official Arch Linux kernels
    #https://wiki.archlinux.org/index.php/AppArmor
    local linuxType="$1"
    if ( $efi ); then
        pacmanSyNC "apparmor python-notify2 python-psutil"
        yay -S --nonconfirm "apparmor.d-git"
        sudo systemctl enable apparmor.service

        local file="/boot/loader/entries/archlinux${linuxType}.conf"
        kernel_boot_parameter_config "lsm=landlock,lockdown,yama,integrity,apparmor,bpf"
        #kernel_boot_parameter_config "apparmor=1"
        #kernel_boot_parameter_config "security=apparmor"

        #AppArmor increases boot time as it parses the configuration
        config_uncomment "write-cache" "#" "/etc/apparmor/parser.conf"

        echo "Please check after the next reboot if ArrArmor works. Run on the consode [aa-enabled] and it should return [yes]! You can check the status as well with [apparmor_status]"
        #Add group and user that is allowed to see the audit logs
        sudo groupadd -r audit
        sudo gpasswd -a "$userName" audit
        config_setvalue "log_group = " "audit" "/etc/audit/auditd.conf"
        #Create Desktop Launcher for notification
        echo "[Desktop Entry]
Type=Application
Name=AppArmor Notify
Comment=Receive on screen notifications of AppArmor denials
TryExec=aa-notify
Exec=aa-notify -p -s 1 -w 60 -f /var/log/audit/audit.log
StartupNotify=false
NoDisplay=true" | tee "${HOME}/.config/autostart/apparmor-notify.desktop"
        echo "Please check after the next reboot if ArrArmor-Notifications work. Run on the consode [pgrep -ax aa-notify]."
    else
        echo "Legacy Bios Grub mode - not supported to install apparmor"
    fi
}

usbguard_install_config() {
    pacmanSyNC "usbguard"
    #Gui and notifications for USBGuard
    echo "Please decide if you need the USBGuard-Qt interface:"
    yay -Sy "usbguard-qt"
    #Installled already in desktop section: yay -S --noconfirm "usbguard-qt"
    echo "usbguard --- ALLOW all current devices, BLOCK all new attached devices"
    usbguard generate-policy | sudo tee "/etc/usbguard/rules.conf"
    local file="/etc/usbguard/usbguard-daemon.conf"
    config_setvalue "IPCAllowedUsers=" "$userName" "$file"
    usbguard_desktop_autostart
    sudo systemctl enable usbguard.service
    sudo systemctl start usbguard.service
}

usbguard_desktop_autostart() {
    if pacman -Q "usbguard-qt" &> /dev/null; then
        local path="${HOME}/.config/autostart"
        mkdir -p $path
        local file="${path}/usbguard-qt.desktop"
        #Autostart USBGuard-QT Desktop Icon Notifier
        echo "[Desktop Entry]
Categories=System;
Comment=USBGuard-Qt
Exec=usbguard-qt
GenericName=USBGuard
Icon=usbguard-icon
Keywords=USB;USBGuard;Qt;
Name=USBGuard
TryExec=usbguard-qt
Type=Application" | tee "$file" &> /dev/null
    fi
}

bash_user_logout_timeout() {
    #https://wiki.archlinux.org/title/Security#Automatic_logout
    #BUG Linux bash doesn't logout the user when not active for a long time
    #BUG It kills even sessions on Desktop Wayland
    local file "/etc/profile.d/shell-timeout.sh"
    echo "TMOUT="$(( 60*10 ))";
[ -z "$DISPLAY" ] && export TMOUT;
case $( /usr/bin/tty ) in
	/dev/tty[0-9]*) export TMOUT;;
esac" | sudo tee "$file" &> /dev/null
}

serialconsole_enable() {
    #https://wiki.archlinux.org/title/Working_with_the_serial_console
    if pacman -Q "linux" &> /dev/null; then
        local file="/boot/loader/entries/archlinux.conf"
        kernel_boot_parameter_config "console=tty0"
        kernel_boot_parameter_config "console=ttyS0,115200"
    fi
    if pacman -Q "linux-hardened" &> /dev/null; then
        local file="/boot/loader/entries/archlinux-hardened.conf"
        kernel_boot_parameter_config "console=tty0"
        kernel_boot_parameter_config "console=ttyS0,115200"
    fi
}

linux_32bitsupport_disable() {
    #Disable in Linux >= 6.7 the 32bit support of x86, 64 bit only. 6.10 >= allows disable it for aarch64
    #https://www.phoronix.com/news/Linux-6.7-IA32-Emulation-Boot
    local linuxType="$1"
    if pacman -Q "linux${linuxType}" &> /dev/null; then
        local file="/boot/loader/entries/archlinux${linuxType}.conf"
        if [[ "$machineArch" == "x86_64" ]]; then
            kernel_boot_parameter_config "ia32_emulation=0"
        elif [[ "$machineArch" == "aarch64" ]]; then
            kernel_boot_parameter_config "arm64.no32bit_el0"
        fi
    fi
}

linux_32bitsupport_enable() {
    #Only for Gaming? Steam & Proton?
    local linuxType="$1"
    if pacman -Q "linux${linuxType}" &> /dev/null; then
        local file="/boot/loader/entries/archlinux${linuxType}.conf"
        if [[ "$machineArch" == "x86_64" ]]; then
            config_replacevalue "ia32_emulation=0" "" "$file"
        elif [[ "$machineArch" == "aarch64" ]]; then
            config_replacevalue "arm64.no32bit_el0" "" "$file"
        fi
    fi
}

plymouth_enable() {
    read -r -p "$(tput setaf $color)Do you want to enable graphical boot by using $(tput setaf 2)plymouth$(tput setaf $color)? Only for desktop+laptop! Continue?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        plymouth_install
        plymouth_config
        read -r -p "$(tput setaf $color)Please reboot and check if the Plymouth graphical boot works. If it doesn't work, remove <splash> from the kernel boot parameter! Reboot now?$(tput sgr0)  [y/N]"
        if [[ "$REPLY" =~ [yY] ]]; then
            sudo systemctl reboot
        else
            echo "Please reboot with plymouth graphical boot later!"
        fi
    else
        mknitcpio_build
    fi
}

plymouth_install() {
    pacmanSyNC "plymouth plymouth-kcm"
    #if ! pacman -Q plymouth-theme-arch-breeze-git &> /dev/null; then
    #    yay -S --noconfirm plymouth-theme-arch-breeze-git
    #fi
}

plymouth_config() {
    if ( $efi ); then
        if pacman -Q dracut &> /dev/null; then
            plymouth_dracut_simpledrm
        else
            local file="/etc/mkinitcpio.conf"
            #BUG plymouth is getting added mutliple times. sd-plymouth got replaced with plymouth (git 2023-04-01)!
            config_replacevalue "systemd" "systemd plymouth" "$file"
            #BUG Workaround
            config_replacevalue "plymouth plymouth" "plymouth" "$file"
            #BUG Sometimes encrypt is missing ??? use sd-plymouth additionally?  sd-encrypt got replaced with encrypt (git 2023-04-01)! Encrypt is not required anymore? => Remove!
            #config_replacevalue " encrypt" " sd-encrypt" "$file"
            #config_replacevalue "plymouth" "plymouth encrypt" "$file"
            #BUG Workaround
            #config_replacevalue "encrypt encrypt" "encrypt" "$file"
            #sudo systemctl disable sddm.service
            #BUG SDDM-plymouth not available anymore???
            #sudo systemctl enable sddm-plymouth.service
        fi

        #NOT enabling splash for Linux-lts as this is a fallback and linux-hardened is for production systems
        if pacman -Q "linux" &> /dev/null; then
            local file="/boot/loader/entries/archlinux.conf"
            kernel_boot_parameter_config "splash"
        fi

        #Delay until plymouth is loaded, default 5 is too long!
        local file="/etc/plymouth/plymouthd.conf"
        #Is this the default? Not required?
        config_setvalue "ShowDelay=" "0" "$file"
        #HiDPI
        echo "DeviceScale=2" | sudo tee -a "$file"

        #This will rebuild as initrd/mkinitcpio !!! => Use bgrt instead of arch-breeze
        sudo plymouth-set-default-theme -R bgrt

    else
        echo "Legacy Bios Grub mode - not supported to install plymouth"
    fi
}

#plymouth_disable() {
#}

lamp_install() {
    apache_install
    php_install
    mariadb_install
    certbot_install
}

mariadb_install() {
    #https://wiki.archlinux.org/index.php/MariaDB
    pacmanSyNC "mariadb"
    sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
    sudo chattr +C /var/lib/mysql
    sudo systemctl enable mariadb.service
    sudo systemctl start mariadb.service

    local pass1="init1"
    local pass2="init2"
    while [[ "$pass1" != "$pass2" ]]
    do
        read -r -p "$(tput setaf $color)Please enter a password for the mysql database and for the user $userName:$(tput sgr0)"
        local pass1="$REPLY"
        read -r -p "$(tput setaf $color)... please confirm the password again:$(tput sgr0)"
        local pass2="$REPLY"
    done
    if [[ "$pass1" == "$pass2" ]]; then
        mysql -u root -p
        CREATE USER \'$userName\'@\'localhost\' IDENTIFIED BY \'$pass1\';
        GRANT ALL PRIVILEGES ON mydb.* TO \'$userName\'@\'localhost\';
        FLUSH PRIVILEGES;
        quit
    else
        echo "Error. Passwords do not match."
    fi

    #Interactive questionaire
    sudo mysql_secure_installation
}

mariadb_upgrade() {
    echo "MariaDB Upgrade Database"
    if [[ "$mariaUser" == "root" ]]; then
        mysql_upgrade -u "$mariaUser" -p
    else
        echo "[  WARNING  ] MariaDB upgrade with normal user $mariaUser is not recommended as it requires --force --force"
        mysql_upgrade --force --force -u "$mariaUser" -p
    fi
}

mariadb_chech_database() {
    echo "MariaDB Check Database"
    mysqlcheck --all-databases -u "$userName" -p -c
}

mariadb_analyze_tables() {
    echo "MariaDB Analyze Tables"
    mysqlcheck --all-databases -u "$userName" -p -a
}

mariadb_repair_tables() {
    echo "MariaDB Repair Tables"
    mysqlcheck --all-databases -u "$userName" -p -r
}

mariadb_optimize_tables() {
    echo "MariaDB Optimize Tables"
    mysqlcheck --all-databases -u "$userName" -p -o
}

apache_install() {
    #https://wiki.archlinux.org/index.php/Apache_HTTP_Server
    pacmanSyNC "apache"
    sudo systemctl enable httpd.service
    sudo systemctl start httpd.service

    echo "Check and open port in firewall for httpd"
}

php_install () {
    #https://wiki.archlinux.org/index.php/PHP#Installation
    #https://wiki.archlinux.org/index.php/Apache_HTTP_Server#PHP
    pacmanSyNC "php php-fpm"

    local file="/etc/httpd/conf/httpd.conf"
    config_uncomment "LoadModule proxy_module modules/mod_proxy.so" "#" "$file"
    config_uncomment "LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so" "#" "$file"

    local file="/etc/httpd/conf/extra/php-fpm.conf"
    echo "DirectoryIndex index.php index.html
<FilesMatch \.php$>
    SetHandler \"proxy:unix:/run/php-fpm/php-fpm.sock|fcgi://localhost/\"
</FilesMatch>" | sudo tee "$file"

    http_includes

    #Enable mysql support
    local file="/etc/php/php.ini"
    config_uncomment "extension=pdo_mysql" ";" "$file"
    config_uncomment "extension=mysqli" ";" "$file"

    sudo systemctl enable php-fpm.service
    sudo systemctl start php-fpm.service
    sudo systemctl restart httpd.service
}

phpmyadmin_install() {
    #https://wiki.archlinux.org/index.php/PhpMyAdmin
    pacmanSyNC "phpmyadmin"

    #Required extensions
    local file="/etc/php/php.ini"
    config_uncomment "extension=bz2" ";" "$file"
    config_uncomment "extension=zip" ";" "$file"

    local file="/etc/httpd/conf/extra/phpmyadmin.conf"
    echo "Alias /phpmyadmin \"/usr/share/webapps/phpMyAdmin\"
<Directory \"/usr/share/webapps/phpMyAdmin\">
    DirectoryIndex index.php
    AllowOverride All
    Options FollowSymlinks
    Require all granted
</Directory>" | sudo tee "$file"

    #Add phpmyadmin to includes, see http_includes

    sudo systemctl restart php-fpm.service
    sudo systemctl restart httpd.service
}

http_includes() {
    #BUG This will be added multiple times when re-run setup
    echo "
#php-fpm & phpmyadmin configuration
Include conf/extra/php-fpm.conf
Include conf/extra/phpmyadmin.conf" | tee -a "/etc/httpd/conf/httpd.conf"
}

certbot_install() {
    #https://wiki.archlinux.org/index.php/Letsencrypt
    pacmanSyNC "certbot"
    certbot_setup
}

certbot_setup() {
    local file="/etc/letsencrypt/cli.ini"
    echo "# +001 Created by archbot
# All flags used by the client can be configured here. Run Certbot with
# \"--help\" to learn more about the available options.

# Use a 4096 bit RSA key instead of 2048
rsa-key-size = 4096

# Uncomment and update to register with the specified e-mail address
# you will get automatic e-mail notifications if the certificate expires 20 and 10 days upfront
#email = admin@example.com

# Uncomment and update to generate certificates for the specified domains.
# domains = example.com, www.example.com
#domains =

# Uncomment to use the standalone authenticator on port 443
# authenticator = standalone
#standalone-supported-challenges = tls-sni-01
preferred-challenges = tls-sni-01

# Uncomment to use the webroot authenticator. Replace webroot-path with the
# path to the public_html / webroot folder being served by your web server.
# authenticator = webroot
# webroot-path = /usr/share/nginx/html

# Terms of Use. Accept LE terms of use: Default: False
# agree-tos = True
# -001" | sudo tee "$file"

    echo "Please configure manually $file"
    echo "Then run [certbot --preferred-challenges tls-sni-01 renew] to create or update a certificate"
}

cups_install() {
    #https://wiki.archlinux.org/index.php/CUPS
    pacmanSyNC "cups"
    #https://github.com/apple/cups/issues/5747
    local file="/etc/cups/cupsd.conf"
    echo "
archbot: avoid storing printed documents information to /var/spool/cups/
PreserveJobFiles No
PreserveJobHistory No" | sudo tee -a "$file"
    #sudo systemctl enable org.cups.cupsd.socket
    sudo systemctl enable cups.socket
    # Is this step required to create some config files?
    cupsctl
    sudo systemctl start cups.socket
    cupsctl
    #Then open http://localhost:631 and add a printer
    #Recommended to use ipp and enter a address, example address ipp://192.168.1.200/BINARY_P1
}

kde_cleanup() {
    printf "\nRemove not required packages...\n"
    #TEST: Remove only installed packages to avoid errors if application wouldn't exist
    local rPackageAll=""
    rPackageAll="kde-applications-meta kde-multimedia-meta kde-education-meta kde-network-meta kde-utilities-meta kde-sdk-meta kde-pim-meta kde-accessibility-meta kde-graphics-meta kmix"
    getInstalledPackages rPackages rPackageAll
    sudo pacman -R --noconfirm ${rPackages[*]} &> /dev/null

    pacmanRscNC "kde-games-meta konqueror k3b blinken kanagram khangman kwave kalgebra cantor kalzium rocs artikulate kgeography ktouch kturtle minuet step kiten klettres kwordquiz parley kig kmplot kbruch dragon kfloppy lokalize cervisia kmouth kmousetool kteatime khelpcenter audiocd-kio kopete telepathy-qt umbrello archey3 diskmonitor-git kmymoney mesa-demos lib32-mesa-demos plasma-sdk kolourpaint falkon"

    local iPackageAll=""
    iPackageAll="kate kompare kfind kdf kcalc kgpg ktimer sweeper marble print-manager filelight dolphin-plugins ark"
    getMissingPackages iPackages iPackageAll
    #Do not add "" to variable as otherwise this is recognized as one string
    checkArray emptyArray iPackages
    if ( ! $emptyArray ); then
        sudo pacman -Sy --noconfirm --asexplicit  ${iPackages[*]}
    fi
}

kde_init_autostart() {
    local initProfile="$1"
    if [[ "$initProfile" == "--now" ]] || [[ "$initProfile" == "" ]]; then
        mkdir -p ${HOME}/.local/bin/
        local file="${HOME}/.local/bin/kde-init.sh"
    else
        sudo mkdir -p /home/${initProfile}/.local/bin/
        local file="/home/${initProfile}/.local/bin/kde-init.sh"
    fi
    #Maliit init locale and theme
    echo "#!/bin/bash" | sudo tee "$file" &> /dev/null
    echo "#Onetime Maliit init locale and theme" | sudo tee -a "$file" &> /dev/null
    echo "gsettings set org.maliit.keyboard.maliit enabled-languages \"['en', '$languageCode', 'emoji']\"" | sudo tee -a "$file" &> /dev/null
    #echo "gsettings set org.maliit.keyboard.maliit theme BreezeDark" | sudo tee -a "$file" &> /dev/null
    if [[ "$initProfile" == "--now" ]] || [[ "$initProfile" == "" ]]; then
        sudo chown -R ${userName}:${userName} ${HOME}/.local/bin/
        user_profile_kde
        initProfile=""
        echo "#bash /opt/pacbot/userprofile.sh --inituser" | sudo tee -a "$file" &> /dev/null
    else
        sudo chown -R ${initProfile}:${initProfile} /home/${initProfile}/.local/
        echo "bash /opt/pacbot/userprofile.sh --inituser" | sudo tee -a "$file" &> /dev/null
    fi
    sudo chmod +x "$file"

    #BUG Cannot be done before 1st run
    #local file="${HOME}/.config/plasma-org.kde.plasma.desktop-appletsrc"
    #Replace the standard menu as it has a bad user interface, kickoff is the classic menu, kickerdash is touch screen friendly
    #config_replacevalue "plugin=org.kde.plasma.kicker" "plugin=org.kde.plasma.kickdash" "$file"
    #Do not use the minimize to desktop effect when the user want to minimze to desktop
    #config_replacevalue "plugin=org.kde.plasma.showdesktop" "plugin=org.kde.plasma.minimizeall" "$file"

    autostart_link_script "$initProfile"
}

tlp_config() {
    # Enable Power Management TLP
    printf "\nEnable $(tput setaf 2)TLP$(tput sgr0) for Power Management...\n"
    sudo systemctl enable tlp.service
    #Doesn't exist anymore
    #sudo systemctl enable tlp-sleep.service
    local file="/etc/tlp.d/10-$scriptName.conf"
    #config_uncomment "SOUND_POWER_SAVE_ON_AC=" "#" "$file"
    #config_setvalue "SOUND_POWER_SAVE_ON_AC=" "1" "$file"
    echo "#Created by $scriptName" | sudo tee "$file" &> /dev/null
    echo "SOUND_POWER_SAVE_ON_AC=1" | sudo tee -a "$file"
    # Is this save on server's? Save min 1W
    echo "#Saves min 1W" | sudo tee -a "$file"
    echo "RUNTIME_PM_ON_AC=auto" | sudo tee -a "$file"
    echo "#Wifi power management saves 0,4W" | sudo tee -a "$file"
    echo "WIFI_PWR_ON_AC=on" | sudo tee -a "$file"
    echo "#ASPM - Check before/after with: sudo lspci -vv | grep 'ASPM.*abled;'" | sudo tee -a "$file" &> /dev/null
    echo "#PCIE_ASPM_ON_AC=powersave" | sudo tee -a "$file" &> /dev/null
    echo "PCIE_ASPM_ON_BAT=powersave" | sudo tee -a "$file"
    echo "" | sudo tee -a "$file"
    echo "#Add additional USB devices that are OK when suspended (lsusb). Device specific, custom:" | sudo tee -a "$file"
    echo "#USB_ALLOWLIST="1111:2222 3333:4444"" | sudo tee -a "$file"
    #read -r -p "$(tput setaf $color)Do you want to manually adjust power settings in [$file]?$(tput sgr0)  [y/N]"
    #if [[ "$REPLY" =~ [yY] ]]; then
    #    sudo nano "$file"
    #fi

    powersave_vm_writeback
}

powersave_recommendations() {
    #https://gitlab.freedesktop.org/drm/amd/-/issues/3195
    #Disabling scaling helps not only AMD systems, but as well on Intel even though Intel uses for video playback the 3D engines much less. Tested with approximatly 1W power save on a N200
    echo "[  INFO  ] To save power on OpenGL desktop and video playback, disable scaling and instead just increase the font size"
}

tuned_install_config() {
    yay -S --noconfirm "tuned"
    yay -S --noconfirm "tuned-ppd"
    sudo systemctl enable tuned.service
    sudo systemctl enable tuned-ppd.service
    local file="/etc/tuned/tuned-main.conf"
    local file="/etc/tuned/ppd.conf"
}

powersave_vm_writeback() {
    echo "Power Saving - Increase VM writeback from 5 to 60 seconds"
    local file="/etc/sysctl.d/dirty.conf"
    echo "vm.dirty_writeback_centisecs = 6000" | sudo tee $file &> /dev/null
}

memtest_install() {
    if [[ "$statusVM" == "none" ]]; then
        echo "Install $(tput setaf 2)memtest86+$(tput sgr0) to test and verify RAM..."
        if ( $efi ); then
            pacmanSyNC "memtest86+-efi"
            local file="/boot/loader/entries/memtest86.conf"
            echo "title    MemTest86+
efi   /memtest86+/memtest.efi" | sudo tee "$file" &> /dev/null
        else
            pacmanSyNC "memtest86+"
            sudo grub-mkconfig -o /boot/grub/grub.cfg
        fi
    fi
}

enable_dotanddnssec() {
    #Network - enable local DNS over TLS to mitigate risk of bad routers and man in the middle attacks
    #https://wiki.archlinux.org/index.php/Systemd-resolved
    printf "\nEnable local $(tput setaf 2)DNS over TLS$(tput sgr0) in systemd-resolved\n"
    #Enable systemd-resolved and link it to /etc/resolv.conf
    sudo rm /etc/resolv.conf
    enable_network
    #Use [resolvectl status] to check actual settings

    #Enable DNS over TLS
    sudo mkdir -p /etc/systemd/resolved.conf.d/
    local file="/etc/systemd/resolved.conf.d/dns_over_tls.conf"
    echo "[Resolve]" | sudo tee "$file" &> /dev/null
    #Use cloudflare, they say they create no logs and no country specific censorship https://developers.cloudflare.com/1.1.1.1/setting-up-1.1.1.1/
    #2620:fe::fe#dns.quad9.net 9.9.9.9#dns.quad9.net
    echo "DNS=2606:4700:4700::1111#1dot1dot1dot1.cloudflare-dns.com 2606:4700:4700::1001#1dot1dot1dot1.cloudflare-dns.com 1.1.1.1#1dot1dot1dot1.cloudflare-dns.com 1.0.0.1#1dot1dot1dot1.cloudflare-dns.com" | sudo tee -a "$file"
    #OpenNIC https://servers.opennic.org BUT NO DNSSEC???
    #echo "DNS=2a03:4000:28:365::1#jabber-germany.de 2a03:4000:4d:c92:88c0:96ff:fec6:b9d#morbitzer.de 94.16.114.254#jabber-germany.de 194.36.144.87#morbitzer.de" | sudo tee -a "$file"
    #Required to avoid using the default config fallback that might leak data to google
    echo "FallbackDNS=2620:fe::fe#dns.quad9.net 9.9.9.9#dns.quad9.net" | sudo tee -a "$file"
    #echo "FallbackDNS=2606:4700:4700::1111#1dot1dot1dot1.cloudflare-dns.com 1.1.1.1#1dot1dot1dot1.cloudflare-dns.com" | sudo tee -a "$file"
    echo "DNSOverTLS=yes" | sudo tee -a "$file"
    echo "MulticastDNS=yes" | sudo tee -a "$file"
    echo "DNSSEC=true" | sudo tee -a "$file"

    #Enable mDNS for .local resolution in the local network
    #Can this be done in the conf.d above???
    #local file="/etc/systemd/resolved.conf"
    #config_uncomment "MulticastDNS=" "#" "$file"

    #Must be activated as well for NetworkManager
    sudo mkdir -p /etc/NetworkManager/conf.d/
    local file="/etc/NetworkManager/conf.d/localdiscovery.conf"
    echo "[connection]" | sudo tee "$file" &> /dev/null
    echo "mdns=yes" | sudo tee -a "$file"

    #BUG If you plan to use mDNS and use a firewall, make sure to open UDP port 5353

    sudo systemctl restart systemd-resolved.service
    #To see a statistics use [resolvectl statistics]
}

opennic_dnssec(){
    #TEST add anchor key for dnssec of opennic
    local file="/etc/dnssec-trust-anchors.d/opennic.positive"
    local opennicAnchor=$(dig DNSKEY . @195.201.99.61 +short)
    sudo mkdir -p "/etc/dnssec-trust-anchors.d/"
    echo ". IN DNSKEY $opennicAnchor" | sudo tee "$file"
}

ipv6_privacy_networkmanager_enable () {
    #https://wiki.archlinux.org/index.php/IPv6#Privacy_extensions
    #Network manager uses stable private ipv6 addresses by default
    local file="/etc/NetworkManager/NetworkManager.conf"
    #file exists by default
    echo "[connection]" | sudo tee -a "$file" &> /dev/null
    echo "ipv6.ip6-privacy=2" | sudo tee -a "$file"
}

firewall_config() {
    #Enable Firewall, firewalld is based on nftables
    printf "\nEnable $(tput setaf 2)firewalld$(tput sgr0) service...\n"
    sudo systemctl enable firewalld.service
    sudo systemctl start firewalld.service
    #Control the firewall with firewall-cmd from the terminal
    #SSH port is open by default

    #Notebook - External
    # - Block everything
    #All - Intranet
    # - SSH(default)
    #Server - Internet Ports
    # - 443(https), 993(imaps), 9090(cockpit), 8080(kodi), all from specific IP address(glusterfs)
    #Server - Intranet Ports
    # - SSH(default)
}

systemd_config() {
    journalctl_config
    systemd_reboot_config
    kernel_disable_coredumps
}

journalctl_config() {
    #Do not set the logs size too big as it might be leak to much user data
    file="/etc/systemd/journald.conf"
    echo "[  Config  ] Set $(tput setaf 2)SystemMaxUse=128M$(tput sgr0) in $file to limit log size..."
    config_uncomment "SystemMaxUse=" "#" "$file"
    config_setvalue "SystemMaxUse=" "128M" "$file"
}

systemd_reboot_config() {
    #Default timout for services is 90 seconds and it is way too long, Fedora defaults now to 15
    #/etc/systemd/user.conf ???
    file="/etc/systemd/system.conf"
    systemd_service_config

    #For user@services ?
    file="/etc/systemd/user.conf"
    systemd_service_config
}

systemd_service_config() {
    echo "Set $(tput setaf 2)DefaultTimeoutStopSec=15s$(tput sgr0) in $file to limit reboot time..."
    config_uncomment "DefaultTimeoutStartSec=" "#" "$file"
    config_setvalue "DefaultTimeoutStartSec=" "60s" "$file"
    config_uncomment "DefaultTimeoutStopSec=" "#" "$file"
    config_setvalue "DefaultTimeoutStopSec=" "15s" "$file"
}

kernel_disable_coredumps() {
    #https://wiki.archlinux.org/title/Core_dump#Disabling_automatic_core_dumps
    file="/etc/systemd/coredump.conf.d/custom.conf"
    echo "[  Config  ] Disable $(tput setaf 2)Core Dumps$(tput sgr0) in $file to limit disk usage and increase performance..."
    echo "[Coredump]
Storage=none
ProcessSizeMax=0" | sudo tee "$file" &> /dev/null
}


firmware_tools_arm() {
    if [[ "$machineArch" == "aarch64" ]]; then
        #https://gist.github.com/XSystem252/d274cd0af836a72ff42d590d59647928
        yay -Sy --noconfirm rpi-eeprom
        #Change from critical to stable in FIRMWARE_RELEASE_STATUS="stable"
        #sudo nano /etc/default/rpi-eeprom-update
        config_setvalue "FIRMWARE_RELEASE_STATUS=" "\"stable\"" "/etc/default/rpi-eeprom-update"
    fi
}

####################################
############# OBSOLETE #############
####################################

logrotate_install() {
    #Required to rotate legacy log files that are not send to systemd journalctl
    pacmanSyNC logrotate
    sudo systemctl enable logrotate.timer
    sudo systemctl start logrotate.timer
    # Default config should be ok /etc/logrotate.conf
}

xorg_locale() {
    #Workaround for SDDM is required as xorg doesn't use the locale.conf!!!
    local file="/etc/X11/xorg.conf.d/"
    sudo mkdir -p $file
    file="${file}00-keyboard.conf"
    echo "Section \"InputClass\"
        Identifier \"system-keyboard\"
        MatchIsKeyboard \"on\"
        Option \"XkbLayout\" \"de\"
EndSection" | sudo tee "$file" &> /dev/null
}

firefox_install() {
    #Enable Firefox instead of Firefox-Developer as it shows some broken functionalities on Wayland (no icon, addon popups are just white, ...)
    #BUG Font rendering of gtk apps is blurry/unsharp on Wayland without xdg-desktop-portal-gtk (requires gnome-desktop as a dependecy!)
    pacmanSyNC "firefox firefox-i18n-${languageCode} firefox-ublock-origin firefox-decentraleyes"

    # Secure Firefox by reducing usability, firefox-extension-privacybadger could be potentially installed again on Firefox addon website, so do not install it via pacman
    #pacmanSyNC "firefox-umatrix firefox-noscript"
}

mail_install() {
    #kmail is working badly on Wayland, akonadi will crash on logout and consume 100% CPU after next login
    #pacmanSyNC "kmail bogofilter kaddressbook korganizer"
    pacmanSyNC "thunderbird thunderbird-i18n-${languageCode}"
}

firmware_security() {
    if [[ "$machineArch" == "x86_64" ]]; then
        if ( $cpuIntel ); then
            yay -Sy --noconfirm me_cleaner-git
        fi
    fi
}

kde_systemd_startup() {
    #http://blog.davidedmundson.co.uk/blog/plasma-and-the-systemd-startup/
    #Works only in Plasma >= 5.21 and systemd >= 246
    kwriteconfig5 --file startkderc --group General --key systemdBoot true
    #Check if it worked
    systemctl --user status plasma-plasmashell.service
}

install_pikaur() {
    #Bug: yay doesn't use edited dependencies of PKGBUILD's, so another helper is required
    #On a production system one AUR helper is more than enough
    local yayPackage=$(echo "$aurPackage" | cut -c -3)
    if [[ "$yayPackage" == "yay" ]]; then
        if ( ! $prodServer ); then
            if ! pacman -Q pikaur &> /dev/null; then
                $aurPackage -Sy --noconfirm pikaur
            fi
        fi
    fi
}

debug_symbols_enable() {
    echo "Enabling debug symbols for new compiled packages"
    local file="/etc/makepkg.conf.d/archbot.conf"
    config_setvalue "OPTIONS=" "(strip docs !libtool !staticlibs emptydirs zipman purge debug)" "$file"

    echo "Arch Linux doesn't offer Debug Symbols in standard installations. Debug Symbols are now activated in [/etc/makepkg] for new build packages. If an application crashes, rebuild it manually with the [MAKEPKG] file from Arch Linux (use asp checkout packagename to download the source   ) or if available use the -git package from AUR. If the application will show still missing information during an application crash you can get with [pacman -Qo /path/of/lib] the package that would need to be rebuild."

    echo ""
    echo "If you need to debug kwin, read the documentation: https://community.kde.org/KWin/Debugging"
}

debug_symbols_disable() {
    echo "Disabling debug symbols for new compiled packages"
    local file="/etc/makepkg.conf.d/archbot.conf"
    config_setvalue "OPTIONS=" "(strip docs !libtool !staticlibs emptydirs zipman purge !debug)" "$file"
}

videoencode_install() {
    #TESTING - how to convert to new codes
    pacmanSyNC "handbrake svt-av1 svt-vp9"
}

nbfc_install() {
    #https://github.com/hirschmann/nbfc
    # Noisy laptop fan, then enable notebook fan control, required e.g. on HP Envy x360 convertible
    yay -S --noconfirm nbfc
    sudo systemctl enable nbfc.service --now
    cd /opt/nbfc
    ls -al /opt/nbfc/Configs
    echo "Please run the following 3 commands after selecting an appropriate config"
    echo "mono nbfc.exe config --apply \"Config file name without extension\""
    echo "mono nbfc.exe start"
    echo "mono nbfc.exe status --all"
}

plasma6_fastpath() {
    #Get rid of some Qt5 packages or packages that still depend from qt5
    sudo pacman -D --asexplicit spectacle okular skanlite gwenview kamera kwayland-integration
    sudo pacman -Rsc kde-graphics-meta kcolorchooser kimagemapeditor kolourpaint kquickcharts5 kquickimageeditor5 kruler krunner5 qqc2-desktop-style5 qt5-webview wayland-protocols packagekit-qt5 attica5 knewstuff5 frameworkintegration5 tuxclocker qt5-virtualkeyboard kdelibs4support khotkeys libdbusmenu-qt5 knotifications5 kwallet5 signon-kwallet-extension kaccounts-integration kaccounts-providers kio5 kparts5 kdeclarative5 kio-fuse kcmutils5 kconfig5 kdesu5 kservice5 kemoticons ki18n5 kimageformats5 kitemmodels5 phonon-qt5-gstreamer phonon-qt5 kwayland5 krita marble openrgb kompare rapid-photo-downloader monero-gui jellyfin-media-player retroarch maliit-keyboard krdc skanlite pacmanlogviewer guiscrcpy corectrl kdevelop
    #Important missing packages???
    sudo pacman -Rsc clipgrab
    #Qt6 OK: ark jamesdsp kget ktorrent kup btrfs-assistant goverlay arianna kgpg
    #Plasma 6 Deleted: ksysguard systemd-kcm
    pacmanSyNC libdbusmenu-qt6 koko
    #To get rid of VLC dependencies from Qt5
    yay phonon-qt6-mpv
}

matrixsh_script_install() {
    # Preconditions
    pacmanSyNC "coreutils file jq"
    git clone https://github.com/fabianonline/matrix.sh
    echo "text" | sh /home/rainer/matrix.sh/matrix.sh --homeserver=https://matrix.org --token=syt_cmY_XXXXXX --room=\!YYYYYYY:matrix.org -
}

greetd_install() {
    #https://wiki.archlinux.org/index.php/Greetd
    local session="$1"
    if [[ "$1" == "" ]]; then
        session="startplasma-wayland"
    fi
    local user="$2"
    if [[ "$2" == "" ]]; then
        user="$userName"
    fi
    pacmanSyNC "greetd"
    local file="/etc/greetd/config.toml"
    config_setvalue "command = " "\"$session\"" "$file"
    config_setvalue "user = " "\"$user\"" "$file"
    sudo systemctl disable sddm.service
    sudo systemctl enable greetd.service
}

kwinft_install() {
    #yay -Sy kwayland-git
    #yay -Sy kscreenlocker-git
    #Wrapland and libkscreen-kwinft-git is installed as a dependency
    yay -Sy kwinft
}

kwinft_disman_install() {
    chaotic_aur_repository
    #This will replace libkscreen -> disman
    #Wrapland can be installed side by side, required for disman
    #yay -Sy --noconfirm wrapland-git
    yay -Sy --noconfirm disman-git
    #Replacing kscreen -> kdisplay, so no --noconfirm allowed here
    yay -Sy kdisplay-git
}

mesavulkangit_install() {
    mesagit_repository
    #enable_testing
    if pacman -Q vulkan-mesa-layers &> /dev/null; then
        sudo pacman -R --noconfirm vulkan-mesa-layers &> /dev/null
    fi
    #Do not add --noconfirm as it will skip mesa-git to mesa downgrade
    #Use stable OpenGL Mesa combined with Vulkan-git
    #BUG Doesn't work if chaotic is active
    sudo pacman -Sy mesa lib32-mesa libdrm-git lib32-libdrm-git mesa-demos lib32-mesa-demos vulkan-icd-loader lib32-vulkan-icd-loader vulkan-radeon-git lib32-vulkan-radeon-git vulkan-mesa-layer-git lib32-vulkan-mesa-layer-git llvm-libs-git lib32-llvm-libs-git
    #--overwrite=/usr/lib/libLLVM-10.0.0.so --overwrite=/usr/lib/libLLVM-10.so --overwrite=/usr/lib/libLTO.so.10 --overwrite=/usr/lib32/libLLVM-10.0.0.so --overwrite=/usr/lib32/libLLVM-10.so --overwrite=/usr/lib32/libLTO.so.10
}

mesagit_install() {
    mesagit_repository
    #enable_testing
    if pacman -Q vulkan-mesa-layers &> /dev/null; then
        sudo pacman -R --noconfirm lib32-vulkan-mesa-layers &> /dev/null
        sudo pacman -R --noconfirm vulkan-mesa-layers &> /dev/null
    fi
    if pacman -Q lib32-libva-mesa-driver &> /dev/null; then
        sudo pacman -R --noconfirm lib32-libva-mesa-driver &> /dev/null
    fi
    #Do not add --noconfirm as it will skip mesa-git to mesa downgrade
    # use *vulkan-mesa-layers-* instead of *layer*
    sudo pacman -Sy mesa-git lib32-mesa-git libdrm-git lib32-libdrm-git mesa-demos lib32-mesa-demos vulkan-icd-loader lib32-vulkan-icd-loader vulkan-radeon-git lib32-vulkan-radeon-git vulkan-mesa-layers-git lib32-vulkan-mesa-layers-git llvm-libs-git lib32-llvm-libs-git
}

mesagitchaotic_install() {
    # Mesa-git is required for Chaotic-Aur to install libdrm-git lib32-libdrm-git
    mesagit_repository
    #enable_testing
    if pacman -Q vulkan-mesa-layers &> /dev/null; then
        sudo pacman -R --noconfirm lib32-vulkan-mesa-layers &> /dev/null
        sudo pacman -R --noconfirm vulkan-mesa-layers &> /dev/null
    fi
    # Doesn't work anymore
    #if pacman -Q lib32-llvm-libs &> /dev/null; then
    #    sudo pacman -R --noconfirm lib32-llvm-libs
    #fi
    #Not conflicting
    #if pacman -Q llvm-libs &> /dev/null; then
    #    sudo pacman -R --noconfirm llvm-libs
    #fi
    #Do not add --noconfirm as it will skip mesa-git to mesa downgrade
    sudo pacman -Sy mesa-git lib32-mesa-git libdrm-git lib32-libdrm-git mesa-demos lib32-mesa-demos vulkan-icd-loader lib32-vulkan-icd-loader vulkan-radeon-git lib32-vulkan-radeon-git vulkan-mesa-layers-git lib32-vulkan-mesa-layers-git llvm-libs-git lib32-llvm-libs-git
    #Is llvm-libs-git lib32-llvm-libs-git not supported anymore by mesa-git? It enforces stable llvm-libs now.
}

####################################
########### Experimental ###########
####################################

check_ioscheduler() {
    printf "\nCheck IO Scheduler\n"
	printf "\nExpected Schedulers: SSD=mq-deadline HDD=bfq HDD+SMR=mq-deadline USB=bfq \n"
	disk_scheduler_show
	#Multiqueue is the fefault now, scsi_mod.use_blk_mq=1 is not required anymore
    #read -r -p "Please add [scsi_mod.use_blk_mq=1] as a kernel parameter in [/boot/loader/entries/archlinux.conf] Continue?  [y/N]"
    #read -r -p "Do you want to edit the kernel parameters in [/boot/loader/entries/archlinux.conf]. Continue?  [y/N]"
	#if [[ "$REPLY" =~ [yY] ]]; then
	#    sudo nano /boot/loader/entries/archlinux.conf
	#fi
}

dracut_install() {
    if ( $efi ); then
        #https://wiki.archlinux.org/title/Dracut
        local kerneltype=$1
        if ! pacman -Q dracut &> /dev/null; then
            pacmanSyNC "dracut"
        fi
        dracut_config
        #if ( $gpuAMD ); then
            #BUG How can dracut include amdgpu and i915? Plymouth is added automatically, see lsinitrd -m /boot/initramfs-linux-dracut.img
        #    echo "add_drivers+=\" amdgpu \"" | sudo tee -a "$file"
        #    sudo dracut --hostonly --add-drivers amdgpu --force /boot/initramfs-"${kerneltype}"-dracut.img
        #elif ( $gpuIntel ); then
        #    echo "add_drivers+=\" i915 \"" | sudo tee -a "$file"
        #    sudo dracut --hostonly --add-drivers i915 --force /boot/initramfs-"${kerneltype}"-dracut.img
        #else
            sudo dracut --hostonly --force /boot/initramfs-"${kerneltype}"-dracut.img
        #fi
        sudo dracut --zstd --force /boot/initramfs-"${kerneltype}"-fallback-dracut.img

        sudo cp "/boot/loader/entries/arch${kerneltype}.conf" "/boot/loader/entries/arch${kerneltype}-dracut.conf"
        sudo cp "/boot/loader/entries/arch${kerneltype}-fallback.conf" "/boot/loader/entries/arch${kerneltype}-fallback-dracut.conf"
        sudo cp "/boot/loader/entries/arch${kerneltype}-fallback-rescue.conf" "/boot/loader/entries/arch${kerneltype}-fallback-rescue-dracut.conf"
        config_replacevalue "initramfs-${kerneltype}.img" "initramfs-${kerneltype}-dracut.img" "/boot/loader/entries/arch${kerneltype}-dracut.conf"
        config_replacevalue "initramfs-${kerneltype}-fallback.img" "initramfs-${kerneltype}-fallback-dracut.img" "/boot/loader/entries/arch${kerneltype}-fallback-dracut.conf"
        config_replacevalue "initramfs-${kerneltype}-fallback.img" "initramfs-${kerneltype}-fallback-dracut.img" "/boot/loader/entries/arch${kerneltype}-fallback-rescue-dracut.conf"
        #Restrict firmware bload plymouth.use-simpledrm
        if pacman -Q plymouth &> /dev/null; then
            dracut_plymouth_simpledrm
        fi

        #dracut_default
    else
        echo "Not supported in legacy bios grub mode"
    fi
}

dracut_plymouth_simpledrm() {
    if [[ "$kerneltype" == "" ]]; then
        local kerneltype="linux"
    fi
    local file="/boot/loader/entries/arch${kerneltype}-dracut.conf"
    echo "Boot Parameter for Plymouth plymouth.use-simpledrm"
    kernel_boot_parameter_config "plymouth.use-simpledrm"
}

dracut_default() {
    #Use dracut by default
    config_replacevalue "archlinux.conf" "archlinux-dracut.conf" "/boot/loader/loader.conf"
    sudo ln -sf /dev/null /etc/pacman.d/hooks/90-mkinitcpio-install.hook

    #dracut_replace_mkinitcpio
}

dracut_replace_mkinitcpio() {
    local kerneltype=linux
    if pacman -Q $kerneltype &> /dev/null; then
        dracut_delete_mkinitcpio-systemdboot
    fi
    local kerneltype=linux-lts
    if pacman -Q $kerneltype &> /dev/null; then
        dracut_delete_mkinitcpio-systemdboot
    fi
    local kerneltype=linux-hardened
    if pacman -Q $kerneltype &> /dev/null; then
        dracut_delete_mkinitcpio-systemdboot
    fi

    sudo pacman -Rsc mkinitcpio
    sudo rm "/etc/mkinitcpio.conf"
}

dracut_delete_mkinitcpio-systemdboot() {
    if pacman -Q $kerneltype &> /dev/null; then
        #NOT NECESSARY IF DRACUT IS INSTALLED BY ARCHINSTALLBOT
        sudo rm /boot/loader/entries/arch${kerneltype}.conf
        sudo rm /boot/loader/entries/arch${kerneltype}-fallback.conf
        sudo rm /boot/loader/entries/arch${kerneltype}-fallback-rescue.conf

        sudo rm /boot/initramfs-${kerneltype}.img
        sudo rm /boot/initramfs-${kerneltype}-fallback.img
    fi
}

clamav_install() {
    # Mainly for file/mail servers
    pacmanSyNC "clamav"
    sudo freshclam
    sudo systemctl enable clamav-daemon.service
}

ipfs_install_setup() {
    #https://wiki.archlinux.org/index.php/IPFS
    printf "\nThis will install and setup IPFS\n"
    printf "\nYou can use it to store all public data all around the world\n"
    printf "\nIf the data is important to you, make sure to always host one copy on your own hardware\n"
    pacmanSyNC "go-ipfs"
    ipfs init

    echo "[Unit]
Description=IPFS daemon
After=network.target

[Service]
ExecStart=/usr/bin/ipfs daemon --migrate --mount
Restart=on-failure

[Install]
WantedBy=default.target" | tee "${HOME}/.config/systemd/user/ipfs.service"

    ipfs_fuse

    #ipfs_enable
    loginctl enable-linger "$userName"

    #start
    ipfs_start

    printf "\nYou can access the content from the internet from you local host with http://localhost:5001/webui or install in Firefox the addon IPFS Companion\n"
    printf "\nAll files that you store on your local node are copied to ${HOME}/.ipfs/\n"
}

ipfs_enable() {
    systemctl --user enable --now ipfs
}

ipfs_start() {
    if pacman -Q "go-ipfs" &> /dev/null; then
        systemctl --user start --now ipfs
    else
        echo "IPFS is not installed"
    fi
}

ipfs_seed_files() {
    ipfs add -r /var/cache/pacman/pkg/
}

ipfs_fuse() {
    #https://github.com/ipfs/go-ipfs/blob/master/docs/fuse.md
    #These directories are fix!
    sudo mkdir -p /ipfs
    sudo mkdir -p /ipns
    sudo chown $userName /ipfs
    sudo chown $userName /ipns
    #sudo usermod -G fuse -a $userName
    #Allow all non root users to use this mount
    config_uncomment "user_allow_other" "#" "/etc/fuse.conf"
    ipfs config --json Mounts.FuseAllowOther true
}

#encrypt_disk_random() {
    ##### BUG #####
    #read -r -p "Overwrite disk securly with random data. Please use a random password. Continue? [y/N]"
    #if [[ "$REPLY" =~ [yY] ]]; then
        #check_target_partition
        #sudo cryptsetup open --type plain /dev/sdXY container
        #sudo dd if=/dev/zero of=/dev/mapper/container iflag=nocache oflag=direct bs=4096
    #fi
#}

pipewire_enable() {
    #https://wiki.archlinux.org/index.php/Pipewire
    # BUG wireplumber instead of pipewire-media-session to use enhanced config, but then no sound ????
    #pacmanSyNC "pipewire pipewire-pulse pipewire-alsa pipewire-jack gst-plugin-pipewire wireplumber"
    # ATTENTION Use pacman -Sy to ask user to replace packages like jack2
    #pacmanSyNC "$pipewirePackages"
    #Install lib32 packages only on demand
    #pacmanSyNC "$pipewirePackagesX86"
    #systemctl --user status pipewire.socket
    #systemctl --user status pipewire.service
    #Screensharing xdg-desktop-portal works only for fullscreen in WebRTC in Firefox, installed together with KDE above
    # Not required anymore as pulse replaced pulseaudio?
    #yay -S --noconfirm lib32-pipewire-pulse-git

    # Dropins are not required anymore as pipewire-pulse is enough?
    #Replace Pulseaudio with Pipewire
    #yay -S --noconfirm pipewire-pulse-dropin
    # ??? yay -S --noconfirm pipewire-alsa-dropin
    #yay -S --noconfirm pipewire-jack-dropin
    #yay -S --noconfirm lib32-pipewire-pulse-dropin
    #yay -S --noconfirm lib32-pipewire-jack-dropin

    # Enabling pulse is enough now?
    systemctl --user enable --now pipewire-pulse.socket

    # Show sound info
    #pactl info
}

#noise_suppression_setup() {
    #https://github.com/werman/noise-suppression-for-voice#pipewire
#}

#pipewire_disable() {
#    systemctl --user disable --now pipewire-pulse.socket
    #sudo pacman -Rsc pipewire-pulse pipewire-alsa pipewire-jack pipewire-pulse-dropin pipewire-jack-dropin lib32-pipewire lib32-pipewire-pulse lib32-pipewire-jack lib32-pipewire-pulse-dropin lib32-pipewire-jack-dropin
#    sudo pacman -Rsc pipewire-pulse pipewire-alsa pipewire-jack lib32-pipewire lib32-pipewire-jack
    #pulseaudio-equalizer pulseaudio-jack pulseaudio-zeroconf
    #pulseaudio-eqaulizer-ladspa can be replaced with easyeffects and lsp-plugins
#    pacmanSyNC "pulseaudio pulseaudio-also pulseaudio-bluetooth pulseaudio-equalizer-ladspa"
#}

bug_workarounds() {
    bug_workaround_pam
    bash_user_logout_timeout
}

bug_workaround_pam() {
    # https://bugs.archlinux.org/task/67644
    local file="/etc/security/faillock.conf"
    echo "Enable workaround in $file to not block user accounts without user interaction in cockpit"
    config_uncomment "deny =" "#" "$file"
    config_setvalue "deny =" " 10" "$file"
    # Doesn't work
    #config_uncomment "local_users_only" " #" "$file"
}

bug_workaroud_cockpit_hosts() {
    #Cockpit broke without a warning and without informing any user the functionality to connect to your configured remote bug_workaroud_cockpit_hosts and it broke the ssh-login to remote hosts!!!
    #https://cockpit-project.org/blog/cockpit-322.html
    #https://github.com/cockpit-project/cockpit/pull/20749
    echo "Workaround: Enable multi hosts & ssh-login to remote hosts/machiness"
    echo "#Archbot
[WebService]
AllowMultiHost=yes" | tee "/etc/cockpit/cockpit.conf"
}

jellyfin_install() {
    #https://wiki.archlinux.org/title/Jellyfin
    #Install Jellyfin Media center
    #nfs-utils is required to mount nfs share
    pacmanSyNC "jellyfin-server jellyfin-web nfs-utils"
    sudo systemctl enable jellyfin.service
    sudo systemctl start jellyfin.service

    echo "You can now open Jellyfin at http://IPADDRESS:8096 and do the initial setup."

    local DOMAIN_NAME="????????"
    jellyfin_reverse_proxy "$1"
    nginx_install

    jellyfin_nfs_mount "$1"

    echo "If you want to access the server via a domain name, then enable port fowarding (NAT) for IPv4 (8920)"
    echo "If you wand to access the servic via a domain name, then open port in filrewall (8920)"
    echo ""
    echo "Please make sure to move the default folder location /var/lib/jellyfin/metadata to a drive with a lot of free space. Probably / is not a good location, use e.g. /home"
}

jellyfin_reverse_proxy() {
    local DOMAIN_NAME="$1"
    local pathNginx="/etc/nginx/conf.d"
    sudo mkdir -p "$pathNginx"
    local file="$pathNginx/$DOMAIN_NAME.conf"
    echo "server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name $DOMAIN_NAME;

    # use a variable to store the upstream proxy
    # in this example we are using a hostname which is resolved via DNS
    # (if you are not using DNS remove the resolver line and change the variable to point to an IP address e.g `set $jellyfin 127.0.0.1`)
    #set $jellyfin jellyfin;
    set $jellyfin 127.0.0.1;
    #resolver 127.0.0.1 valid=30;

    ssl_certificate /etc/letsencrypt/live/$DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    #ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    #add_header Strict-Transport-Security "max-age=31536000" always;
    ssl_trusted_certificate /etc/letsencrypt/live/$DOMAIN_NAME/chain.pem;
    ssl_stapling on;
    ssl_stapling_verify on;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    # Content Security Policy
    # See: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
    # Enforces https content and restricts JS/CSS to origin
    # External Javascript (such as cast_sender.js for Chromecast) must be whitelisted.
    #add_header Content-Security-Policy "default-src https: data: blob: http://image.tmdb.org; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' https://www.gstatic.com/cv/js/sender/v1/cast_sender.js https://www.youtube.com blob:; worker-src 'self' blob:; connect-src 'self'; object-src 'none'; frame-ancestors 'self'";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        # Disable buffering when the nginx proxy gets very resource heavy upon streaming
        proxy_buffering off;
    }

    # location block for /web - This is purely for aesthetics so /web/#!/ works instead of having to go to /web/index.html/#!/
    location = /web/ {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096/web/index.html;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    location /socket {
        # Proxy Jellyfin Websockets traffic
        proxy_pass http://$jellyfin:8096;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }
}
    " | sudo tee "$file" &> /dev/null
}

jellyfin_nfs_mount() {
    local DOMAIN_NAME="$1"
    local file="/etc/fstab"
    echo "
192.168.1.21:/  /mnt/nfs/$DOMAIN_NAME/  nfs  defaults,noatime,noauto,x-systemd.automount,x-systemd.device-timeout=100ms,x-systemd.mount-timeout=60,x-systemd.idle-timeout=5min,nofail,_netdev,vers=4 0 0" | sudo tee -a $file

    sudo systemctl daemon-reload
    #MOUNT???
}

nginx_install() {
    pacmanSyNC "nginx-mainline"

    #INFO Without any configuration, nginx works in archlinux and you can visit already the https site http://127.0.0.1

    local file="/etc/nginx/nginx.conf"
    echo "user http;
worker_processes auto;
worker_cpu_affinity auto;

events {
    multi_accept on;
    worker_connections 1024;
}

http {
    charset utf-8;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    server_tokens off;
    log_not_found off;
    types_hash_max_size 4096;
    client_max_body_size 16M;

    # MIME
    include mime.types;
    default_type application/octet-stream;

    # logging
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log warn;

    # load configs
    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
    " | sudo tee "$file" &> /dev/null


    #local file="/etc/letsencrypt/options-ssl-nginx.conf"
    #echo "
    #" | tee "$file" &> /dev/null

    sudo systemctl enable nginx.service
    sudo systemctl start nginx.service
}

frp_server_install() {
    yay -Sy --noconfirm frps-bin
    local file="/etc/frp/frps.toml"
    #bindPort = 7000
    echo "
webServer.addr = \"0.0.0.0\"
webServer.port = 7500
webServer.user = \"admin\"
webServer.password = \"admin\"" | sudo tee -a "$file"
    sudo systemctl enable frps.service
    sudo rm "/etc/frp/frps.ini"
    sudo ln -s "$file" "/etc/frp/frps.ini"
    sudo systemctl start frps.service
    echo "[  firewall  ] Please open port 7000 tcp in firewall!"
}


frp_client_install_user() {
    read -r -p "$(tput setaf $color)Please type the name of the $(tput setaf 2)FRP server$(tput sgr0) managing all the port fowardings:"
    local serverTarget="$REPLY"
    if [[ "$serverTarget" != "" ]]; then
        echo "[ INFO ] actual hostname $hostName"
        read -r -p "$(tput setaf $color)Please type the name of the $(tput setaf 2)port$(tput sgr0) that should be opened on the FRP server"
        local serverForwardPort="$REPLY"
        if [[ "$serverForwardPort" != "" ]]; then
            frp_client_install "$serverTarget" "$frp_client_install"
        fi
    fi
}

frp_client_install() {
    local serverTarget="$1"
    local serverForwardPort="$2"
    if [[ "$serverForwardPort" == "" ]]; then
        echo "[ ERROR ] Please prode a port that the server can setup to forward the traffic"
        exit
    fi
    yay -Sy --noconfirm frpc-bin
    local file="/etc/frp/frpc.toml"
    config_setvalue "serverAddr = " "\"$serverTarget\"" "$file"
    echo "
[[proxies]]
name = \"${hostName}-ssh\"
type = \"tcp\"
localIP = \"127.0.0.1\"
localPort = 22
remotePort = $serverForwardPort" | sudo tee -a "$file"
    #sudo frpc -c /etc/frp/frpc.toml
    sudo systemctl enable frpc.service
    #BUG How to start server including config file???
    sudo rm "/etc/frp/frpc.ini"
    sudo ln -s "$file" "/etc/frp/frpc.ini"
    sudo systemctl start frpc.service
    echo "Now you can test on your server if you can run connect via a local port to a remote ssh server (ssh -oPort=$serverForwardPort test@x.x.x.x)"
}

sane_install() {
    #TEST this required user intervention
    pacmanSyNC "sane sane-airscan skanpage"
    #Scanner may require additional firmware, example Brother printer
    #https://wiki.archlinux.org/title/SANE/Scanner-specific_problems#Brother
    #yay -Sy --noconfirm brother-dcpj572dw
    #yay -Sy --noconfirm brscan4
    #yay -Sy --noconfirm brscan-skey
    #sudo systemctl stop firewalld.service
    #sudo systemctl disable firewalld.service
    #echo "IPv4 adddress of your machine:"
    #ip a | grep "inet " | grep "/24" | awk '{ print $2 }' | rev | cut -c 4- | rev
    #read -r -p "$(tput setaf $color)Please enter the IPv4 address of $(tput setaf 2) your printer$(tput sgr0):"
    #if [[ "$REPLY" != "" ]] && [[ "$REPLY" != " " ]]; then
    #    sudo brsaneconfig4 -a name=SCANNER_DCPJ572DW model=DCP-J52DW ip="$REPLY"
    #else
    #    echo "Error: Please enter a IPv4 address. Exit"
    #fi
}


btrfs_desktop_tools_install() {
    #Only for desktop?
    yay -Sy --noconfirm btrfs-desktop-notification-git

    #Gui to setup btrfs advanced functions
    yay -Sy --noconfirm btrfsmaintenance
    yay -Sy --noconfirm btrfs-assistant
}

btrfs_snapshots_install() {
    #snap-pac creates a snapshot before and after installing upgrades
    #snapper and duperemove are installed as default packages
    pacmanSyNC "snap-pac"

    sudo systemctl enable snapper-cleanup.timer

    #https://wiki.archlinux.org/title/Snapper
    # Create manual timeline snapshot
    #snapper -c root create -c number
}

stylus_tools() {
    pacmanSyNC "write_stylus logseq-desktop-bin xournalpp"
}

auto-cpufreq_enable() {
    #https://github.com/AdnanHodzic/auto-cpufreq
    #Power saving
    #BUG This can seriously slow down some notebooks e.g. Intel and AMD
    yay -S --noconfirm auto-cpufreq
    sudo systemctl enable --now auto-cpufreq.service
}

auto-cpufreq_disable() {
    sudo systemctl disable --now auto-cpufreq.service
}

####################################
############ Interface #############
####################################

runParameter=$1
runParameter2=$2
runParameter3=$3
if [[ "$runParameter" == "--local" ]]; then
    export pkgPath="."
    if [[ "$(uname -m)" == "aarch64" ]] && [[ "$runParameter2" == "--firstbootarm" ]]; then
        export pkgPath="/home/alarm/pacbot/src/pacbot"
    fi
else
    export pkgPath="/opt/pacbot"
fi

if [[ "$runParameter" == "--test" ]]; then
    #Execute function directly (just for testing)
    init_archbot
    echo "[ $runParameter2 $runParameter3 ] run function - TEST MODE"
    $runParameter2 $runParameter3
fi

if [[ "$runParameterLoc" != "--source-only" ]]; then
    if [[ "$EUID" -eq 0 ]]; then
        #root
        if [[ "$(uname -m)" == "aarch64" ]]; then
            printf "$(tput setaf 2)[    setup    ]$(tput sgr0) $scriptName - first run Arch Linux ARM setup mode! \n"
            init_archbot
            archlinuxarm_after_install_root
        else
            if ( $enableHomed ); then
                init_archbot
                archlinux_homed_after_install_root
            else
                printf "$scriptName should not run with root privileges, it works only with sudo!\n"
            fi
        fi
    else
        init_archbot
        if ( $createdProfile ); then
            interface_archbot
        else
            user_profile_create
            interface_archbot
        fi
    fi
fi
