#!/bin/bash

####################################
########## pacbot settings #########
####################################

export aurHelper="yay"

# Hostname Scheme, proposal: 1st digit location, 2nd server type, 3rd and .. running number. Example 
export HOSTNAMEID_PROD1="p"
export HOSTNAMEID_PROD2="s"
export HOSTNAMEID_IMAGE="i"
export HOSTNAMEID_USERMANAGED="u"
export HOSTNAMEID_USERMANAGED_UPDATEPACBOT="v"

# Critical Services that require restart after upgrade
export CRITICALSERVICES=("sshd.service"
        "httpd.service"
        "php-fpm.service"
        "mariadb.service"
        "nfs-server.service"
        "glusterd.service"
        "dovecot.service"
        "postfix.service"
        "fail2ban.service"
        "libvirtd.service")

# Critical Services that require restart after upgrade
export CRITICALPACKAGES=("openssh"
        "openssl"
        "apache"
        "php"
        "mariadb"
        "nfs-utils"
        "glusterfs"
        "dovecot"
        "postfix"
        "fail2ban"
        "libvirt"
        "linux"
        "linux-hardened"
        "linux-lts"
        "glibc"
        "systemd"
        "mesa")

if [[ "$(uname -m)" == "aarch64" ]]; then
    #Some packages are not available on aarch64
    # Package Dependencies NOT checked during install via PKGBUILD
    export PACKAGES=(#"gawk"
            #"git"
            #"pacman-contrib"
            #"pacutils"
            #"python"
            #"python-dateutil"
            #"python-xmltodict"
            "smartmontools"
            #"sed"
            #"sudo"
            "fwupd"
            #"nano"
            #"dialog"
            "kexec-tools"
            "tracer"
            #"bc"
            )
            
    # AUR Package Dependencies
    export PACKAGESAUR=("yay-bin"
            "pacman-fix-permissions"
            "downgrade"
            "etc-update"
            "rpi-eeprom")
else
    # Package Dependencies NOT checked during install via PKGBUILD
    export PACKAGES=(#"gawk"
            #"git"
            #"pacman-contrib"
            #"pacutils"
            "reflector"
            #"python"
            #"python-dateutil"
            #"python-xmltodict"
            "smartmontools"
            #"sed"
            #"sudo"
            "fwupd"
            #"nano"
            #"dialog"
            #"kexec-tools"
            "etc-update"
            "tracer"
            #"bc"
            )
            
    # AUR Package Dependencies
    export PACKAGESAUR=("yay"
            "pacman-fix-permissions"
            "downgrade")
fi
        
# Packages Explicitly Installed by Archbot
export PACKAGESARCHBOT=("systemd-boot-pacman-hook")

# System generated files that are not owned by any installed package directly (lost fles)
#QUESTION: Should critical files like /etc/sudoers.d/wheel be excluded as well?
export SYSTEMGENERATEDFILES=("/boot/EFI/"
        #Package installations upgrade hook
        "/boot/initramfs-linux-fallback.img"
        "/boot/initramfs-linux-hardened-fallback.img"
        "/boot/initramfs-linux-lts-fallback.img"
        "/boot/initramfs-linux-hardened.img"
        "/boot/initramfs-linux-lts.img"
        "/boot/initramfs-linux.img"
        "/boot/vmlinuz-linux"
        "/boot/vmlinuz-linux-hardened"
        "/boot/vmlinuz-linux-lts"
        "/boot/vmlinuz-linux-zen"
        # systemd-boot
        "/boot/loader/random-seed"
        # hwclock --systohc during installation
        "/etc/adjtime"        
        # lsb-release installation
        "/etc/os-release"
        # systemd
        "/etc/machine-id"
        "/etc/machine-info"
        "/usr/lib/udev/hwdb.bin"
        "/etc/.updated"
        "/var/.updated"
        "/var/lib/dbus/machine-id"
        # mkinitcpio
        "/etc/mkinitcpio.d/linux.preset"
        "/etc/mkinitcpio.d/linux-lts.preset"
        "/etc/mkinitcpio.d/linux-hardened.preset"
        # ldconfig
        "/etc/ld.so.cache"
        # mysql database directory: /var/lib/mysql/
        "/var/lib/mysql/"
        # installation files/archinstallbot: /etc/vconsole.conf /etc/hostname /etc/locale.conf /boot/loader/loader.conf
        "/etc/hostname"
        "/etc/locale.conf"
        "/etc/localtime"
        "/etc/vconsole.conf"
        "/etc/systemd/network/wired.network"
        "/etc/sysctl.d/40-ipv6.conf"
        "/etc/archinstallbot.conf"
        "/swap/swapfile"
        # archbot creates: /etc/X11/xorg.conf.d/00-keyboard.conf /etc/tlp.d/10-archbot.conf
        "/etc/tlp.d/10-archbot.conf"
        "/etc/X11/xorg.conf.d/00-keyboard.conf"
        "/etc/systemd/network/br1.netdev"
        "/etc/systemd/network/br1.network"
        "/etc/sysctl.d/99-swappiness.conf"
        "/etc/snapper/configs/root"
        # sensors-detect
        "/etc/conf.d/lm_sensors"
        # Automatically generated files: cups
        "/etc/printcap"
        # Automatically generated files: plymouth
        "/var/lib/plymouth/boot-duration"
        # Automatically generated files: fwupd
        "/var/lib/fwupd/gnupg/private-keys-v1.d"
        "/var/lib/fwupd/gnupg/pubring.kbx"
        "/var/lib/fwupd/gnupg/trustdb.gpg"
        "/var/lib/fwupd/metadata/lvfs/metadata.xml.xz.jcat"
        "/var/lib/fwupd/metadata/lvfs/metadata.xml.zst.jcat"
        "/var/lib/fwupd/metadata/lvfs/metadata.xml.zst"
        "/var/lib/fwupd/metadata/lvfs-testing/metadata.xml.xz.jcat"
        "/var/lib/fwupd/metadata/lvfs-testing/metadata.xml.zst.jcat"
        "/var/lib/fwupd/metadata/lvfs-testing/metadata.xml.zst"
        "/var/lib/fwupd/pending.db"
        "/var/lib/fwupd/pki/client.pem"
        "/var/lib/fwupd/pki/secret.key"
        # SYSTEMGENERATEDDIRECTORIES KDE settings / archbot
        "/etc/sddm.conf.d/kde_settings.conf"
        "/etc/sddm.conf.d/virtualkbd.conf"
        # SYSTEMGENERATEDDIRECTORIES not covered by pacreport, config files created by archinstallbot !
        "/boot/loader/entries/archlinux.conf"
        "/boot/loader/entries/archlinux-fallback.conf"
        "/boot/loader/entries/archlinux-fallback-rescue.conf"
        "/boot/loader/entries/archlinux-hardened.conf"
        "/boot/loader/entries/archlinux-hardened-fallback.conf"
        "/boot/loader/entries/archlinux-hardened-fallback-rescue.conf"
        "/boot/loader/entries/archlinux-lts.conf"
        "/boot/loader/entries/archlinux-lts-fallback.conf"
        "/boot/loader/entries/archlinux-lts-fallback-rescue.conf"
        "/boot/loader/entries/archlinux-zen.conf"
        "/boot/loader/entries/archlinux-zen-fallback.conf"
        "/boot/loader/entries/archlinux-zen-fallback-rescue.conf"
        "/boot/loader/loader.conf"
        "/etc/sudoers.d/wheel"
        "/etc/vconsole.conf"
        # SYSTEMGENERATEDDIRECTORIES not covered by pacreport, config files created by archbot !
        "/boot/loader/entries/memtest86.conf"
        "/etc/sysctl.d/80-gamecompatibility.conf"
        "/etc/systemd/resolved.conf.d/dns_over_tls.conf"
        "/etc/tmpfiles.d/consistent-response-time-for-gaming.conf"
        "/etc/tlp.d/10-archbot.conf"
        "/etc/makepkg.conf.d/archbot.conf"
        "/etc/NetworkManager/conf.d/localdiscovery.conf"
        "/etc/NetworkManager/conf.d/wifi_backend.conf"
        "/etc/udisks2/mount_options.conf"
        "/usr/lib/ssh/ssh-askpass"
        )

# System generated directoris that are not owned by any installed package directly (lost files)
# /boot/loader/ is created locally during installation
export SYSTEMGENERATEDDIRECTOY=("/boot/loader/"
        "/boot/loader/entries"
        # Created by archinstallbot
        "/.snapshots/"
        "/swap/"
        # Created by archbot
        "/etc/sddm.conf.d/"
        "/etc/snapper/configs/"
        "/etc/systemd/resolved.conf.d/"
        # Created by installed packages (not tracked by upstream?)
        "/etc/iwd/"
        "/var/lib/btrfs/"
        "/var/lib/colord/"
        "/var/lib/dbus/"
        "/var/lib/dovecot/"
        "/var/lib/fail2ban/"
        "/var/lib/flatpak/"
        "/var/lib/fwupd/"
        "/var/lib/fwupd/gnupg"
        "/var/lib/fwupd/metadata"
        "/var/lib/fwupd/metadata/lvfs"
        "/var/lib/fwupd/metadata/lvfs-testing"
        "/var/lib/fwupd/pki"
        "/var/lib/fwupd/quirks.d"
        "/var/lib/fwupd/remotes.d"
        "/var/lib/geoclue/"
        "/var/lib/iwd/"
        "/var/lib/kodi/"
        "/var/lib/letsencrypt/"
        "/var/lib/machines/"
        "/var/lib/nfs/"
        "/var/lib/redis/"
        "/var/lib/sddm/"
        "/var/lib/systemd/"
        "/var/lib/tpm2-tss/"
        "/var/spool/cups/"
        )
        
# Not required pacnew files that should not be merged
export DELETEPACNEWFILES=(
        #mirrorlist is auto generated by reflector, so it can be deleted safely
        "/etc/pacman.d/mirrorlist.pacnew"
        #shadow contains all user and groups created by Arch Linux pacman package installations and by the user, the shadow.pacnew contains only a root account line and is not required at all
        "/etc/shadow.pacnew"
        #gshadow contains system unique group information and should never be overwritten by a new config, so it can be deleted safely
        "/etc/gshadow.pacnew"
        #group contains system unique group information and should never be overwritten by a new config, so it can be deleted safely
        "/etc/group.pacnew"
        #passwd contains system unique user information and should never be overwritten by a new config, so it can be deleted safely
        "/etc/passwd.pacnew"
        #This is an auto generated file, changes are done only by virsh according to the comments in this file
        "/etc/libvirt/qemu/networks/default.xml.pacnew"
        #fstab contains system unique partition/mount information and should never be overwritten by a new config, so it can be deleted safely
        "local file=""/etc/fstab.pacnew"
        #crypttab contains system unique partition/mount information and should never be overwritten by a new config, so it can be deleted safely
        "/etc/crypttab.pacnew"
        #exports contains system unique NFS mount information and should never be overwritten by a new config, so it can be deleted safely
        "/etc/exports.pacnew"
        #This is an auto generated file, changes are done only by systemd-resolved
        "/etc/resolv.conf.pacnew"
        )
        
# Not required cache files that are stored in the wrong directory
export DELETECACHEFILES=(
        "/usr/lib/gio/modules/giomodule.cache"
        "/usr/share/icons/hicolor/icon-theme.cache"
        # Versioned?
        "/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache"
        "/usr/lib32/gdk-pixbuf-2.0/2.10.0/loaders.cache"
        "/usr/lib/gtk-3.0/3.0.0/immodules.cache"
        "/usr/lib32/gtk-3.0/3.0.0/immodules.cache"
        "/usr/lib/gtk-4.0/4.0.0/media/giomodule.cache"
        "/usr/lib/gtk-4.0/4.0.0/printbackends/giomodule.cache"
        "/usr/lib/gconv/gconv-modules.cache"
        "/usr/lib32/gconv/gconv-modules.cache"
        "/usr/share/icons/Adwaita/icon-theme.cache"
        "/usr/share/icons/AdwaitaLegacy/icon-theme.cache"
        "/usr/share/icons/breeze-dark/icon-theme.cache"
        "/usr/share/icons/breeze/icon-theme.cache"
        )
