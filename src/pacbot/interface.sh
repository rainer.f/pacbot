#!/bin/bash
# pacbot - Automated Pacman and System Maintenance, author Rainer Finke, license GPLv3

####################################
######### Global Variables #########
####################################

#BUG x releases
#scriptVersion=$(pacman -Q pacbot 2> /dev/null | awk '{ print $2 }' | cut -c -1)
#only works for xx releases
scriptVersion=$(pacman -Q pacbot 2> /dev/null | awk '{ print $2 }' | cut -c -2)
#xx.y releases
#scriptVersion=$(pacman -Q pacbot 2> /dev/null | awk '{ print $2 }' | cut -c -4)
#Output s=important for log file filter, i=infor for log file, v=verboose without logfile
export outputType="s"
documentOnceInLogFile="$outputType"
debugName="->"
unameO=$(uname -o)
#Remove last occurence of -hardened as it is twice in the name of linux-hardened
unameR=$(uname -r | sed '$s/-hardened$//')
unameM=$(uname -m)
color=3

####################################
############## General #############
####################################

check_exit() {
    #Notify user, ask for attention
    #notify_user "Done"

    printf "\n"
    printf "$(tput setaf $color)Done - Press enter to continue$(tput sgr0)\n";
    read
    #If dialog is shown, then you cannot review the output on the terminal
    #dialog --title "Done" --msgbox 'Press Enter to continue' 6 20
}

interfaceType() {
    if ! pacman -Q dialog &> /dev/null; then
        echo "Error: Please install [dialog] to use program with an interface!"
        # For testing
        if [[ "$distributionActual" != "postmarketOS" ]] && [[ "$distributionActual" != "PureOS" ]]; then
            exit 1
        fi
    fi
}

set_hello() {
    helloText1="$scriptDescription"
    helloText2="00 $scriptName-$scriptVersion$runParameter $userName@$hostName $unameO $unameR $unameM"
    helloText3="\/ PC=$computerType AC=$powerText VM=$statusVM PROD=$prodServer IMAGE=$imageOnly"
    helloText4="CriticalServices=(${runningCriticalServicesShort[*]}) MissingPkg/AUR=(${missingPackage[*]}/${missingPackageAur[*]})"
    print_hello
}

print_hello() {
    #Only for terminal, not shown in dialog, ONLY write to log file on first run
    printText "$helloText1" 0 1 "v"
    printText "$helloText2" 0 1 "$documentOnceInLogFile"
    printText "$helloText3" 0 1 "$documentOnceInLogFile"
    printText "$helloText4" 0 1 "$documentOnceInLogFile"
    documentOnceInLogFile="v"
    #NOTE Only visible in terminal output, not in dialog for user!
    show_ipaddr
}

show_status() {
    # All information is visible already in pacbot/archbot
    #print_hello
    system_info
    show_ipaddr
    #btrfs_scrub_status_show
    disk_info_list
    show_upgraded_packages
    #upgrade_show true
    #show_flatpak_updates
}

notify_user() {
    local message="$1"
    local urgency="$2"
    # low, normal, critical
    if [[ "$urgency" == "" ]]; then
        urgency="normal"
    fi
    #Check if user started script from desktop terminal
    case "$(tty)" in
    "/dev/pts"*)
        uid=$(loginctl show-session $XDG_SESSION_ID | grep "User" | awk -F "=" 'FNR == 1 {print $2}')
        #user=$(loginctl show-session $XDG_SESSION_ID |  awk -F "=" 'FNR == 3 {print $2}')
        #export DISPLAY=:0.0
        #export $(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep session)/environ)
        #sudo -u $user DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus
        #notify-send "$message" --icon=dialog-information
        sudo -u "$userName" DISPLAY=:0.0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus notify-send -a "$scriptName AAA-sudo-u" 'Maintenance & Upgrade' \'"$message"\' --icon=dialog-information --urgency="$urgency"
        #notify-send -a "$scriptName BBB" 'Maintenance & Upgrade' \'"$message"\' --icon=dialog-information --urgency="$urgency"
        #DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus su -c notify-send -a "$scriptName" 'Maintenance & Upgrade' \'"$message"\' --icon=dialog-information --urgency="$urgency" - "$userName"
        #export DISPLAY=:0.0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bu su - "$userName" -c /usr/bin/notify-send -a "$scriptName" 'Maintenance & Upgrade' \'"$message"\' --icon=dialog-information --urgency="$urgency"
        #notify-send.py
        #notify-send.py "$@" &
        #sudo -u "$userName" DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus notify-send.py -a "$scriptName" "$message" --urgency="$urgency"
        #sudo -u "$userName" DISPLAY=:1 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus /usr/bin/notify-desktop -a "$scriptName AAA-sudo-u" 'Maintenance & Upgrade' \'"$message"\' --icon=dialog-information --urgency="$urgency"
    ;;
    "/dev/tty"*) echo "tty mode" ;;
    *) echo "tty not available" > /dev/stderr ;;
    esac
}

show_ipaddr() {
    #BUG Test multiple IP addresses
    local ipv4=$(ip a | grep dynamic | grep "inet " | grep -Ev secondary | awk '{ print $2 }' | rev | cut -c 4- | rev)
    #Remove line break and print string in one line awk '$1=$1' RS="\|END\|"
    local ipv6=$(ip a | grep dynamic | grep "inet6" | grep -Ev deprecated | grep -Ev temporary | awk '{ print $2 }' | sort -u | awk '$1=$1' RS="|END|")
    printText "ipv4 ${ipv4[*]} ipv6 ${ipv6[*]}" 0 1 "i"
}

####################################
######### archbot interface ########
####################################

interface_archbot() {
    interfaceType
    interface_dialog
}

interface_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\n$helloText1 \n$helloText2 \n$helloText3 \n$helloText4 \n\nTasks:" 23 75 8 \
            1 "Setup Arch Linux" \
            2 "Admin Tasks" \
            3 "System Info" \
            4 "Network Info" \
            5 "Testing & Git Packages" \
            6 "Download & Install Linux" \
            7 "Reboot System" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) archlinux_init_dialog;;
        2) archlinux_regulartasks_dialog;;
        3) systeminfo_dialog;;
        4) network_dialog;;
        5) archlinux_testing_dialog;;
        6) install_dialog;;
        7) reboot_dialog;;
        *) clear; exit;;
    esac
    done
}

install_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nInstaller:" 16 80 8 \
            1 "Download & Create Arch Linux USB-Stick" \
            2 "Install Arch Linux (from USB Stick or existing Arch Linux)" \
            3 "Download & Create Arch Linux ARM SD-Card - Rasperry Pi 4 64bit - V3D GPU" \
            4 "Download & Create Arch Linux ARM SD-Card - Rasperry Pi 3 64bit - VC4 GPU" \
            5 "Download & Create openSUSE minimal network edition USB-Stick" \
            6 "Download & Create openSUSE KDE edition USB-Stick" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) run_usbstickbot; check_exit;;
        2) run_archinstallbot; check_exit;;
        3) run_archinstallbot_arm "rp4"; check_exit;;
        4) run_archinstallbot_arm "rp3"; check_exit;;
        5) run_usbstickbot "opensuse"; check_exit;;
        6) run_usbstickbot "opensuse-kde"; check_exit;;
        *) clear; break;;
    esac
    done
}

archlinux_init_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nSetup Tasks:" 19 90 8 \
            1 "Init: Enable-Default-Services Linux-LTS Filesysten-noatime" \
            2 "Init: BasicPackages Kernel-Compression AUR PowerManagement Pacbot" \
            3 "Option A: KDE-Desktop" \
            4 "Option B: Media Center" \
            5 "Option C: Server - Linux-Hardened Libvirt Apache PHP MariaDB Certbot" \
            6 "Optimize: IO-Scheduler Optimized-Services BootTime" \
            7 "Secure: Firewall, Linux-hardened, limit user pace and AppArmor" \
            8 "Gameing: Install game clients and wine & game emulation" \
            9 "Workarounds" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) archlinux_init_step1; check_exit;;
        2) archlinux_init_step2; check_exit;;
        3) archlinux_init_step3_desktop; check_exit;;
        4) archlinux_init_step3_mediacenter; check_exit;;
        5) archlinux_init_step3_server; check_exit;;
        6) archlinux_optimize; check_exit;;
        7) archlinux_security; check_exit;;
        8) gaming_install; check_exit;;
        9) workaround_dialog;;
        *) clear; break;;
    esac
    done
}

workaround_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nSetup Workarounds:" 11 90 8 \
            1 "KODI: Replace mouse rightlick with back button (required for Orbsmart)" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) kodi_config_defaults; check_exit;;
        *) clear; break;;
    esac
    done
}

archlinux_regulartasks_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nAdmin Tasks:" 18 60 8 \
            1 "pacbot - Automated Pacman & System Maintenance" \
            2 "MariaDB (mysql server)" \
            3 "Add new User" \
            4 "gamebot - Steam helper" \
            5 "IPFS start" \
            6 "FRP Server Install (Reverse Proxy)" \
            7 "FRP Client Install (Open Port Forwarding on Server)" \
            8 "Scan for newly attached disk" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) run_pacbot;;
        2) mariadb_dialog;;
        3) user_add_manual; check_exit;;
        4) run_gamebot;;
        5) ipfs_start; check_exit;;
        6) frp_server_install; check_exit;;
        7) frp_client_install_user; check_exit;;
        8) disk_attached_scan; check_exit;;
        *) clear; break;;
    esac
    done
}

mariadb_dialog() {
    #Select the user or root for database tasks
    if pacman -Q mariadb &> /dev/null; then
        read -r -p "$(tput setaf $color)Do you want to use your own user to login to MariaDB (mysql) database? If not, then root will be used. $(tput sgr0)  [y/N]"
        local mariaUser=""
        if [[ "$REPLY" =~ [yY] ]]; then
            mariaUser="$userName"
        else
            mariaUser="root"
        fi

        while true; do
        REPLY=$(dialog --stdout --title "archbot" --menu "\nMariaDB Database Tasks:" 15 60 8 \
                1 "Upgrade Database" \
                2 "Optimze All Tables" \
                3 "Check Database" \
                4 "Analyze All Tables" \
                5 "Repair All Tables" \
                0 "Back")
        clear;
        case "$REPLY" in
            1) mariadb_upgrade; check_exit;;
            2) mariadb_optimize_tables; check_exit;;
            3) mariadb_chech_database; check_exit;;
            4) mariadb_analyze_tables; check_exit;;
            5) mariadb_repair_tables; check_exit;;
            *) clear; break;;
        esac
        done
    else
        echo "MariaDB is not installed on this system"
        check_exit
    fi
}

systeminfo_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nShow System Info:" 18 60 8 \
            1 "System Info" \
            2 "systemd timers" \
            3 "Devive Tree Info" \
            4 "Sensor and Temperature Data" \
            5 "RAM Info (sudo)" \
            6 "Disk Alignment" \
            7 "Security Audit" \
            8 "Installed Packages Sizes (pacman/flatpak)" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) system_info; check_exit;;
        2) show_timers_manual; check_exit;;
        3) device_info; check_exit;;
        4) sensors_show; check_exit;;
        5) ram_show; check_exit;;
        6) check_disks_alignment; check_exit;;
        7) security_audit; check_exit;;
        8) packages_installed_show_size; check_exit;;
        *) clear; break;;
    esac
    done
}

network_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nInfo:" 13 60 8 \
            1 "Open local Router Webinterface (OpenWrt)" \
            2 "Scan Local Network Devices IPv4 (arp-scan)" \
            3 "Scan local Network (nmap)" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) website_router_local_open; check_exit;;
        2) network_scan; check_exit;;
        3) network_scan_nmap; check_exit;;
        *) clear; break;;
    esac
    done
}

reboot_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nReboot:" 13 65 8 \
            1 "Reboot System Secure (check boot files)" \
            2 "Reboot System without UEFI Checks (kexec)" \
            3 "Reboot to UEFI BIOS" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) systemctl_reboot; check_exit;;
        2) kexec_reboot; check_exit;;
        3) systemctl_reboot_firmware; check_exit;;
        *) clear; break;;
    esac
    done
}

archlinux_testing_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "archbot" --menu "\nTesting Tasks:" 27 115 8 \
            1 "Pacman: Enable Testing Repositories" \
            2 "Pacman: Disable Testing Repositories" \
            3 "Pacman: Enable KDE & Qt Testing Repositories" \
            4 "Pacman: Disable KDE & Qt Testing Repositories" \
            5 "Pacman: Add & Enable Chaotic-Aur Repositories" \
            6 "Pacman: Disable Chaotic-Aur Repositories" \
            7 "Pacman: Enable Qt5Debug Repository & Debug Symbols for New Compiled Packages" \
            8 "Pacman: Disable Qt5Debug Repository & Debug Symbols" \
            9 "Pacman: Add Parabola Libre Repository" \
            10 "Pacman: Disable Parabola Libre Repository & Remove Parabola Keyring" \
            11 "Pacman: Check and install Downgraded Packages (after disabling testing)" \
            12 "Dracut" \
            13 "Plymouth: Enable Graphical Boot Screen" \
            14 "Battery: Enable Auto-CPUfreq" \
            15 "Battery: Disable Auto-CPUfreq" \
            16 "IPFS" \
            17 "Migrate to Parabola GNU/Linux-libre. WARNING: System or some hardware won't work anymore in Parabola!!!" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) testing_enable; check_exit;;
        2) testing_disable; check_exit;;
        3) testingkde_enable; check_exit;;
        4) testingkde_disable; check_exit;;
        5) chaotic_aur_repository; check_exit;;
        6) chaotic-aur_disable; check_exit;;
        7) debug_enable; check_exit;;
        8) debug_disable; check_exit;;
        9) parabola_enable; check_exit;;
        10) parabola_disable; check_exit;;
        11) install_downgradedpackages; check_exit;;
        12) dracut_install "linux"; check_exit;;
        13) plymouth_enable; check_exit;;
        14) auto-cpufreq_enable; check_exit;;
        15) auto-cpufreq_disable; check_exit;;
        16) ipfs_install_setup; check_exit;;
        17) parabola_migration_from_arch; check_exit;;
        *) clear; break;;
    esac
    done

            #12 "Mesa: (Re-)install Mesa-Stable + Vulkan-Stable" \
            #13 "Mesa: (Re-)install Mesa-Stable + Vulkan-Git" \
            #14 "Mesa: (Re-)install Mesa-Git + Vulkan-Git" \
            #15 "Mesa: (Re-)install Mesa-Git + Vulkan-Git (chaotic-aur)" \

        #12) mesa_install false; check_exit;;
        #13) mesavulkangit_install; check_exit;;
        #14) mesagit_install; check_exit;;
        #15) mesagitchaotic_install; check_exit;;
}

####################################
#### archbot Interface Functions ###
####################################

run_usbstickbot(){
    local target="$1"
    if [[ "$target" == "archlinux" ]]; then
        bash "$pkgPath"/usbstickbot.sh "$target"
    elif [[ "$target" == "opensuse" ]]; then
        bash "$pkgPath"/usbstickbot.sh "$target"
    elif [[ "$target" == "opensuse-kde" ]]; then
        bash "$pkgPath"/usbstickbot.sh "$target"
    else
        bash "$pkgPath"/usbstickbot.sh
    fi
}

run_archinstallbot() {
    bash "$pkgPath"/archinstallbot.sh
}

run_archinstallbot_arm() {
    local target="$1"
    if [[ "$target" == "rp4" ]]; then
        #Raspberry Pi 4 B
        bash "$pkgPath"/archinstallbot.sh --aarch64 --"$target"
    elif [[ "$target" == "rp3" ]]; then
        #Raspberry Pi 3
        bash "$pkgPath"/archinstallbot.sh --aarch64 --"$target"
    elif [[ "$target" == "oc2" ]]; then
        #Odroid-C2
        bash "$pkgPath"/archinstallbot.sh --aarch64 --"$target"
    fi
}

run_pacbot() {
    #sudo is not required anymore, handled by pacbot itself
    if [[ "$runParameter" == "--local" ]]; then
        bash "$pkgPath"/pacbot.sh "$runParameter"
    else
        bash "$pkgPath"/pacbot.sh
    fi
}

run_gamebot() {
    bash "$pkgPath"/gamebot.sh
}

website_router_local_open() {
    echo "Open local Router Webinterface in your Webbrowser (OpenWrt)"
    ipv4_get_network
    ipv4Address="${ipv4Address}.1"
    if [[ "$ipv4Address" != "" ]] && [[ "$ipv4Address" != ".1" ]]; then
        xdg-open "https:$ipv4Address"
    else
        echo "[   Error  ] Please run the above shown apr-scan manually!"
    fi
}

ipv4_get_network() {
    ipv4Address=$(ip a | grep "inet " | grep "/24" | awk '{ print $2 }' | rev | cut -c 4- | rev | cut -c -9 | awk 'NR==1 { print }')
}

network_scan() {
    echo "Running command [ sudo arp-scan X.X.X.0/24 ]"
    ipv4_get_network
    ipv4Address="${ipv4Address}.0"
    echo "Your actual IP Address is:"
    ip a | grep "inet " | grep "/24" | awk '{ print $2 }' | rev | cut -c 4- | rev
    #read -r -p "$(tput setaf $color)Please enter a IPv4 address $(tput setaf 2)but replace the 4th section with 0$(tput sgr0):"
    if [[ "$ipv4Address" != "" ]] && [[ "$ipv4Address" != ".0" ]]; then
        sudo arp-scan "$ipv4Address"/24
    else
        echo "[   Error  ] Please run the above shown apr-scan manually!"
    fi
}

network_scan_nmap() {
    echo "Run command sudo nmap X.X.X.Y. Actual available IPv4 addresses:"
    ip a | grep "inet " | grep "/24" | awk '{ print $2 }' | rev | cut -c 4- | rev
    read -r -p "$(tput setaf $color)Please enter a IPv4 address $(tput setaf 2)but replace the 4th section with the device address$(tput sgr0):"
    if [[ "$REPLY" != "" ]] && [[ "$REPLY" != " " ]]; then
        #sudo only required to show mac address???
        sudo nmap "$REPLY"
    else
        echo "Please enter a IPv4 address"
    fi
}

####################################
#### pacbot Interface Functions ####
####################################

disks_health() {
    smart_health_show
    btrfs_scrub_status_show
}

smart_health_show_manual() {
    printText "$debugName smartctl -H /dev/DISK" 1 0 "v"
    smart_health_show
}

smart_health_show() {
    local deviceCount=0
    smart_health
    for disk in "${smartHealthText[@]}"; do
        printText "${smartHealthText[$deviceCount]}" 0 1 "$outputType"
        ((deviceCount++))
    done
}

btrfs_filesystem_show_manual() {
    local deviceCount=0
    btrfs_filesystem_show
    for mount in "${btrfsFilesystemShowText[@]}"; do
        printText "$debugName btrfs filesystem show ${btrfsMountPoint[$deviceCount]}" 1 0 "v"
        printText "${btrfsFilesystemShowText[$deviceCount]}" 0 1 "$outputType"

        printText "$debugName btrfs filesystem df ${btrfsMountPoint[$deviceCount]}" 1 0 "v"
        printText "${btrfsFilesystemDFText[$deviceCount]}" 0 1 "$outputType"

        printText "$debugName btrfs filesystem usage ${btrfsMountPoint[$deviceCount]}" 1 0 "v"
        printText "${btrfsFilesystemUsageText[$deviceCount]}" 0 1 "$outputType"
        echo "
------------------------------------------------
------------------------------------------------"
        ((deviceCount++))
    done
    printf "\\n"

    btrfs_mount_options_deprecated
}

update_time_manual() {
    printText "$debugName systemctl restart systemd-timesyncd.service" 1 0 "v"
	update_time
	printText "[TimeSync]" 1 2 "$outputType"
}

update_mirrorlist_manual() {
    if [[ "$(uname -m)" == "aarch64" ]]; then
        mirrorListText="[ skipped ] reflector is not available on aarch64"
    else
        printText "$debugName reflector --verbose --latest 10 --number 10 --age 3 --protocol https --sort rate --save /etc/pacman.d/mirrorlist" 1 1 "v"
        update_mirrorlist_status
    fi
}

update_mirrorlist_status() {
    if [[ "$(uname -m)" != "aarch64" ]]; then
        update_mirrorlist
        printText "[$mirrorListText]" 0 2 "$outputType"
    fi
}

remove_pacmanlock_manual() {
    printText "$debugName find /var/lib/pacman/db.lck -exec rm {} \;  -print" 1 1 "v"
    remove_pacmanlock
}

download_updates_manual() {
    local refreshMirrors=$1
    if ( $refreshMirrors ); then
        printText "$debugName pacman -Syyuw" 1 1 "v"
    else
        printText "$debugName pacman -Syuw" 1 1 "v"
    fi
	download_updates "$refreshMirrors"
	printText "[$pacmanDownloadText]" 0 2 "$outputType"
}

diskspace_cleanup_manual() {
    clean_paccache_manual
    clean_btrfs_snapshots_manual
    btrfs_balance_root
}

clean_paccache_manual() {
    printText "$debugName paccache -ruk0 && paccache -rk 2" 1 1 "v"
	clean_paccache
	printText "[$paccacheText]" 0 2 "$outputType"
}

clean_btrfs_snapshots_manual() {
    printText "$debugName systemctl restart snapper-cleanup.service" 1 1 "v"
    btrfs_snapshot_cleanup
}

upgrade_system_manual() {
    local acceptReplacements=$1
    if ( $acceptReplacements ); then
        printText "$debugName yes | pacman -Syuq --noconfirm --needed" 1 1 "v"
    else
        printText "$debugName pacman -Syuq --noconfirm --needed" 1 1 "v"
    fi
    upgrade_system "$acceptReplacements"
    print_status_test3
}

upgrade_aur_manually() {
    printText "$debugName $aurHelper -Syua" 1 1 "v"
    upgrade_aur
    printText "$aurUpgradeText" 0 2 "$outputType"
}

package_requires_restart_manual() {
    #printText "$debugName lsof +c 0 | grep -w DEL | awk '1 { print $1 \": \" $NF }' | sort -u" 1 1 "v"
    #printText "$debugName lsof +c 0 | grep 'DEL.*lib' | awk '1 { print \$1 \": \" \$NF }' | sort -u" 1 1 "v"
    printText "$debugName tracer --services-only" 1 1 "v"
    #printText "Listing packages that may require a restart will run some seconds..." 0 1 "v"
    #package_requires_restart
    restart_upgraded_running_applications
    printText "Review Applications that might require Restart" 1 2 "i"
}

reset_keyring_manual() {
    read -r -p "Do you really want to $(tput setaf $color)reset pacman keyring?$(tput sgr0) THIS SHOULD BE DONE ONLY IF <pacman-key --refresh-keys> SHOWS A LOT OF ERRORS! [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        printText "$debugName rm -R /etc/pacman.d/gnupg && pacman-key --init && pacman-key --populate archlinux" 1 1 "v"
        reset_keyring
    fi
}

update_firmware_manual() {
    printText "$debugName fwupdmgr get-updates && update-smart-drivedb" 1 2 "v"
    update_firmware_status
}

update_firmware_status() {
    update_firmware
    printText "$updateSmartFirmwareText" 0 2 "$outputType"
}

fetch_news_show_uprade() {
	arch_news
	#printText "" 0 1 "v"
	echo ""

    upgrade_show true
    show_flatpak_updates
}

remove_broken_symlinks_manual() {
    printText "Finding broken symlinks takes one minute..." 0 1 "v"
    remove_broken_symlinks_status
}

remove_broken_symlinks_status() {
    remove_broken_symlinks
    #printText "$brokenSymlinksText ${broken_symlinks[*]} " 0 2 "i"
    printText "$brokenSymlinksText ${broken_symlinks[*]}" 1 1 "$outputType"
    #printf '%s\n' "${broken_symlinks[@]}"
    #printText "$brokenSymlinksText" 0 2 "i"
}

fix_permissions_manual() {
    printText "$debugName pacman-fix-permissions" 1 2 "v"
    fix_permissions_status
}

fix_permissions_status() {
    fix_permissions
    printText "[ NEEDED PACKAGE UPGRADES ] $fixFolderPermission" 0 1 "$outputType"
    printText "[$fixFolderPermissionsText]" 1 0 "$outputType"
}

verify_installedpackages_manual() {
    printText "$debugName paccheck --sha256sum --noextract --noupgrade --db-files --files" 1 1 "v"
    verify_installedpackages_status
}

verify_installedpackages_status() {
    verify_installedpackages
}

enforce_package_install_manual() {
    local installPackage=""
    printText "$debugName pacman -Sy --overwrite \"*\" PACKAGENAME" 1 1 "v"
    printText "Note: if you do not provide a packagename, the system upgrade procedure will start to check if there is a package with the issue of overwriting an existing file." 1 1 "v"
    if [[ "$1" == "" ]]; then
        read -r -p "Which package must be installed overwriting all exising files? Package name: "
        installPackage="$REPLY"
    fi
    enforce_package_install "$installPackage"
}

show_lost_files_manual() {
    #printText "$debugName lostfiles" 1 1 "v"
    printText "$debugName pacreport --unowned-files (PLUS added by pacbot unkown files of sytem directories because pacreport only lists the directory itself but not the content)" 1 1 "v"
    show_lost_files_status
}

show_lost_files_status() {
    # Delete not required pacfiles first as otherwise they will be shown as lost files
    delete_notrequired_pacfiles
    delete_notrequired_cachefiles
    cups_delete_cached_files

    show_unowned_files
    #As an array is printed it is not a list anymore, but sort will create a list here now
    IFS=$'\n'
    local sortedOutput=$(for i in ${lostFilesArr[*]}; do echo "$i"; done | sort)
    #list
    printText "[$lostFilesStatusText] $sortedOutput" 0 1 "$outputType"

    printText "$debugName pacreport --missing-files" 1 1 "v"
    show_missing_files
}

btrfs_balance_start_manual() {
    printText "$debugName btrfs balance start -dusage=20 -v MOUNTPOINT" 1 1 "v"
    btrfs_balance_start_status
}

btrfs_balance_start_status() {
    btrfs_balance_start
    printText "[${balanceFinishedText[*]}] ${balanceLogText[*]}" 1 1 "$outputType"
}

refresh_keyring_manual() {
    printText "$debugName pacman-key --refresh-keys" 1 1 "v"
    refresh_keyring_status
}

refresh_keyring_status() {
    refresh_keyring
    printText "[PacmanKeyRefresh]" 0 2 "$outputType"
}

update_flatpak_manual() {
    printText "$debugName flatpak update" 1 2 "v"
    update_flatpak_status
}

update_flatpak_status() {
    update_flatpak
    printText "$updateFlatpakText" 1 1 "$outputType"
}

system_errors_manual() {
    printText "$debugName systemctl --failed && journalctl -p 3 -xb" 1 1 "v"
    system_errors
}

smart_info_xall_manual() {
    printText "$debugName smartctl -x /dev/DISKNAME" 1 1 "v"
    smart_info_xall_status
}

smart_info_xall_status() {
    smart_info_xall
    printText "${smartInfoText2[@]}" 1 1 "outputType"
}

smart_check_manual() {
    local type="$1"
    printText "$debugName smartctl -t $type /dev/DISKNAME" 1 1 "v"
    smart_check_status "$type"
}

smart_check_status() {
    local type="$1"
    smart_check "$type"
    printText "${smartCheckText2[@]}" 1 1 "outputType"
    if [[ "$type" == "long" ]]; then
        printText "This test will run several hours, please check results later with option 1" 1 1 "v"
    else
        printText "This test will run several minutes, please check results later with option 1" 1 1 "v"
    fi
}

disk_attached_scan() {
    echo "Scan for new online attached HDD/SDD disk after boot..."
    for host in /sys/class/scsi_host/*; do echo "- - -" | sudo tee $host/scan; ls /dev/sd* ; done
}

pacbot_systemd_timer_manual() {
    printText "$debugName systemctl enable ***.service" 1 1 "v"
    pacbot_systemd_timer "daily"
    pacbot_systemd_timer "weekly"
    show_timers
    printText "" 0 1 "v"
}

glusterfs_status_manual() {
    printText "$debugName gluster volume info && gluster volume status" 1 1 "v"
    glusterfs_status
}

glusterfs_scrub_manual() {
    printText "$debugName gluster volume bitrot VOLUMENAME scrub ondemand" 1 1 "v"
    glusterfs_scrub
}

show_local_packages_manual() {
    printText "$debugName pacman -Qe" 1 1 "v"
    show_local_packages
}

reinstall_allpackages_manual() {
    printText "$debugName pacman -Qnq | pacman -S -" 1 1 "v"
    reinstall_allpackages
}

pacbot_update_git_manual() {
    printText "$debugName git clone https://gitlab.com/rainer.f/pacbot.git && cd pacbot && makepkg -sirc" 1 1 "v"
    pacbot_update_git_status
}

pacbot_update_git_status() {
    pacbot_update_git_config
    printText "$pacbotUpdateText" 1 2 "v"
}

pacbot_update_git_config() {
    #Init here archbot.sh as otherwise the functions are not available if this function is started by pacbot.sh!!!
    pacbot_update_git
    archbot_config_updater
}

show_timers_manual() {
    printText "$debugName systemctl list-timers" 1 1 "v"
    show_timers
}

btrfs_scrub_start_manual() {
    #printText "$debugName btrfs scrub start MOUNTPOINT" 1 1 "v"
    btrfs_scrub_start
    printText "Btrfs Scrub will run minutes or hours depending from the amount of stored data... Please come back later to check the status!" 1 1 "v"
}

btrfs_scrub_start_status_manual() {
    btrfs_scrub_status_show
    btrfs_scrub_start_manual
}


btrfs_scrub_status_start_manual_again() {
    declare -A btrfsHealthStatusARR
    btrfs_scrub_status_show
    btrfs_scrub_start_manual_again
}

btrfs_scrub_start_manual_again() {
    echo ""
    read -r -p "$(tput setaf $color)Make a complete BTRFS SCRUB run again for all devices?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        btrfs_scrub_start_manual
    fi
}

print_status_test1() {
    printText "[TimeSync] [$mirrorListText]" 1 2 "$outputType"
}

print_status_test2() {
    printText "[$pacmanDownloadText] [$paccacheText]" 1 1 "$outputType"
}

print_status_test3() {
    #BUG updateCount is only calculated during getting update and not later again when installing updates, so it could be possible that updates are installed in the meantime
    #if (( $updateCount > 0 )); then
        printText "$pacmanUpgradeText" 2 2 "$outputType"
    #fi
}

print_status_test4() {
    printText "$updateSmartFirmwareText $updateFlatpakText" 1 1 "$outputType"
}

print_status_image() {
    printText "[Image] - No Maintenance required!" 1 0 "$outputType"
}

user_add_manual() {
    read -r -p "Do you want to add a $(tput setaf $color)new sudo/admin user?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        userNameNew=""
        user_init "wheel"
        user_add_manual_kde
    else
        user_init ""
        user_add_manual_kde
    fi
}

user_add_manual_kde() {
    read -r -p "Do you want setup a $(tput setaf $color)KDE profile?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        kde_init_autostart "$userNameNew"
    fi
}

pacman_clean_cache_manuel() {
    read -r -p "Do you want to clean the $(tput setaf $color)pacman cache?$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        pacman_clean_cache
    fi
}

manual_activities() {
    # Inform about additional manual activities
    # pacman-fix-permissions is automated, no need to look at it
    local manualReviewText="REVIEW from time to time: logs, orphan packages, lostfiles, system-health (scrub+smart)"
    local manualActivitiesText="Upgrade UEFI/Bios and firmware"
    printText "$manualReviewText" 0 2 "i"
    printText "$manualActivitiesText" 1 2 "i"
}

####################################
########## pacbot service ##########
####################################

maintenance_daily_tasks() {
    if ( ! $imageOnly ); then
        update_time
        update_mirrorlist
        print_status_test1
        download_updates false
        diskspace_cleanup
        print_status_test2
        upgrade_system false
        print_status_test3
        update_flatpak_status
        upgrade_aur_automatically
        #orphan_packages
        if ( $updatePacbot ); then
            pacbot_update_git_config
        fi
        restart_upgraded_running_applications
    else
        print_status_image
    fi
}

maintenance_weekly_tasks() {
    if ( ! $imageOnly ); then
        update_time
        update_mirrorlist
        print_status_test1
        download_updates true
        diskspace_cleanup
        refresh_keyring_status
        print_status_test2
        upgrade_system false
        print_status_test3
        update_flatpak_status
        upgrade_aur_automatically
        orphan_packages
        if ( $updatePacbot ); then
            pacbot_update_git_config
        fi
        update_firmware_status
        print_status_test4
        remove_broken_symlinks_status
        fix_permissions_status
        verify_installedpackages_status
        restart_upgraded_running_applications
        show_lost_files_status
        smart_health_show
        manual_activities
    else
        print_status_image
    fi
    btrfs_balance_start_status
    btrfs_scrub_status_show
    btrfs_scrub_start
}

####################################
######### pacbot interface #########
####################################

systemhealth_functions_dialog() {
    while true; do
        REPLY=$(dialog --stdout --title "$scriptName" --menu "\nFilesystem & Disk Health Checks:" 21 70 8 \
            1 "Btrfs Scrub Status, Stats & Check" \
            2 "Btrfs Scrub in Foreground and Log Errors (long run time)" \
            3 "Btrfs Filesystem, Size and RAID Info" \
            4 "Detailed Disk, Filesystem & Mount Info" \
            5 "Smart Status & Health Info" \
            6 "Smart Detailed Health Status" \
            7 "Smart Disk Test Short" \
            8 "Smart Disk Test Conveyance" \
            9 "Smart Disk Test Long" \
            10 "GlusterFS Status" \
            11 "GlusterFS Scrub" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) btrfs_scrub_status_start_manual_again; check_exit;;
        2) btrfs_scrub_start_foreground_reset_statistic; check_exit;;
        3) btrfs_filesystem_show_manual; check_exit;;
        4) disk_info_list; check_exit;;
        5) smart_health_show_manual; check_exit;;
        6) smart_info_xall_manual; check_exit;;
        7) smart_check_manual "short"; check_exit;;
        8) smart_check_manual "conveyance"; check_exit;;
        9) smart_check_manual "long"; check_exit;;
        10) glusterfs_status_manual; check_exit;;
        11) glusterfs_scrub_manual; check_exit;;
        *) clear; break;;
    esac
    done
}

additional_functions_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\nExtra Tasks:" 21 100 8 \
            1 "Show Arch Linux News and System Updates" \
            2 "Install a new Package" \
            3 "Show Packages Installed by User" \
            4 "Reinstall All Packages" \
            5 "Clean Pacman Cache" \
            6 "Enforce Package Installation Overwriting Exisitng FILES (ONLY USE IF YOU KNOW WHAT YOU DO)" \
            7 "Reset Pacman Keyring (ONLY USE IF YOU KNOW WHAT YOU DO)" \
            8 "Downgrade a Package (downgrade)" \
            9 "Install Missing Packages for pacbot" \
            10 "Enable pacbot-daily.service & pacbot-weekly.service" \
            11 "Update pacbot" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) fetch_news_show_uprade; check_exit;;
        2) install_package; check_exit;;
        3) show_local_packages_manual; check_exit;;
        4) reinstall_allpackages_manual; check_exit;;
        5) pacman_clean_cache_manuel; check_exit;;
        6) enforce_package_install_manual; check_exit;;
        7) reset_keyring_manual; check_exit;;
        8) downgrade_package; check_exit;;
        9) install_missingpackages; check_exit;;
        10) pacbot_systemd_timer_manual; check_exit;;
        11) pacbot_update_git_manual; check_exit;;
        *) clear; break;;
    esac
    done
}

maintenance_manual_usercheck_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\nManual Maintenance Tasks:" 16 75 8 \
            1 "Upgrade System accepting Replacements" \
            2 "System Errors (Log)" \
            3 "Pacbot-weekly.service Messages (Log)" \
            4 "Remove Orphan Packages (pacman/flatpak), Optional & AUR Packages" \
            5 "Review & Merge New Pacfiles (etc-update)" \
            6 "Firmware Upgrades" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) upgrade_system_manual true; check_exit;;
        2) system_errors_manual; check_exit;;
        3) review_pacbot_log; check_exit;;
        4) orphan_packages; check_exit;;
        5) review_pacfiles; check_exit;;
        6) firmware_updates_install; check_exit;;
        *) clear; break;;
    esac
    done
}

maintenance_manual_daily_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\nDaily Maintenance Tasks:" 20 82 8 \
            1 "RUN ALL TASKS (non-verboose mode)" \
            2 "Update Time" \
            3 "Update Mirrorlist" \
            4 "Remove Old Pacman Lock" \
            5 "Download Updates" \
            6 "Clean Paccache & old Btrfs Snapshots" \
            7 "Upgrade System (-m) & Restart Critical Services" \
            8 "Upgrade Flatpak Packages" \
            9 "Upgrade AUR Packages (-m)" \
            10 "Restart Upgraded Running Services & Show Applications Requiring A Restart (manual)" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) maintenance_daily_tasks; check_exit;;
        2) update_time_manual; check_exit;;
        3) update_mirrorlist_manual; check_exit;;
        4) remove_pacmanlock_manual; check_exit;;
        5) download_updates_manual false; check_exit;;
        6) diskspace_cleanup_manual; check_exit;;
        7) upgrade_system_manual false; check_exit;;
        8) update_flatpak_manual; check_exit;;
        9) upgrade_aur_manually; check_exit;;
        10) restart_upgraded_running_applications; check_exit;;
        *) clear; break;;
    esac
    done
}

maintenance_manual_weekly_dialog() {
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\nWeekly Maintenance Tasks:" 30 83 8 \
            1 "RUN ALL TASKS (non-verboose mode)" \
            2 "Update Time" \
            3 "Update Mirrorlist" \
            4 "Remove Old Pacman Lock" \
            5 "Refresh Mirrors Download Updates" \
            6 "Refresh Keyring" \
            7 "Clean Paccache & old Btrfs Snapshots" \
            8 "Upgrade System (-m) & Restart Critical Services" \
            9 "Upgrade Flatpak Packages" \
            10 "Upgrade AUR Packages (-m)" \
            11 "Show Firmware Updates & Get Smart HDD Data (manual)" \
            12 "Remove Orphan Packages, Show Optional & AUR Packages (-m)" \
            13 "Remove Broken Symlinks" \
            14 "Fix Permissions (-m)" \
            15 "Verify Installed Packages" \
            16 "Restart Upgraded Running Services & Show Applications Requiring A Restart (manual)" \
            17 "Show Lost/Missing & Unowned Files of Packages" \
            18 "Show Smart Health of Disks" \
            19 "Show Manual Activities" \
            20 "Btrfs Balance Start" \
            21 "Btrfs Scrub Start" \
            0 "Back")
    clear;
    case "$REPLY" in
        1) maintenance_weekly_tasks; check_exit;;
        2) update_time_manual; check_exit;;
        3) update_mirrorlist_manual; check_exit;;
        4) remove_pacmanlock_manual; check_exit;;
        5) download_updates_manual true; check_exit;;
        6) refresh_keyring_manual; check_exit;;
        7) diskspace_cleanup_manual; check_exit;;
        8) upgrade_system_manual false; check_exit;;
        9) update_flatpak_manual; check_exit;;
        10) upgrade_aur_manually; check_exit;;
        11) update_firmware_manual; check_exit;;
        12) orphan_packages; check_exit;;
        13) remove_broken_symlinks_manual; check_exit;;
        14) fix_permissions_manual; check_exit;;
        15) verify_installedpackages_manual; check_exit;;
        16) restart_upgraded_running_applications; check_exit;;
        17) show_lost_files_manual; check_exit;;
        18) smart_health_show_manual; check_exit;;
        19) manual_activities; check_exit;;
        20) btrfs_balance_start_manual; check_exit;;
        21) btrfs_scrub_start_status_manual; check_exit;;
        *) clear; break;;
    esac
    done
}

maintenance_manual() {
    interfaceType
    maintenance_manual_dialog
}

maintenance_manual_dialog() {
    #Print more lines (+2) in dialog in case you have critical services!!!
    while true; do
    REPLY=$(dialog --stdout --title "$scriptName" --menu "\n$helloText1 \n$helloText2 \n$helloText3 \n$helloText4 \n\nMaintenance Tasks:" 23 79 8 \
            1 "System Status" \
            2 "Daily Upgrade Job" \
            3 "Weekly Maintenance Job" \
            4 "Manual Jobs" \
            5 "Extra" \
            6 "Filesystem & Disk Checks" \
            0 "Exit")
    clear;
    case "$REPLY" in
        1) show_status; check_exit;;
        2) maintenance_manual_daily_dialog;;
        3) maintenance_manual_weekly_dialog;;
        4) maintenance_manual_usercheck_dialog;;
        5) additional_functions_dialog;;
        6) systemhealth_functions_dialog;;
        *) clear; break;;
    esac
    done
}
