#!/bin/sh
# Manual Workflow:
# 1. git clone https://gitlab.com/rainer.f/pacbot.git && cd pacbot && sh release.sh   ###or git pull?
# 2. Upload new package to https://gitlab.com/rainer.f/pacbot/tree/master/pkg
# 3. Delete old released package
# 4. If necessary include new files in PKGBUILD now.
# 5. Upload new PKGBUILD file from your desktop to https://gitlab.com/rainer.f/pacbot/blob/master/PKGBUILD (contains increased Version and new sha256 hash)
# 6. Write CHANGELOG and create new gitlab Release.

base="pacbot"
version="$(grep -i '^pkgver=' PKGBUILD | cut -c 8-)"
release="$(grep -i '^pkgrel=' PKGBUILD | cut -c 8-)"
tardirsrc="src/pacbot"
tardirpkg="pkg"

release_snapshot() {
    #tar -cvjf "$tardirpkg/$base-$version".tar.bz2 -C "$tardirsrc/" .
    #sha256sum "$tardirpkg/$base-$version".tar.bz2

    #zstd
    tar -cv -I"zstd -19 -T0" -f "$tardirpkg/$base-$version.$release".tar.zstd -C "$tardirsrc/" .
    package=$(sha256sum "$tardirpkg/$base-$version.$release".tar.zstd)
    echo "$package"
    sha256=$(echo $package | awk '{ print $1 }')
    config_setvalue "sha256sums=" "('$sha256')" "PKGBUILD"
}

config_setvalue() {
    local setting="$1"
    local newValue="$2"
    local filename="$3"
    sed -i "s/\(^${setting}\).*/\1${newValue}/" "$filename"
}

runParameter=$1
if [[ "$runParameter" == "--test-insecure-install" ]]; then
    # test for gitlab-ci.yml
    # Temporary development snapshot
    release="DEV"
    release_snapshot
    # su - nobody makepkg -src => ERROR -> su: failed to execute rc: No such file or directory
    sudo -u nobody makepkg -sfc --noconfirm
    # local packages require no package signing
    #pacman -U --noconfirm "$base-$version.$release-1-any.tar.zstd"
    pacman -U --noconfirm "$(sudo -u $userName makepkg --packagelist)"
else
    versionNew=$(( $version + 1 ))
    read -r -p "$(tput setaf 3)>>>>>>>>>> Do you want to increase the actual released version $version to $versionNew ? <<<<<<<<<$(tput sgr0) [y/N]"
    if [[ "$REPLY" =~ [yY] ]]; then
        config_setvalue "pkgver=" "$versionNew" "PKGBUILD"
        version="$(grep -i '^pkgver=' PKGBUILD | cut -c 8-)"
    fi
    release_snapshot
fi
